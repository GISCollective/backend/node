/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
#!/usr/bin/node
const spawn = require("child_process").spawn;

const dbList = ["ogm", "app", "hbw"];
const fs = require("fs");

async function main() {
  for (db of dbList) {
    await cleanBackups(db);
    await backupDb(db);
  }
}

function cleanBackups(dbName) {
  if (!fs.existsSync(dbName)) {
    fs.mkdirSync(dbName);
  }

  let dates = [];
  let bytes = 0;

  fs.readdirSync(dbName).forEach((file) => {
    dates.push(toDate(file));
    bytes += fs.statSync(`${dbName}/${file}`).size;
  });

  dates = dates.sort((a, b) => (a.getTime() - b.getTime()) * -1);
  console.log(
    `\n\n========= \nThere are ${
      dates.length
    } backup files for ${dbName}. Total size: ${parseInt(
      bytes / 1024 / 1024
    )}MB`
  );

  const mostRecent = dates.slice(0, 3);
  const daily = dates
    .filter((elem, pos) => {
      return indexOfDay(dates, elem) == pos;
    })
    .slice(0, 5);

  console.log(
    `Most recent: `,
    mostRecent.map((a) => a.toISOString()).join(", ")
  );
  console.log(`Daily: `, daily.map((a) => a.toISOString()).join(", "));

  fs.readdirSync(dbName).forEach((file) => {
    const date = toDate(file);
    const isRecent = mostRecent.find((a) => a.getTime() == date.getTime());
    const isDaily = daily.find((a) => a.getTime() == date.getTime());

    if (!isRecent && !isDaily) {
      console.log(`=> DELETE `, date.toISOString());
      fs.unlinkSync(`${dbName}/${file}`);
    }
  });
}

function backupDb(dbName) {
  const now = new Date();
  const timeStamp = now.toISOString();

  console.log(` => CREATING NEW BACKUP FOR ${dbName} at ${timeStamp}`);

  let backupProcess = spawn("mongodump", [
    `--db=${dbName}`,
    `--archive=${dbName}/${dbName}-${timeStamp}.archive`,
    "--gzip",
  ]);

  return new Promise((resolve) => {
    backupProcess.on("exit", (code, signal) => {
      console.log(
        "execution time: ",
        parseInt((new Date().getTime() - now.getTime()) / 1000),
        "seconds"
      );

      if (code) console.log("Backup process exited with code ", code);
      else if (signal)
        console.error("Backup process was killed with singal ", signal);
      else console.log("Successfully backedup the database");

      resolve();
    });
  });
}

function toDate(fileName) {
  const pos = fileName.indexOf("-") + 1;

  return new Date(fileName.substr(pos).replace(".archive", ""));
}

function sameDay(d1, d2) {
  return (
    d1.getFullYear() === d2.getFullYear() &&
    d1.getMonth() === d2.getMonth() &&
    d1.getDate() === d2.getDate()
  );
}

function indexOfDay(list, date) {
  let index = -1;

  list.forEach((item, i) => {
    if (sameDay(item, date)) {
      index = i;
      return false;
    }
  });

  return index;
}

main().catch(console.error);
