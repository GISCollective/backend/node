/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import DiskSizeStats from "../../source/stats/DiskSize.js";
import Db from "models/source/db.js";
import { MongoMemoryServer } from "mongodb-memory-server";
import CalendarService from "tasks/source/lib/CalendarService.js";

import { should } from "chai";
should();

describe("DiskSizeStats", function () {
  let diskSizeStats;
  let db;
  let mongoServer;

  beforeEach(async function () {
    this.timeout(60000);

    mongoServer = await MongoMemoryServer.create();
    await mongoServer.getUri();

    db = new Db({
      url: `mongodb://${mongoServer._instanceInfo.ip}:${mongoServer._instanceInfo.port}/tests`,
      configuration: { collection: "test" },
    });

    await db.connectWithPromise();

    diskSizeStats = new DiskSizeStats();

    await db.addItem("preference", {
      name: "secret.smtp.authType",
      value: "auth type",
    });

    CalendarService.instance = { now: "2021-11-30T13:55:48" };
  });

  this.afterEach(async function () {
    await db.close();
    await mongoServer.stop();
  });

  it("should have the `DiskSizeStats` name", function () {
    diskSizeStats.name.should.equal("DiskSizeStats");
  });

  it("should create a meta with the collections size", async function () {
    await diskSizeStats.start(db);

    const records = await db.query("meta", {});

    records.should.have.length(1);
    records[0].data.preference.diskSize = 74;

    records[0].should.deep.contain({
      changeIndex: 0,
      data: {
        preference: {
          count: 1,
          dataSize: 74,
          diskSize: 74,
        },
      },
      itemId: "2021-11-30T13:55:48",
      model: "",
      type: "stats.size",
    });
  });
});
