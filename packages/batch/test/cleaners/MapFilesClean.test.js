/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import MapFilesClean from "../../source/cleaners/MapFilesClean.js";
import { expect } from "chai";
import Db from "models/source/db.js";
import { MongoMemoryServer } from "mongodb-memory-server";
import { ObjectId } from "mongodb";

import { should } from "chai";
should();

describe("MapFilesClean", function () {
  let mapFilesClean;
  let db;
  let mongoServer;

  beforeEach(async function () {
    this.timeout(60000);

    mongoServer = await MongoMemoryServer.create();
    await mongoServer.getUri();

    db = new Db({
      url: `mongodb://${mongoServer._instanceInfo.ip}:${mongoServer._instanceInfo.port}/tests`,
      configuration: { collection: "test" },
    });

    await db.connectWithPromise();
  });

  afterEach(async function () {
    await db.close();
    await mongoServer.stop();
  });

  beforeEach(function () {
    mapFilesClean = new MapFilesClean();
  });

  it("should have the `MapFilesClean` name", function () {
    mapFilesClean.name.should.equal("MapFilesClean");
  });

  describe("hasDbRecord", async function () {
    it("should false when the db is empty", async function () {
      const result = await MapFilesClean.hasDbRecord(
        db,
        "5ee3b1f7bbd03a0100c14ff5"
      );
      result.should.equal(false);
    });

    describe("when there is a map.file", function () {
      let fileId;

      beforeEach(async function () {
        const result = await db.addItem("map.files", {
          length: 0,
          chunkSize: 0,
          filename: "utf8 Mobile Test Map-2020-02-07T16_16_05.441877.csv",
          metadata: { mime: "text/csv", etag: "5e3d93e46f210d010021a5f0" },
        });

        fileId = result.insertedId;
      });

      it("returns true when there is a MapFile", async function () {
        await db.addItem("mapFile", {
          file: fileId,
        });

        const result = await MapFilesClean.hasDbRecord(db, fileId);
        result.should.equal(true);
      });
    });
  });

  describe("start", async function () {
    it("does not fail when the db is empty", async function () {
      const result = await mapFilesClean.start(db);

      result.should.deep.equal({
        total: 0,
        invalid: 0,
        orphaned: 0,
        deleted: 0,
      });
    });

    describe("when there is a map.file", function () {
      let fileId;

      beforeEach(async function () {
        const result = await db.addItem("map.files", {
          length: 0,
          chunkSize: 0,
          filename: "utf8 Mobile Test Map-2020-02-07T16_16_05.441877.csv",
          metadata: { mime: "text/csv", etag: "5e3d93e46f210d010021a5f0" },
        });
        fileId = result.insertedId.toString();

        await db.addItem("map.chunks", {
          files_id: new ObjectId(fileId),
        });
      });

      it("does not delete the file when is linked to a mapFile record", async function () {
        await db.addItem("mapFile", {
          file: fileId,
        });

        const result = await mapFilesClean.start(db);
        let mapFile = await db.getItemWithQuery("map.files", {
          _id: new ObjectId(fileId),
        });
        let mapChunk = await db.getItemWithQuery("map.chunks", {
          files_id: new ObjectId(fileId),
        });

        result.should.deep.equal({
          total: 1,
          orphaned: 0,
          invalid: 0,
          deleted: 0,
        });
        mapFile.should.exist;
        mapChunk.should.exist;
      });

      it("deletes the file when is not linked to a mapFile record", async function () {
        const result = await mapFilesClean.start(db);
        let mapFile = await db.getItemWithQuery("map.files", {
          _id: new ObjectId(fileId),
        });
        let mapChunk = await db.getItemWithQuery("map.chunks", {
          files_id: new ObjectId(fileId),
        });

        result.should.deep.equal({
          total: 1,
          orphaned: 1,
          invalid: 0,
          deleted: 1,
        });

        expect(mapFile).not.exist;
        expect(mapChunk).not.exist;
      });
    });
  });
});
