/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import PictureFilesClean from "../../source/cleaners/PictureFilesClean.js";
import { expect } from "chai";
import Db from "models/source/db.js";
import { MongoMemoryServer } from "mongodb-memory-server";
import { ObjectId } from "mongodb";

import { should } from "chai";
should();

describe("PictureFilesClean", function () {
  let pictureFilesClean;
  let db;
  let mongoServer;

  beforeEach(async function () {
    this.timeout(60000);

    mongoServer = await MongoMemoryServer.create();
    await mongoServer.getUri();

    db = new Db({
      url: `mongodb://${mongoServer._instanceInfo.ip}:${mongoServer._instanceInfo.port}/tests`,
      configuration: { collection: "test" },
    });

    await db.connectWithPromise();
  });

  afterEach(async function () {
    await db.close();
    await mongoServer.stop();
  });

  beforeEach(function () {
    pictureFilesClean = new PictureFilesClean();
  });

  it("should have the `PictureFilesClean` name", function () {
    pictureFilesClean.name.should.equal("PictureFilesClean");
  });

  describe("hasDbRecord", async function () {
    it("should false when the db is empty", async function () {
      const result = await PictureFilesClean.hasDbRecord(
        db,
        "5ee3b1f7bbd03a0100c14ff5"
      );
      result.should.equal(false);
    });

    describe("when there is a pictures.file", function () {
      let fileId;

      beforeEach(async function () {
        const result = await db.addItem("pictures.files", {
          length: 0,
          chunkSize: 0,
          filename: "",
        });

        fileId = result.insertedId;
      });

      it("returns true when there is a Picture", async function () {
        await db.addItem("pictures", {
          picture: fileId,
        });

        const result = await PictureFilesClean.hasDbRecord(db, fileId);
        result.should.equal(true);
      });
    });
  });

  describe("hasValidFile", async function () {
    it("should false when the db is empty", async function () {
      const result = await PictureFilesClean.hasValidFile(
        db,
        "5ee3b1f7bbd03a0100c14ff5"
      );
      result.should.equal(false);
    });

    describe("when there is a pictures.file", function () {
      let fileId;

      beforeEach(async function () {
        const result = await db.addItem("pictures.files", {
          length: 0,
          chunkSize: 0,
          filename: "",
        });

        fileId = result.insertedId;
      });

      it("returns true when there is a Picture file", async function () {
        const insertResult = await db.addItem("pictures", {
          picture: fileId,
        });

        const result = await PictureFilesClean.hasValidFile(
          db,
          insertResult.insertedId
        );
        result.should.equal(true);
      });
    });

    describe("when there is a missing pictures.file", function () {
      let fileId;

      beforeEach(async function () {
        const result = await db.addItem("pictures.files", {
          length: 0,
          chunkSize: 0,
          filename: "",
        });

        fileId = result.insertedId;
      });

      it("returns false", async function () {
        const insertResult = await db.addItem("pictures", {
          picture: "64b30c4063f3228dc0a2d4aa",
        });

        const result = await PictureFilesClean.hasValidFile(
          db,
          insertResult.insertedId
        );
        result.should.equal(false);
      });
    });
  });

  describe("start", async function () {
    it("does not fail when the db is empty", async function () {
      const result = await pictureFilesClean.start(db);

      result.should.deep.equal({
        total: 0,
        orphaned: 0,
        invalid: 0,
        deleted: 0,
        deletedRecord: 0,
        orphanedRecord: 0,
        totalRecord: 0,
      });
    });

    describe("when there is a file", function () {
      let fileId;

      beforeEach(async function () {
        const result = await db.addItem("pictures.files", {
          length: 0,
          chunkSize: 0,
          filename: "utf8 Mobile Test Map-2020-02-07T16_16_05.441877.csv",
          metadata: { mime: "text/csv", etag: "5e3d93e46f210d010021a5f0" },
        });
        fileId = result.insertedId.toString();

        await db.addItem("pictures.chunks", {
          files_id: new ObjectId(fileId),
        });
      });

      it("does not delete the file when is linked to a record", async function () {
        await db.addItem("pictures", {
          picture: fileId,
        });

        const result = await pictureFilesClean.start(db);
        let files = await db.getItemWithQuery("pictures.files", {
          _id: new ObjectId(fileId),
        });
        let chunks = await db.getItemWithQuery("pictures.chunks", {
          files_id: new ObjectId(fileId),
        });

        result.should.deep.equal({
          total: 1,
          orphaned: 0,
          invalid: 0,
          deleted: 0,
          deletedRecord: 0,
          orphanedRecord: 0,
          totalRecord: 1,
        });
        files.should.exist;
        chunks.should.exist;
      });

      it("deletes the file when is not linked to a record", async function () {
        const result = await pictureFilesClean.start(db);
        let files = await db.getItemWithQuery("pictures.files", {
          _id: new ObjectId(fileId),
        });
        let chunks = await db.getItemWithQuery("pictures.chunks", {
          files_id: new ObjectId(fileId),
        });

        result.should.deep.equal({
          total: 1,
          orphaned: 1,
          invalid: 0,
          deleted: 1,
          deletedRecord: 0,
          orphanedRecord: 0,
          totalRecord: 0,
        });

        expect(files).not.exist;
        expect(chunks).not.exist;
      });
    });

    describe("when there is a file with a parentId meta", function () {
      let fileId;
      const parentId = "605b531077753f01007feaf3";

      beforeEach(async function () {
        const result = await db.addItem("pictures.files", {
          length: 0,
          chunkSize: 0,
          filename: "utf8 Mobile Test Map-2020-02-07T16_16_05.441877.csv",
          metadata: {
            mime: "text/csv",
            etag: "5e3d93e46f210d010021a5f0",
            parentId: parentId,
          },
        });
        fileId = result.insertedId.toString();

        await db.addItem("pictures.chunks", {
          files_id: new ObjectId(fileId),
        });
      });

      it("does not delete the file when the meta parentId is linked to a record", async function () {
        await db.addItem("pictures", {
          picture: parentId,
        });

        const result = await pictureFilesClean.start(db);
        let files = await db.getItemWithQuery("pictures.files", {
          _id: new ObjectId(fileId),
        });
        let chunks = await db.getItemWithQuery("pictures.chunks", {
          files_id: new ObjectId(fileId),
        });

        result.should.deep.equal({
          total: 1,
          orphaned: 0,
          invalid: 0,
          deleted: 0,
          deletedRecord: 1,
          orphanedRecord: 1,
          totalRecord: 1,
        });
        files.should.exist;
        chunks.should.exist;
      });

      it("deletes the file when is not linked to a record", async function () {
        const result = await pictureFilesClean.start(db);
        let files = await db.getItemWithQuery("pictures.files", {
          _id: new ObjectId(fileId),
        });
        let chunks = await db.getItemWithQuery("pictures.chunks", {
          files_id: new ObjectId(fileId),
        });

        result.should.deep.equal({
          total: 1,
          orphaned: 1,
          invalid: 0,
          deleted: 1,
          deletedRecord: 0,
          orphanedRecord: 0,
          totalRecord: 0,
        });

        expect(files).not.exist;
        expect(chunks).not.exist;
      });
    });

    describe("when there is a picture without a file", function () {
      let pictureId;

      beforeEach(async function () {
        const insertResult = await db.addItem("pictures", {
          picture: "64b30c4063f3228dc0a2d4aa",
        });

        pictureId = insertResult.insertedId;
      });

      it("deletes the picture", async function () {
        const result = await pictureFilesClean.start(db);

        const record = await await db.getItem("pictures", pictureId);

        expect(record).not.to.exist;
        expect(result).to.deep.equal({
          deleted: 0,
          deletedRecord: 1,
          invalid: 0,
          orphaned: 0,
          orphanedRecord: 1,
          total: 0,
          totalRecord: 1,
        });
      });
    });

    describe("when there is a picture with a file", function () {
      let pictureId;

      let fileId;
      const parentId = "605b531077753f01007feaf3";

      beforeEach(async function () {
        const result = await db.addItem("pictures.files", {
          length: 0,
          chunkSize: 0,
          filename: "utf8 Mobile Test Map-2020-02-07T16_16_05.441877.csv",
          metadata: {
            mime: "text/csv",
            etag: "5e3d93e46f210d010021a5f0",
            parentId: parentId,
          },
        });
        fileId = result.insertedId.toString();

        await db.addItem("pictures.chunks", {
          files_id: new ObjectId(fileId),
        });

        const insertResult = await db.addItem("pictures", {
          picture: fileId,
        });

        pictureId = insertResult.insertedId;
      });

      it("keeps the picture", async function () {
        const result = await pictureFilesClean.start(db);

        const record = await await db.getItem("pictures", pictureId);

        expect(record).to.exist;
        expect(result).to.deep.equal({
          deleted: 0,
          invalid: 0,
          orphaned: 0,
          total: 1,
          deletedRecord: 0,
          orphanedRecord: 0,
          totalRecord: 1,
        });
      });
    });
  });
});
