/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import IconsClean from "../../source/cleaners/IconsClean.js";
import Db from "models/source/db.js";
import { MongoMemoryServer } from "mongodb-memory-server";
import { ObjectId } from "mongodb";

import { should } from "chai";
should();

describe("iconsClean", function () {
  let iconsClean;
  let broadcast;
  let db;
  let mongoServer;

  beforeEach(async function () {
    this.timeout(60000);

    mongoServer = await MongoMemoryServer.create();
    await mongoServer.getUri();

    db = new Db({
      url: `mongodb://${mongoServer._instanceInfo.ip}:${mongoServer._instanceInfo.port}/tests`,
      configuration: { collection: "test" },
    });

    await db.connectWithPromise();
  });

  afterEach(async function () {
    await db.close();
    await mongoServer.stop();
  });

  beforeEach(function () {
    iconsClean = new IconsClean();
    broadcast = {
      messages: [],
      push(channel, message) {
        this.messages.push({ channel, message });
      },
    };
  });

  it("should have the `IconsClean` name", function () {
    iconsClean.name.should.equal("IconsClean");
  });

  describe("start", async function () {
    it("does not fail when the db is empty", async function () {
      const result = await iconsClean.start(db);

      result.should.deep.equal({
        total: 0,
        deleted: 0,
      });
    });

    describe("when there is an icon without a set", function () {
      let iconId;

      beforeEach(async function () {
        const response = await db.addItem("icons", {
          name: "test",
          iconSet: new ObjectId("5ca7bfb6ecd8490100cab971"),
        });

        iconId = response.insertedId.toString();
      });

      it("deletes the icon", async function () {
        const result = await iconsClean.start(db, broadcast);

        result.should.deep.equal({
          total: 1,
          deleted: 1,
        });

        const iconCount = await db.count("icons", {});
        iconCount.should.equal(0);
      });

      it("broadcasts an icon deletion", async function () {
        await iconsClean.start(db, broadcast);

        broadcast.messages.should.deep.equal([
          {
            channel: "Icon.change",
            message: {
              modelName: "Icon",
              type: "delete",
              before: {
                _id: iconId,
                iconSet: "5ca7bfb6ecd8490100cab971",
                name: "test",
              },
            },
          },
        ]);
      });
    });

    describe("when there is an icon with a set", function () {
      beforeEach(async function () {
        const response = await db.addItem("iconsets", {});

        await db.addItem("icons", {
          iconSet: response.insertedId,
        });
      });

      it("does not delete the icon", async function () {
        const result = await iconsClean.start(db);

        result.should.deep.equal({
          total: 1,
          deleted: 0,
        });

        const iconCount = await db.count("icons", {});
        iconCount.should.equal(1);
      });
    });
  });
});
