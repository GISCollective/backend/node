/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import SoundFilesClean from "../../source/cleaners/SoundFilesClean.js";
import { expect } from "chai";
import Db from "models/source/db.js";
import { MongoMemoryServer } from "mongodb-memory-server";
import { ObjectId } from "mongodb";

import { should } from "chai";
should();

describe("SoundFilesClean", function () {
  let soundFilesClean;
  let db;
  let mongoServer;

  beforeEach(async function () {
    this.timeout(60000);

    mongoServer = await MongoMemoryServer.create();
    await mongoServer.getUri();

    db = new Db({
      url: `mongodb://${mongoServer._instanceInfo.ip}:${mongoServer._instanceInfo.port}/tests`,
      configuration: { collection: "test" },
    });

    await db.connectWithPromise();
  });

  afterEach(async function () {
    await db.close();
    await mongoServer.stop();
  });

  beforeEach(function () {
    soundFilesClean = new SoundFilesClean();
  });

  it("should have the `SoundFilesClean` name", function () {
    soundFilesClean.name.should.equal("SoundFilesClean");
  });

  describe("hasDbRecord", async function () {
    it("should false when the db is empty", async function () {
      const result = await SoundFilesClean.hasDbRecord(
        db,
        "5ee3b1f7bbd03a0100c14ff5"
      );
      result.should.equal(false);
    });

    describe("when there is a sounds.file", function () {
      let fileId;

      beforeEach(async function () {
        const result = await db.addItem("sounds.files", {
          length: 0,
          chunkSize: 0,
          filename: "",
        });

        fileId = result.insertedId;
      });

      it("returns true when there is a Sound", async function () {
        await db.addItem("sounds", {
          sound: fileId,
        });

        const result = await SoundFilesClean.hasDbRecord(db, fileId);
        result.should.equal(true);
      });
    });
  });

  describe("start", async function () {
    it("does not fail when the db is empty", async function () {
      const result = await soundFilesClean.start(db);

      result.should.deep.equal({
        total: 0,
        orphaned: 0,
        invalid: 0,
        deleted: 0,
      });
    });

    describe("when there is a file", function () {
      let fileId;

      beforeEach(async function () {
        const result = await db.addItem("sounds.files", {
          length: 0,
          chunkSize: 0,
          filename: "utf8 Mobile Test Map-2020-02-07T16_16_05.441877.csv",
          metadata: { mime: "text/csv", etag: "5e3d93e46f210d010021a5f0" },
        });
        fileId = result.insertedId.toString();

        await db.addItem("sounds.chunks", {
          files_id: new ObjectId(fileId),
        });
      });

      it("does not delete the file when is linked to a record", async function () {
        await db.addItem("sounds", {
          sound: fileId,
        });

        const result = await soundFilesClean.start(db);
        let files = await db.getItemWithQuery("sounds.files", {
          _id: new ObjectId(fileId),
        });
        let chunks = await db.getItemWithQuery("sounds.chunks", {
          files_id: new ObjectId(fileId),
        });

        result.should.deep.equal({
          total: 1,
          orphaned: 0,
          invalid: 0,
          deleted: 0,
        });
        files.should.exist;
        chunks.should.exist;
      });

      it("deletes the file when is not linked to a record", async function () {
        const result = await soundFilesClean.start(db);
        let files = await db.getItemWithQuery("sounds.files", {
          _id: new ObjectId(fileId),
        });
        let chunks = await db.getItemWithQuery("sounds.chunks", {
          files_id: new ObjectId(fileId),
        });

        result.should.deep.equal({
          total: 1,
          orphaned: 1,
          invalid: 0,
          deleted: 1,
        });

        expect(files).not.exist;
        expect(chunks).not.exist;
      });
    });
  });
});
