/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import ContributorsClean from "../../source/cleaners/ContributorsClean.js";
import Db from "models/source/db.js";
import { MongoMemoryServer } from "mongodb-memory-server";

import { should } from "chai";
should();

describe("ContributorsClean", function () {
  let contributorsClean;
  let mongoServer;
  let db;

  beforeEach(async function () {
    this.timeout(60000);

    mongoServer = await MongoMemoryServer.create();
    await mongoServer.getUri();

    db = new Db({
      url: `mongodb://${mongoServer._instanceInfo.ip}:${mongoServer._instanceInfo.port}/tests`,
      configuration: { collection: "test" },
    });

    await db.connectWithPromise();
  });

  afterEach(async function () {
    await db.close();
    await mongoServer.stop();
  });

  beforeEach(function () {
    contributorsClean = new ContributorsClean();
  });

  it("should have the `ContributorsClean` name", function () {
    contributorsClean.name.should.equal("ContributorsClean");
  });

  describe("fixContributors", function () {
    it("returns an empty string when an empty string is passed", async function () {
      const result = await contributorsClean.fixContributors(db, []);

      result.should.deep.equal([]);
    });

    it("returns the provided list when the id matches an user id with a string prop", async function () {
      const insertResult = await db.addItem("users", {});
      const id = insertResult.insertedId.toString();

      const result = await contributorsClean.fixContributors(db, [id, "other"]);

      result.should.deep.equal([id]);
    });
  });

  describe("start", async function () {
    it("does not fail when the db is empty", async function () {
      const result = await contributorsClean.start(db);

      result.should.deep.equal({
        total: 0,
        updated: 0,
      });
    });

    it("removes the invalid users ids from the contributors list", async function () {
      const insertResult = await db.addItem("sites", {
        contributors: [""],
      });

      const result = await contributorsClean.start(db);
      let site = await db.getItemWithQuery("sites", {
        _id: insertResult.insertedId,
      });

      result.should.deep.equal({
        total: 1,
        updated: 1,
      });
      site.contributors.should.deep.equal([]);
    });
  });
});
