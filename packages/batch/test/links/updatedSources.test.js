/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import UpdatedSources from "../../source/links/updatedSources.js";
import Db from "models/source/db.js";
import { MongoMemoryServer } from "mongodb-memory-server";
import CalendarService from "tasks/source/lib/CalendarService.js";

import { should } from "chai";
should();

describe("checking updated sources", function () {
  let updatedSources;
  let db;
  let mongoServer;
  let pictureId;
  let featureId;

  beforeEach(async function () {
    this.timeout(60000);

    mongoServer = await MongoMemoryServer.create();
    await mongoServer.getUri();

    db = new Db({
      url: `mongodb://${mongoServer._instanceInfo.ip}:${mongoServer._instanceInfo.port}/tests`,
      configuration: { collection: "test" },
    });

    await db.connectWithPromise();

    CalendarService.instance = {
      now: "2021-11-30T13:55:48",
      nowDate: new Date("2021-11-30T13:55:48"),
    };

    updatedSources = new UpdatedSources();

    let insertResult = await db.addItem("pictures", { meta: {} });
    pictureId = insertResult.insertedId.toString();

    insertResult = await db.addItem("sites", {});
    featureId = insertResult.insertedId.toString();
  });

  this.afterEach(async function () {
    await db.close();
    await mongoServer.stop();
  });

  it("should have the `PictureUpdatedSource` name", function () {
    updatedSources.name.should.equal("PictureUpdatedSource");
  });

  it("does not mark pictures when it has no link", async function () {
    await db.addItem("sites", { pictures: [pictureId] });

    const stats = await updatedSources.start(db);

    const record = await db.getItem("pictures", pictureId);

    expect(record.meta).to.deep.equal({});
    expect(stats).to.deep.equal({
      total: 0,
      updated: 0,
    });
  });

  it("marks a picture when the source link is deleted", async function () {
    const insertResult = await db.addItem("pictures", {
      meta: {
        link: {
          model: "Feature",
          modelId: "64a17b6d905a4cd0aabff888",
        },
      },
    });
    pictureId = insertResult.insertedId.toString();

    const stats = await updatedSources.start(db);

    const record = await db.getItem("pictures", pictureId);
    const str = { ...record.meta.link };

    expect(str).to.deep.equal({
      model: "Feature",
      modelId: "64a17b6d905a4cd0aabff888",
      noLinkInSource: new Date("2021-11-30T13:55:48"),
    });
    expect(stats).to.deep.equal({
      total: 1,
      updated: 1,
    });
  });

  it("does not mark a picture when the source link contains the id", async function () {
    let insertResult = await db.addItem("pictures", {
      meta: {
        link: {
          model: "Feature",
          modelId: `${featureId}`,
        },
      },
    });
    pictureId = insertResult.insertedId.toString();

    await db.setItem("sites", featureId, { picture: pictureId });

    const stats = await updatedSources.start(db);

    const record = await db.getItem("pictures", pictureId);
    const str = { ...record.meta.link };

    expect(str).to.deep.equal({
      model: "Feature",
      modelId: `${featureId}`,
    });
    expect(stats).to.deep.equal({
      total: 1,
      updated: 0,
    });
  });

  it("marks a picture when the source link contains the id", async function () {
    let insertResult = await db.addItem("pictures", {
      meta: {
        link: {
          model: "Feature",
          modelId: `${featureId}`,
        },
      },
    });
    pictureId = insertResult.insertedId.toString();

    const stats = await updatedSources.start(db);

    const record = await db.getItem("pictures", pictureId);
    const str = { ...record.meta.link };

    expect(str).to.deep.equal({
      model: "Feature",
      modelId: `${featureId}`,
      noLinkInSource: new Date("2021-11-30T13:55:48"),
    });
    expect(stats).to.deep.equal({
      total: 1,
      updated: 1,
    });
  });
});
