/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import PictureLinks from "../../source/links/pictures.js";
import Db from "models/source/db.js";
import { MongoMemoryServer } from "mongodb-memory-server";
import CalendarService from "tasks/source/lib/CalendarService.js";

import { should } from "chai";
should();

describe("adding picture links", function () {
  let pictureLinks;
  let db;
  let mongoServer;
  let pictureId;

  beforeEach(async function () {
    this.timeout(60000);

    mongoServer = await MongoMemoryServer.create();
    await mongoServer.getUri();

    db = new Db({
      url: `mongodb://${mongoServer._instanceInfo.ip}:${mongoServer._instanceInfo.port}/tests`,
      configuration: { collection: "test" },
    });

    await db.connectWithPromise();

    CalendarService.instance = { now: "2021-11-30T13:55:48" };

    pictureLinks = new PictureLinks();

    const insertResult = await db.addItem("pictures", {});
    pictureId = insertResult.insertedId.toString();
  });

  this.afterEach(async function () {
    await db.close();
    await mongoServer.stop();
  });

  it("should have the `AddPictureLinks` name", function () {
    pictureLinks.name.should.equal("AddPictureLinks");
  });

  describe("features", function () {
    it("adds the meta to an unlinked picture to a feature", async function () {
      const insertResult = await db.addItem("sites", { pictures: [pictureId] });
      const siteId = insertResult.insertedId.toString();

      const stats = await pictureLinks.start(db);

      const record = await db.getItem("pictures", pictureId);

      expect(record.meta).to.deep.equal({
        link: { model: "Feature", modelId: siteId.toString() },
      });
      expect(stats).to.deep.equal({
        totalPictures: 1,
        skip: 0,
        missmatch: 0,
        totalRecords: 1,
        updated: 1,
        pictureNotFound: 0,
      });
    });

    it("does not add a link to a missing picture", async function () {
      await db.addItem("sites", { pictures: ["64a141e736a35cce8734fbdb"] });

      const stats = await pictureLinks.start(db);

      const record = await db.getItem("pictures", pictureId);

      expect(record.meta).to.be.undefined;
      expect(stats).to.deep.equal({
        totalRecords: 1,
        totalPictures: 1,
        skip: 0,
        missmatch: 0,
        updated: 0,
        pictureNotFound: 1,
      });
    });

    it("does not count a feature with no pictures", async function () {
      await db.addItem("sites", { pictures: [] });

      const stats = await pictureLinks.start(db);

      expect(stats).to.deep.equal({
        totalRecords: 0,
        totalPictures: 0,
        skip: 0,
        missmatch: 0,
        updated: 0,
        pictureNotFound: 0,
      });
    });
  });

  describe("maps", function () {
    it("adds the meta to an unlinked map cover", async function () {
      const insertResult = await db.addItem("maps", { cover: pictureId });
      const mapId = insertResult.insertedId.toString();

      const stats = await pictureLinks.start(db);

      const record = await db.getItem("pictures", pictureId);

      expect(record.meta).to.deep.equal({
        link: { model: "Map", modelId: mapId.toString() },
      });
      expect(stats).to.deep.equal({
        totalPictures: 1,
        skip: 0,
        missmatch: 0,
        totalRecords: 1,
        updated: 1,
        pictureNotFound: 0,
      });
    });

    it("adds the meta to an unlinked map squareCover", async function () {
      const insertResult = await db.addItem("maps", { squareCover: pictureId });
      const mapId = insertResult.insertedId.toString();

      const stats = await pictureLinks.start(db);

      const record = await db.getItem("pictures", pictureId);

      expect(record.meta).to.deep.equal({
        link: { model: "Map", modelId: mapId.toString() },
      });
      expect(stats).to.deep.equal({
        totalPictures: 1,
        skip: 0,
        missmatch: 0,
        totalRecords: 1,
        updated: 1,
        pictureNotFound: 0,
      });
    });

    it("adds the meta to an unlinked small sprite", async function () {
      const insertResult = await db.addItem("maps", {
        sprites: { small: pictureId },
      });
      const mapId = insertResult.insertedId.toString();

      const stats = await pictureLinks.start(db);

      const record = await db.getItem("pictures", pictureId);

      expect(record.meta).to.deep.equal({
        link: { model: "Map", modelId: mapId.toString() },
      });
      expect(stats).to.deep.equal({
        totalPictures: 1,
        skip: 0,
        missmatch: 0,
        totalRecords: 1,
        updated: 1,
        pictureNotFound: 0,
      });
    });

    it("adds the meta to an unlinked large sprite", async function () {
      const insertResult = await db.addItem("maps", {
        sprites: { large: pictureId },
      });
      const mapId = insertResult.insertedId.toString();

      const stats = await pictureLinks.start(db);

      const record = await db.getItem("pictures", pictureId);

      expect(record.meta).to.deep.equal({
        link: { model: "Map", modelId: mapId.toString() },
      });
      expect(stats).to.deep.equal({
        totalPictures: 1,
        skip: 0,
        missmatch: 0,
        totalRecords: 1,
        updated: 1,
        pictureNotFound: 0,
      });
    });

    it("ignores maps with null covers", async function () {
      const insertResult = await db.addItem("maps", { squareCover: null });
      const mapId = insertResult.insertedId.toString();

      const stats = await pictureLinks.start(db);

      const record = await db.getItem("pictures", pictureId);

      expect(stats).to.deep.equal({
        totalPictures: 0,
        skip: 0,
        missmatch: 0,
        totalRecords: 0,
        updated: 0,
        pictureNotFound: 0,
      });
    });
  });

  describe("teams", function () {
    it("adds the meta to an unlinked team logo", async function () {
      const insertResult = await db.addItem("teams", { logo: pictureId });
      const teamId = insertResult.insertedId.toString();

      const stats = await pictureLinks.start(db);

      const record = await db.getItem("pictures", pictureId);

      expect(record.meta).to.deep.equal({
        link: { model: "Team", modelId: teamId.toString() },
      });
      expect(stats).to.deep.equal({
        totalPictures: 1,
        skip: 0,
        missmatch: 0,
        totalRecords: 1,
        updated: 1,
        pictureNotFound: 0,
      });
    });

    it("adds the meta to an unlinked team pictures", async function () {
      const insertResult = await db.addItem("teams", { pictures: [pictureId] });
      const teamId = insertResult.insertedId.toString();

      const stats = await pictureLinks.start(db);

      const record = await db.getItem("pictures", pictureId);

      expect(record.meta).to.deep.equal({
        link: { model: "Team", modelId: teamId.toString() },
      });
      expect(stats).to.deep.equal({
        totalPictures: 1,
        skip: 0,
        missmatch: 0,
        totalRecords: 1,
        updated: 1,
        pictureNotFound: 0,
      });
    });
  });

  describe("campaign answers", function () {
    it("adds the meta to an unlinked picture", async function () {
      const insertResult = await db.addItem("campaignAnswers", {
        pictures: [pictureId],
      });
      const mapId = insertResult.insertedId.toString();

      const stats = await pictureLinks.start(db);

      const record = await db.getItem("pictures", pictureId);

      expect(record.meta).to.deep.equal({
        link: { model: "CampaignAnswer", modelId: mapId.toString() },
      });
      expect(stats).to.deep.equal({
        totalPictures: 1,
        skip: 0,
        missmatch: 0,
        totalRecords: 1,
        updated: 1,
        pictureNotFound: 0,
      });
    });
  });

  describe("articles", function () {
    it("adds the meta to an unlinked cover", async function () {
      const insertResult = await db.addItem("articles", { cover: pictureId });
      const teamId = insertResult.insertedId.toString();

      const stats = await pictureLinks.start(db);

      const record = await db.getItem("pictures", pictureId);

      expect(record.meta).to.deep.equal({
        link: { model: "Article", modelId: teamId.toString() },
      });
      expect(stats).to.deep.equal({
        totalPictures: 1,
        skip: 0,
        missmatch: 0,
        totalRecords: 1,
        updated: 1,
        pictureNotFound: 0,
      });
    });

    it("adds the meta to an unlinked pictures in content", async function () {
      const insertResult = await db.addItem("articles", {
        content: {
          blocks: [
            {
              type: "image",
              data: {
                withBorder: false,
                file: {
                  url: `http://localhost:9091/pictures/${pictureId}/picture/lg`,
                },
                stretched: false,
                withBackground: false,
                caption: "",
              },
            },
          ],
        },
      });
      const teamId = insertResult.insertedId.toString();

      const stats = await pictureLinks.start(db);

      const record = await db.getItem("pictures", pictureId);

      expect(record.meta).to.deep.equal({
        link: { model: "Article", modelId: teamId.toString() },
      });
      expect(stats).to.deep.equal({
        totalPictures: 1,
        skip: 0,
        missmatch: 0,
        totalRecords: 1,
        updated: 1,
        pictureNotFound: 0,
      });
    });

    it("adds the meta to an unlinked picture", async function () {
      const insertResult = await db.addItem("articles", {
        pictures: [pictureId],
      });
      const teamId = insertResult.insertedId.toString();

      const stats = await pictureLinks.start(db);

      const record = await db.getItem("pictures", pictureId);

      expect(record.meta).to.deep.equal({
        link: { model: "Article", modelId: teamId.toString() },
      });
      expect(stats).to.deep.equal({
        totalPictures: 1,
        skip: 0,
        missmatch: 0,
        totalRecords: 1,
        updated: 1,
        pictureNotFound: 0,
      });
    });
  });

  describe("base map", function () {
    it("adds the meta to an unlinked cover", async function () {
      const insertResult = await db.addItem("basemaps", { cover: pictureId });
      const teamId = insertResult.insertedId.toString();

      const stats = await pictureLinks.start(db);

      const record = await db.getItem("pictures", pictureId);

      expect(record.meta).to.deep.equal({
        link: { model: "BaseMap", modelId: teamId.toString() },
      });
      expect(stats).to.deep.equal({
        totalPictures: 1,
        skip: 0,
        missmatch: 0,
        totalRecords: 1,
        updated: 1,
        pictureNotFound: 0,
      });
    });
  });

  describe("campaigns", function () {
    it("adds the meta to an unlinked cover", async function () {
      const insertResult = await db.addItem("campaigns", { cover: pictureId });
      const teamId = insertResult.insertedId.toString();

      const stats = await pictureLinks.start(db);

      const record = await db.getItem("pictures", pictureId);

      expect(record.meta).to.deep.equal({
        link: { model: "Campaign", modelId: teamId.toString() },
      });
      expect(stats).to.deep.equal({
        totalPictures: 1,
        skip: 0,
        missmatch: 0,
        totalRecords: 1,
        updated: 1,
        pictureNotFound: 0,
      });
    });
  });

  describe("icon sets", function () {
    it("adds the meta to an unlinked cover", async function () {
      const insertResult = await db.addItem("iconsets", { cover: pictureId });
      const teamId = insertResult.insertedId.toString();

      const stats = await pictureLinks.start(db);

      const record = await db.getItem("pictures", pictureId);

      expect(record.meta).to.deep.equal({
        link: { model: "IconSet", modelId: teamId.toString() },
      });
      expect(stats).to.deep.equal({
        totalPictures: 1,
        skip: 0,
        missmatch: 0,
        totalRecords: 1,
        updated: 1,
        pictureNotFound: 0,
      });
    });
  });

  describe("page", function () {
    it("adds the meta to an unlinked cover", async function () {
      const insertResult = await db.addItem("page", { cover: pictureId });
      const teamId = insertResult.insertedId.toString();

      const stats = await pictureLinks.start(db);

      const record = await db.getItem("pictures", pictureId);

      expect(record.meta).to.deep.equal({
        link: { model: "Page", modelId: teamId.toString() },
      });
      expect(stats).to.deep.equal({
        totalPictures: 1,
        skip: 0,
        missmatch: 0,
        totalRecords: 1,
        updated: 1,
        pictureNotFound: 0,
      });
    });

    it("adds the meta to an unlinked picture in a page col", async function () {
      const insertResult = await db.addItem("page", {
        cols: [
          {
            data: {
              source: {
                id: `${pictureId}`,
                model: "picture",
              },
              classes: [],
            },
            type: "picture",
          },
        ],
      });

      const teamId = insertResult.insertedId.toString();

      const stats = await pictureLinks.start(db);

      const record = await db.getItem("pictures", pictureId);

      expect(record.meta).to.deep.equal({
        link: { model: "Page", modelId: teamId.toString() },
      });
      expect(stats).to.deep.equal({
        totalPictures: 1,
        skip: 0,
        missmatch: 0,
        totalRecords: 1,
        updated: 1,
        pictureNotFound: 0,
      });
    });

    it("adds the meta to an unlinked picture in a page col galery", async function () {
      const insertResult = await db.addItem("page", {
        cols: [
          {
            name: "0.0.0",
            row: 0,
            col: 0,
            gid: "",
            data: {
              ids: [`${pictureId}`, "64a17b6d905a4cd0aabff888"],
            },
            type: "pictures",
            container: 0,
          },
        ],
      });

      const teamId = insertResult.insertedId.toString();

      const stats = await pictureLinks.start(db);

      const record = await db.getItem("pictures", pictureId);

      expect(record.meta).to.deep.equal({
        link: { model: "Page", modelId: teamId.toString() },
      });
      expect(stats).to.deep.equal({
        totalPictures: 2,
        skip: 0,
        missmatch: 0,
        totalRecords: 1,
        updated: 1,
        pictureNotFound: 1,
      });
    });
  });

  describe("presentations", function () {
    it("adds the meta to an unlinked cover", async function () {
      const insertResult = await db.addItem("presentation", {
        cover: pictureId,
      });
      const teamId = insertResult.insertedId.toString();

      const stats = await pictureLinks.start(db);

      const record = await db.getItem("pictures", pictureId);

      expect(record.meta).to.deep.equal({
        link: { model: "Presentation", modelId: teamId.toString() },
      });
      expect(stats).to.deep.equal({
        totalPictures: 1,
        skip: 0,
        missmatch: 0,
        totalRecords: 1,
        updated: 1,
        pictureNotFound: 0,
      });
    });

    it("adds the meta to an unlinked picture in a page col", async function () {
      const insertResult = await db.addItem("presentation", {
        cols: [
          {
            data: {
              source: {
                id: `${pictureId}`,
                model: "picture",
              },
              classes: [],
            },
            type: "picture",
          },
        ],
      });

      const teamId = insertResult.insertedId.toString();

      const stats = await pictureLinks.start(db);

      const record = await db.getItem("pictures", pictureId);

      expect(record.meta).to.deep.equal({
        link: { model: "Presentation", modelId: teamId.toString() },
      });
      expect(stats).to.deep.equal({
        totalPictures: 1,
        skip: 0,
        missmatch: 0,
        totalRecords: 1,
        updated: 1,
        pictureNotFound: 0,
      });
    });
  });

  describe("user profiles", function () {
    it("adds the meta to an unlinked picture", async function () {
      const insertResult = await db.addItem("userProfile", {
        picture: pictureId,
      });
      const teamId = insertResult.insertedId.toString();

      const stats = await pictureLinks.start(db);

      const record = await db.getItem("pictures", pictureId);

      expect(record.meta).to.deep.equal({
        link: { model: "UserProfile", modelId: teamId.toString() },
      });
      expect(stats).to.deep.equal({
        totalPictures: 1,
        skip: 0,
        missmatch: 0,
        totalRecords: 1,
        updated: 1,
        pictureNotFound: 0,
      });
    });
  });

  describe("spaces", function () {
    it("adds the meta to an unlinked cover", async function () {
      const insertResult = await db.addItem("space", { cover: pictureId });
      const teamId = insertResult.insertedId.toString();

      const stats = await pictureLinks.start(db);

      const record = await db.getItem("pictures", pictureId);

      expect(record.meta).to.deep.equal({
        link: { model: "Space", modelId: teamId.toString() },
      });
      expect(stats).to.deep.equal({
        totalPictures: 1,
        skip: 0,
        missmatch: 0,
        totalRecords: 1,
        updated: 1,
        pictureNotFound: 0,
      });
    });

    it("adds the meta to an unlinked logo", async function () {
      const insertResult = await db.addItem("space", { logo: pictureId });
      const teamId = insertResult.insertedId.toString();

      const stats = await pictureLinks.start(db);

      const record = await db.getItem("pictures", pictureId);

      expect(record.meta).to.deep.equal({
        link: { model: "Space", modelId: teamId.toString() },
      });
      expect(stats).to.deep.equal({
        totalPictures: 1,
        skip: 0,
        missmatch: 0,
        totalRecords: 1,
        updated: 1,
        pictureNotFound: 0,
      });
    });

    it("adds the meta to an unlinked picture in a page col", async function () {
      const insertResult = await db.addItem("space", {
        cols: {
          main: {
            data: {
              source: {
                id: `${pictureId}`,
                model: "picture",
              },
              classes: [],
            },
            type: "picture",
          },
        },
      });

      const teamId = insertResult.insertedId.toString();

      const stats = await pictureLinks.start(db);

      const record = await db.getItem("pictures", pictureId);

      expect(record.meta).to.deep.equal({
        link: { model: "Space", modelId: teamId.toString() },
      });
      expect(stats).to.deep.equal({
        totalPictures: 1,
        skip: 0,
        missmatch: 0,
        totalRecords: 1,
        updated: 1,
        pictureNotFound: 0,
      });
    });
  });
});
