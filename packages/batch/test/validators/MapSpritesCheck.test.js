/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import MapSpritesCheck from "../../source/validators/MapSpritesCheck.js";
import Db from "models/source/db.js";
import { MongoMemoryServer } from "mongodb-memory-server";
import { expect } from "chai";

import { should } from "chai";
should();

describe("Map Sprites Check", function () {
  let mapSpritesCheck;
  let mongoServer;
  let db;
  let broadcast;

  beforeEach(async function () {
    broadcast = {
      messages: [],
      push(channel, message) {
        this.messages.push({ channel, message });
      },
    };

    this.timeout(60000);

    mongoServer = await MongoMemoryServer.create();
    await mongoServer.getUri();

    db = new Db({
      url: `mongodb://${mongoServer._instanceInfo.ip}:${mongoServer._instanceInfo.port}/tests`,
      configuration: { collection: "test" },
    });

    await db.connectWithPromise();

    mapSpritesCheck = new MapSpritesCheck();
  });

  afterEach(async function () {
    await db.close();
    await mongoServer.stop();
  });

  it("should have the `MapSpritesCheck` name", function () {
    mapSpritesCheck.name.should.equal("MapSpritesCheck");
  });

  it("triggers a `Map.spriteAll` and `Map.spriteDefault` task when there is no picture", async function () {
    await mapSpritesCheck.start(db, broadcast);

    expect(broadcast.messages).deep.equal([
      { channel: "Map.spriteAll", message: { id: "_", uid: "_" } },
      {
        channel: "Map.spriteDefault",
        message: { id: "default", uid: "default" },
      },
    ]);
  });

  describe("when the spriteAll and spriteDefault exist", function () {
    beforeEach(async function () {
      await db.addItem("pictures", {
        meta: {
          link: {
            type: "sprite",
            size: 1,
            modelId: "_",
            model: "map",
          },
        },
      });

      await db.addItem("pictures", {
        meta: {
          link: {
            type: "sprite",
            size: 2,
            modelId: "_",
            model: "map",
          },
        },
      });

      await db.addItem("pictures", {
        meta: {
          link: {
            type: "sprite",
            size: 1,
            modelId: "default",
            model: "map",
          },
        },
      });

      await db.addItem("pictures", {
        meta: {
          link: {
            type: "sprite",
            size: 2,
            modelId: "default",
            model: "Map",
          },
        },
      });
    });

    it("does not triggers a `Map.spriteAll` or `Map.spriteDefault` tasks", async function () {
      await mapSpritesCheck.start(db, broadcast);

      expect(broadcast.messages).deep.equal([]);
    });

    describe("when a map with the default icon sets exists", function () {
      beforeEach(async function () {
        await db.addItem("maps", {
          iconSets: {
            useCustomList: false,
          },
        });
      });

      it("does not triggers a `Map.spriteAll` or `Map.spriteDefault` tasks", async function () {
        await mapSpritesCheck.start(db, broadcast);

        expect(broadcast.messages).deep.equal([]);
      });
    });

    describe("when a map with a custom icon sets exists and without sprite", function () {
      let map;

      beforeEach(async function () {
        map = await db.addItem("maps", {
          iconSets: {
            useCustomList: true,
          },
        });
      });

      it("triggers a `Map.sprite` task", async function () {
        await mapSpritesCheck.start(db, broadcast);

        expect(broadcast.messages).deep.equal([
          {
            channel: "Map.sprite",
            message: { id: `${map.insertedId}`, uid: `${map.insertedId}` },
          },
        ]);
      });
    });

    describe("when a map with a custom icon sets exists and with sprite", function () {
      let map;
      let picture1;
      let picture2;

      beforeEach(async function () {
        map = await db.addItem("maps", {
          iconSets: {
            useCustomList: true,
          },
        });

        const mapId = `${map.insertedId}`;

        let result = await db.addItem("pictures", {
          meta: {
            link: {
              type: "sprite",
              size: 1,
              modelId: `${mapId}`,
              model: "Map",
            },
          },
        });
        picture1 = `${result.insertedId}`;

        result = await db.addItem("pictures", {
          meta: {
            link: {
              type: "sprite",
              size: 2,
              modelId: `${mapId}`,
              model: "Map",
            },
          },
        });
        picture2 = `${result.insertedId}`;

        await db.setItem("maps", mapId, {
          iconSets: {
            useCustomList: true,
          },
          sprites: {
            small: picture1,
            large: picture2,
          },
        });
      });

      it("does not trigger a map.sprite task", async function () {
        await mapSpritesCheck.start(db, broadcast);

        expect(broadcast.messages).deep.equal([]);
      });
    });

    describe("when a map with sprite picture ids exists and missing pictures", function () {
      let mapId;

      beforeEach(async function () {
        const map = await db.addItem("maps", {
          iconSets: {
            useCustomList: true,
          },
        });

        mapId = `${map.insertedId}`;

        await db.setItem("maps", mapId, {
          iconSets: {
            useCustomList: true,
          },
          sprites: {
            small: "6595f877f52540010084d5d4",
            large: "6595f877f52540010084d5aa",
          },
        });
      });

      it("removes the sprites ids", async function () {
        await mapSpritesCheck.start(db, broadcast);

        const map = await await db.getItem("maps", mapId);

        expect(map.sprites).to.deep.equal({});
      });
    });
  });
});
