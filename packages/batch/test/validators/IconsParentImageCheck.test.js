/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import IconsParentImageCheck from "../../source/validators/IconsParentImageCheck.js";
import Db from "models/source/db.js";
import { MongoMemoryServer } from "mongodb-memory-server";
import { ObjectId } from "mongodb";

import { should } from "chai";
should();

describe("Icons Parent Image Check", function () {
  let instance;
  let iconSetId;
  let mongoServer;
  let db;

  beforeEach(async function () {
    this.timeout(60000);

    mongoServer = await MongoMemoryServer.create();
    const url = mongoServer.getUri();

    db = new Db(
      {
        url,
        configuration: { collection: "test" },
      },
      true,
    );

    await db.connectWithPromise();

    instance = new IconsParentImageCheck();

    const insertResult = await db.addItem("iconsets", {
      visibility: {
        isDefault: true,
        isPublic: true,
        team: new ObjectId("5ca78e2160780601008f69e6"),
      },
    });

    iconSetId = insertResult.insertedId;
  });

  afterEach(async function () {
    await db.close();
    await mongoServer.stop();
  });

  it("should have the `IconsParentImageCheck` name", function () {
    instance.name.should.equal("IconsParentImageCheck");
  });

  it("ignores an icon that has no parent", async function () {
    const result = await db.addItem("icons", { parent: "" });
    const iconId = result.insertedId;

    const results = await instance.start(db);

    let icon = await db.getItemWithQuery("icons", { _id: iconId });

    expect(results).to.deep.equal({
      total: 0,
      updated: 0,
    });
    expect(icon).to.deep.equal({
      _id: iconId,
      parent: "",
    });
  });

  it("ignores an icon that has parent and no image", async function () {
    const parentResult = await db.addItem("icons", { parent: "" });
    const parentIconId = parentResult.insertedId;

    const result = await db.addItem("icons", { parent: `${parentIconId}` });
    const iconId = result.insertedId;

    const results = await instance.start(db);

    let icon = await db.getItemWithQuery("icons", { _id: iconId });

    expect(results).to.deep.equal({
      total: 0,
      updated: 0,
    });
    expect(icon).to.deep.equal({
      _id: iconId,
      parent: `${parentIconId}`,
    });
  });

  it("copies the parent image id when useParent is true", async function () {
    const parentResult = await db.addItem("icons", {
      parent: "",
      image: { value: "1" },
    });
    const parentIconId = parentResult.insertedId;

    const result = await db.addItem("icons", {
      parent: `${parentIconId}`,
      image: { useParent: true },
    });
    const iconId = result.insertedId;

    const results = await instance.start(db);

    let icon = await db.getItemWithQuery("icons", { _id: iconId });

    expect(results).to.deep.equal({
      total: 1,
      updated: 1,
    });
    expect(icon).to.deep.equal({
      _id: iconId,
      parent: `${parentIconId}`,
      image: {
        useParent: true,
        value: "1",
      },
    });
  });

  it("copies the root image id when the parent icon has a parent", async function () {
    const rootResult = await db.addItem("icons", {
      parent: "",
      image: { value: "root" },
    });
    const rootIconId = rootResult.insertedId;

    const parentResult = await db.addItem("icons", {
      parent: `${rootIconId}`,
      image: { value: "1" },
    });
    const parentIconId = parentResult.insertedId;

    const result = await db.addItem("icons", {
      parent: `${parentIconId}`,
      image: { useParent: true },
    });
    const iconId = result.insertedId;

    const results = await instance.start(db);

    let icon = await db.getItemWithQuery("icons", { _id: iconId });

    expect(results).to.deep.equal({
      total: 1,
      updated: 1,
    });
    expect(icon).to.deep.equal({
      _id: iconId,
      parent: `${parentIconId}`,
      image: {
        useParent: true,
        value: "1",
      },
    });
  });

  it("removes the parent when it does not exist", async function () {
    const parent = `${new ObjectId()}`;
    const result = await db.addItem("icons", {
      parent,
      image: { useParent: true },
    });
    const iconId = result.insertedId;

    const results = await instance.start(db);

    let icon = await db.getItemWithQuery("icons", { _id: iconId });

    expect(results).to.deep.equal({
      total: 1,
      updated: 1,
    });
    expect(icon).to.deep.equal({
      _id: iconId,
      parent,
      image: {
        useParent: false,
        value: "",
      },
    });
  });
});
