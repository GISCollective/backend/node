/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import PicturesHashCheck from "../../source/validators/PicturesHashCheck.js";

import { should } from "chai";
should();

describe("Pictures Hash Check", function () {
  let picturesHashCheck;

  beforeEach(function () {
    picturesHashCheck = new PicturesHashCheck();
  });

  it("should have the `PicturesHashCheck` name", function () {
    picturesHashCheck.name.should.equal("PicturesHashCheck");
  });

  it("should request all pictures", function () {
    const mockDb = {
      get(model, params) {
        this.model = model;
        this.params = params;
      },

      getAllMeta() {},
    };

    picturesHashCheck.start(mockDb);

    mockDb.model.should.equal("pictures");
    mockDb.params.should.be.a("function");
  });

  it("should trigger the Picture.Hash when a picture has no hash", async function () {
    const mockDb = {
      get(model, params) {
        params({ _id: "1" });
      },

      getAllMeta() {},

      getMeta(type, itemId) {},
    };

    const mockBroadcast = {
      push(channel, message) {
        this.channel = channel;
        this.message = message;
      },
    };

    await picturesHashCheck.start(mockDb, mockBroadcast);

    mockBroadcast.should.haveOwnProperty("channel");
    mockBroadcast.should.haveOwnProperty("message");

    mockBroadcast.channel.should.equal("Picture.Hash");
    mockBroadcast.message.should.deep.equal({ id: "1", uid: "1" });
  });

  it("should not trigger the Picture.Hash when a picture has a hash", async function () {
    const mockDb = {
      get(model, params) {
        params({ _id: "1", hash: "some-hash" });
      },

      getAllMeta() {},

      getMeta(type, itemId) {},
    };

    const mockBroadcast = {
      push(channel, message) {
        this.channel = channel;
        this.message = message;
      },
    };

    await picturesHashCheck.start(mockDb, mockBroadcast);

    mockBroadcast.should.not.haveOwnProperty("channel");
    mockBroadcast.should.not.haveOwnProperty("message");
  });

  it("should not trigger the Picture.Hash when a picture has an empty string hash", async function () {
    const mockDb = {
      get(model, params) {
        params({ _id: "1", hash: "" });
      },

      getAllMeta() {},

      getMeta(type, itemId) {},
    };

    const mockBroadcast = {
      push(channel, message) {
        this.channel = channel;
        this.message = message;
      },
    };

    await picturesHashCheck.start(mockDb, mockBroadcast);

    mockBroadcast.should.not.haveOwnProperty("message");
    mockBroadcast.should.not.haveOwnProperty("channel");
  });
});
