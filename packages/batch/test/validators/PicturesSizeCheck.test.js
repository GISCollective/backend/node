/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import PicturesSizeCheck from "../../source/validators/PicturesSizeCheck.js";
import Db from "models/source/db.js";
import { MongoMemoryServer } from "mongodb-memory-server";
import { should } from "chai";
import { uploadFile } from "models/source/files.js";
should();

describe("Pictures Size Check", function () {
  let picturesSizeCheck;
  let mongoServer;
  let db;
  let webpFile;
  let heicFile;
  let xlxsFile;

  beforeEach(async function () {
    this.timeout(60000);

    mongoServer = await MongoMemoryServer.create();
    const url = mongoServer.getUri();

    db = new Db(
      {
        url,
        configuration: { collection: "test" },
      },
      false
    );

    await db.connectWithPromise();

    picturesSizeCheck = new PicturesSizeCheck();

    const storage = db.getFileStorage("pictures");

    webpFile = await uploadFile(storage, "test/fixtures/test.webp", {
      mime: "image/webp",
    });

    heicFile = await uploadFile(storage, "test/fixtures/test.heic", {
      mime: "image/heic",
    });

    xlxsFile = await uploadFile(storage, "test/fixtures/test.xlsx", {
      mime: "application/xlsx",
    });
  });

  afterEach(async function () {
    await db.close();
    await mongoServer.stop();
  });

  it("should have the `PicturesSizeCheck` name", function () {
    picturesSizeCheck.name.should.equal("PicturesSizeCheck");
  });

  it("does nothing when there is a picture with width and height", async function () {
    await db.addItem("pictures", { meta: { width: 100, height: 200 } });

    const stats = await picturesSizeCheck.start(db);

    stats.should.deep.equal({ total: 0, updated: 0, errors: 0 });
  });

  it("updates the picture size when there is a webp image with no width", async function () {
    this.timeout(10000);

    const insertResult = await db.addItem("pictures", {
      picture: webpFile._id,
      meta: { height: 200 },
    });

    const stats = await picturesSizeCheck.start(db);

    stats.should.deep.equal({ total: 1, updated: 1, errors: 0 });

    const picture = await db.getItem("pictures", insertResult.insertedId);

    picture.meta.format.should.deep.equal("WEBP");
    picture.meta.height.should.deep.equal(3024);
    picture.meta.mime.should.deep.equal("image/webp");
    picture.meta.width.should.deep.equal(4032);
  });

  it.skip("updates the picture size when there is a heic image with no height", async function () {
    this.timeout(10000);

    const insertResult = await db.addItem("pictures", {
      picture: heicFile._id,
      meta: { width: 200 },
    });

    const stats = await picturesSizeCheck.start(db);

    stats.should.deep.equal({ total: 1, updated: 1, errors: 0 });

    const picture = await db.getItem("pictures", insertResult.insertedId);

    if (!picture.meta) {
      picture.meta = {};
    }

    picture.meta.format.should.deep.equal("HEIC");
    picture.meta.height.should.deep.equal(3906);
    picture.meta.mime.should.contain("heic");
    picture.meta.width.should.deep.equal(7292);
  });

  it("ignores the picture size when there is a xlsx image with no size", async function () {
    this.timeout(10000);

    const insertResult = await db.addItem("pictures", {
      picture: xlxsFile._id,
      meta: {},
    });

    const stats = await picturesSizeCheck.start(db);

    stats.should.deep.equal({ total: 1, updated: 0, errors: 0 });

    const picture = await db.getItem("pictures", insertResult.insertedId);

    picture.meta.should.deep.equal({});
  });
});
