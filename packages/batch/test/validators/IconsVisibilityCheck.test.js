/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import IconsVisibilityCheck from "../../source/validators/IconsVisibilityCheck.js";
import Db from "models/source/db.js";
import { MongoMemoryServer } from "mongodb-memory-server";
import { ObjectId } from "mongodb";

import { should } from "chai";
should();

describe("Icons Visibility Check", function () {
  let iconsVisibilityCheck;
  let iconSetId;
  let mongoServer;
  let db;

  beforeEach(async function () {
    this.timeout(60000);

    mongoServer = await MongoMemoryServer.create();
    const url = mongoServer.getUri();

    db = new Db(
      {
        url,
        configuration: { collection: "test" },
      },
      true
    );

    await db.connectWithPromise();

    iconsVisibilityCheck = new IconsVisibilityCheck();

    const insertResult = await db.addItem("iconsets", {
      visibility: {
        isDefault: true,
        isPublic: true,
        team: new ObjectId("5ca78e2160780601008f69e6"),
      },
    });

    iconSetId = insertResult.insertedId;
  });

  afterEach(async function () {
    await db.close();
    await mongoServer.stop();
  });

  it("should have the `IconsVisibilityCheck` name", function () {
    iconsVisibilityCheck.name.should.equal("IconsVisibilityCheck");
  });

  it("updates the icon with the set visibility when they are not the same", async function () {
    const result = await db.addItem("icons", { iconSet: iconSetId });
    const iconId = result.insertedId;

    const results = await iconsVisibilityCheck.start(db);

    let icon = await db.getItemWithQuery("icons", { _id: iconId });

    expect(results).to.deep.equal({
      total: 1,
      updated: 1,
    });
    expect(icon.visibility).to.deep.equal({
      isDefault: true,
      isPublic: true,
      team: new ObjectId("5ca78e2160780601008f69e6"),
    });
  });

  it("does not update an icon when it has the same visibility as the set", async function () {
    const result = await db.addItem("icons", {
      visibility: {
        isDefault: true,
        isPublic: true,
        team: new ObjectId("5ca78e2160780601008f69e6"),
      },
      iconSet: iconSetId,
    });

    const iconId = result.insertedId;

    const results = await iconsVisibilityCheck.start(db);

    let icon = await db.getItemWithQuery("icons", { _id: iconId });

    expect(results).to.deep.equal({
      total: 1,
      updated: 0,
    });
    expect(icon.visibility).to.deep.equal({
      isDefault: true,
      isPublic: true,
      team: new ObjectId("5ca78e2160780601008f69e6"),
    });
  });

  it("does not update an icon when the set was deleted", async function () {
    const result = await db.addItem("icons", {
      visibility: {
        isDefault: true,
        isPublic: true,
        team: new ObjectId("5ca78e2160780601008f69e6"),
      },
      iconSet: iconSetId,
    });
    await db.deleteWithQuery("iconsets", {});

    const iconId = result.insertedId;

    const results = await iconsVisibilityCheck.start(db);

    let icon = await db.getItemWithQuery("icons", { _id: iconId });

    expect(results).to.deep.equal({
      total: 1,
      updated: 1,
    });
    expect(icon.visibility).to.equal(null);
  });
});
