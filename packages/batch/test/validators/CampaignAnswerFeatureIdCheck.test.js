/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import CampaignAnswerFeatureIdCheck from "../../source/validators/CampaignAnswerFeatureIdCheck.js";
import Db from "models/source/db.js";
import { MongoMemoryServer } from "mongodb-memory-server";
import { expect } from "chai";

import { should } from "chai";
should();

describe("CampaignAnswerFeatureIdCheck", function () {
  let campaignAnswerFeatureId;
  let mongoServer;
  let db;

  beforeEach(async function () {
    this.timeout(60000);

    mongoServer = await MongoMemoryServer.create();
    await mongoServer.getUri();

    db = new Db({
      url: `mongodb://${mongoServer._instanceInfo.ip}:${mongoServer._instanceInfo.port}/tests`,
      configuration: { collection: "test" },
    });

    await db.connectWithPromise();
  });

  afterEach(async function () {
    await db.close();
    await mongoServer.stop();
  });

  beforeEach(function () {
    campaignAnswerFeatureId = new CampaignAnswerFeatureIdCheck();
  });

  it("should have the `CampaignAnswerFeatureId` name", function () {
    campaignAnswerFeatureId.name.should.equal("CampaignAnswerFeatureIdCheck");
  });

  describe("start", async function () {
    it("does not fail when the db is empty", async function () {
      const result = await campaignAnswerFeatureId.start(db);

      result.should.deep.equal({
        total: 0,
        updated: 0,
      });
    });

    it("sets the featureId to @unknown when the feature is not found", async function () {
      let insertResult = await db.addItem("campaignAnswers", {});
      let answerId = insertResult.insertedId;

      const result = await campaignAnswerFeatureId.start(db);

      result.should.deep.equal({
        total: 1,
        updated: 1,
      });

      let answer = await db.getItemWithQuery("campaignAnswers", {
        _id: answerId,
      });

      expect(answer.featureId).to.equal("@unknown");
    });

    it("sets the feature id when it's matched by source", async function () {
      let insertResult = await db.addItem("campaignAnswers", {});
      let answerId = insertResult.insertedId;

      let featureResult = await await db.addItem("sites", {
        source: {
          type: "Campaign",
          remoteId: answerId.toString(),
        },
      });

      const result = await campaignAnswerFeatureId.start(db);

      result.should.deep.equal({
        total: 1,
        updated: 1,
      });

      let answer = await db.getItemWithQuery("campaignAnswers", {
        _id: answerId,
      });
      expect(answer.featureId).to.equal(featureResult.insertedId.toString());
    });
  });
});
