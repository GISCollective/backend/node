/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import CampaignAnswerStatusCheck from "../../source/validators/CampaignAnswerStatusCheck.js";
import Db from "models/source/db.js";
import { MongoMemoryServer } from "mongodb-memory-server";
import { expect } from "chai";

import { should } from "chai";
should();

describe("CampaignAnswerStatusCheck", function () {
  let campaignAnswerStatusCheck;
  let db;
  let mongoServer;

  beforeEach(async function () {
    this.timeout(60000);

    mongoServer = await MongoMemoryServer.create();
    await mongoServer.getUri();

    db = new Db({
      url: `mongodb://${mongoServer._instanceInfo.ip}:${mongoServer._instanceInfo.port}/tests`,
      configuration: { collection: "test" },
    });

    await db.connectWithPromise();
  });

  afterEach(async function () {
    await db.close();
    await mongoServer.stop();
  });

  beforeEach(function () {
    campaignAnswerStatusCheck = new CampaignAnswerStatusCheck();
  });

  it("should have the `CampaignAnswerStatusCheck` name", function () {
    campaignAnswerStatusCheck.name.should.equal("CampaignAnswerStatusCheck");
  });

  describe("updateAnswer", async function () {
    it("sets the status to `processing` when there is no feature and featureId", async function () {
      let answer = { featureId: "something" };

      campaignAnswerStatusCheck.updateAnswer(answer);

      expect(answer).to.deep.equal({
        featureId: "something",
        status: 4,
      });
    });

    it("sets the status to `rejected` when there is no feature and the featureId is set", async function () {
      let answer = {};

      campaignAnswerStatusCheck.updateAnswer(answer);

      expect(answer).to.deep.equal({
        status: 0,
      });
    });

    it("sets the status to `pending` when there is a feature with pending visibility", async function () {
      let answer = {};
      let feature = {
        visibility: -1,
      };

      campaignAnswerStatusCheck.updateAnswer(answer, feature);

      expect(answer).to.deep.equal({
        status: 1,
      });
    });

    it("sets the status to `public` when there is a feature with public visibility", async function () {
      let answer = {};
      let feature = {
        visibility: 1,
      };

      campaignAnswerStatusCheck.updateAnswer(answer, feature);

      expect(answer).to.deep.equal({
        status: 2,
      });
    });

    it("sets the status to `private` when there is a feature with public visibility", async function () {
      let answer = {};
      let feature = {
        visibility: 0,
      };

      campaignAnswerStatusCheck.updateAnswer(answer, feature);

      expect(answer).to.deep.equal({
        status: 3,
      });
    });
  });

  describe("start", async function () {
    it("does not fail when the db is empty", async function () {
      const result = await campaignAnswerStatusCheck.start(db);

      result.should.deep.equal({
        total: 0,
        updated: 0,
      });
    });

    it("sets the status to `processing` when there is an answer without a feature", async function () {
      let insertResult = await db.addItem("campaignAnswers", {});
      let answerId = insertResult.insertedId;

      const result = await campaignAnswerStatusCheck.start(db);

      result.should.deep.equal({
        total: 1,
        updated: 1,
      });

      let answer = await db.getItemWithQuery("campaignAnswers", {
        _id: answerId,
      });

      expect(answer.status).to.equal(0);
    });

    it("sets the status to `rejected` when there is an answer with a deleted feature", async function () {
      let insertResult = await db.addItem("campaignAnswers", {
        featureId: "5fa7aeaf3115a00100fa24ae",
      });
      let answerId = insertResult.insertedId;

      const result = await campaignAnswerStatusCheck.start(db);

      result.should.deep.equal({
        total: 1,
        updated: 1,
      });

      let answer = await db.getItemWithQuery("campaignAnswers", {
        _id: answerId,
      });

      expect(answer.status).to.equal(4);
    });

    it("sets the status to `rejected` when there is an answer with an unknown feature", async function () {
      let insertResult = await db.addItem("campaignAnswers", {
        featureId: "@unknown",
      });
      let answerId = insertResult.insertedId;

      const result = await campaignAnswerStatusCheck.start(db);

      result.should.deep.equal({
        total: 1,
        updated: 1,
      });

      let answer = await db.getItemWithQuery("campaignAnswers", {
        _id: answerId,
      });

      expect(answer.status).to.equal(4);
    });
  });
});
