/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { ObjectId } from "mongodb";

function recLookup(obj, path) {
  let parts = path.split(".");

  if (!obj) {
    return;
  }

  if (parts.length == 1) {
    return obj?.[parts[0]];
  }
  return recLookup(obj?.[parts[0]], parts.slice(1).join("."));
}

const config = {
  sites: {
    lists: ["pictures"],
  },
  campaignAnswers: {
    lists: ["pictures"],
  },
  maps: {
    items: ["cover", "squareCover", "sprites.small", "sprites.large"],
  },
  teams: {
    items: ["logo"],
    lists: ["pictures"],
  },
  articles: {
    items: ["cover"],
    lists: ["pictures"],
  },
  basemaps: {
    items: ["cover"],
  },
  campaigns: {
    items: ["cover"],
  },
  iconsets: {
    items: ["cover", "sprites.small", "sprites.large"],
  },
  page: {
    items: ["cover"],
  },
  presentation: {
    items: ["cover"],
  },
  space: {
    items: ["cover", "logo", "logoSquare"],
  },
  userProfile: {
    items: ["picture"],
  },
};

const typeName = {
  sites: "Feature",
  maps: "Map",
  teams: "Team",
  campaignAnswers: "CampaignAnswer",
  articles: "Article",
  basemaps: "BaseMap",
  campaigns: "Campaign",
  iconsets: "IconSet",
  page: "Page",
  presentation: "Presentation",
  space: "Space",
  userProfile: "UserProfile",
};

export default class AddPictureLinks {
  constructor() {
    this.name = "AddPictureLinks";
  }

  extractFromArticle(content) {
    if (typeof content != "object" || content === null) {
      return [];
    }

    let pictures =
      content.blocks
        .filter((a) => a.type == "image")
        .map((a) => a.data?.file?.url)
        .filter((a) => a.endsWith("/picture/lg"))
        .flatMap((a) => a.split("/").filter((a) => ObjectId.isValid(a))) ?? [];

    return pictures;
  }

  extractFromPageCols(cols) {
    if (!Array.isArray(cols)) {
      return [];
    }

    let pictures =
      cols
        .filter((a) => a.data?.source?.model == "picture")
        .map((a) => a.data?.source?.id)
        .filter((a) => ObjectId.isValid(a)) ?? [];

    let idList =
      cols
        .filter((a) => a.type == "pictures")
        .flatMap((a) => a.data?.ids)
        .filter((a) => ObjectId.isValid(a)) ?? [];

    return [...pictures, ...idList];
  }

  async addMeta(db, pictureId, model, modelId) {
    const picture = await db.getItem("pictures", pictureId);

    if (!picture) {
      return "pictureNotFound";
    }

    if (typeof picture.meta != "object" || picture.meta === null) {
      picture.meta = {};
    }

    if (typeof picture.meta.link != "object") {
      picture.meta.link = {};
    }

    if (
      typeof picture.meta.link.modelId == "string" &&
      modelId != picture.meta.link.modelId
    ) {
      return "missmatch";
    }

    if (
      typeof picture.meta.link.model == "string" &&
      model != picture.meta.link.model
    ) {
      return "missmatch";
    }

    if (
      model === picture.meta.link.model &&
      modelId === picture.meta.link.modelId
    ) {
      return "skip";
    }

    picture.meta.link.model = model;
    picture.meta.link.modelId = modelId;

    await db.setItem("pictures", picture._id, picture);

    return "updated";
  }

  async start(db) {
    const stats = {
      totalPictures: 0,
      totalRecords: 0,
      updated: 0,
      pictureNotFound: 0,
      missmatch: 0,
      skip: 0,
    };

    for (let modelName in config) {
      const query = { $or: [] };
      const lists = config[modelName].lists ?? [];
      const items = config[modelName].items ?? [];

      if (modelName == "articles") {
        query["$or"].push({ content: { $exists: true } });
      }

      if (
        modelName == "page" ||
        modelName == "presentation" ||
        modelName == "space"
      ) {
        query["$or"].push({ cols: { $exists: true } });
      }

      for (let listName of lists) {
        query["$or"].push({
          [listName]: { $exists: true, $not: { $size: 0 } },
        });
      }

      for (let itemName of items) {
        query["$or"].push({
          [itemName]: { $exists: true, $ne: null },
        });
      }

      await db.query(modelName, query, async (record) => {
        let pictures = [];

        for (let listName of lists) {
          pictures = [...pictures, ...(record[listName] ?? [])];
        }

        for (let itemName of items) {
          const value = recLookup(record, itemName);

          if (value) {
            pictures.push(value);
          }
        }

        if (modelName == "articles") {
          pictures = [
            ...pictures,
            ...this.extractFromArticle(record["content"]),
          ];
        }

        if (modelName == "page" || modelName == "presentation") {
          pictures = [...pictures, ...this.extractFromPageCols(record["cols"])];
        }

        if (modelName == "space") {
          pictures = [
            ...pictures,
            ...this.extractFromPageCols(Object.values(record["cols"] ?? {})),
          ];
        }

        for (let pictureId of pictures) {
          const status = await this.addMeta(
            db,
            pictureId,
            typeName[modelName],
            record._id.toString()
          );

          stats[status]++;
          stats.totalPictures++;
        }

        stats.totalRecords++;
      });
    }

    return stats;
  }
}
