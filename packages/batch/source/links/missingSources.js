/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { ObjectId } from "mongodb";
import CalendarService from "tasks/source/lib/CalendarService.js";

class PictureMissingSource {
  constructor() {
    this.name = "PictureMissingSource";
  }
  async start(db) {
    const stats = {
      total: 0,
      updated: 0,
    };
    const query = {
      "meta.link.noSource": {
        $exists: false,
      },
    };
    await db.query("pictures", query, async (record) => {
      if (!ObjectId.isValid(record.meta?.link?.modelId)) {
        return;
      }

      const exists = await db.exists(record.meta?.link?.model, {
        _id: new ObjectId(record.meta?.link?.modelId),
      });
      if (!exists) {
        if (typeof record.meta != "object" || record.meta === null) {
          record.meta = {};
        }
        if (typeof record.meta.link != "object" || record.meta.link === null) {
          record.meta.link = {};
        }
        record.meta.link.noSource = CalendarService.instance.nowDate;
        await db.setItem("pictures", record._id, record);
        stats.updated++;
      }
      stats.total++;
    });
    return stats;
  }
}
export default PictureMissingSource;
