/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import CalendarService from "tasks/source/lib/CalendarService.js";
import { ObjectId } from "mongodb";

class PictureUpdatedSource {
  constructor() {
    this.name = "PictureUpdatedSource";
  }
  async start(db) {
    const stats = {
      total: 0,
      updated: 0,
    };
    const query = {
      "meta.link.model": {
        $exists: true,
      },
      "meta.link.modelId": {
        $exists: true,
      },
    };
    await db.query("pictures", query, async (record) => {
      if (!ObjectId.isValid(record.meta?.link?.modelId)) {
        return;
      }

      const source = await db.getItem(
        record.meta.link.model,
        record.meta.link.modelId
      );
      const exists = JSON.stringify(source).includes(record._id.toString());
      if (record._id.toString() == "64a1a45b905a4cd0aabff8bc") {
        console.log(exists, JSON.stringify(source), record._id);
      }
      if (!exists) {
        record.meta.link.noLinkInSource = CalendarService.instance.nowDate;
        await db.setItem("pictures", record._id, record);
        stats.updated++;
      }
      stats.total++;
    });
    return stats;
  }
}
export default PictureUpdatedSource;
