/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { Pushgateway, register, Gauge } from "prom-client";
import Db from "models/source/db.js";
import Broadcast from "hmq-js-client/source/broadcast.js";
import Config from "hmq-js-client/source/config.js";

import CampaignAnswerFeatureIdCheck from "./validators/CampaignAnswerFeatureIdCheck.js";
import CampaignAnswerStatusCheck from "./validators/CampaignAnswerStatusCheck.js";
import ContributorsClean from "./cleaners/ContributorsClean.js";
import DiskSizeStats from "./stats/DiskSize.js";
import IconFilesClean from "./cleaners/IconFilesClean.js";
import IconsClean from "./cleaners/IconsClean.js";
import IconsVisibilityCheck from "./validators/IconsVisibilityCheck.js";
import IconsParentImageCheck from "./validators/IconsParentImageCheck.js";
import IssueFilesClean from "./cleaners/IssueFilesClean.js";
import MapFilesClean from "./cleaners/MapFilesClean.js";
import PictureFilesClean from "./cleaners/PictureFilesClean.js";
import PictureFilesConversion from "./cleaners/PictureFilesConversion.js";
import PicturesHashCheck from "./validators/PicturesHashCheck.js";
import SoundFilesClean from "./cleaners/SoundFilesClean.js";
import TranslationFilesClean from "./cleaners/TranslationFilesClean.js";
import { SearchMetaCheckList } from "./validators/SearchMetaCheck.js";
import AddPictureLinks from "./links/pictures.js";
import PictureMissingSource from "./links/missingSources.js";
import PictureUpdatedSource from "./links/updatedSources.js";
import PicturesSizeCheck from "./validators/PicturesSizeCheck.js";

import path from "path";
import yargs from "yargs";

import { fileURLToPath } from "url";

function resolve(name) {
  return process.env[name];
}

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const argv = yargs(process.argv.slice(2)).default(
  "configuration",
  function randomValue() {
    return path.join(__dirname, `../config/configuration.json`);
  }
).argv;

const appConfig = new Config(argv["configuration"], resolve).toJson();
const dbConfig = new Config(
  appConfig["general"]["db"] + "/mongo.json",
  resolve
).toJson();

const db = new Db(dbConfig, true);
const broadcast = new Broadcast(appConfig["http"], appConfig["httpMq"]);

const jobs = [
  IconsParentImageCheck,
  AddPictureLinks,
  PictureMissingSource,
  PictureUpdatedSource,
  PicturesHashCheck,
  CampaignAnswerFeatureIdCheck,
  CampaignAnswerStatusCheck,
  MapFilesClean,
  IconFilesClean,
  PictureFilesClean,
  // MapSpritesCheck,
  SoundFilesClean,
  IssueFilesClean,
  TranslationFilesClean,
  IconsClean,
  ContributorsClean,
  IconsVisibilityCheck,
  ...SearchMetaCheckList,
  PictureFilesConversion,
  DiskSizeStats,
  PicturesSizeCheck,
];

const promPg = new Pushgateway(
  appConfig?.general?.stats?.pushGatewayUrl,
  {
    timeout: 5000,
  },
  register
);

function pushMetrics() {
  if (!appConfig?.general?.stats) {
    console.log("stats are not set");
    return;
  }

  return promPg
    .push({
      jobName: "node-batch",
      groupings: { namespace: appConfig?.general?.stats?.namespace },
    })
    .then(({ resp, body }) => {
      console.log("Done pushing metrics.", body);
    })
    .catch((err) => {
      console.log("Failed pushing metrics.", err);
    });
}

const gauges = {};

function collectMetrics(job, stats) {
  if (typeof stats != "object") {
    return;
  }

  for (const key in stats) {
    if (isNaN(stats[key])) {
      continue;
    }

    if (!gauges[key]) {
      gauges[key] = new Gauge({
        name: key,
        help: key,
        labelNames: ["name"],
      });
    }

    console.log("push", key, job, stats[key]);
    gauges[key].set({ name: job }, stats[key]);
  }
}

db.connect(function (err) {
  if (err) {
    return console.error(err);
  }

  console.log("CONNECTED");

  const promises = jobs
    .map((job) => new job())
    .map((job) => {
      console.log(`Starting "${job.name}"...`);
      return job.start(db, broadcast).then((result) => {
        console.log(`"${job.name}" Done.`, result ?? {});

        collectMetrics(job.name, result);
        return { [job.name]: result };
      });
    });

  Promise.all(promises)
    .then((results) => {
      console.log(results);
      db.close();
      console.log("Done.");

      return pushMetrics();
    })
    .catch((err) => {
      db.close();
      console.log("Done with errors. ", err);

      return pushMetrics();
    });
});
