/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/

class IconsVisibilityCheck {
  constructor() {
    this.name = "IconsVisibilityCheck";
    this.cache = {};
  }
  async getSet(db, id) {
    if (!this.cache[id]) {
      this.cache[id] = await db.getItem("iconsets", id);
    }
    return this.cache[id];
  }
  async start(db) {
    const stats = {
      total: 0,
      updated: 0,
    };
    await db.get("icons", async (icon) => {
      stats.total++;
      const iconSet = await this.getSet(db, icon.iconSet);
      if (
        JSON.stringify(iconSet?.visibility) == JSON.stringify(icon.visibility)
      ) {
        return;
      }
      icon.visibility = iconSet?.visibility;
      await db.setItem("icons", icon._id, icon);
      stats.updated++;
    });
    return stats;
  }
}
export default IconsVisibilityCheck;
