/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/

export default class IconsParentImageCheck {
  constructor() {
    this.name = "IconsParentImageCheck";
    this.cache = {};
  }

  async getIconId(db, iconId) {
    if (!this.cache[iconId]) {
      this.cache[iconId] = await db.getItem("icons", iconId);
    }

    if(this.cache[iconId].parent && this.cache[iconId].image.useParent) {
      return this.getIconId(db, this.cache[iconId].parent);
    }

    return this.cache[iconId].image.value ?? "";
  }

  async start(db) {
    const stats = {
      total: 0,
      updated: 0,
    };

    await db.query("icons", { "parent": { $ne: "" }, "image.useParent": true }, async (icon) => {
      stats.total++;

      if(!icon?.image?.useParent) {
        return;
      }

      try {
        const newValue = await this.getIconId(db, icon.parent);

        if(newValue == icon.image.value) {
          return;
        }

        icon.image.value = newValue;
      } catch(err) {
        console.error(err);
        icon.image.value = "";
        icon.image.useParent = false;
      }

      await db.setItem("icons", icon._id, icon);
      stats.updated++;
    });
    return stats;
  }
}
