/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { ObjectId } from "mongodb";
import CalendarService from "tasks/source/lib/CalendarService.js";

export class SearchMetaCheck {
  collectionName = "";
  relatedModel = "";

  async checkDuplicatedData(db, broadcast, stats, duplicated) {
    const yearsAgo = CalendarService.instance.nowDate;
    yearsAgo.setMonth(yearsAgo.getMonth() - 150);

    await db.query(
      "searchMeta",
      {
        relatedModel: this.relatedModel,
        $or: [
          { lastDuplicateCheckOn: { $exists: false } },
          { lastDuplicateCheckOn: { $lt: yearsAgo } },
        ],
      },
      async (meta) => {
        stats.totalDuplicationCheck++;

        if (stats.totalDuplicationCheck % 100 == 0) {
          console.log("totalDuplicationCheck", stats.totalDuplicationCheck);
        }

        const relatedId = meta.relatedId;
        const count = await db.count("searchMeta", {
          relatedModel: this.relatedModel,
          relatedId,
        });

        if (count <= 1 || duplicated.includes(relatedId)) {
          meta.lastDuplicateCheckOn = CalendarService.instance.nowDate;

          await db.setItem("searchMeta", meta._id, meta);
          return;
        }

        stats.duplicated++;

        await db.deleteWithQuery("searchMeta", {
          relatedId,
          relatedModel: this.relatedModel,
        });

        duplicated.push(relatedId);
        await broadcast.push("searchMeta.update", {
          id: relatedId,
          uid: relatedId,
          model: this.relatedModel,
        });
      }
    );
  }

  async start(db, broadcast) {
    const stats = {
      totalRecords: 0,
      totalMeta: 0,
      updated: 0,
      deleted: 0,
      totalDuplicationCheck: 0,
      duplicated: 0,
    };
    const duplicated = [];

    const sixMonthsAgo = CalendarService.instance.nowDate;
    sixMonthsAgo.setMonth(sixMonthsAgo.getMonth() - 6);

    await this.checkDuplicatedData(db, broadcast, stats, duplicated);

    /// remove search data for deleted features
    await db.query(
      "searchMeta",
      { relatedModel: this.relatedModel },
      async (meta) => {
        const id = meta.relatedId;
        stats.totalMeta++;

        const query = {
          _id: new ObjectId(`${id}`),
        };

        const exists = await db.count(this.collectionName, query);

        if (exists) {
          return;
        }

        stats.deleted++;

        await db.deleteWithQuery("searchMeta", {
          _id: meta._id,
          relatedModel: this.relatedModel,
        });
      }
    );

    /// regenerate search data for items without a lastChangedOn or categories fields
    await db.query(
      "searchMeta",
      {
        relatedModel: this.relatedModel,
        $or: [
          { lastChangeOn: { $exists: false } },
          { categories: { $exists: false } },
        ],
      },
      async (meta) => {
        const id = meta.relatedId;
        stats.totalMeta++;

        stats.updated++;
        await broadcast.push("searchMeta.update", {
          id,
          uid: id,
          model: this.relatedModel,
        });
      }
    );

    let promises = [];
    /// regenerate search data for recent changes
    await db.query(
      this.collectionName,
      { "info.lastChangeOn": { $gt: sixMonthsAgo } },
      async (record) => {
        promises.push(
          new Promise(async (resolve, reject) => {
            try {
              const id = record._id.toString();
              stats.totalRecords++;

              if (stats.totalRecords % 1000 == 0) {
                console.log(this.relatedModel, "=", stats);
              }

              const exists = await db.exists("searchMeta", {
                relatedId: id,
                relatedModel: this.relatedModel,
              });

              if (exists || duplicated.includes(id)) {
                return resolve();
              }

              stats.updated++;
              await broadcast.push("searchMeta.update", {
                id,
                uid: id,
                model: this.relatedModel,
              });
              resolve();
            } catch (err) {
              reject(err);
            }
          })
        );

        if (promises.length > 1000) {
          await Promise.all(promises);
          promises = [];
        }
      }
    );

    await Promise.all(promises);

    return stats;
  }
}

export class MapSearchMetaCheck extends SearchMetaCheck {
  constructor() {
    super();

    this.name = "MapSearchMetaCheck";

    this.collectionName = "maps";
    this.relatedModel = "Map";
  }
}

export class FeatureSearchMetaCheck extends SearchMetaCheck {
  constructor() {
    super();

    this.name = "FeatureSearchMetaCheck";

    this.collectionName = "sites";
    this.relatedModel = "Feature";
  }
}

export class TeamSearchMetaCheck extends SearchMetaCheck {
  constructor() {
    super();

    this.name = "TeamSearchMetaCheck";

    this.collectionName = "teams";
    this.relatedModel = "Team";
  }
}

export class CampaignSearchMetaCheck extends SearchMetaCheck {
  constructor() {
    super();

    this.name = "CampaignSearchMetaCheck";

    this.collectionName = "campaigns";
    this.relatedModel = "Campaign";
  }
}

export class IconSearchMetaCheck extends SearchMetaCheck {
  constructor() {
    super();

    this.name = "IconSearchMetaCheck";

    this.collectionName = "icons";
    this.relatedModel = "Icon";
  }
}

export class IconSetSearchMetaCheck extends SearchMetaCheck {
  constructor() {
    super();

    this.name = "IconSearchMetaCheck";

    this.collectionName = "iconsets";
    this.relatedModel = "IconSet";
  }
}

export class ArticleSearchMetaCheck extends SearchMetaCheck {
  constructor() {
    super();

    this.name = "ArticleSearchMetaCheck";

    this.collectionName = "articles";
    this.relatedModel = "Article";
  }
}

export class EventSearchMetaCheck extends SearchMetaCheck {
  constructor() {
    super();

    this.name = "EventSearchMetaCheck";

    this.collectionName = "events";
    this.relatedModel = "Event";
  }
}

export const SearchMetaCheckList = [
  FeatureSearchMetaCheck,
  MapSearchMetaCheck,
  TeamSearchMetaCheck,
  CampaignSearchMetaCheck,
  IconSearchMetaCheck,
  IconSetSearchMetaCheck,
  ArticleSearchMetaCheck,
  EventSearchMetaCheck,
];
