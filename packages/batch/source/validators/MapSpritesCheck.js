/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { ObjectId } from "mongodb";

/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
class MapSpritesCheck {
  constructor() {
    this.name = "MapSpritesCheck";
  }

  async checkMapSpritesSize(db, id, size) {
    const spriteCount = await db.count("pictures", {
      "meta.link.type": "sprite",
      "meta.link.modelId": `${id}`,
      "meta.link.size": parseInt(size),
    });

    return spriteCount > 0;
  }

  async checkMapUnlinkedSprites(db, id) {
    const spriteSize1 = await this.checkMapSpritesSize(db, id, 1);
    const spriteSize2 = await this.checkMapSpritesSize(db, id, 2);

    return spriteSize1 && spriteSize2;
  }

  async checkMapLinkedSprites(db, map) {
    if (!map.sprites?.small || !map.sprites?.large) {
      return false;
    }

    const smallPicture = await db.exists("pictures", {
      _id: new ObjectId(map.sprites?.small),
    });
    const largePicture = await db.exists("pictures", {
      _id: new ObjectId(map.sprites?.large),
    });

    return smallPicture && largePicture;
  }

  async start(db, broadcast) {
    const stats = {
      total: 2,
      generated: 0,
    };

    const hasSpritesAll = await this.checkMapUnlinkedSprites(db, "_");
    if (!hasSpritesAll) {
      await broadcast.push("Map.spriteAll", {
        id: "_",
        uid: "_",
      });
      stats.generated++;
    }

    const hasSpritesDefault = await this.checkMapUnlinkedSprites(db, "default");
    if (!hasSpritesDefault) {
      await broadcast.push("Map.spriteDefault", {
        id: "default",
        uid: "default",
      });
      stats.generated++;
    }

    await db.query(
      "maps",
      {
        "iconSets.useCustomList": true,
      },
      async (map) => {
        stats.total++;
        const hasSprites = await this.checkMapLinkedSprites(db, map);

        if (!hasSprites) {
          map.sprites = {};

          await db.setItem("maps", map._id, map);

          await broadcast.push("Map.sprite", {
            id: `${map._id}`,
            uid: `${map._id}`,
          });
          stats.generated++;
        }
      }
    );
    return stats;
  }
}

export default MapSpritesCheck;
