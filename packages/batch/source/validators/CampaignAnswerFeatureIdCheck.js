/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/

export default class CampaignAnswerFeatureIdCheck {
  get name() {
    return "CampaignAnswerFeatureIdCheck";
  }

  async checkFeature(db, sourceId) {
    return await db.getItemWithQuery("sites", {
      "source.type": "Campaign",
      "source.remoteId": sourceId,
    });
  }

  async start(db) {
    const stats = {
      total: 0,
      updated: 0,
    };

    await db.get("campaignAnswers", async (answer) => {
      stats.total++;

      if (answer.featureId) {
        return;
      }

      const feature = await this.checkFeature(db, answer._id.toString());

      if (feature) {
        answer.featureId = feature._id.toString();
      } else {
        answer.featureId = "@unknown";
      }

      stats.updated++;
      await db.setItem("campaignAnswers", answer._id, answer);
    });

    return stats;
  }
}
