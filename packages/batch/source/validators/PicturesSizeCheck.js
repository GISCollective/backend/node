/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/

import { ObjectId } from "mongodb";
import { getPictureInfo } from "models/source/files.js";
import fs from "fs";

class PicturesSizeCheck {
  constructor() {
    this.name = "PicturesSizeCheck";
  }

  async start(db) {
    const stats = {
      total: 0,
      updated: 0,
      errors: 0,
    };

    const query = {
      $or: [
        { "meta.width": { $exists: false } },
        { "meta.height": { $exists: false } },
      ],
    };

    const storage = db.getFileStorage("pictures");

    await db.query("pictures", query, async (picture) => {
      stats.total++;

      const files = await storage.find({ _id: new ObjectId(picture.picture) });

      for await (let file of files) {
        try {
          if (
            !file.metadata.mime ||
            file.metadata.mime?.indexOf?.("image") !== 0
          ) {
            continue;
          }

          const size = await getPictureInfo(storage, file, stats);

          if (!picture.meta) {
            picture.meta = {};
          }

          picture.meta.width = size.width;
          picture.meta.height = size.height;
          picture.meta.format = size.format;
          picture.meta.mime = size["mime type"];
          picture.meta.properties = size.properties;

          await db.setItem("pictures", picture._id, picture);
          stats.updated++;

          if (fs.existsSync(size.path)) {
            fs.unlinkSync(size.path);
          }
        } catch (err) {
          console.error(err);
        }
      }
    });

    return stats;
  }
}

export default PicturesSizeCheck;
