/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/

export default class PicturesHashCheck {
  constructor() {
    this.name = "PicturesHashCheck";
  }

  async start(db, broadcast) {
    const stats = {
      total: 0,
      updated: 0,
    };

    await db.get("pictures", async (picture) => {
      const id = picture._id;
      stats.total++;

      if (typeof picture.hash == "string") {
        return;
      }

      stats.updated++;
      await broadcast.push("Picture.Hash", {
        id,
        uid: id,
      });
    });

    return stats;
  }
}
