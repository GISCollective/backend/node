/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/

const FeatureVisibility = {
  [-1]: "pending",
  0: "unpublished",
  1: "published",
};

const CampaignAnswerStatus = {
  processing: 0,
  pending: 1,
  published: 2,
  unpublished: 3,
  rejected: 4,
};

export default class CampaignAnswerStatusCheck {
  get name() {
    return "CampaignAnswerStatusCheck";
  }

  updateAnswer(answer, feature) {
    answer.status = CampaignAnswerStatus["processing"];

    if (!feature && answer.featureId) {
      answer.status = CampaignAnswerStatus["rejected"];
    }

    let statusString = FeatureVisibility[feature?.visibility];

    if (CampaignAnswerStatus[statusString]) {
      answer.status = CampaignAnswerStatus[statusString];
    }
  }

  async start(db) {
    const stats = {
      total: 0,
      updated: 0,
    };

    await db.get("campaignAnswers", async (answer) => {
      stats.total++;
      let shouldUpdate = false;
      let feature;

      if (answer.featureId && answer.featureId != "@unknown") {
        feature = await db.getItem("sites", answer.featureId);
      }

      const before = answer.status;

      this.updateAnswer(answer, feature);

      if (before != answer.status) {
        stats.updated++;
        await db.setItem("campaignAnswers", answer._id, answer);
      }
    });

    return stats;
  }
}
