/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/

import CalendarService from "tasks/source/lib/CalendarService.js";

class DiskSizeStats {
  constructor() {
    this.name = "DiskSizeStats";
  }

  async start(db) {
    const collectionNames = await db.db.listCollections().toArray();

    const meta = {
      type: "stats.size",
      model: "",
      itemId: CalendarService.instance.now,
      changeIndex: 0,
      data: {},
    };

    for (let item of collectionNames) {
      const stats = await db.db
        .collection(item.name)
        .aggregate([{ $collStats: { storageStats: {} } }])
        .toArray();

      meta.data[item.name.replace(".", "-")] = {
        count: stats[0]?.storageStats.count,
        diskSize: stats[0]?.storageStats.storageSize ?? 0,
        dataSize: stats[0]?.storageStats.size ?? 0,
      };
    }

    await db.addItem("meta", meta);

    return {};
  }
}

export default DiskSizeStats;
