/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import GenericFilesClean from "./GenericFilesClean.js";

class TranslationFilesClean {
  static options = {
    model: "translation",
    recordKey: "file",
    bucket: "translation",
  };

  constructor() {
    this.name = "TranslationFilesClean";
  }

  start(db) {
    const filesClean = new GenericFilesClean(db, TranslationFilesClean.options);

    return filesClean.start();
  }

  static hasDbRecord(db, fileId) {
    const filesClean = new GenericFilesClean(db, TranslationFilesClean.options);

    return filesClean.hasDbRecord(fileId);
  }

  static hasChunks(db, fileId) {
    const filesClean = new GenericFilesClean(db, TranslationFilesClean.options);

    return filesClean.hasChunks(fileId);
  }
}

export default TranslationFilesClean;
