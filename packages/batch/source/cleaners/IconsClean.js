/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/

import json from "models/source/json.js";

class IconsClean {
  constructor() {
    this.name = "IconsClean";
  }
  async start(db, broadcast) {
    const stats = {
      total: 0,
      deleted: 0,
    };
    await db.get("icons", async (icon) => {
      stats.total++;
      const iconSetExists = await db.exists("iconsets", {
        _id: icon.iconSet,
      });
      if (!iconSetExists) {
        const before = json.flatten(JSON.parse(JSON.stringify(icon)));
        before._id = icon._id.toString();
        await db.deleteWithQuery("icons", {
          _id: icon._id,
        });
        broadcast.push("Icon.change", {
          modelName: "Icon",
          type: "delete",
          before,
        });
        stats.deleted++;
      }
    });
    return stats;
  }
}
export default IconsClean;
