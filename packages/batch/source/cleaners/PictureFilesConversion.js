/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import fs from "fs";
import imagemin from "imagemin";
import imageminWebp from "imagemin-webp";
import im from "imagemagick";
import { getPictureInfo, downloadFile } from "models/source/files.js";

async function uploadFile(source, file, localPath) {
  const filename = `${file.filename}.webp`;

  const destinationStream = source.openUploadStream(filename, {
    metadata: { ...file.metadata, mime: "image/webp" },
    contentType: "image/webp",
  });

  const stream = fs.createReadStream(localPath);

  return await new Promise((resolve, reject) => {
    stream.on("error", reject);

    destinationStream.on("error", reject);
    destinationStream.on("finish", () => {
      resolve(destinationStream.gridFSFile);
    });

    stream.on("open", function () {
      stream.pipe(destinationStream);
    });
  });
}

function optimizeColors(path) {
  return new Promise((resolve, reject) => {
    try {
      im.convert([path, "-colorspace", "sRGB", path], function (err, stdout) {
        if (err) return reject(err);
        resolve();
      });
    } catch (err) {
      reject(err);
    }
  });
}

function optimizeOrientation(path) {
  return new Promise((resolve, reject) => {
    try {
      im.convert([path, "-auto-orient", path], function (err, stdout) {
        if (err) return reject(err);
        resolve();
      });
    } catch (err) {
      reject(err);
    }
  });
}

async function disableOptimization(db, file, picture, reason) {
  file.metadata.disableOptimization = true;
  file.metadata.disableOptimizationReason = reason;

  await db.setItem("pictures.files", file._id, file);

  if (!picture) {
    return;
  }

  if (!picture.meta) {
    picture.meta = {};
  }

  picture.meta.disableOptimization = true;
  picture.meta.disableOptimizationReason = reason;

  await db.setItem("pictures", picture._id, picture);
}

async function updatePictureMeta(storage, path, picture) {
  const size = await getPictureInfo(storage, path, {});

  if (!picture.meta) {
    picture.meta = {};
  }

  picture.meta.width = size.width;
  picture.meta.height = size.height;
  picture.meta.format = size.format;
  picture.meta.mime = size["mime type"];
  picture.meta.properties = size.properties;
}

export default class PictureFilesConversion {
  constructor() {
    this.name = "PictureFilesConversion";
  }

  async convertFile(db, storage, file, picture, stats) {
    let path = "";
    let destination = "";
    stats.total++;

    if (!picture) {
      stats.pictureNotFound++;
      await disableOptimization(db, file, picture, `no parent picture model`);

      return;
    }

    try {
      path = await downloadFile(storage, file);

      if (!fs.existsSync(path)) {
        return;
      }

      if (file?.metadata?.mime == "image/jpeg") {
        await optimizeColors(path);
      }

      await optimizeOrientation(path);

      if (picture) {
        await updatePictureMeta(storage, path, picture);
      }

      destination = path.substring(0, path.length - 3) + "webp";

      await imagemin([path], {
        destination: `pictures`,
        plugins: [
          imageminWebp({
            quality: 80,
          }),
        ],
      });

      const result = await uploadFile(storage, file, destination);
      picture.picture = result._id.toString();

      await db.setItem("pictures", picture._id, picture);
      stats.updated++;
    } catch (err) {
      stats.errors++;
      console.error("Can't convert file: ", JSON.stringify(file));
      console.error(err);

      disableOptimization(db, file, picture, `${err}`);
    }

    if (fs.existsSync(path)) {
      fs.unlinkSync(path);
    }

    if (fs.existsSync(destination)) {
      fs.unlinkSync(destination);
    }
  }

  async start(db) {
    if (!fs.existsSync("pictures")) {
      fs.mkdirSync("pictures");
    }
    const stats = {
      total: 0,
      updated: 0,
      errors: 0,
      pictureNotFound: 0,
    };

    const storage = db.getFileStorage("pictures");
    const files = storage.find({
      "metadata.disableOptimization": { $ne: true },
      "metadata.parentId": {
        $exists: false,
      },
      "metadata.mime": {
        $in: ["image/jpeg", "image/png"],
      },
    });

    for await (let file of files) {
      const picture = await db.getItemWithQuery("pictures", {
        picture: `${file._id}`,
      });

      if (picture?.meta?.disableOptimization) {
        await disableOptimization(db, file, picture, "");
        continue;
      }

      await this.convertFile(db, storage, file, picture, stats);
    }

    return stats;
  }
}
