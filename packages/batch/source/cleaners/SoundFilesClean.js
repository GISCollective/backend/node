/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import GenericFilesClean from "./GenericFilesClean.js";

class SoundFilesClean {
  static options = {
    model: "sounds",
    recordKey: "sound",
    bucket: "sounds",
  };

  constructor() {
    this.name = "SoundFilesClean";
  }

  start(db) {
    const filesClean = new GenericFilesClean(db, SoundFilesClean.options);

    return filesClean.start();
  }

  static hasDbRecord(db, fileId) {
    const filesClean = new GenericFilesClean(db, SoundFilesClean.options);

    return filesClean.hasDbRecord(fileId);
  }
}

export default SoundFilesClean;
