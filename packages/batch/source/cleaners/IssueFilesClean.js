/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import GenericFilesClean from "./GenericFilesClean.js";

export default class IssueFilesClean {
  static options = {
    model: "issue",
    recordKey: "file",
    bucket: "issues",
  };
  constructor() {
    this.name = "IssueFilesClean";
  }
  start(db) {
    const filesClean = new GenericFilesClean(db, IssueFilesClean.options);
    return filesClean.start();
  }
  static hasDbRecord(db, fileId) {
    const filesClean = new GenericFilesClean(db, IssueFilesClean.options);
    return filesClean.hasDbRecord(fileId);
  }
}
