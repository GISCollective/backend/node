/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { ObjectId } from "mongodb";

/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
export default class ContributorsClean {
  constructor() {
    this.name = "ContributorsClean";
  }

  async isValidUserId(db, id) {
    if (!ObjectId.isValid(id)) {
      return false;
    }

    return await db.exists("users", { _id: new ObjectId(id) });
  }

  async fixContributors(db, list) {
    const result = [];

    if (!list) {
      return result;
    }

    for (const id of list) {
      if (await this.isValidUserId(db, id)) {
        result.push(id);
      }
    }

    return result;
  }

  async start(db) {
    const stats = {
      total: 0,
      updated: 0,
    };

    await db.get(`sites`, async (feature) => {
      stats.total++;

      const updatedList = await this.fixContributors(db, feature.contributors);

      if (updatedList.length != feature.contributors?.length) {
        feature.contributors = updatedList;
        await db.setItem("sites", feature._id, feature);

        stats.updated++;
      }
    });

    return stats;
  }

  static hasDbRecord(db, fileId) {
    const filesClean = new GenericFilesClean(db, SoundFilesClean.options);

    return filesClean.hasDbRecord(fileId);
  }
}
