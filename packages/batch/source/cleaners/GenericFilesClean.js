/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { ObjectId } from "mongodb";

class GenericFilesClean {
  constructor(db, options) {
    this.db = db;
    this.options = options;
  }

  async hasDbRecord(fileId) {
    if (typeof fileId == "object") {
      return this.hasDbRecord(fileId.toString?.() ?? "");
    }

    if (typeof fileId != "string") {
      return false;
    }

    let record = await this.db.getItemWithQuery(this.options.model, {
      $or: [
        { [this.options.recordKey]: fileId },
        { [this.options.recordKey]: new ObjectId(fileId) },
      ],
    });

    return Boolean(record);
  }

  async hasValidFile(pictureId) {
    if (typeof pictureId == "object") {
      return this.hasValidFile(pictureId.toString?.() ?? "");
    }

    if (typeof pictureId != "string") {
      return false;
    }

    let record = await this.db.getItem(
      this.options.model,
      new ObjectId(pictureId)
    );

    if (!record) {
      return false;
    }

    try {
      return await this.db.exists(`${this.options.bucket}.files`, {
        _id: new ObjectId(record.picture),
      });
    } catch (err) {
      console.error(record.picture, err);

      return false;
    }
  }

  async hasChunks(files_id) {
    return await this.db.exists(`${this.options.bucket}.chunks`, {
      $or: [{ files_id: files_id }, { files_id: new ObjectId(files_id) }],
    });
  }

  async checkRecords() {
    const stats = {
      total: 0,
      deleted: 0,
      orphaned: 0,
    };

    await this.db.get(this.options.model, async (record) => {
      stats.total++;
      let isLinked = await this.hasValidFile(record._id.toString());

      if (isLinked) {
        return;
      }

      stats.orphaned++;

      try {
        console.log(
          `Found a record to delete isLinked=${isLinked} record._id=${record._id}`
        );
        await this.db.deleteWithQuery(this.options.model, { _id: record._id });
        stats.deleted++;
      } catch (err) {
        console.error(err);
      }
    });

    return stats;
  }

  async start() {
    let total = 0;
    let orphaned = 0;
    let invalid = 0;
    let deleted = 0;

    await this.db.get(`${this.options.bucket}.files`, async (file) => {
      total++;
      let isLinked = await this.hasDbRecord(file._id.toString());

      if (!isLinked) {
        isLinked = await this.hasDbRecord(file?.metadata?.parentId);
      }

      const isValid = await this.hasChunks(file._id.toString());

      if (isLinked && isValid) {
        return;
      }

      if (!isLinked) {
        orphaned++;
      }

      if (!isValid) {
        invalid++;
      }

      try {
        console.log(
          `Found a file to delete isLinked=${isLinked} isValid=${isValid} file._id=${file._id}`
        );
        await this.db.deleteFile(this.options.bucket, file._id);
        deleted++;
      } catch (err) {
        console.error(err);
      }
    });

    return {
      total,
      orphaned,
      invalid,
      deleted,
    };
  }
}

export default GenericFilesClean;
