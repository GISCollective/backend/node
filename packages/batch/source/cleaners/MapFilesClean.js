/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import GenericFilesClean from "./GenericFilesClean.js";

class MapFilesClean {
  static options = {
    model: "mapFile",
    recordKey: "file",
    bucket: "map",
  };
  constructor() {
    this.name = "MapFilesClean";
  }
  start(db) {
    const filesClean = new GenericFilesClean(db, MapFilesClean.options);
    return filesClean.start();
  }
  static hasDbRecord(db, fileId) {
    const filesClean = new GenericFilesClean(db, MapFilesClean.options);
    return filesClean.hasDbRecord(fileId);
  }
}

export default MapFilesClean;
