/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/

import GenericFilesClean from "./GenericFilesClean.js";

class PictureFilesClean {
  static options = {
    model: "pictures",
    recordKey: "picture",
    bucket: "pictures",
  };

  constructor() {
    this.name = "PictureFilesClean";
  }

  async start(db) {
    const filesClean = new GenericFilesClean(db, PictureFilesClean.options);
    const stats = await filesClean.start();

    const recordStats = await filesClean.checkRecords();

    for (const key in recordStats) {
      stats[`${key}Record`] = recordStats[key];
    }

    return stats;
  }

  static hasDbRecord(db, fileId) {
    const filesClean = new GenericFilesClean(db, PictureFilesClean.options);
    return filesClean.hasDbRecord(fileId);
  }

  static hasValidFile(db, pictureId) {
    const filesClean = new GenericFilesClean(db, PictureFilesClean.options);
    return filesClean.hasValidFile(pictureId);
  }
}

export default PictureFilesClean;
