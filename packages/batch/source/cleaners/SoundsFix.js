/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
class SoundsFix {
  constructor() {
    this.name = "SoundsFix";
  }
  async start(db, broadcast) {
    const storage = db.getFileStorage("pictures");
    return db.get("sounds", async (sound) => {
      const files = storage.find({
        filename: sound.sound,
      });
      for await (const file of files) {
        try {
          const newFile = await db.copyFile("pictures", file, "sounds");
          console.log("newFile", newFile);
          sound.sound = newFile._id;
          await db.setItem("sounds", sound._id, sound);
        } catch (err) {
          console.error(err);
        }
      }
    });
  }
}
export default SoundsFix;
