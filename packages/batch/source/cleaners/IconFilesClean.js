/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import GenericFilesClean from "./GenericFilesClean.js";

export default class IconFilesClean {
  static options = {
    model: "icons",
    recordKey: "image.value",
    bucket: "icons",
  };

  constructor() {
    this.name = "IconFilesClean";
  }

  start(db) {
    const filesClean = new GenericFilesClean(db, IconFilesClean.options);
    return filesClean.start();
  }

  static hasDbRecord(db, fileId) {
    const filesClean = new GenericFilesClean(db, IconFilesClean.options);
    return filesClean.hasDbRecord(fileId);
  }
}
