/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import ArticleTrigger from "../../source/triggers/ArticleTrigger.js";
import { expect } from "chai";
import Db from "models/source/db.js";
import { MongoMemoryServer } from "mongodb-memory-server";

import { should } from "chai";
should();

describe("ArticleTrigger", function () {
  let db;
  let mongoServer;
  let articleTrigger;
  let broadcast;
  let channel;
  let message;

  beforeEach(async function () {
    this.timeout(60000);

    mongoServer = await MongoMemoryServer.create();
    await mongoServer.getUri();

    db = new Db({
      url: `mongodb://${mongoServer._instanceInfo.ip}:${mongoServer._instanceInfo.port}/tests`,
      configuration: { collection: "test" },
    });

    await db.connectWithPromise();

    articleTrigger = new ArticleTrigger();

    channel = null;
    message = null;

    broadcast = {
      push(c, m) {
        channel = c;
        message = m;
      },
    };
  });

  this.afterEach(async function () {
    await db.close();
    await mongoServer.stop();
  });

  it("should have the `ArticleTrigger` name", function () {
    ArticleTrigger.name.should.equal("ArticleTrigger");
  });

  it("publishes an article when it is in the past and private", async function () {
    let article = await db.addItem("articles", {
      type: "newsletter-article",
      releaseDate: "2020-02-02T00:00:00Z",
      visibility: {
        isPublic: false
      },
    });
    const id = article.insertedId;

    const stats = await articleTrigger.start(db, broadcast);

    expect(stats).to.deep.equal({
      total: 1,
      published: 1,
    });

    article = await db.getItem("articles", id);

    expect(article.visibility).to.deep.equal({
      "isPublic": true
    });

  });

  it("does not publish an article when it is in the future and private", async function () {
    let article = await db.addItem("articles", {
      type: "newsletter-article",
      releaseDate: "2050-02-02T00:00:00Z",
      visibility: {
        isPublic: false
      },
    });
    const id = article.insertedId;

    const stats = await articleTrigger.start(db, broadcast);

    expect(stats).to.deep.equal({
      total: 1,
      published: 0,
    });

    article = await db.getItem("articles", id);

    expect(article.visibility).to.deep.equal({
      "isPublic": false
    });

  });
});
