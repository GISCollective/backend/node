/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import NewsletterTrigger from "../../source/triggers/NewsletterTrigger.js";
import { expect } from "chai";
import Db from "models/source/db.js";
import { MongoMemoryServer } from "mongodb-memory-server";

import { should } from "chai";
should();

describe("NewsletterTrigger", function () {
  let db;
  let mongoServer;
  let newsletterTrigger;
  let broadcast;
  let channel;
  let message;

  beforeEach(async function () {
    this.timeout(60000);

    mongoServer = await MongoMemoryServer.create();
    await mongoServer.getUri();

    db = new Db({
      url: `mongodb://${mongoServer._instanceInfo.ip}:${mongoServer._instanceInfo.port}/tests`,
      configuration: { collection: "test" },
    });

    await db.connectWithPromise();

    newsletterTrigger = new NewsletterTrigger();

    channel = null;
    message = null;

    broadcast = {
      push(c, m) {
        channel = c;
        message = m;
      },
    };
  });

  this.afterEach(async function () {
    await db.close();
    await mongoServer.stop();
  });

  it("should have the `NewsletterTrigger` name", function () {
    newsletterTrigger.name.should.equal("NewsletterTrigger");
  });

  it("triggers a task for a pending newsletter article in the past", async function () {
    const article = await db.addItem("articles", {
      type: "newsletter-article",
      releaseDate: "2020-02-02T00:00:00Z",
      status: "pending",
    });
    const id = article.insertedId.toString();

    const stats = await newsletterTrigger.start(db, broadcast);

    expect(channel).to.equal("article.publish");
    expect(message).to.deep.equal({ id, uid: id });
    expect(stats).to.deep.equal({
      total: 1,
      published: 1,
    });
  });

  it("does not trigger a task for a pending article in the past that is not a newsletter", async function () {
    const article = await db.addItem("articles", {
      type: "other",
      releaseDate: "2020-02-02T00:00:00Z",
      status: "pending",
    });

    const stats = await newsletterTrigger.start(db, broadcast);

    expect(channel).not.to.exist;
    expect(message).not.to.exist;
    expect(stats).to.deep.equal({
      total: 0,
      published: 0,
    });
  });

  it("does not trigger a task for a draft article", async function () {
    const article = await db.addItem("articles", {
      type: "newsletter-article",
      releaseDate: "2020-02-02T00:00:00Z",
      status: "draft",
    });

    const stats = await newsletterTrigger.start(db, broadcast);

    expect(channel).not.to.exist;
    expect(message).not.to.exist;
    expect(stats).to.deep.equal({
      total: 0,
      published: 0,
    });
  });

  it("does not trigger a task for an article in the past", async function () {
    const article = await db.addItem("articles", {
      type: "newsletter-article",
      releaseDate: "2050-02-02T00:00:00Z",
      status: "pending",
    });

    const stats = await newsletterTrigger.start(db, broadcast);

    expect(channel).not.to.exist;
    expect(message).not.to.exist;
    expect(stats).to.deep.equal({
      total: 1,
      published: 0,
    });
  });
});
