/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import EventNextOccurrenceCheck from "../../source/validators/EventNextOccurrenceCheck.js";
import Db from "models/source/db.js";
import { MongoMemoryServer } from "mongodb-memory-server";
import CalendarService from "tasks/source/lib/CalendarService.js";

import { should } from "chai";
should();

describe("EventNextOccurrenceCheck", function () {
  let eventNextOccurrenceCheck;
  let privateMapId;
  let now;
  let mongoServer;
  let db;
  let broadcast;

  beforeEach(async function () {
    this.timeout(60000);

    broadcast = {
      messages: [],
      push(channel, message) {
        this.messages.push({ channel, message });
      },
    };

    mongoServer = await MongoMemoryServer.create();
    const url = mongoServer.getUri();

    db = new Db({
      url,
      configuration: { collection: "test" },
    });

    await db.connectWithPromise();

    eventNextOccurrenceCheck = new EventNextOccurrenceCheck();

    CalendarService.instance = {
      nowDate: new Date("2022-01-01T13:30:10Z"),
    };
  });

  afterEach(async function () {
    await db.close();
    await mongoServer.stop();
  });

  it("should have the `EventNextOccurrenceCheck` name", function () {
    eventNextOccurrenceCheck.name.should.equal("EventNextOccurrenceCheck");
  });

  it("updates the event when the next occurrence is in the past", async function () {
    const result = await db.addItem("events", {
      nextOccurrence: {
        end: new Date("2018-01-01T13:30:10Z"),
        begin: new Date("2018-01-01T12:30:10Z"),
      },
    });

    const id = result.insertedId;

    const results = await eventNextOccurrenceCheck.start(db, broadcast);

    expect(results).to.deep.equal({
      updated: 1,
    });

    expect(broadcast.messages).to.deep.equal([
      {
        channel: "Event.generateOccurrences",
        message: {
          id: id.toString(),
          uid: id.toString(),
        },
      },
    ]);
  });

  it("updates the event when the next occurrence is null and the interval end is in the future", async function () {
    const result = await db.addItem("events", {
      nextOccurrence: null,
      entries: [
        {
          repetition: "monthly",
          end: new Date("2018-01-01T13:30:10Z"),
          begin: new Date("2018-01-01T12:30:10Z"),
          intervalEnd: new Date("2024-02-01T13:30:10Z"),
        },
      ],
    });

    const id = result.insertedId;

    const results = await eventNextOccurrenceCheck.start(db, broadcast);

    expect(results).to.deep.equal({
      updated: 1,
    });

    expect(broadcast.messages).to.deep.equal([
      {
        channel: "Event.generateOccurrences",
        message: {
          id: id.toString(),
          uid: id.toString(),
        },
      },
    ]);
  });

  it("does not update the event when the next occurrence is in the future", async function () {
    await db.addItem("events", {
      nextOccurrence: {
        end: new Date("2024-01-01T13:30:10Z"),
        begin: new Date("2024-01-01T12:30:10Z"),
      },
    });

    const results = await eventNextOccurrenceCheck.start(db, broadcast);

    expect(results).to.deep.equal({
      updated: 0,
    });

    expect(broadcast.messages).to.deep.equal([]);
  });
});
