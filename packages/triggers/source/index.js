/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { Pushgateway, register, Gauge } from 'prom-client';
import Db from "models/source/db.js";
import Broadcast from "hmq-js-client/source/broadcast.js";
import Config from "hmq-js-client/source/config.js";

import NewsletterTrigger from "./triggers/NewsletterTrigger.js";
import EventNextOccurrenceCheck from "./validators/EventNextOccurrenceCheck.js"
import ArticleTrigger from "./triggers/ArticleTrigger.js";

import path from "path";
import yargs from "yargs";

import { fileURLToPath } from "url";

function resolve(name) {
  return process.env[name];
}

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const argv = yargs(process.argv.slice(2)).default(
  "configuration",
  function randomValue() {
    return path.join(__dirname, `../config/configuration.json`);
  }
).argv;

const appConfigFile = argv["configuration"];
console.log({ appConfigFile });

const appConfig = new Config(appConfigFile, resolve).toJson();

const dbConfigFile = appConfig["general"]["db"] + "/mongo.json";
console.log({ dbConfigFile });
const dbConfig = new Config(dbConfigFile, resolve).toJson();

const db = new Db(dbConfig, true);
const broadcast = new Broadcast(appConfig["http"], appConfig["httpMq"]);

const jobs = [
  EventNextOccurrenceCheck,
  NewsletterTrigger,
  ArticleTrigger
];


const promPg = new Pushgateway(
  appConfig?.general?.stats?.pushGatewayUrl,
  {
    timeout: 5000
  },
  register
)

function pushMetrics() {
  if(!appConfig?.general?.stats) {
    console.log("stats are not set");
    return;
  }

  return promPg.push({ jobName: 'node-batch', groupings: { namespace: appConfig?.general?.stats?.namespace } })
    .then(({resp, body}) => {
      console.log("Done pushing metrics.", body);
    })
    .catch(err => {
      console.log("Failed pushing metrics.", err);
    });
}

const gauges = {};

function collectMetrics(job, stats) {
  if(typeof stats != "object") {
    return;
  }

  for(const key in stats) {
    if(isNaN(stats[key])) {
      continue;
    }

    if(!gauges[key]) {
      gauges[key] = new Gauge({
        name: key,
        help: key,
        labelNames: ['name']
      });
    }

    console.log("push", key, job, stats[key])
    gauges[key].set({name: job}, stats[key]);
  }
}

db.connect(function (err) {
  if (err) {
    return console.error(err);
  }

  const promises = jobs
    .map((job) => new job())
    .map((job) => {
      console.log(`Starting "${job.name}"...`);
      return job.start(db, broadcast).then((result) => {
        console.log(`"${job.name}" Done.`, result ?? {});

        collectMetrics(job.name, result);
        return { [job.name]: result };
      });
    });

  Promise.all(promises)
    .then((results) => {
      console.log(results);
      db.close();
      console.log("Done.");

      return pushMetrics();
    })
    .catch((err) => {
      db.close();
      console.log("Done with errors. ", err);

      return pushMetrics();
    });
});
