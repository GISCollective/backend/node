/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
export default class NewsletterTrigger {
  constructor() {
    this.name = "NewsletterTrigger";
  }

  async start(db, broadcast) {
    const articles = await db.query("articles", {
      type: "newsletter-article",
      status: "pending",
    });

    let total = 0;
    let published = 0;

    for (const article of articles) {
      total++;
      let diff =
        new Date().getTime() - new Date(article["releaseDate"]).getTime();

      if (diff > 0) {
        published++;
        const id = article._id.toString();
        await broadcast.push(`article.publish`, {
          id,
          uid: id,
        });
      }
    }

    return {
      total,
      published,
    };
  }
}

