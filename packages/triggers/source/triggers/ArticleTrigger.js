/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
export default class ArticleTrigger {
  constructor() {
    this.name = "ArticleTrigger";
  }

  async start(db, broadcast) {
    const articles = await db.query("articles", {
      "visibility": { isPublic: false }
    });

    let total = 0;
    let published = 0;

    for (const article of articles) {
      total++;

      let diff = new Date().getTime() - new Date(article["releaseDate"]).getTime();

      if (diff > 0) {
        article["visibility"]["isPublic"] = true;
        await db.setItem("articles", article._id, article);
        published++;
      }
    }

    return {
      total,
      published,
    };
  }
}

