/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import CalendarService from "tasks/source/lib/CalendarService.js";

export default class EventNextOccurrenceCheck {
  constructor() {
    this.name = "EventNextOccurrenceCheck";
    this.cache = {};
  }

  async start(db, broadcast) {
    const stats = {
      updated: 0,
    };

    const now = CalendarService.instance.nowDate;

    const query = {
      $or: [
        {
          "nextOccurrence.end": { $lt: now },
        },
        {
          nextOccurrence: null,
          entries: {
            $elemMatch: {
              intervalEnd: {
                $gt: now,
              },
            },
          },
        },
      ],
    };

    await db.query("events", query, async (event) => {
      const id = event._id.toString();
      broadcast.push("Event.generateOccurrences", { id, uid: id });
      stats.updated++;
    });

    return stats;
  }
}
