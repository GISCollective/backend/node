/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { MetaQuery } from "../../lib/MetaQuery.js";
import {
  parseStream,
  fileOperation,
  transformRecord,
  normalize,
} from "./lib/base.js";
import { MapFileResolvers } from "../dataBindings/lib/MapFileResolvers.js";
import { map as _map } from "async";
import { ObjectId } from "mongodb";
import { mimeFromFileObject } from "../../lib/mime.js";

export async function mapFilePreview(db, message, appConfig) {
  const { mapFile, fileObject, map } = await fileOperation(db, message.id);
  const fileStream = await db.getFile("map", mapFile.file);

  const records = [];

  const destinationMap = mapFile.options.destinationMap
    ? new ObjectId(mapFile.options.destinationMap)
    : undefined;
  const resolvers = new MapFileResolvers(
    db,
    map.visibility.team,
    destinationMap
  );

  await parseStream(
    mimeFromFileObject(fileObject),
    fileStream,
    (record) => {
      records.push(
        transformRecord(normalize(record), mapFile.options, resolvers)
      );
    },
    (key, value) => {
      if (!mapFile.options[key]) {
        mapFile.options[key] = value;
      }
    },
    { preview: 10 }
  );

  const metaQuery = new MetaQuery("MapFile", "mapFile.preview", db);
  const mapFileMeta = await metaQuery.get(message.id);

  const preview = await _map(records, async (a) => a);

  mapFileMeta.data = {
    preview,
  };

  await db.setItem("meta", mapFileMeta._id, mapFileMeta);

  return "";
}
