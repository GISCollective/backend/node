/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import BatchJobService from "../../lib/BatchJobService.js";
import { fileOperation, normalize, parseStream } from "./lib/base.js";
import CalendarService from "../../lib/CalendarService.js";
import { lookup } from "mime-types";
import { ExternalDataAnalyze } from "./lib/ExternalDataAnalyze.js";
import { MetaQuery } from "../../lib/MetaQuery.js";

export async function mapFileAnalyze(db, message) {
  const { mapFile, fileObject, map } = await fileOperation(db, message.id);

  const externalDataAnalyze = new ExternalDataAnalyze();
  const fileStream = await db.getFile("map", mapFile.file);

  let fileMime = fileObject?.metadata?.mime;

  if (!fileObject?.metadata?.mime) {
    fileMime = lookup(fileObject?.filename);
  }

  await parseStream(
    fileMime,
    fileStream,
    (data, errors) => {
      externalDataAnalyze.addData(normalize(data));
      externalDataAnalyze.addErrors(errors);
    },
    (key, value) => {
      mapFile.options[key] = value;
    }
  );

  const metaQuery = new MetaQuery("MapFile", "mapFile.import", db);
  await metaQuery.set(message.id, {
    total: externalDataAnalyze.total,
    errors: externalDataAnalyze.errors,
  });

  mapFile.options.analyzedAt = CalendarService.instance.now;
  mapFile.options.fields = externalDataAnalyze.fields;
  mapFile.options.destinationMap = map._id;

  await db.setItem("mapFile", mapFile._id, mapFile);

  await BatchJobService.instance.upsertByName(
    `mapFile.import ${mapFile._id}`,
    map?.visibility?.team
  );

  return "";
}
