/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/

export function isSpecialField(key, value) {
  if (!value || typeof value !== "object") {
    return false;
  }

  if (key === "geometry") {
    return true;
  }

  if (key === "description" && !!value.blocks) {
    return true;
  }

  return false;
}

export class ExternalDataAnalyze {
  fieldIndexes = {};
  fields = [];
  errors = [];
  total = 0;

  getField(key) {
    const existingField = this.fields.find((a) => a.key == key);

    if (existingField) {
      return existingField;
    }

    const entry = {
      key: key,
      destination: ExternalDataAnalyze.guessTransformedKey(key),
      preview: [],
    };

    return entry;
  }

  addField(field) {
    const index = this.fieldIndexes[field.key];

    if (isNaN(index)) {
      this.fieldIndexes[field.key] = this.fields.length;
      return this.fields.push(field);
    }

    this.fields[index] = field;
  }

  stringify(value) {
    if (value === undefined || value === null) {
      return "";
    }

    let result = `${value}`;

    if (typeof value == "object") {
      result = JSON.stringify(value);
    }

    return result;
  }

  addArrayObjectValue(parentKey, data) {
    data.forEach((item) => {
      this.addObjectValue(`${parentKey}[]`, item);
    });
  }

  addObjectValue(parentKey, data) {
    if (data === null || data === undefined) {
      return;
    }

    Object.keys(data).forEach((key) => {
      if (parentKey == "attributes" && typeof data[key] === "object") {
        return this.addObjectValue(`${parentKey}.${key}`, data[key]);
      }

      const entry = this.getField(`${parentKey}.${key}`);
      const value = this.stringify(data[key]);

      if (
        value !== "" &&
        !entry.preview.find((a) => a == value) &&
        entry.preview.length <= 5
      ) {
        entry.preview.push(value);
      }

      this.addField(entry);
    });
  }

  addData(data) {
    this.total++;

    Object.keys(data).forEach((key) => {
      if (
        Array.isArray(data[key]) &&
        data[key].length > 0 &&
        typeof data[key][0] == "object" &&
        key.indexOf(".") == -1 &&
        key != "geometry"
      ) {
        return this.addArrayObjectValue(key, data[key]);
      }

      if (
        !Array.isArray(data[key]) &&
        typeof data[key] == "object" &&
        key.indexOf(".") == -1 &&
        !isSpecialField(key, data[key])
      ) {
        return this.addObjectValue(key, data[key]);
      }

      const value = this.stringify(data[key]);
      const entry = this.getField(key);

      if (
        value !== "" &&
        value.length < 500 &&
        !entry.preview.find((a) => a == value) &&
        entry.preview.length <= 5
      ) {
        entry.preview.push(value);
      }

      this.addField(entry);
    });
  }

  addErrors(list) {
    list.forEach((error) => {
      this.errors.push({
        code: `${error.type}.${error.code}`,
        col: -1,
        line: error.row,
      });
    });
  }

  static guessTransformedKey(key) {
    const lowerKey = key.toLowerCase();

    switch (lowerKey) {
      case "_id":
      case "id":
        return "_id";

      case "info.createdon":
        return "info.createdOn";

      case "info.lastchangeon":
        return "info.lastChangeOn";

      case "info.author":
        return "info.author";

      case "author":
      case "info.originalauthor":
        return "info.originalAuthor";

      case "pictures":
      case "pictures.url":
        return "pictures";

      case "audio":
      case "sounds.url":
      case "sounds":
        return "sounds";

      case "maps":
      case "maps.name":
        return "maps";

      case "lon":
      case "position.lon":
        return "position.lon";

      case "lat":
      case "position.lat":
        return "position.lat";

      case "geometry":
      case "position.wkt":
      case "position.geojson":
        return "position";

      case "pictures.name":
        return "pictures";

      case "icons.name":
        return "icons";

      case "maps":
      case "description":
      case "position":
      case "icons":
      case "visibility":
      case "contributors":
      case "author":
      case "name":
        return lowerKey;
    }

    const pieces = key
      .split(".")
      .filter((a) => a.trim() != "" && a.trim() != "attributes");

    if (pieces.length == 0) {
      return "";
    }

    if (pieces.length == 1) {
      return `attributes.other.${pieces[0]}`;
    }

    return `attributes.${pieces[0]}.${pieces.slice(1).join("_")}`;
  }
}
