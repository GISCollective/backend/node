/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/

import { enforce } from "../../../lib/error.js";
import _ from "lodash";
import wkx from "wkx";
import dayjs from "dayjs";
import Papa from "papaparse";
import { validateFeature } from "models/source/validators/feature.js";
import Pick from "stream-json/filters/Pick.js";
import StreamChain from "stream-chain";
const { chain } = StreamChain;
import StreamJson from "stream-json";
import JsonlParser from "stream-json/jsonl/Parser.js";
import StreamValues from "stream-json/streamers/StreamValues.js";
const { streamValues } = StreamValues;
import async from "async";

import reproject from "reproject";
const { toWgs84 } = reproject;

import CalendarService from "../../../lib/CalendarService.js";
import Handlebars from "handlebars";

export function parseVisibility(value) {
  const dictionary = {
    1: 1,
    "-1": -1,
    approved: 1,
    pending: -1,
  };

  const key = `${value}`.toLowerCase().trim();

  return dictionary[key] ?? 0;
}

export async function fileOperation(db, id) {
  const mapFile = await db.getItem("mapFile", id);
  enforce(mapFile, `There is no map file with id: ${id}`);

  const map = await db.getItem("maps", mapFile.map);
  enforce(map, `The map is associated to a missing map. Map file id: ${id}`);
  enforce(
    map?.visibility?.team,
    `The map is associated to an invalid team. Map file id: ${id}`
  );

  if (!mapFile.options) {
    mapFile.options = {};
  }

  const fileObject = await db.getFileObject("map", mapFile.file);

  return { mapFile, fileObject, map };
}

export function parseValue(value) {
  if (typeof value === "object") {
    return value;
  }

  try {
    return JSON.parse(value);
  } catch (err) {
    return `${value}`;
  }
}

export function isValidAttributeDestination(key, value) {
  if (["name", "description"].indexOf(key) != -1) {
    return true;
  }

  const pieces = key.split(".");
  let maxLen = 3;

  if (typeof value == "object") {
    maxLen = 2;
  }

  if (pieces.length == 0) {
    return `empty string keys can not be imported`;
  }

  if (pieces[0] != "attributes") {
    return `${key} must begin with "attributes."`;
  }

  if (pieces.length != maxLen) {
    return `${key} must have only ${maxLen} "."`;
  }

  if (Array.isArray(value)) {
    const nonObjectItems = value.filter(
      (a) => typeof a !== "object" || Array.isArray(a)
    );

    if (nonObjectItems.length > 0) {
      return `${key} must contain only objects`;
    }
  }

  return true;
}

export function parseValueList(value) {
  let valueList = value;

  if (typeof valueList == "string") {
    valueList = value.split(";");
  }

  if (!valueList.length) {
    return;
  }

  return valueList.filter((a) => a != "");
}

export async function getAuthor(value, resolvers) {
  const valueList = parseValueList(value);

  if (!valueList) {
    return;
  }

  const authors = await resolvers.queryUsers(valueList);
  if (authors.length == 0) {
    return;
  }

  return authors[0];
}

const newFeatureString = JSON.stringify({
  maps: [],
  icons: [],
  name: "",
  description: "",
  position: {
    type: "Point",
    coordinates: [0, 0],
  },
  info: {
    changeIndex: 0,
  },
});

class RecordBuilder {
  constructor(newObject, resolvers) {
    this.newObject = newObject;
    this.errors = [];
    this.resolvers = resolvers;
  }

  applyCRS(crs) {
    try {
      this.newObject.position = toWgs84(this.newObject.position, crs);
    } catch (err) {
      this.errors.push({
        key: "position",
        destination: "position",
        error: `Cannot transform position to Wgs84. Error: ${err}`,
      });
    }
  }

  async addIcons(value, isPrimary) {
    let valueList = parseValueList(value);

    if (!valueList) {
      return;
    }

    if (isPrimary) {
      valueList = valueList.slice(0, 1);
    }

    const icons = this.newObject.icons ?? [];
    const result = await this.resolvers.queryIcons(valueList);

    this.newObject.icons = isPrimary
      ? [...result, ...icons]
      : [...icons, ...result];

    const allIcons = this.newObject.icons;
    this.newObject.icons = [];

    for (const icon of allIcons) {
      if (
        this.newObject.icons.map((a) => a.toString()).includes(icon.toString())
      ) {
        continue;
      }

      this.newObject.icons.push(icon);
    }
  }

  async addValue(field, value) {
    if ([null, undefined, "null", "undefined"].indexOf(value) != -1) {
      return;
    }

    const destination = field.destination.trim();

    if (destination == "") {
      return;
    }

    let author;
    let date;
    let valueList;

    try {
      switch (destination) {
        case "_id":
          break;
        case "position.wkt":
        case "position":
          if (typeof value === "string") {
            this.newObject.position = wkx.Geometry.parse(value).toGeoJSON();
          }

          if (typeof value === "object" && value.type) {
            this.newObject.position = value;
          }

          break;
        case "visibility":
          const intVisibility = parseVisibility(value);
          this.newObject.visibility = intVisibility;
          break;
        case "position.lon":
          enforce(
            !isNaN(parseFloat(value)),
            "The `lon` value is not a number."
          );
          this.newObject.position.coordinates[0] = parseFloat(value);
          break;
        case "position.lat":
          enforce(
            !isNaN(parseFloat(value)),
            "The `lat` value is not a number."
          );
          this.newObject.position.coordinates[1] = parseFloat(value);
          break;
        case "maps":
          if (this.newObject.maps?.length) {
            break;
          }

          valueList = parseValueList(value);

          if (valueList) {
            this.newObject.maps = await this.resolvers.queryMaps(valueList);
          }

          break;
        case "pictures":
          valueList = parseValueList(value);

          if (valueList) {
            this.newObject.pictures = await this.resolvers.handlePictures(
              valueList
            );
          }

          break;
        case "sounds":
          valueList = parseValueList(value);

          if (valueList) {
            this.newObject.sounds = await this.resolvers.handleSounds(
              valueList
            );
          }

          break;
        case "contributors":
          valueList = parseValueList(value);

          if (valueList) {
            this.newObject.contributors = await this.resolvers.queryUsers(
              valueList
            );
          }

          break;

        case "info.createdOn":
          date = dayjs(value);

          if (date.isValid()) {
            if (!this.newObject.info) {
              this.newObject.info = {};
            }

            this.newObject.info.createdOn = date.toISOString();
          } else {
            this.errors.push({
              key: field.key,
              destination: field.destination,
              error: "Invalid date time format",
            });
          }

          break;

        case "info.lastChangeOn":
          date = dayjs(value);

          if (date.isValid()) {
            if (!this.newObject.info) {
              this.newObject.info = {};
            }

            this.newObject.info.lastChangeOn = date.toISOString();
          } else {
            this.errors.push({
              key: field.key,
              destination: field.destination,
              error: "Invalid date time format",
            });
          }

          break;

        case "info.originalAuthor":
        case "author":
          author = await getAuthor(value, this.resolvers);

          if (!author) {
            break;
          }

          if (!this.newObject.info) {
            this.newObject.info = {};
          }

          this.newObject.info.originalAuthor = author;

          break;

        case "info.author":
          author = await getAuthor(value, this.resolvers);

          if (!author) {
            break;
          }

          if (!this.newObject.info) {
            this.newObject.info = {};
          }

          this.newObject.info.author = author;

          break;

        case "icons":
          await this.addIcons(value, false);
          break;

        case "primary icon":
          await this.addIcons(value, true);
          break;

        default:
          let isValid = isValidAttributeDestination(destination, value);

          if (isValid === true) {
            _.set(this.newObject, destination, value);
          }

          if (isValid === false) {
            this.errors.push({
              key: field.key,
              destination: field.destination,
              error: "Field has an invalid destination.",
            });
          }

          if (typeof isValid === "string") {
            this.errors.push({
              key: field.key,
              destination: field.destination,
              error: isValid,
            });
          }
      }
    } catch (err) {
      this.errors.push({
        key: field.key,
        destination: field.destination,
        error: err.toString ? err.toString() : JSON.parse(JSON.stringify(err)),
      });
      console.error(err);
    }
  }

  async addListValues(field, values) {
    const destination = field.destination.trim();

    if (destination == "") {
      return;
    }

    const pieces = destination.split(".");
    const key = pieces[pieces.length - 1];
    const listPath = pieces.slice(0, -1).join(".");

    let index = 0;
    for (const value of values) {
      let isValid = isValidAttributeDestination(destination, value);

      if (!isValid || value == "undefined" || value == "null") {
        continue;
      }

      let list = _.get(this.newObject) ?? [];

      if (!Array.isArray(list)) {
        throw new Error(
          `The destination can not be an object: '${destination}'.`
        );
      }

      _.set(this.newObject, `${listPath}[${index}]${key}`, value);

      index++;
    }
  }

  get result() {
    if (this.newObject.position) {
      this.newObject.position = simplifyMultiGeometry(this.newObject.position);
    }

    const r = { result: this.newObject, errors: this.errors };

    const validationError = validateFeature(this.newObject);

    if (validationError) {
      r.validationError = validationError;
    }

    return r;
  }
}

export function simplifyMultiGeometry(geojson) {
  if (!geojson || typeof geojson != "object") {
    return geojson;
  }

  if (geojson.type == "MultiPolygon" && geojson.coordinates.length == 1) {
    return {
      type: "Polygon",
      coordinates: geojson.coordinates[0],
    };
  }

  if (geojson.type == "MultiLineString" && geojson.coordinates.length == 1) {
    return {
      type: "LineString",
      coordinates: geojson.coordinates[0],
    };
  }

  if (geojson.type == "MultiPoint" && geojson.coordinates.length == 1) {
    return {
      type: "Point",
      coordinates: geojson.coordinates[0],
    };
  }

  return geojson;
}

export function transformModelInfo(record, options) {
  if (!record.info) {
    record.info = {};
  }

  let changeIndex = isNaN(record.info.changeIndex)
    ? 0
    : parseInt(record.info.changeIndex) + 1;

  if (options.userId && !record.info.originalAuthor) {
    record.info.originalAuthor = options.userId;
  }

  if (options.userId && !record.info.author) {
    record.info.author = options.userId;
  }

  if (!record.info.createdOn) {
    record.info.createdOn = new Date(CalendarService.instance.now);
  }

  record.info.lastChangeOn = new Date(CalendarService.instance.now);
  record.info.changeIndex = changeIndex;
}

export function transformDescription(record, options, originalData) {
  if (!options.descriptionTemplate) {
    return;
  }

  const template = Handlebars.compile(options.descriptionTemplate.trim());
  record.description = template(originalData);
}

export async function transformRecord(record, options, resolvers) {
  let newObject;

  if (options.updateBy == "_id") {
    const id = options?.fields?.find((a) => a.destination == "_id")?.key;
    newObject = await resolvers.featureById(_.get(record, id));
  }

  if (options.updateBy == "name") {
    const name = options?.fields?.find((a) => a.destination == "name").key;
    newObject = await resolvers.featureByName(_.get(record, name));
  }

  if (options.updateBy == "remoteId" && record.remoteId) {
    newObject = await resolvers.featureByRemoteId(record.remoteId);
  }

  if (!newObject) {
    newObject = JSON.parse(newFeatureString);
  }

  transformModelInfo(newObject, options);

  try {
    transformDescription(newObject, options, record);
  } catch (err) {
    console.error(err);
  }

  if (options.source) {
    newObject.source = {
      type: options.source.type,
      modelId: options.source.modelId,
      syncAt: new Date(CalendarService.instance.now),
    };
  }

  if (record.remoteId && newObject.source) {
    newObject.source.remoteId = record.remoteId;
  }

  if (options.destinationMap) {
    newObject.maps = await resolvers.queryMaps([`${options.destinationMap}`]);
  }

  if (resolvers && !resolvers.iconSets && newObject?.maps?.[0]) {
    resolvers.iconSets =
      (await resolvers?.queryMapIconSets?.(newObject.maps[0])) ?? [];
  }

  const fields = options.fields ?? [];
  const recordBuilder = new RecordBuilder(newObject, resolvers);

  if (options.extraIcons?.length > 0) {
    await recordBuilder.addValue({ destination: "icons" }, options.extraIcons);
  }

  for (const field of fields) {
    let isListValue = field.key.indexOf("[]") > -1;

    if (isListValue) {
      const keyPieces = field.key.split("[].");
      const values = _.get(record, keyPieces[0]).map((a) =>
        parseValue(_.get(a, keyPieces[1]))
      );

      await recordBuilder.addListValues(field, values);
    }

    if (!isListValue) {
      const value = parseValue(_.get(record, field.key));
      await recordBuilder.addValue(field, value);
    }
  }

  if (recordBuilder.result.result.visibility === undefined) {
    await recordBuilder.addValue({ destination: "visibility" }, 1);
  }

  if (options.crs && options.crs != "") {
    recordBuilder.applyCRS(options.crs);
  }

  if (resolvers?.getComputedVisibility) {
    recordBuilder.result.result.computedVisibility =
      await resolvers.getComputedVisibility(recordBuilder.result.result);
  }

  return recordBuilder.result;
}

export function normalize(data) {
  if (data.type == "Feature") {
    return { ...data.properties, geometry: data.geometry };
  }

  return data;
}

export function parseCsvStream(fileStream, itemHandler, metaHandler, options) {
  return new Promise((resolve) => {
    let done = resolve;
    let index = 0;

    Papa.parse(fileStream, {
      header: true,
      worker: true,
      preview: options.preview,
      step: function (results) {
        index++;
        itemHandler(results.data, results.errors);

        if (index == options.preview) {
          done?.();
          done = null;
        }
      },
      complete: function () {
        done?.();
      },
      error: function (err) {
        console.log("error", err);
        done?.();
      },
    });
  });
}

export function parseJsonStream(fileStream, itemHandler, metaHandler, options) {
  let index = -1;

  return new Promise((resolve, reject) => {
    const pipeline = chain([
      fileStream,
      StreamJson.parser(),
      Pick.pick({
        filter: (path, data) => {
          if (
            path.length == 3 &&
            path[0] == "crs" &&
            path[1] == "properties" &&
            path[2] == "name" &&
            data?.name == "stringValue"
          ) {
            metaHandler("crs", `${data.value}`);
          }

          return path.length == 2 && path[0] == "features";
        },
      }),
      streamValues(),
    ]);

    pipeline.on("data", (data) => {
      if (options?.preview && JSON.stringify(data).length > 1024 * 1024 * 5) {
        return;
      }

      index++;

      if (options?.preview && index >= options?.preview) {
        pipeline.destroy();
        return resolve();
      }

      itemHandler(data.value, []);
    });

    pipeline.on("error", reject);
    pipeline.on("end", resolve);
  });
}

export function parseJsonLinesStream(
  fileStream,
  itemHandler,
  metaHandler,
  options
) {
  const jsonlParser = new JsonlParser();

  return new Promise((resolve, reject) => {
    const pipeline = chain([fileStream, jsonlParser]);

    pipeline.on("data", (data) => {
      itemHandler(data.value, []);
    });
    pipeline.on("error", reject);
    pipeline.on("end", resolve);
  });
}

export async function parseStream(
  mime,
  fileStream,
  itemHandler,
  metaHandler,
  options = { preview: 0 }
) {
  if (mime == "text/csv") {
    await parseCsvStream(fileStream, itemHandler, metaHandler, options);
    return;
  }

  if (
    mime == "text/json" ||
    mime == "application/json" ||
    mime == "application/geo+json"
  ) {
    await parseJsonStream(fileStream, itemHandler, metaHandler, options);
    return;
  }

  if (mime == "text/ndjson" || mime == "application/ndjson") {
    await parseJsonLinesStream(fileStream, itemHandler, metaHandler, options);
    return;
  }
}
