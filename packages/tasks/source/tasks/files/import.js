/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import {
  parseStream,
  fileOperation,
  transformRecord,
  normalize,
} from "./lib/base.js";
import { enforce } from "../../lib/error.js";
import async from "async";
import BatchJobService from "../../lib/BatchJobService.js";
import { MetaQuery } from "../../lib/MetaQuery.js";
import CalendarService from "../../lib/CalendarService.js";
import { MapFileResolvers } from "../dataBindings/lib/MapFileResolvers.js";
import { ObjectId } from "mongodb";
import dayjs from "dayjs";
import { downloadPicture } from "../dataBindings/lib/download.js";
import { mimeFromFileObject } from "../../lib/mime.js";

export async function mapFileImportStream(
  db,
  fileObject,
  readStream,
  mapFile,
  teamId,
  resolvers
) {
  const metaQuery = new MetaQuery("MapFile", "mapFile.import", db);
  const mapFileMeta = await metaQuery.get(mapFile._id);
  enforce(mapFileMeta.data.total, "The file is not yet analyzed.");

  const batchName = `mapFile.import ${mapFile._id}`;
  let batchJob = await BatchJobService.instance.upsertByName(batchName, teamId);
  const runningEntry = batchJob.runHistory.find((a) => a.status == "running");

  if (runningEntry) {
    const ping = dayjs(runningEntry.ping);
    const now = dayjs(CalendarService.instance.now);

    const diff = now.diff(ping, "minute");

    if (diff < 5) {
      return "";
    }

    runningEntry.status = "timeout";
    await BatchJobService.instance.updateRun(batchName, runningEntry);
  }

  let currentRun = await BatchJobService.instance.start(batchName);

  currentRun.total = mapFileMeta.data.total;
  await BatchJobService.instance.updateRun(batchName, currentRun);

  const queue = async.queue(async function (record) {
    currentRun.ping = CalendarService.instance.now;
    currentRun.sent++;

    try {
      const transformed = await transformRecord(
        normalize(record),
        mapFile.options,
        resolvers
      );

      if (!transformed.validationError) {
        const feature = transformed.result;

        if (feature._id) {
          await db.setItem("sites", feature._id, feature);
        } else {
          await db.addItem("sites", feature);
        }

        currentRun.processed++;
      } else {
        currentRun.errors++;
      }
    } catch (err) {
      await BatchJobService.instance.logError(batchJob._id, `${err}`);
      currentRun.errors++;
    }

    if (currentRun.sent % 100 == 0) {
      await BatchJobService.instance.log(
        batchJob._id,
        `Processed ${currentRun.sent} items...`
      );
      await BatchJobService.instance.updateRun(batchName, currentRun);
    }
  }, 1);

  let fileMime = mimeFromFileObject(fileObject);

  parseStream(
    fileMime,
    readStream,
    (record) => {
      queue.push(record);
    },
    (key, value) => {
      if (!mapFile.options[key]) {
        mapFile.options[key] = value;
      }
    }
  );

  await queue.drain();

  currentRun.status = "success";
  currentRun.total = currentRun.sent;

  await BatchJobService.instance.log(
    batchJob._id,
    `Processed ${currentRun.sent} items...`
  );
  await BatchJobService.instance.updateRun(batchName, currentRun);
}

export async function mapFileImport(db, message, appConfig) {
  const { mapFile, fileObject, map } = await fileOperation(db, message.id);
  const fileStream = await db.getFile("map", mapFile.file);

  const destinationMap = mapFile.options.destinationMap
    ? new ObjectId(mapFile.options.destinationMap)
    : undefined;
  const resolvers = new MapFileResolvers(
    db,
    map.visibility.team,
    destinationMap
  );

  resolvers.downloadPicture = downloadPicture(db, message.userId);

  const batchName = `mapFile.import ${mapFile._id}`;
  let batchJob = await BatchJobService.instance.upsertByName(
    batchName,
    map.visibility?.team
  );

  await BatchJobService.instance.log(batchJob._id, `Started import`);

  try {
    await mapFileImportStream(
      db,
      fileObject,
      fileStream,
      mapFile,
      map?.visibility?.team,
      resolvers
    );
  } catch (err) {
    await BatchJobService.instance.logError(batchJob._id, `${err}`);
    console.error(err);
    await BatchJobService.instance.stopWithError(batchName);
  }

  await BatchJobService.instance.log(batchJob._id, `DONE!`);
  return "";
}
