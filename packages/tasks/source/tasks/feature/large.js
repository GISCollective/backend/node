/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/

export async function featureLarge(db, message) {
  const feature = await db.getItem("sites", message.id);

  let pointCount = 0;

  if (feature.position.type == "MultiPolygon") {
    for (const polygon of feature.position.coordinates) {
      for (const ring of polygon) {
        pointCount += ring.length;
      }
    }
  }

  if (
    feature.position.type == "Polygon" ||
    feature.position.type == "MultiLineString"
  ) {
    for (const ring of feature.position.coordinates) {
      pointCount += ring.length;
    }
  }

  if (feature.position.type == "LineString") {
    pointCount += feature.position.coordinates.length;
  }

  feature.isLarge = pointCount > 1000;

  await db.setItem("sites", feature._id, feature);

  return "";
}
