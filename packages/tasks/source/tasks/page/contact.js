/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import sanitizeHtml from "sanitize-html";
import { sendEmail } from "../../lib/services/email.js";

export async function pageContact(db, message) {
  if (typeof message.message == "string") {
    const preparedMessage = `From: ${message.name} ${message.email}\n\n${message.message}`;

    message.text = sanitizeHtml(preparedMessage, {
      allowedTags: [],
      allowedAttributes: false,
    });
  } else {
    message.text = "";
  }

  message.to = [message.sendTo];
  message.replyTo = message.email;
  message.subject = `${message.subject} - Website contact form`;

  await sendEmail(db, message);

  return "";
}
