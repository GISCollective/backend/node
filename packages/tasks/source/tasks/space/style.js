/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Color from "color";
import { MetaQuery } from "../../lib/MetaQuery.js";
import { dasherize } from "../../lib/string.js";

export function getKeyColorMap(destination, name, colorName) {
  destination[`--bs-${name}`] = `--bs-${colorName}-500`;
  destination[`--bs-${name}-rgb`] = `--bs-${colorName}-500-rgb`;
  destination[`--bs-${name}-bg-subtle`] = `--bs-${colorName}-100`;
  destination[`--bs-${name}-border-subtle`] = `--bs-${colorName}-200`;
  destination[`--bs-${name}-text`] = `--bs-${colorName}-600`;
  destination[`--bs-${name}-shade-100`] = `--bs-${colorName}-shade-100`;
  destination[`--bs-${name}-shade-200`] = `--bs-${colorName}-shade-200`;
  destination[`--bs-${name}-shade-300`] = `--bs-${colorName}-shade-300`;
}

export function addColor(colorList, name, color) {
  colorList.push({ name: `--bs-${name}`, value: color.hex() });
  colorList.push({
    name: `--bs-${name}-rgb`,
    value: color
      .rgb()
      .array()
      .map((a) => Math.round(a))
      .join(", "),
  });
}

export function copyVariables(colorList, map) {
  for (const destination of Object.keys(map)) {
    const source = map[destination];
    const value = colorList.find((a) => a.name == source)?.value;

    if (value) {
      colorList.push({ name: destination, value });
    }
  }

  return colorList;
}

export function colorScale(color) {
  let minFactor = 0;
  let maxFactor = 0;
  let tmpColor = color;

  while (tmpColor.luminosity() < 1) {
    minFactor += 0.01;
    tmpColor = color.lighten(minFactor);
  }

  while (tmpColor.luminosity() > 0.01) {
    maxFactor += 0.01;
    tmpColor = color.darken(maxFactor);
  }

  const lightStep = minFactor / 5;
  const darkStep = maxFactor / 5;

  return [
    color.lighten(lightStep * 4),
    color.lighten(lightStep * 3),
    color.lighten(lightStep * 2),
    color.lighten(lightStep),
    color,
    color.darken(darkStep),
    color.darken(darkStep * 2),
    color.darken(darkStep * 3),
    color.darken(darkStep * 4),
  ];
}

export function generateColorVariables(result, name, value, isDark) {
  if (!value || typeof value != "string") {
    return result;
  }

  const color = Color(value);

  let scale = colorScale(color);

  if (isDark) {
    scale.reverse();
  }

  addColor(result, name, color);
  addColor(result, `${name}-100`, scale[0]);
  addColor(result, `${name}-200`, scale[1]);
  addColor(result, `${name}-300`, scale[2]);
  addColor(result, `${name}-400`, scale[3]);
  addColor(result, `${name}-500`, scale[4]);
  addColor(result, `${name}-600`, scale[5]);
  addColor(result, `${name}-700`, scale[6]);
  addColor(result, `${name}-800`, scale[7]);
  addColor(result, `${name}-900`, scale[8]);

  addColor(result, `${name}-shade-100`, scale[5].darken(0.35));
  addColor(result, `${name}-shade-200`, scale[5].darken(0.3));
  addColor(result, `${name}-shade-300`, scale[5].darken(0.25));

  return result;
}

const propMap = {
  ff: "font-family",
  fw: "font-weight",
  text: "font-size",
};

const fontWeightMap = {
  bold: "bold",
  normal: "normal",
  light: "lighter",
};

const fontSizeMap = {
  "05": "0.5",
  "06": "0.6",
  "07": "0.7",
  "08": "0.8",
  "09": "0.9",
  10: "1",
  11: "1.1",
  12: "1.2",
  13: "1.3",
  14: "1.4",
  15: "1.5",
  16: "1.7",
  17: "1.9",
  18: "2",
  19: "2.2",
  20: "2.4",
  21: "2.6",
  22: "2.8",
  23: "3",
  24: "3.2",
  25: "3.4",
  26: "3.6",
  27: "3.8",
  28: "4",
  29: "4.2",
  30: "4.4",
};

function toFontProp(cls) {
  const pieces = cls.replace(/[\W_]+/g, "-").split("-");
  const rest = pieces.slice(1);

  const prop = propMap[pieces[0]];
  let val = "";

  if (prop == "font-family") {
    val = `var(--ff-${rest.join("-")})`;
  }

  if (prop == "font-weight") {
    val = fontWeightMap[rest[0]];
  }

  if (prop == "font-size") {
    val = `${fontSizeMap[rest[1]]}rem`;
  }

  if (!prop || !val) {
    return;
  }

  return `${prop}: ${val}`;
}

const nameToSelector = {
  h1: "h1",
  h2: "h2",
  h3: "h3",
  h4: "h4",
  h5: "h5",
  h6: "h6",
  paragraph: "p, div, ul, ol",
};

export function generateFontStyles(fontStyles, prefixes = [""]) {
  let result = [];

  for (const name in fontStyles) {
    const props = fontStyles[name]
      .map(toFontProp)
      .filter((a) => a)
      .join("; ");

    for (const prefix of prefixes) {
      result.push(`${prefix}${nameToSelector[name]} { ${props} }`);
    }
  }

  return result.sort().join("\n");
}


export function generateCustomColorsStyles(colors, prefixes = [""]) {
  if(!colors) {
    return "";
  }

  let result = [];

  for (const color of colors) {
    const name = dasherize(color.name);

    for (const prefix of prefixes) {
      result.push(`${prefix} .bg-${name} { background-color: rgba(var(--bs-${name}-rgb), 1) !important; }`);
      result.push(`${prefix} .text-${name} { color: rgba(var(--bs-${name}-rgb), 1) !important; }`);
      result.push(`${prefix} .border-${name}, .border-color-${name} { border-color: rgba(var(--bs-${name}-rgb), 1) !important; }`);
      result.push(`${prefix} .btn-${name} { --bs-btn-color: var(--bs-${name}-text);
  --bs-btn-bg: var(--bs-${name});
  --bs-btn-border-color: var(--bs-${name});
  --bs-btn-hover-color: var(--bs-${name}-text);
  --bs-btn-hover-bg: var(--bs-${name}-var1);
  --bs-btn-hover-border-color: var(--bs-${name}-var1);
  --bs-btn-focus-shadow-rgb: 49, 132, 253;
  --bs-btn-active-color: var(--bs-${name}-text);
  --bs-btn-active-bg: var(--bs-${name}-var2);
  --bs-btn-active-border-color: var(--bs-${name}-var2);
  --bs-btn-active-shadow: inset 0 3px 5px rgba(0, 0, 0, 0.125);
  --bs-btn-disabled-color: var(--bs-${name}-text);
  --bs-btn-disabled-bg: rgba(var(--bs-${name}-rgb), 0.8);
  --bs-btn-disabled-border-color: rgba(var(--bs-${name}-rgb), 0.8);
}`);
    }
  }

  return result.sort().join("\n");
}

export function generateStyle(palette, isDark) {
  const copyMap = {};
  const result = [];

  getKeyColorMap(copyMap, "primary", "blue");
  getKeyColorMap(copyMap, "success", "green");
  getKeyColorMap(copyMap, "danger", "red");
  getKeyColorMap(copyMap, "warning", "yellow");
  getKeyColorMap(copyMap, "info", "cyan");

  for (const name of Object.keys(palette)) {
    if(name == "customColors") {
      continue;
    }

    generateColorVariables(result, name, palette[name], isDark);
  }

  const customColors = palette.customColors ?? [];

  for(let value of customColors) {
    const color = Color(isDark ? value.darkValue : value.lightValue);
    let textColor = color.lighten(0.8);
    let var1Color = color.lighten(0.2);
    let var2Color = color.lighten(0.4);

    if(color.isLight()) {
      textColor = color.darken(0.8);
      var1Color = color.darken(0.2);
      var2Color = color.darken(0.4);
    }

    addColor(result, dasherize(value.name), color);
    addColor(result, dasherize(value.name) + "-text", textColor);
    addColor(result, dasherize(value.name) + "-var1", var1Color);
    addColor(result, dasherize(value.name) + "-var2", var2Color);
  }

  copyVariables(result, copyMap);

  return result.map((a) => `${a.name}: ${a.value};`).join("\n");
}

export async function spaceStyle(db, message) {
  const id = message.id;

  const space = await db.getItem("space", id);
  const metaQuery = new MetaQuery("Space", "cssStyle", db);

  const cssVars =
    `[data-bs-theme=light-${id}] {\n${generateStyle(
      space.colorPalette
    )} }\n\n` +
    `[data-bs-theme=dark-${id}] {\n${generateStyle(space.colorPalette)} }\n\n` +
    `${generateFontStyles(space.fontStyles, [
      `[data-bs-theme=light-${id}] `,
      `[data-bs-theme=dark-${id}] `,
    ])}\n\n` +
    `${generateCustomColorsStyles(space.colorPalette?.customColors ?? [], [
      `[data-bs-theme=light-${id}] `,
      `[data-bs-theme=dark-${id}] `,
    ])}\n\n`;

  await metaQuery.set(space._id.toString(), { cssVars });

  return "";
}
