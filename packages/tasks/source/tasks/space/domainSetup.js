/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import k8s from "@kubernetes/client-node";
import { enforce } from "../../lib/error.js";
import { sendMessage } from "../notifications/message.js";

function createKubeMessage(host, id) {
  const secretName = `space-${id}`;

  return {
    apiVersion: "networking.k8s.io/v1",
    kind: "Ingress",
    metadata: {
      annotations: {
        "cert-manager.io/cluster-issuer": "letsencrypt-prod",
        "kubernetes.io/ingress.class": "nginx",
        "nginx.ingress.kubernetes.io/proxy-body-size": "20m",
        "nginx.ingress.kubernetes.io/rewrite-target": "/$1",
        "nginx.ingress.kubernetes.io/x-forwarded-prefix": "true",
        "giscollective/space-id": `${id}`,
      },
      name: host,
      labels: {
        createdBy: "node-client",
      },
    },
    spec: {
      rules: [
        {
          host,
          http: {
            paths: [
              {
                path: "/(sitemap\.txt|sitemap\.xml)",
                pathType: "ImplementationSpecific",
                backend: {
                  service: {
                    name: "ogm-api-service",
                    port: {
                      number: 80,
                    },
                  },
                },
              },
              {
                backend: {
                  service: {
                    name: "ogm-fonts-service",
                    port: {
                      number: 80,
                    },
                  },
                },
                path: "/api-v1/fonts/(.*)",
                pathType: "ImplementationSpecific",
              },
              {
                backend: {
                  service: {
                    name: "ogm-fonts-service",
                    port: {
                      number: 80,
                    },
                  },
                },
                path: "/api-v1/fonts.json",
                pathType: "ImplementationSpecific",
              },
              {
                backend: {
                  service: {
                    name: "ogm-api-service",
                    port: {
                      number: 80,
                    },
                  },
                },
                path: "/api-v1/(.*)",
                pathType: "ImplementationSpecific",
              },
              {
                backend: {
                  service: {
                    name: "ogm-frontend-service",
                    port: {
                      number: 80,
                    },
                  },
                },
                path: "/(.*)",
                pathType: "ImplementationSpecific",
              },
            ],
          },
        },
      ],
      tls: [
        {
          hosts: [host],
          secretName,
        },
      ],
    },
  };
}

async function getIngressById(k8sApi, namespace, spaceId) {
  let response;
  let matches = [];

  do {
    response = await k8sApi.listNamespacedIngress(
      namespace,
      false,
      false,
      response?.body?.metadata?._continue,
      undefined,
      undefined,
      1
    );

    for (const item of response.body.items) {
      if (item.metadata.annotations["giscollective/space-id"] == spaceId) {
        matches.push(item);
      }
    }
  } while (response?.body?.metadata?._continue);

  return matches;
}

async function handleSetupError(err, space, db, broadcast) {
  console.error(err);

  const text =
    err.body?.message ?? err.message ?? "There was an unknown error.";

  await sendMessage(db, broadcast, {
    uniqueKey: `domain-setup-fail-${new Date().toISOString()}`,
    type: "admin",
    to: {
      type: "",
      value: "",
    },
    subject: `kubernetes error on domain setup for ${space.domain}`,
    text,
    html: text,
    actions: {},
    useGenericTemplate: true,
  });

  space.domainStatus = "needs-admin-setup";
  await db.setItem("space", space._id, space);

  return "";
}

export async function domainSetup(db, message, broadcast, kc) {
  const namespace = process.env.KUBE_NAMESPACE ?? "ogm";

  const space = await db.getItem("space", message.id);
  space.domainStatus = "setting-up";
  await db.setItem("space", space._id, space);

  if (space.domain.length < 4) {
    space.domainStatus = "invalid-domain";
    await db.setItem("space", space._id, space);

    return "";
  }

  try {
    const k8sApi = kc.makeApiClient(k8s.NetworkingV1Api);

    const existing = await getIngressById(k8sApi, namespace, message.id);
    console.log(`There are ${existing.length} ingress(es) for this space`);
    let ingressExists = false;

    for (const item of existing) {
      enforce(
        !item.metadata.annotations["meta.helm.sh/release-name"],
        "Got a helm ingress!"
      );
      enforce(
        item.metadata.annotations["giscollective/space-id"] == message.id,
        `Got a wrong ingress! giscollective/space-id="${item.metadata.annotations["giscollective/space-id"]}". expected "${message.id}" `
      );

      const hosts = item.spec.rules.map((a) => a.host).filter((a) => a);

      if (hosts.includes(space.domain) && !ingressExists) {
        ingressExists = true;
        continue;
      }

      console.log(
        `DELETE Ingress ${item.metadata.name} with hosts ${hosts.join(", ")}`
      );
      await k8sApi.deleteNamespacedIngress(item.metadata.name, namespace);
    }

    if (ingressExists) {
      console.log(
        `SKIP Ingress ${space.name} with domain ${space.domain}. It already exists.`
      );
      return "";
    }

    const kubeMessage = createKubeMessage(space.domain, message.id);

    console.log("CREATE Ingress", space.domain);
    await k8sApi.createNamespacedIngress(namespace, kubeMessage);
  } catch (err) {
    return handleSetupError(err, space, db, broadcast);
  }

  await sendMessage(db, broadcast, {
    uniqueKey: `domain-setup-done-${new Date().toISOString()}`,
    type: "message",
    to: {
      type: "team",
      value: space.visibility.team,
    },
    subject: `Your domain ${space.domain} is reserved`,
    text: `Great news! Your domain ${space.domain} is officially reserved, and your space will be up and running there soon.`,
    html: `<p>Great news! Your domain <b>${space.domain}</b> is officially reserved, and your space will be up and running there soon.</p>`,
    actions: {
      "View website": `https://${space.domain}`,
    },
    useGenericTemplate: true,
  });

  space.domainStatus = "ready";
  await db.setItem("space", space._id, space);

  return "";
}
