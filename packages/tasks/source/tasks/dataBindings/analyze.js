/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { parseStream, normalize } from "../files/lib/base.js";
import { queue } from "async";
import BatchJobService from "../../lib/BatchJobService.js";
import CalendarService from "../../lib/CalendarService.js";
import { getAirtableStream } from "./lib/airtable.js";
import { MapFileResolvers } from "./lib/MapFileResolvers.js";
import { enforce } from "models/source/exception.js";

export class AnalyzeFeatureStreamJob {
  constructor(db, userId) {
    this.db = db;
    this.userId = userId;

    this.queue = queue(async (record) => {
      await this.handleRecord(record);
    }, 1);

    this.fields = {};
  }

  prepareDataBinding(dataBinding) {
    this.dataBinding = dataBinding;
    this.name = `${dataBinding.connection.type} import for ${dataBinding._id}`;
    this.teamId = dataBinding.visibility.team;
    this.mapId = dataBinding.destinationMap;
    this.stream = getAirtableStream(
      dataBinding.connection.config._apiKey,
      dataBinding.connection.config.baseId,
      dataBinding.connection.config.tableName,
      dataBinding.connection.config.viewName
    );
    this.mime = this.stream.mime;

    this.resolvers = new MapFileResolvers(this.db, this.teamId, this.mapId);

    this.fields = {};
    dataBinding.fields.forEach((field) => {
      this.fields[field.key] = {
        key: field.key,
        destination: field.destination ?? "",
        preview: [],
      };
    });
  }

  async handleRecord(record) {
    this.currentRun.ping = CalendarService.instance.now;
    this.currentRun.sent++;

    const normalizedRecord = normalize(record);

    Object.keys(normalizedRecord).forEach((key) => {
      if (!this.fields[key]) {
        this.fields[key] = {
          key,
          destination: "",
          preview: [],
        };
      }

      if (
        this.fields[key].preview.length < 5 &&
        !this.fields[key].preview.includes(normalizedRecord[key])
      ) {
        this.fields[key].preview.push(`${normalizedRecord[key]}`);
      }
    });

    if (this.currentRun.sent % 100 == 0) {
      await BatchJobService.instance.log(
        batchJob._id,
        `Processed ${this.currentRun.sent} items...`
      );
      await BatchJobService.instance.updateRun(batchName, this.currentRun);
    }
  }

  async drainQueue() {
    parseStream(this.mime, this.stream, (record) => {
      this.queue.push(record);
    });

    await this.stream.promise;
    await this.queue.drain();

    await this.updateDataBinding();
  }

  async updateDataBinding() {
    this.dataBinding = await this.db.getItem(
      "dataBinding",
      this.dataBinding._id
    );

    enforce(
      this.dataBinding,
      `There is no data binding with id "${this.dataBinding._id}"`
    );
    this.dataBinding.fields = Object.entries(this.fields)
      .filter((a) => a[1].preview.length > 0)
      .map((a) => a[1]);

    await this.db.setItem(
      "dataBinding",
      this.dataBinding._id,
      this.dataBinding
    );
  }

  async run() {
    await BatchJobService.instance.runByNameAndTeam(
      this.name,
      this.teamId,
      async (batchJob, currentRun) => {
        this.batchJob = batchJob;
        this.currentRun = currentRun;

        try {
          await this.drainQueue();
        } catch (err) {
          console.error(err);
          await BatchJobService.instance.log(batchJob._id, `${err}`);
        }
      }
    );
  }
}

export async function dataBindingAnalyze(db, message) {
  const job = new AnalyzeFeatureStreamJob(db, message.userId ?? "@anonymous");

  const dataBinding = await db.getItem("dataBinding", message.id);
  await job.prepareDataBinding(dataBinding);
  await job.run();

  return "";
}
