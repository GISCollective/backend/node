/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { parseStream, transformRecord, normalize } from "../files/lib/base.js";
import { queue } from "async";
import BatchJobService from "../../lib/BatchJobService.js";
import CalendarService from "../../lib/CalendarService.js";
import ModelChangeNotificationService from "../../lib/ModelChangeNotificationService.js";
import { getAirtableStream } from "./lib/airtable.js";
import { MapFileResolvers } from "./lib/MapFileResolvers.js";
import { downloadPicture } from "./lib/download.js";

export class ImportFeatureStreamJob {
  constructor(db, userId) {
    this.db = db;
    this.userId = userId;

    this.queue = queue(async (record) => {
      await this.handleRecord(record);
    }, 1);
  }

  prepareDataBinding(dataBinding) {
    this.dataBinding = dataBinding;
    this.name = `${dataBinding.connection.type} import for ${dataBinding._id}`;
    this.teamId = dataBinding.visibility.team;
    this.mapId = dataBinding.destination.modelId;
    this.stream = getAirtableStream(
      dataBinding.connection.config._apiKey,
      dataBinding.connection.config.baseId,
      dataBinding.connection.config.tableName,
      dataBinding.connection.config.viewName
    );
    this.mime = this.stream.mime;
    this.deleteNonSyncedRecords =
      dataBinding.destination.deleteNonSyncedRecords;
    this.startAt = new Date(CalendarService.instance.now);

    this.resolvers = new MapFileResolvers(this.db, this.teamId, this.mapId);
    this.resolvers.downloadPicture = downloadPicture(this.db, this.userId);

    this.transformOptions = {
      destinationMap: dataBinding.destination?.modelId,
      fields: dataBinding.fields,
      extraIcons: dataBinding.extraIcons,
      crs: dataBinding.crs,
      updateBy: dataBinding.updateBy,
      userId: this.userId,
      descriptionTemplate: dataBinding.descriptionTemplate,
      source: {
        type: "DataBinding",
        modelId: dataBinding._id.toString(),
      },
    };
  }

  async handleRecord(record) {
    this.currentRun.ping = CalendarService.instance.now;
    this.currentRun.sent++;

    try {
      const transformed = await transformRecord(
        normalize(record),
        this.transformOptions,
        this.resolvers
      );

      if (!transformed.validationError?.code) {
        this.saveRecord(transformed.result);
        this.currentRun.processed++;
      } else {
        console.log(transformed.validationError);
        this.currentRun.errors++;
      }
    } catch (err) {
      console.log(err);

      await BatchJobService.instance.logError(batchJob._id, `${err}`);
      this.currentRun.errors++;
    }

    if (this.currentRun.sent % 100 == 0) {
      await BatchJobService.instance.log(
        batchJob._id,
        `Processed ${this.currentRun.sent} items...`
      );
      await BatchJobService.instance.updateRun(batchName, this.currentRun);
    }
  }

  async drainQueue() {
    parseStream(this.mime, this.stream, (record) => {
      this.queue.push(record);
    });

    await this.stream.promise;
    await this.queue.drain();

    console.log("DRAINED!");
  }

  async saveRecord(feature) {
    if (feature._id) {
      const before = await this.db.getItem("sites", feature._id);
      await this.db.setItem("sites", feature._id, feature);

      ModelChangeNotificationService.instance.updated(
        "Feature",
        before,
        feature
      );
      return;
    }

    await this.db.addItem("sites", feature);
    ModelChangeNotificationService.instance.added("Feature", feature);
  }

  async deleteOldRecords() {
    if (!this.deleteNonSyncedRecords) {
      return;
    }

    await this.db.deleteWithQuery("sites", {
      "source.syncAt": { $lt: this.startAt },
      "source.modelId": this.transformOptions?.source?.modelId,
      "source.type": this.transformOptions?.source?.type,
    });
  }

  async run() {
    await BatchJobService.instance.runByNameAndTeam(
      this.name,
      this.teamId,
      async (batchJob, currentRun) => {
        this.batchJob = batchJob;
        this.currentRun = currentRun;

        await this.drainQueue();

        await this.deleteOldRecords();
      }
    );
  }
}

export async function dataBindingImport(db, message) {
  const job = new ImportFeatureStreamJob(db, message.userId ?? "@anonymous");

  const dataBinding = await db.getItem("dataBinding", message.id);
  await job.prepareDataBinding(dataBinding);
  await job.run();

  return "";
}
