/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import stream from "stream";
const { Readable } = stream;

import Airtable from "airtable";
const { configure, base } = Airtable;
const _base = base;

export function getAirtableStream(apiKey, baseId, tableName, viewName) {
  let tunnel;
  let resolve;

  const normalizeValue = (value) => {
    if (Array.isArray(value)) {
      return value.map((a) => normalizeValue(a));
    }

    if (value?.url) {
      return value.url;
    }

    return value;
  };

  const start = () => {
    configure({
      endpointUrl: "https://api.airtable.com",
      apiKey: `${apiKey}`,
    });

    console.log("Connecting to airtable.com...");

    const base = _base(`${baseId}`);

    const selector = {};

    if (viewName && viewName.trim() != "") {
      selector.view = viewName;
    }

    base(`${tableName}`)
      .select(selector)
      .eachPage(
        function page(records, fetchNextPage) {
          console.log(
            "Fetching airtable page with",
            records.length,
            " records."
          );
          records.forEach(function (record) {
            const niceRecord = { ...record.fields, remoteId: record.id };

            for (const key of Object.keys(niceRecord)) {
              niceRecord[key] = normalizeValue(niceRecord[key]);
            }

            tunnel.push(JSON.stringify(niceRecord));
            tunnel.push("\n");
          });

          fetchNextPage();
        },
        function done(err) {
          tunnel.push(null);
          tunnel.isReading = false;
          resolve?.();

          if (err) {
            console.error(err);
            return;
          }
        }
      );
  };

  tunnel = new Readable({
    read() {
      if (this.isReading) {
        return;
      }

      this.isReading = true;
      start();
    },
  });

  tunnel.promise = new Promise((r) => {
    resolve = r;
  });
  tunnel.type = "airtable";
  tunnel.apiKey = apiKey;
  tunnel.baseId = baseId;
  tunnel.tableName = tableName;
  tunnel.viewName = viewName;
  tunnel.mime = "text/ndjson";

  return tunnel;
}
