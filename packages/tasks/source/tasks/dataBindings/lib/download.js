/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import got from "got";

export function downloadPicture(db, owner) {
  return async (strUrl) => {
    const url = new URL(strUrl);

    let filename =
      `${Math.random()}`.substring(2, 7) +
      "_" +
      url.pathname.replace(/\//gi, "_");

    let contentType;
    const stream = got.stream(strUrl);

    await new Promise((done, fail) => {
      stream.on("response", (response) => {
        contentType = response.headers["content-type"];
        done();
      });

      stream.on("error", (err) => {
        fail(err);
      });
    });

    if (!contentType || contentType.indexOf("image") != 0) {
      stream.close();
      return;
    }

    const storedFile = await db.storeFileStream(
      stream,
      filename,
      contentType,
      "pictures"
    );
    const newPicture = await db.addItem("pictures", {
      name: "",
      owner,
      picture: `${storedFile._id}`,
      sourceUrl: strUrl,
    });

    return newPicture.insertedId;
  };
}
