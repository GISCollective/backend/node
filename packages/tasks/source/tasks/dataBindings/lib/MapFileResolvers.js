/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { ObjectId } from "mongodb";
import { URL } from "url";
import { getComputedVisibility } from "models/source/validators/feature.js";

const stringIsAValidUrl = (s) => {
  try {
    new URL(s);
    return true;
  } catch (err) {
    return false;
  }
};

export async function searchUser(db, terms) {
  const ids = terms
    .filter((a) => ObjectId.isValid(a))
    .map((a) => new ObjectId(a));
  const emails = terms.filter((a) => !ObjectId.isValid(a));

  const items = await db.query("users", {
    $or: [{ _id: { $in: ids } }, { email: { $in: emails } }],
  });

  return items.map((a) => a._id);
}

export function termsToQuery(terms) {
  const ids = terms
    .filter((a) => ObjectId.isValid(a))
    .map((a) => new ObjectId(a));

  const names = terms
    .filter((a) => !ObjectId.isValid(a))
    .map((a) => `${a}`.trim());

  const result = [];

  if (ids.length == 1) {
    result.push({ _id: ids[0] });
  }

  if (ids.length > 1) {
    result.push({ _id: { $in: ids } });
  }

  if (names.length == 1) {
    result.push({ name: names[0] });
    result.push({ otherNames: names[0] });
  }

  if (names.length > 1) {
    result.push({ name: { $in: names } });
    result.push({ otherNames: { $all: names } });
  }

  return result;
}

export async function searchIds(model, db, terms, query = {}) {
  const or = termsToQuery(terms);

  if (or.length == 0) {
    return [];
  }

  query["$or"] = or;

  const items = await db.query(model, query);

  return items.map((a) => a._id);
}

export async function searchAsset(model, db, terms) {
  const ids = terms.filter((a) => ObjectId.isValid(a)).map((a) => ObjectId(a));
  const sourceUrls = terms.filter((a) => !ObjectId.isValid(a));

  const items = await db.query(model, {
    $or: [{ _id: { $in: ids } }, { sourceUrl: { $in: sourceUrls } }],
  });

  return items.map((a) => a._id);
}

function escapeRegExp(string) {
  return string.replace(/[.*+?^${}()|[\]\\]/g, "\\$&"); // $& means the whole matched string
}

export class MapFileResolvers {
  constructor(db, team, selectedMap) {
    this.db = db;
    this.team = team;
    this.selectedMap = selectedMap;
  }

  async ensureIconSetCache() {
    if (this.iconSets) {
      return;
    }

    const ownedIconSets = await this.db.query("iconsets", {
      "visibility.team": new ObjectId(this.team),
    });
    const publicIconSets = await this.db.query("iconsets", {
      "visibility.isPublic": true,
    });

    this.iconSets = [
      ...ownedIconSets.map((a) => a._id),
      ...publicIconSets.map((a) => a._id),
    ];
  }

  async ensureMapCache() {
    if (this.maps) {
      return;
    }

    const result = await this.db.query("maps", {
      "visibility.team": new ObjectId(this.team),
    });
    this.maps = result.map((a) => a._id);
  }

  async featureById(id) {
    await this.ensureMapCache();

    const result = await this.db.query("sites", {
      _id: new ObjectId(id),
      maps: { $in: this.maps },
    });

    return result[0];
  }

  async featureByName(name) {
    await this.ensureMapCache();

    let maps = this.maps;

    if (this.selectedMap) {
      maps = maps.filter((a) => `${a}` == `${this.selectedMap}`);
    }

    const result = await this.db.query("sites", {
      name: { $options: "i", $regex: `.*${escapeRegExp(name)}.*` },
      maps: { $in: maps },
    });

    return result[0];
  }

  async featureByRemoteId(remoteId) {
    await this.ensureMapCache();

    let maps = this.maps;

    if (this.selectedMap) {
      maps = maps.filter((a) => `${a}` == `${this.selectedMap}`);
    }

    const result = await this.db.query("sites", {
      "source.remoteId": remoteId,
      maps: { $in: maps },
    });

    return result[0];
  }

  async queryMaps(terms) {
    await this.ensureMapCache();

    return searchIds("maps", this.db, terms);
  }

  async queryMapIconSets(mapId) {
    const record = await this.db.getItem("maps", mapId);

    return record?.iconSets?.list ?? [];
  }

  async queryIcons(terms) {
    await this.ensureIconSetCache();

    return searchIds("icons", this.db, terms, {
      iconSet: { $in: this.iconSets },
    });
  }

  async queryUsers(terms) {
    return searchUser(this.db, terms);
  }

  async handleExternalAsset(term, downloadFunc) {
    if (!stringIsAValidUrl(term)) {
      return null;
    }

    if (downloadFunc) {
      return await downloadFunc(term);
    }

    return term;
  }

  async checkAssets(model, terms, downloadFunc) {
    if (!terms?.length) {
      return [];
    }

    let result = [];

    for (let term of terms) {
      let existingAsset = await searchAsset(model, this.db, [term]);

      if (existingAsset.length == 0) {
        const id = await this.handleExternalAsset(term, downloadFunc);
        if (id) {
          existingAsset.push(id);
        }
      }

      result = result.concat(existingAsset);
    }

    return result;
  }

  async handlePictures(terms) {
    return await this.checkAssets("pictures", terms, this.downloadPicture);
  }

  async handleSounds(terms) {
    return await this.checkAssets("sounds", terms, this.downloadSound);
  }

  async getComputedVisibility(feature) {
    return getComputedVisibility(this.db, feature);
  }
}
