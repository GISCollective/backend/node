/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { promises as fs } from "fs";
import { join } from "path";
import sanitizeHtml from "sanitize-html";
import { sendEmail } from "../../lib/services/email.js";
import { measureNewsletterSent } from "../../lib/metrics.js";
import CalendarService from "../../lib/CalendarService.js";

import path from "path";
import { fileURLToPath } from "url";
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

export function blockToHtml(block) {
  switch (block.type) {
    case "paragraph":
      return `<p>${sanitizeHtml(block.data.text)}</p>`;

    case "header":
      return `<h${parseInt(block.data.level)}>${sanitizeHtml(
        block.data.text
      )}</h${parseInt(block.data.level)}>`;

    case "header":
      return `<h${parseInt(block.data.level)}>${sanitizeHtml(
        block.data.text
      )}</h${parseInt(block.data.level)}>`;

    case "quote":
      return `<div class="quote"><p>${sanitizeHtml(
        block.data.text
      )}</p><p class="author">- ${sanitizeHtml(block.data.caption)}</p></div>`;

    case "delimiter":
      return `<div class="delimiter">***</div>`;

    case "list":
      return renderHtmlList(block.data)

    case "image":
      if (block.data?.caption) {
        return `<div><img src="${block.data?.file?.url}" width="100%" alt="${block.data.caption}"><div class="image-caption">${block.data.caption}</div></div>`;
      }

      return `<img src="${block.data?.file?.url}" width="100%">`;

    default:
      return "";
  }
}

export function articleToHtml(blocks) {
  if (!blocks?.length) {
    return "";
  }

  return blocks?.map((a) => blockToHtml(a)).join("\n");
}

export function renderTextList(data, spaces = "") {
  const showNumbers = data.style == "ordered";

  const toItem = (item, index) => {
    let result = "";

    if(typeof item == "string") {
      result = `${spaces}${showNumbers ? index + 1 : "*"}${showNumbers ? "." : ""} ${item}`;
    }

    if(typeof item?.content == "string") {
      result = `${spaces}${showNumbers ? index + 1 : "*"}${showNumbers ? "." : ""} ${item.content}`;

      if(item?.items?.length) {
        result += `\n${renderTextList(item, `${spaces}  `)}`;
      }
    }

    return result;
  }

  return data?.items?.map(toItem)
    .join("\n");
}

export function renderHtmlList(data, spaces = "") {
  const tag = data.style == "ordered" ? "ol" : "ul";

  const toItem = (item, index) => {
    let result = "";

    if(typeof item == "string") {
      result = `${spaces}<li>${item}</li>`;
    }

    if(typeof item?.content == "string") {
      result = `${spaces}<li>${item.content}`;

      if(item?.items?.length) {
        result += `\n${renderHtmlList(item, `${spaces}  `)}\n`;
      }

      result += `${spaces}</li>`;
    }

    return result;
  }

  return `${spaces}<${tag}>\n` + data?.items?.map(toItem)
    .join("\n") + `\n</${tag}>`;
}

export function blockToText(block) {
  const cleanHtml = (text) =>
    sanitizeHtml(text, { allowedTags: [], allowedAttributes: [] });

  switch (block.type) {
    case "paragraph":
      return cleanHtml(block.data.text);

    case "header":
      const separators = {
        1: "---",
        2: "===",
        3: "###",
        4: "+++",
        5: "^^^",
        6: "***",
      };
      const separator = separators[block.data.level] ?? "";
      return `${cleanHtml(block.data.text)}\n${separator}`;

    case "quote":
      return `"${cleanHtml(block.data.text)}"\n - ${cleanHtml(
        block.data.caption
      )}`;

    case "delimiter":
      return "* * *";

    case "list":
      return renderTextList(block.data);

    default:
      return "";
  }
}

export function articleToText(blocks) {
  if (!blocks?.length) {
    return "";
  }

  return blocks?.map((a) => blockToText(a)).join("\n\n");
}

export async function replaceTemplateVars(data, db, newsletterId, articleId) {
  const defaultSpace = await db.getItemWithQuery("space", {
    "visibility.isDefault": true,
    domain: { $ne: "new.opengreenmap.org" }, /// hack. remove when new.opengreenmap.org is deleted
  });

  const newsletter = await db.getItem("newsletter", newsletterId);
  const team = await db.getItem("teams", newsletter.visibility.team);

  return data
    .replaceAll(
      "{{defaultSpaceLogo}}",
      sanitizeHtml(
        `https://${defaultSpace.domain}/api-v1/articles/${articleId}-${newsletterId}/logo`
      )
    )
    .replaceAll("{{defaultSpaceName}}", sanitizeHtml(`${defaultSpace.name}`))
    .replaceAll("{{teamName}}", sanitizeHtml(`${team.name}`))
    .replaceAll("{{newsletterName}}", sanitizeHtml(`${newsletter.name}`))
    .replaceAll(
      "{{teamLogo}}",
      sanitizeHtml(
        `https://${defaultSpace.domain}/api-v1/pictures/${team.logo}/picture/png-512`
      )
    )
    .replaceAll(
      "{{unsubscribeUrl}}",
      sanitizeHtml(
        `https://${defaultSpace.domain}/manage/newsletter/${newsletterId}/unsubscribe`
      )
    )
    .replaceAll("{{spaceUrl}}", sanitizeHtml(`https://${defaultSpace.domain}`));
}

export async function newsletterHtmlTemplate(db, newsletterId, articleId) {
  const templatePath = join(
    __dirname,
    "..",
    "..",
    "..",
    "templates",
    "newsletter.html"
  );
  const data = await fs.readFile(templatePath, "utf-8");

  return await replaceTemplateVars(data, db, newsletterId, articleId);
}

export async function newsletterTextTemplate(db, newsletterId) {
  const templatePath = join(
    __dirname,
    "..",
    "..",
    "..",
    "templates",
    "newsletter.txt"
  );
  const data = await fs.readFile(templatePath, "utf-8");

  return await replaceTemplateVars(data, db, newsletterId);
}

export async function newsletterEmail(db, message, broadcast) {
  const htmlTemplate = await newsletterHtmlTemplate(
    db,
    message.newsletterId,
    message.articleId
  );
  const textTemplate = await newsletterTextTemplate(db, message.newsletterId);

  message.text = textTemplate.replaceAll(
    "{{message}}",
    articleToText(message.content?.blocks)
  );

  message.html = htmlTemplate.replaceAll(
    "{{message}}",
    articleToHtml(message.content?.blocks)
  );

  message.to = message.recipients;

  const emails = await db.query("newsletterEmail", {
    email: { $in: message.to },
  });

  const personalization = emails.map((a) => ({
    email: a.email,
    id: a._id.toString(),
    substitutions: [{
      var: "id",
      value: a._id.toString()
    }]
  }));

  await sendEmail(
    db,
    message,
    personalization,
    (email) => {
      return measureNewsletterSent(
        db,
        email,
        message.newsletterId,
        message.articleId
      );
    },
    (email) => {
      console.log(`Email failed to send. Retry...`);

      broadcast.push("newsletter.retry", {
        ...message,
        to: [email],
        retry: 0,
        triggerDate: CalendarService.instance.nowDate,
      });
    }
  );

  return "";
}
