/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { sendEmail } from "../../lib/services/email.js";
import {
  measureNewsletterSent,
  measureNewsletterFail,
} from "../../lib/metrics.js";
import CalendarService from "../../lib/CalendarService.js";

const maxAttempts = 10;

export async function newsletterRetry(db, message, broadcast) {
  await sendEmail(
    db,
    message,
    {},
    (email) => {
      return measureNewsletterSent(
        db,
        email,
        message.newsletterId,
        message.articleId
      );
    },
    (email) => {
      if (message.retry > maxAttempts) {
        console.log(`Email failed to send after ${message.retry} atemps!`);

        return measureNewsletterFail(
          db,
          email,
          message.newsletterId,
          message.articleId
        );
      }

      console.log(`Email failed to send. Retry...`);
      broadcast.push("newsletter.retry", {
        ...message,
        to: [email],
        retry: message.retry + 1,
        triggerDate: CalendarService.instance.later(10),
      });
    }
  );

  return "";
}
