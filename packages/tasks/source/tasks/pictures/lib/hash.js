/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { encode } from "blurhash";

export class ImageHash {
  _instance = null;

  get instance() {
    return this._instance;
  }

  set instance(value) {
    this._instance = value;
  }

  encode(buffer, size) {
    return encode(new Uint8ClampedArray(buffer), size, size, 4, 4);
  }
}
