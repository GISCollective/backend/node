/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import sharp from "sharp";

export class ImageResize {
  _instance = null;

  constructor() {}

  get instance() {
    return this._instance;
  }

  set instance(value) {
    this._instance = value;
  }

  resizeSquare(stream, size) {
    return sharp(stream)
      .raw()
      .ensureAlpha()
      .resize(size, size, { fit: "cover" })
      .toBuffer();
  }
}
