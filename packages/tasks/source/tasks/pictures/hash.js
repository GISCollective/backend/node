/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { ImageHash } from "./lib/hash.js";
import { ImageResize } from "./lib/resize.js";

function toBuff(stream) {
  let bufs = [];
  return new Promise((resolve, reject) => {
    stream.on("data", function (d) {
      bufs.push(d);
    });
    stream.on("error", reject);
    stream.on("end", function () {
      resolve(Buffer.concat(bufs));
    });
  });
}

export async function pictureHash(db, message) {
  const picture = await db.getItem("pictures", message.id);

  if (!picture) {
    console.log("There is no picture with id:", message.id);
    return "";
  }

  let hash = "U6PZfSjE.AyE_3t7t7R**0o#DgR4_3R*D%xs";

  try {
    const fileStream = await db.getFile("pictures", picture.picture);
    const buf = await toBuff(fileStream);
    const resizedBuffer = await ImageResize.instance.resizeSquare(buf, 32);
    hash = ImageHash.instance.encode(resizedBuffer, 32);
  } catch (err) {
    console.error(err);
  }

  await db.setItem("pictures", picture._id, { hash });

  return "";
}
