/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/

import { getPictureInfo } from "models/source/files.js";
import { ObjectId } from "mongodb";
import fs from "fs";

export async function pictureAnalize(db, message) {
  const picture = await db.getItem("pictures", message.id);

  if (!picture) {
    console.log("There is no picture with id:", message.id);
    return "";
  }

  if (Object.keys(picture?.meta?.properties ?? {}).length > 0) {
    console.log("The picture already has meta properties:", message.id);
    return "";
  }

  const storage = db.getFileStorage("pictures");
  const files = await storage.find({ _id: new ObjectId(`${picture.picture}`) });

  for await (let file of files) {
    try {
      if (!file.metadata.mime || file.metadata.mime?.indexOf?.("image") !== 0) {
        continue;
      }
      const size = await getPictureInfo(storage, file, {});

      if (!picture.meta) {
        picture.meta = {};
      }

      picture.meta.width = size.width;
      picture.meta.height = size.height;
      picture.meta.format = size.format;
      picture.meta.mime = size["mime type"];
      picture.meta.properties = size.properties;

      if (fs.existsSync(size.path)) {
        fs.unlinkSync(size.path);
      }
    } catch (err) {
      console.error(err);
    }
  }

  await db.setItem("pictures", picture._id, { meta: picture.meta });

  return "";
}
