/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { promises as fs } from "fs";
import path, { join } from "path";
import CalendarService from "../../lib/CalendarService.js";
import sanitizeHtml from "sanitize-html";
import { getPreference } from "../../lib/config.js";
import { updateEmailList, getAdminEmails } from "./email.js";
import { fileURLToPath } from "url";
import fetch from "node-fetch";
import TurndownService from "turndown";

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

export async function replaceTemplateVars(data, db, content) {
  const defaultSpace = await db.getItemWithQuery("space", {
    "visibility.isDefault": true,
    "domain": { "$ne": "new.opengreenmap.org" } /// hack. remove when new.opengreenmap.org is deleted
  });

  return data
    .replaceAll("{{message}}", content)
    .replaceAll(
      "{{defaultSpaceLogo}}",
      sanitizeHtml(
        `https://${defaultSpace.domain}/api-v1/pictures/${defaultSpace.logo}/picture/png-512`
      )
    )
    .replaceAll("{{defaultSpaceName}}", sanitizeHtml(`${defaultSpace.name}`))
    .replaceAll("{{spaceUrl}}", sanitizeHtml(`https://${defaultSpace.domain}`));
}

export async function addTemplate(db, name, content) {
  const templatePath = join(__dirname, "..", "..", "..", "templates", name);
  const data = await fs.readFile(templatePath, "utf-8");

  return await replaceTemplateVars(data, db, content);
}

export async function slackNotification(url, message) {
  const turndownService = new TurndownService({
    strongDelimiter: "*",
    emDelimiter: "_",
    fence: "~",
  });

  const body = {
    blocks: [
      {
        type: "section",
        text: {
          type: "mrkdwn",
          text: `*${message.subject}*`,
        },
      },
      {
        type: "section",
        text: {
          type: "mrkdwn",
          text: turndownService.turndown(message.text),
        },
      },
    ],
  };

  const actionNames = Object.keys(message.actions ?? {});

  if (actionNames.length) {
    body.blocks.push({
      type: "actions",
      elements: actionNames.map((key) => ({
        type: "button",
        text: {
          type: "plain_text",
          text: key,
          emoji: true,
        },
        url: message.actions[key],
      })),
    });
  }

  try {
    const response = await fetch(url, {
      method: "post",
      body: JSON.stringify(body),
      headers: { "Content-Type": "application/json" },
    });

    console.log("[slack message]", body);
    console.log("[slack response]", await response.text());
  } catch (err) {
    console.error(err);
  }
}

export async function getRecipients(db, recipients) {
  if (recipients.type == "email") {
    return [recipients.value];
  }

  if (recipients.type == "team") {
    const team = await db.getItem("teams", recipients.value);

    return await updateEmailList(db, [...team.owners, ...team.leaders]);
  }

  if (recipients.type == "user") {
    return await updateEmailList(db, [recipients.value]);
  }

  return [];
}

export async function notificationsMessage(db, request, options) {
  const message = await db.getItem("message", request.id);

  if (message.isSent || message.failCount >= 10) {
    return;
  }

  const isAdmin = message.type == "admin";
  const webhook = await getPreference(db, "integrations.slack.webhook");

  if (isAdmin && webhook) {
    await slackNotification(webhook, message);
    return "";
  }

  const to = isAdmin
    ? await getAdminEmails(db)
    : await getRecipients(db, message.to);

  const email = {
    text: "",
    html: "",
    subject: message.subject,
    to,
  };

  if (typeof message.text == "string") {
    email.text = sanitizeHtml(message.text, {
      allowedTags: [],
      allowedAttributes: false,
    });

    if (Object.keys(message.actions ?? {}).length > 0) {
      email.text +=
        "\n\n" +
        Object.keys(message.actions)
          .map((a) => `${a}: ${message.actions[a]}`)
          .join("\n");
    }
  }

  if (typeof message.html == "string") {
    email.html = sanitizeHtml(message.html);

    if (Object.keys(message.actions ?? {}).length > 0) {
      email.html +=
        "\n\n" +
        Object.keys(message.actions)
          .map(
            (a) =>
              `<a href="${message.actions[a]}" class="email-button">${a}</a>`
          )
          .join("&nbsp;&nbsp;");
    }
  }

  message.sentOn = CalendarService.instance.now;
  message.failCount = message.failCount || 0;

  if (message.useGenericTemplate) {
    email.text = await addTemplate(db, "notification.txt", email.text);
    email.html = await addTemplate(db, "notification.html", email.html);
  }

  try {
    await options.sendEmail(db, email);

    message.isSent = true;
  } catch (err) {
    message.failCount++;
    message.isSent = false;
  }

  await db.setItem("message", message._id, message);

  return "";
}

export async function sendMessage(db, broadcast, message) {
  const result = await db.addItem("message", message);

  const notification = {
    id: result.insertedId.toString(),
  };

  broadcast.push("notifications.message", notification);
}
