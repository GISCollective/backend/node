/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { ObjectId } from "mongodb";
import sanitizeHtml from "sanitize-html";
import { sendEmail } from "../../lib/services/email.js";

export async function getAdminEmails(db) {
  const result = [];

  await db.query(
    "users",
    {
      scopes: "admin",
    },
    (user) => {
      result.push(user.email);
    }
  );

  return result;
}

export async function updateEmailList(db, list) {
  const result = [];
  const strList = list.map((a) => `${a}`);
  const idList = list
    .map((id) => {
      try {
        return new ObjectId(id);
      } catch (err) {
        return null;
      }
    })
    .filter((a) => a);

  await db.query(
    "users",
    {
      $or: [{ email: { $in: strList } }, { _id: { $in: idList } }],
    },
    (user) => {
      result.push(user.email);
    }
  );

  return result;
}

export async function notificationsEmail(db, message) {
  if (typeof message.text == "string") {
    message.text = sanitizeHtml(message.text, {
      allowedTags: [],
      allowedAttributes: false,
    });
  } else {
    message.text = "";
  }

  if (typeof message.html == "string") {
    message.html = sanitizeHtml(message.html);
  } else {
    message.html = message.text;
  }

  message.to = await updateEmailList(db, message.to);

  await sendEmail(db, message);

  return "";
}
