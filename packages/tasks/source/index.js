/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Db from "models/source/db.js";
import Broadcast from "hmq-js-client/source/broadcast.js";
import Config from "hmq-js-client/source/config.js";
import { pictureAnalize } from "./tasks/pictures/analyze.js";
import { pictureHash } from "./tasks/pictures/hash.js";
import { ImageHash } from "./tasks/pictures/lib/hash.js";
import { ImageResize } from "./tasks/pictures/lib/resize.js";
import { mapFileAnalyze } from "./tasks/files/analyze.js";
import { mapFilePreview } from "./tasks/files/preview.js";
import { mapFileImport } from "./tasks/files/import.js";
import { dataBindingImport } from "./tasks/dataBindings/import.js";
import { dataBindingAnalyze } from "./tasks/dataBindings/analyze.js";
import { notificationsEmail } from "./tasks/notifications/email.js";
import { notificationsMessage } from "./tasks/notifications/message.js";
import { newsletterEmail } from "./tasks/newsletter/email.js";
import { newsletterRetry } from "./tasks/newsletter/retry.js";
import { registerNamedProjections } from "./lib/crs.js";
import { domainSetup } from "./tasks/space/domainSetup.js";
import { spaceStyle } from "./tasks/space/style.js";
import { pageContact } from "./tasks/page/contact.js";
import { featureLarge } from "./tasks/feature/large.js";
import { join } from "path";
import yargs from "yargs";
import { Registry, collectDefaultMetrics } from "prom-client";
import k8s from "@kubernetes/client-node";
import { sendEmail } from "./lib/services/email.js";

import BatchJobService from "./lib/BatchJobService.js";
import ModelChangeNotificationService from "./lib/ModelChangeNotificationService.js";

import path from "path";
import { fileURLToPath } from "url";
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

ImageResize.instance = new ImageResize();
ImageHash.instance = new ImageHash();

function resolve(name) {
  return process.env[name];
}

registerNamedProjections();

const argv = yargs(process.argv.slice(2)).default(
  "configuration",
  function randomValue() {
    return join(__dirname, `../config/configuration.json`);
  }
).argv;

const appConfig = new Config(argv["configuration"], resolve).toJson();

const mongoPathConfig = join(appConfig["general"]["db"], `mongo.json`);
const dbConfig = new Config(mongoPathConfig, resolve).toJson();

const db = new Db(dbConfig);
const broadcast = new Broadcast(appConfig["http"], appConfig["hmq"]);

const kc = new k8s.KubeConfig();
kc.loadFromCluster();

const register = new Registry();

function exposeStats(server) {
  collectDefaultMetrics({
    register,
    requestDurationBuckets: [0.1, 0.5, 1, 1.5],
    prefix: "node_tasks_",
  });

  server.route({
    method: "GET",
    path: "/stats",
    handler: async function (request, h) {
      const metrics = await register.metrics();

      const response = h.response(metrics);
      response.type("text/plain");
      return response;
    },
  });
}

db.connect(async function (err) {
  if (err) {
    return console.error(err);
  }

  BatchJobService.instance = new BatchJobService(db);
  ModelChangeNotificationService.instance = new ModelChangeNotificationService(
    db,
    broadcast
  );

  exposeStats(broadcast.server);

  try {
    await broadcast.start();
  } catch (err) {
    db.close();
    return console.error(err);
  }

  setInterval(() => {
    broadcast.resubscribe();
  }, 30 * 1000);

  broadcast.subscribe("Picture.Hash", (message) => pictureHash(db, message));

  broadcast.subscribe("Picture.Analize", (message) =>
    pictureAnalize(db, message)
  );

  broadcast.subscribe("space.domainSetup", (message) =>
    domainSetup(db, message, broadcast, kc)
  );
  broadcast.subscribe("space.style", (message) => spaceStyle(db, message));

  broadcast.subscribe("page.contact", (message) => pageContact(db, message));

  broadcast.subscribe("mapFile.analyze", (message) =>
    mapFileAnalyze(db, message)
  );
  broadcast.subscribe("mapFile.preview", (message) =>
    mapFilePreview(db, message, appConfig)
  );
  broadcast.subscribe("mapFile.import", (message) =>
    mapFileImport(db, message, appConfig)
  );

  broadcast.subscribe("databinding.trigger", (message) =>
    dataBindingImport(db, message)
  );
  broadcast.subscribe("databinding.analyze", (message) =>
    dataBindingAnalyze(db, message)
  );

  broadcast.subscribe("notifications.email", (message) =>
    notificationsEmail(db, message)
  );
  broadcast.subscribe("notifications.message", (message) =>
    notificationsMessage(db, message, { sendEmail })
  );
  broadcast.subscribe("newsletter.email", (message) =>
    newsletterEmail(db, message, broadcast)
  );
  broadcast.subscribe("newsletter.retry", (message) =>
    newsletterRetry(db, message, broadcast)
  );
  broadcast.subscribe("feature.large", (message) => featureLarge(db, message));
});
