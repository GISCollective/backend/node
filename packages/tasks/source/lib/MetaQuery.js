/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
export class MetaQuery {
  constructor(model, type, db) {
    this.model = model;
    this.type = type;
    this.db = db;
  }

  async set(id, data) {
    const meta = await this.get(id);
    meta.data = data;

    await this.db.setItem("meta", meta._id, meta);

    return meta;
  }

  async get(id) {
    let result = await this.db.getMeta(this.type, id);

    if (result) {
      return result;
    }

    result = {
      model: this.model,
      type: this.type,
      itemId: id,
      changeIndex: -1,
      data: {},
    };

    const tmp = await this.db.addItem("meta", result);
    result["_id"] = tmp?.insertedId;

    return result;
  }
}
