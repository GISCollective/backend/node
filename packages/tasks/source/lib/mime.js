/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import path from "path";

const mimes = {
  ".json": "application/json",
  ".geojson": "application/geo+json",
};

export function mimeFromExt(ext) {
  return mimes[ext] ?? "application/binary";
}

export function mimeFromFileObject(fileObject) {
  if (fileObject?.metadata?.mime) {
    return fileObject?.metadata?.mime;
  }

  const ext = path.extname(fileObject?.filename ?? "");

  return mimeFromExt(ext.toLowerCase());
}
