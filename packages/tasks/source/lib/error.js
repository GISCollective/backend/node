/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/

export function enforce(value, message) {
  if (!value) {
    throw new Error(message);
  }
}
