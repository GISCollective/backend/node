/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Json from "models/source/json.js";

class ModelChangeNotificationService {
  constructor(db, broadcast) {
    this.db = db;
    this.broadcast = broadcast;
  }

  deleted(model, record) {
    const before = Json.flatten(JSON.parse(JSON.stringify(record)));
    before._id = record._id.toString();

    this.broadcast.push(`${model}.change`, {
      modelName: `${model}`,
      type: "delete",
      before,
    });
  }

  added(model, record) {
    const after = Json.flatten(JSON.parse(JSON.stringify(record)));
    after._id = record._id.toString();

    this.broadcast.push(`${model}.change`, {
      modelName: `${model}`,
      type: "add",
      after,
    });
  }

  updated(model, recordBefore, recordAfter) {
    const after = Json.flatten(JSON.parse(JSON.stringify(recordAfter)));
    after._id = recordAfter._id.toString();

    const before = Json.flatten(JSON.parse(JSON.stringify(recordBefore)));
    before._id = recordBefore._id.toString();

    this.broadcast.push(`${model}.change`, {
      modelName: `${model}`,
      type: "update",
      before,
      after,
    });
  }
}

export default ModelChangeNotificationService;
