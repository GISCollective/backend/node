/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { enforce } from "models/source/exception.js";

export async function getConfigValue(db, name) {
  return await getPreference(db, `secret.${name}`);
}

export async function getPreference(db, name) {
  const result = await db.getItemWithQuery("preference", {
    name,
  });

  enforce(
    result?.value !== undefined && result?.value !== null,
    `The preference "${name}" is not defined.`
  );

  return result.value;
}
