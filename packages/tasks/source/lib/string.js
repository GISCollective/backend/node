const dasherizeCache = {};

export function dasherize(str) {
  if (!dasherizeCache[str]) {
    dasherizeCache[str] = str
    .replace(/[^0-9a-zA-Z_-\s]/gi, '')
    .replace(/\s{2,}/g, ' ')
    .replace(/([a-z\d])([A-Z])/g, "$1-$2")
    .replace(/[ _]/g, "-")
    .toLowerCase();
  }

  return dasherizeCache[str];
}