/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import CalendarService from "./CalendarService.js";

export async function measureNewsletterEvent(
  db,
  email,
  newsletterId,
  articleId,
  type
) {
  await db.addItem("metrics", {
    labels: {
      relatedId: newsletterId,
      email: email,
    },
    name: articleId,
    reporter: "",
    time: CalendarService.instance.now,
    type,
    value: 1,
  });
}

export async function measureNewsletterSent(
  db,
  email,
  newsletterId,
  articleId
) {
  return measureNewsletterEvent(
    db,
    email,
    newsletterId,
    articleId,
    "newsletterSent"
  );
}

export async function measureNewsletterFail(
  db,
  email,
  newsletterId,
  articleId
) {
  return measureNewsletterEvent(
    db,
    email,
    newsletterId,
    articleId,
    "newsletterFail"
  );
}
