/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import proj4 from "proj4";

const { defs } = proj4;

export function registerNamedProjections() {
  defs("urn:x-ogc:def:crs:EPSG:4326", defs("EPSG:4326"));
  defs("urn:ogc:def:crs:OGC::CRS84", defs("EPSG:4326"));

  defs([
    [
      "EPSG:31700",
      "+proj=sterea +lat_0=46 +lon_0=25 +k=0.99975 +x_0=500000 +y_0=500000 +ellps=krass +towgs84=28,-121,-77,0,0,0,0 +units=m +no_defs ",
    ],
  ]);

  defs("urn:ogc:def:crs:EPSG::31700", defs("EPSG:31700"));

  defs(
    "EPSG:2157",
    "+proj=tmerc +lat_0=53.5 +lon_0=-8 +k=0.99982 +x_0=600000 +y_0=750000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs +type=crs"
  );
}
