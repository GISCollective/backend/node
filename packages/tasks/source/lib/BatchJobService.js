/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import CalendarService from "./CalendarService.js";
import crypto from "crypto";
import { enforce } from "./error.js";
import dayjs from "dayjs";

export default class BatchJobService {
  constructor(db) {
    this.db = db;
  }

  getByName(name) {
    return this.db.getItemWithQuery("batchjobs", { name: `${name}` });
  }

  async upsertByName(name, teamId) {
    enforce(
      `${name}` != "undefined",
      "Can't create a job with the `undefined` name."
    );

    let batchJob = await this.db.getItemWithQuery("batchjobs", {
      name: `${name}`,
    });

    if (!batchJob) {
      const now = CalendarService.instance.now;

      batchJob = {
        name,
        visibility: {
          team: teamId,
          isPublic: false,
        },
        runHistory: [],
        info: {
          createdOn: now,
          lastChangeOn: now,
          changeIndex: 0,
          author: "@batch",
        },
      };

      await this.db.addItem("batchjobs", batchJob);

      batchJob = await this.db.getItemWithQuery("batchjobs", {
        name: `${name}`,
      });
    }

    enforce(batchJob, "Can't create the job record");
    return batchJob;
  }

  async upsertAvailableByName(name, teamId) {
    let batchJob = await this.upsertByName(name, teamId);
    const runningEntry = batchJob.runHistory.find((a) => a.status == "running");

    if (runningEntry) {
      const ping = dayjs(runningEntry.ping);
      const now = dayjs(CalendarService.instance.now);

      const diff = now.diff(ping, "minute");

      if (diff < 5) {
        //throw new Error("There is already a running BatchJob");
      }

      runningEntry.status = "timeout";
      await this.updateRun(name, runningEntry);
    }

    return batchJob;
  }

  async runByNameAndTeam(name, teamId, jobFn) {
    const batchJob = await this.upsertAvailableByName(name, teamId);
    let currentRun = await BatchJobService.instance.start(name);

    await BatchJobService.instance.log(batchJob._id, `Started`);

    try {
      await jobFn(batchJob, currentRun);

      currentRun.status = "success";
      await this.updateRun(name, currentRun);
    } catch (err) {
      await BatchJobService.instance.logError(batchJob._id, `${err}`);
      await BatchJobService.instance.stopWithError(name);
    }

    await BatchJobService.instance.log(batchJob._id, `Done`);
  }

  async updateRun(batchName, runEntry) {
    const batch = await this.getByName(batchName, "");
    enforce(batch, "Batch Job does not exist.");

    batch.runHistory.forEach((item, index) => {
      if (item.runId == runEntry.runId) {
        batch.runHistory[index] = runEntry;
      }
    });

    await this.db.setItem("batchjobs", batch._id, batch);
  }

  async createBatchRun(batchName, status) {
    const runId = crypto.randomBytes(16).toString("hex");

    let entry = {
      runId,
      status,
      time: CalendarService.instance.now,
      ping: CalendarService.instance.now,
      duration: 0,
      total: 0,
      sent: 0,
      processed: 0,
      ignored: 0,
      errors: 0,
    };

    let batchJob = await this.db.getItemWithQuery("batchjobs", {
      name: `${batchName}`,
    });
    if (!batchJob.runHistory) {
      batchJob.runHistory = [];
    }

    batchJob.runHistory.push(entry);

    await this.db.setItem("batchjobs", batchJob._id, batchJob);

    return entry;
  }

  async stopWithError(batchName) {
    const batchJob = await this.getByName(batchName);
    batchJob.runHistory.forEach((entry) => {
      if (["scheduled", "preparing", "running"].indexOf(entry.status) != -1) {
        entry.status = "error";
      }
    });

    await this.db.setItem("batchjobs", batchJob._id, batchJob);
  }

  async start(batchName) {
    const batchJob = await this.getByName(batchName);

    let entry = batchJob.runHistory.find((a) => a.status == "scheduled");

    if (!entry) {
      entry = await this.createBatchRun(batchName, "running");
    }

    return entry;
  }

  async schedule(batchName) {
    return await this.createBatchRun(batchName, "scheduled");
  }

  async log(id, message) {
    let entry = {};

    entry["timestamp"] = dayjs(CalendarService.instance.now).unix();
    entry["reference"] = `${id}`;
    entry["level"] = "info";
    entry["message"] = message;

    await this.db.addItem("logs", entry);
  }

  async logError(id, message) {
    let entry = {};

    entry["timestamp"] = dayjs(CalendarService.instance.now).unix();
    entry["reference"] = `${id}`;
    entry["level"] = "error";
    entry["message"] = message;

    await this.db.addItem("logs", entry);
  }
}
