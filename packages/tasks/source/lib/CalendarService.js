/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
class CalendarService {
  static instance = new CalendarService();

  get now() {
    const date = new Date();
    return date.toISOString();
  }

  get nowDate() {
    return new Date(this.now);
  }

  later(seconds) {
    const date = this.nowDate;

    date.setSeconds(date.getSeconds() + seconds);

    return date;
  }
}

export default CalendarService;
