/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { getConfigValue } from "../config.js";
import { sendMailerSendEmail } from "./mailersend.js";
import { sendSMTPMessage } from "./smtp.js";

export async function sendEmail(
  db,
  message,
  personalization,
  onEmailSend,
  onEmailFail
) {
  const type = await getConfigValue(db, "mail.type");

  if (type == "mailersend.com") {
    await sendMailerSendEmail(
      db,
      message,
      personalization,
      onEmailSend,
      onEmailFail
    );
  }

  if (type == "smtp") {
    await sendSMTPMessage(
      db,
      message,
      personalization,
      onEmailSend,
      onEmailFail
    );
  }
}
