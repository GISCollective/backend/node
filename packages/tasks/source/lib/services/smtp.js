/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { getConfigValue, getPreference } from "../config.js";
import { createTransport } from "nodemailer";

export function applyPersonalization(email, text, personalization) {
  if (!text) {
    return text;
  }

  const selectedPersonalization = personalization?.find?.(
    (a) => a.email == email
  );

  if (!selectedPersonalization) {
    return text;
  }

  for (const substitusion of selectedPersonalization?.substitutions ?? []) {
    text = `${text}`.replaceAll(`{$${substitusion.var}}`, substitusion.value);
  }

  return text;
}

export async function getSmtpConfig(db) {
  const host = await getConfigValue(db, "smtp.host");
  const connectionType = await getConfigValue(db, "smtp.connectionType");
  const user = await getConfigValue(db, "smtp.username");
  const pass = await getConfigValue(db, "smtp.password");
  const port = await getConfigValue(db, "smtp.port");

  return {
    host,
    port: parseInt(port),
    secure: connectionType == "tls",
    tls: {
      rejectUnauthorized: false,
    },
    auth: {
      user,
      pass,
    },
  };
}

export async function sendSMTPMessage(
  db,
  message,
  personalization,
  onEmailSend,
  onEmailFail
) {
  const smtpConfig = await getSmtpConfig(db);
  const from = await getConfigValue(db, "smtp.from");
  const serviceName = await getPreference(db, "appearance.name");

  message.from = `${serviceName} <${from}>`;

  let transporter = createTransport(smtpConfig);
  sendEmail(transporter, message, personalization, onEmailSend, onEmailFail);
}

export async function sendEmail(
  transporter,
  message,
  personalization,
  onEmailSend,
  onEmailFail
) {
  console.log(
    `[SMTP] sending "${message.subject}" to ${message.to.join(", ")}`
  );

  for (let recipient of message.to) {
    const text = applyPersonalization(recipient, message.text, personalization);
    const html = applyPersonalization(recipient, message.html, personalization);

    const options = {
      from: message.from,
      to: recipient,
      subject: message.subject,
      text,
      html,
    };

    if(message.replyTo) {
      replyTo: options.replyTo
    }

    try {
      await transporter.sendMail(options);
    } catch (err) {
      console.error(err);
      await onEmailFail?.(recipient);
      continue;
    }

    await onEmailSend?.(recipient);
  }
}
