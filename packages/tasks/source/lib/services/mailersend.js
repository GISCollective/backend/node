/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { Recipient } from "mailersend";
import { EmailParams } from "mailersend";
import { Sender } from "mailersend";
import MailerSend from "mailersend";

import { getConfigValue, getPreference } from "../config.js";

export async function getMailerSendConfig(db) {
  const key = await getConfigValue(db, "mailersend.key");
  const from = await getConfigValue(db, "mailersend.from");

  return {
    key,
    from,
  };
}

export async function sendMailerSendEmail(
  db,
  message,
  personalization,
  onEmailSend,
  onEmailFail
) {
  const config = await getMailerSendConfig(db);

  const mailersend = new MailerSend({
    api_key: config.key,
  });

  return sendBulkEmail(db, mailersend, message, personalization, onEmailSend);
}

export async function sendBulkEmail(
  db,
  mailersend,
  message,
  personalization,
  onEmailSend,
  onEmailFail
) {
  const config = await getMailerSendConfig(db);
  const serviceName = await getPreference(db, "appearance.name");

  const recipients = message.to.map((a) => new Recipient(a));

  console.log(
    `[MailSender] sending "${message.subject}" to ${message.to.join(", ")}`
  );
  const emailParams = new EmailParams()
    .setFrom(new Sender(config.from, serviceName))
    .setTo(recipients)
    .setPersonalization(personalization ?? [])
    .setSubject(message.subject)
    .setHtml(message.html)
    .setText(message.text);

  try {
    await mailersend.send(emailParams);
  } catch (err) {
    console.error(err);

    if (onEmailFail) {
      for (let recipient of message.to) {
        await onEmailFail(recipient);
      }
    }

    return;
  }

  if (onEmailSend) {
    for (let recipient of message.to) {
      await onEmailSend(recipient);
    }
  }
}
