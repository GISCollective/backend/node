/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { mapFileAnalyze } from "../../../source/tasks/files/analyze.js";
import BatchJobService from "../../../source/lib/BatchJobService.js";
import { createStubInstance, stub, spy, restore } from "sinon";
import Db from "models/source/db.js";
import { createReadStream } from "fs";
import { resolve } from "path";
import { should } from "chai";
should();

import path from "path";
import { fileURLToPath } from "url";
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

describe("The mapFile.analyze task", function () {
  let readStream;
  let db;

  describe("for a csv file", function () {
    beforeEach(function () {
      readStream = createReadStream(
        resolve(__dirname, "..", "..", "_files", "test.csv")
      );
      BatchJobService.instance = createStubInstance(BatchJobService, {
        upsertByName: stub(),
      });

      db = createStubInstance(Db, {
        getItem: stub()
          .onCall(0)
          .returns({ _id: "file-id", mapFile: "5d7691374100fa0100497efc" })
          .onCall(1)
          .returns({ _id: "map-id", visibility: { team: "team-id" } }),
        getFile: readStream,
        setItem: spy(),
        addItem: spy(),
        getMeta: stub()
          .onCall(0)
          .returns(null)
          .onCall(1)
          .returns({ _id: "id" }),
        getFileObject: stub().returns({
          metadata: { mime: "text/csv" },
        }),
      });
    });

    afterEach(restore);

    it("can successfully analyze the file fields", async function () {
      await mapFileAnalyze(db, { id: "some-id" });

      db.setItem.lastCall.firstArg.should.equal("mapFile");
      const options = db.setItem.lastCall.lastArg.options;
      const fields = options.fields;

      BatchJobService.instance.upsertByName.lastCall.args.should.deep.equal([
        "mapFile.import file-id",
        "team-id",
      ]);

      options.destinationMap.should.equal("map-id");
      fields.should.have.length(74);
      fields[0].should.deep.equal({
        destination: "_id",
        key: "_id",
        preview: [
          "5ca8b5c4ef1f7e0100081b4a",
          "5ca8b5c4ef1f7e0100081b4b",
          "5ca8b5c4ef1f7e0100081b4c",
          "5ca8b5c4ef1f7e0100081b4d",
          "5ca8b5c4ef1f7e0100081b4e",
          "5ca8b5c4ef1f7e0100081b4f",
        ],
      });
    });

    it("can parse a file and add it's items to the db", async function () {
      const db = createStubInstance(Db, {
        getItem: stub()
          .onCall(0)
          .returns({ _id: "file-id", mapFile: "5d7691374100fa0100497efc" })
          .onCall(1)
          .returns({ _id: "map-id", visibility: { team: "team-id" } }),
        getFile: readStream,
        setItem: spy(),
        getFileObject: stub().returns({
          metadata: { mime: "text/csv" },
        }),
      });

      await mapFileAnalyze(db, { id: "some-id" });

      db.setItem.firstCall.args.should.deep.equal([
        "meta",
        undefined,
        {
          _id: undefined,
          changeIndex: -1,
          data: {
            errors: [],
            total: 113,
          },
          itemId: "some-id",
          model: "MapFile",
          type: "mapFile.import",
        },
      ]);
    });
  });

  describe("for a feature collection geojson file", function () {
    beforeEach(function () {
      readStream = createReadStream(
        resolve(__dirname, "..", "..", "_files", "featureCollection.geojson")
      );
      BatchJobService.instance = createStubInstance(BatchJobService, {
        upsertByName: stub(),
      });

      db = createStubInstance(Db, {
        getItem: stub()
          .onCall(0)
          .returns({ _id: "file-id", mapFile: "5d7691374100fa0100497efc" })
          .onCall(1)
          .returns({ _id: "map-id", visibility: { team: "team-id" } }),
        getFile: readStream,
        setItem: spy(),
        addItem: spy(),
        getMeta: stub()
          .onCall(0)
          .returns(null)
          .onCall(1)
          .returns({ _id: "id" }),
        getFileObject: stub().returns({
          metadata: { mime: "text/json" },
        }),
      });
    });

    afterEach(restore);

    it("can successfully analyze the file fields", async function () {
      await mapFileAnalyze(db, { id: "some-id" });

      db.setItem.lastCall.firstArg.should.equal("mapFile");
      const options = db.setItem.lastCall.lastArg.options;
      const fields = options.fields;

      BatchJobService.instance.upsertByName.lastCall.args.should.deep.equal([
        "mapFile.import file-id",
        "team-id",
      ]);

      options.destinationMap.should.equal("map-id");
      fields.should.have.length(5);

      fields[0].should.deep.equal({
        key: "Id",
        destination: "_id",
        preview: ["0"],
      });
    });
  });

  describe("for a feature collection geojson file without mime", function () {
    beforeEach(function () {
      readStream = createReadStream(
        resolve(__dirname, "..", "..", "_files", "featureCollection.geojson")
      );
      BatchJobService.instance = createStubInstance(BatchJobService, {
        upsertByName: stub(),
      });

      db = createStubInstance(Db, {
        getItem: stub()
          .onCall(0)
          .returns({ _id: "file-id", mapFile: "5d7691374100fa0100497efc" })
          .onCall(1)
          .returns({ _id: "map-id", visibility: { team: "team-id" } }),
        getFile: readStream,
        setItem: spy(),
        addItem: spy(),
        getMeta: stub()
          .onCall(0)
          .returns(null)
          .onCall(1)
          .returns({ _id: "id" }),
        getFileObject: stub().returns({
          filename: "countries.geojson",
          metadata: { mime: "" },
        }),
      });
    });

    afterEach(restore);

    it("can successfully analyze the file fields", async function () {
      await mapFileAnalyze(db, { id: "some-id" });

      db.setItem.lastCall.firstArg.should.equal("mapFile");
      const options = db.setItem.lastCall.lastArg.options;
      const fields = options.fields;

      BatchJobService.instance.upsertByName.lastCall.args.should.deep.equal([
        "mapFile.import file-id",
        "team-id",
      ]);

      options.destinationMap.should.equal("map-id");
      fields.should.have.length(5);

      fields[0].should.deep.equal({
        key: "Id",
        destination: "_id",
        preview: ["0"],
      });
    });
  });

  describe("for a list of JSON Lines features", function () {
    beforeEach(function () {
      readStream = createReadStream(
        resolve(__dirname, "..", "..", "_files", "features.jsonl")
      );
      BatchJobService.instance = createStubInstance(BatchJobService, {
        upsertByName: stub(),
      });

      db = createStubInstance(Db, {
        getItem: stub()
          .onCall(0)
          .returns({ _id: "file-id", mapFile: "5d7691374100fa0100497efc" })
          .onCall(1)
          .returns({ _id: "map-id", visibility: { team: "team-id" } }),
        getFile: readStream,
        setItem: spy(),
        addItem: spy(),
        getMeta: stub()
          .onCall(0)
          .returns(null)
          .onCall(1)
          .returns({ _id: "id" }),
        getFileObject: stub().returns({
          metadata: { mime: "text/ndjson" },
        }),
      });
    });

    afterEach(restore);

    it("can successfully analyze the file fields", async function () {
      await mapFileAnalyze(db, { id: "some-id" });

      db.setItem.lastCall.firstArg.should.equal("mapFile");
      const options = db.setItem.lastCall.lastArg.options;
      const fields = options.fields;

      BatchJobService.instance.upsertByName.lastCall.args.should.deep.equal([
        "mapFile.import file-id",
        "team-id",
      ]);

      options.destinationMap.should.equal("map-id");

      fields.should.have.length(5);

      fields[0].should.deep.equal({
        key: "Id",
        destination: "_id",
        preview: ["0"],
      });
    });
  });

  describe("for the oceans Json file", function () {
    let db;

    beforeEach(function () {
      readStream = createReadStream(
        resolve(__dirname, "..", "..", "_files", "oceans.geojson")
      );
      BatchJobService.instance = createStubInstance(BatchJobService, {
        upsertByName: stub(),
      });

      this.timeout(60000);
      db = createStubInstance(Db, {
        getItem: stub()
          .onCall(0)
          .returns({ _id: "file-id", mapFile: "5d7691374100fa0100497efc" })
          .onCall(1)
          .returns({ _id: "map-id", visibility: { team: "team-id" } }),
        getFile: readStream,
        setItem: spy(),
        addItem: spy(),
        getMeta: stub()
          .onCall(0)
          .returns(null)
          .onCall(1)
          .returns({ _id: "id" }),
        getFileObject: stub().returns({
          metadata: { mime: "text/json" },
        }),
      });
    });

    afterEach(restore);

    it("can successfully analyze the file fields", async function () {
      await mapFileAnalyze(db, { id: "some-id" });

      db.setItem.lastCall.firstArg.should.equal("mapFile");
      const options = db.setItem.lastCall.lastArg.options;
      const fields = options.fields;

      BatchJobService.instance.upsertByName.lastCall.args.should.deep.equal([
        "mapFile.import file-id",
        "team-id",
      ]);

      options.destinationMap.should.equal("map-id");

      fields.should.have.length(4);

      fields[0].should.deep.equal({
        destination: "attributes.other.scalerank",
        key: "scalerank",
        preview: ["0"],
      });
    });
  });
});
