/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import BatchJobService from "../../../source/lib/BatchJobService.js";
import { restore } from "sinon";
import Db from "models/source/db.js";
import fs from "fs";
import { resolve } from "path";
import { MongoMemoryServer } from "mongodb-memory-server";
import CalendarService from "../../../source/lib/CalendarService.js";
import { should } from "chai";
import { MetaQuery } from "../../../source/lib/MetaQuery.js";
should();

import path from "path";
import { fileURLToPath } from "url";
import { mapFileImportStream } from "../../../source/tasks/files/import.js";
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

describe("The mapFile.import task", function () {
  let db;
  let mapFile;
  let mongoServer;
  let readStream;
  let batchJobService;

  const resolvers = {
    queryMaps(terms) {
      return [`queried: ${terms}`];
    },

    queryIcons(terms) {
      return [`icons queried: ${terms}`];
    },

    queryUsers(terms) {
      return [`users queried: ${terms}`, "another user"];
    },

    handlePictures(terms) {
      return [`pictures preview queried: ${terms}`];
    },

    handleSounds(terms) {
      return [`sounds preview queried: ${terms}`];
    },
  };

  beforeEach(async function () {
    this.timeout(60000);

    mongoServer = await MongoMemoryServer.create();
    await mongoServer.getUri();

    readStream = fs.createReadStream(
      resolve(__dirname, "..", "..", "_files", "test.csv")
    );

    db = new Db({
      url: `mongodb://${mongoServer._instanceInfo.ip}:${mongoServer._instanceInfo.port}/tests`,
      configuration: { collection: "test" },
    });

    batchJobService = new BatchJobService(db);
    BatchJobService.instance = batchJobService;

    readStream = fs.createReadStream(
      resolve(__dirname, "..", "..", "_files", "test.csv")
    );

    mapFile = {
      _id: "60bd37e9cf0b02de7b998a36",
      options: {
        fields: [
          { key: "maps", destination: "maps" },
          { key: "name", destination: "name" },
          { key: "description", destination: "description" },
          { key: "position.geoJson", destination: "position" },
        ],
      },
    };

    await db.connectWithPromise();

    CalendarService.instance = { now: "now" };
    const metaQuery = new MetaQuery("MapFile", "mapFile.import", db);
    await metaQuery.set("60bd37e9cf0b02de7b998a36", {
      total: 100,
      errors: 0,
    });
  });

  afterEach(async function () {
    restore();

    await db.close();
    await mongoServer.stop();
  });

  it("can successfully import the file fields", async function () {
    readStream = fs.createReadStream(
      resolve(__dirname, "..", "..", "_files", "test.csv")
    );
    await mapFileImportStream(
      db,
      { metadata: { mime: "text/csv" } },
      readStream,
      mapFile,
      "team-id",
      resolvers
    );

    const list = [];
    await db.get("sites", (item) => {
      list.push(item);
    });

    list.length.should.equal(113);
    list[0].should.deep.contain({
      name: "Community Center",
      description: "the community center run by the city hall.\n\n#pingpong",
      maps: ["queried: 5ca89e42ef1f7e010007f5cb"],
      position: { coordinates: [24.498192, 47.133197], type: "Point" },
    });
  });

  it("can successfully import a file with one line", async function () {
    readStream = fs.createReadStream(
      resolve(__dirname, "..", "..", "_files", "short.csv")
    );
    await mapFileImportStream(
      db,
      { metadata: { mime: "text/csv" } },
      readStream,
      mapFile,
      "team-id",
      resolvers
    );

    const list = [];
    await db.get("sites", (item) => {
      list.push(item);
    });

    list.length.should.equal(1);
    list[0].should.deep.contain({
      name: "Community Center",
      description: "the community center run by the city hall.\n\n#pingpong",
      maps: ["queried: 5ca89e42ef1f7e010007f5cb"],
      position: { coordinates: [24.498192, 47.133197], type: "Point" },
    });
  });

  it("adds extra icons when they are set", async function () {
    readStream = fs.createReadStream(
      resolve(__dirname, "..", "..", "_files", "short.csv")
    );
    mapFile["options"]["extraIcons"] = ["other-icon"];
    await mapFileImportStream(
      db,
      { metadata: { mime: "text/csv" } },
      readStream,
      mapFile,
      "team-id",
      resolvers
    );

    const list = [];
    await db.get("sites", (item) => {
      list.push(item);
    });

    list.length.should.equal(1);
    list[0].icons.should.deep.equal(["icons queried: other-icon"]);
  });

  it("creates a batch run on import", async function () {
    readStream = fs.createReadStream(
      resolve(__dirname, "..", "..", "_files", "short.csv")
    );
    await mapFileImportStream(
      db,
      { metadata: { mime: "text/csv" } },
      readStream,
      mapFile,
      "team-id",
      resolvers
    );

    const list = [];
    await db.get("batchjobs", (item) => {
      list.push(item);
    });

    list.length.should.equal(1);
    list[0].runHistory.length.should.equal(1);

    list[0].runHistory[0].should.deep.contain({
      status: "success",
      duration: 0,
      total: 1,
      sent: 1,
      processed: 1,
      ignored: 0,
      errors: 0,
    });
  });

  it("starts an import if a batch job is scheduled", async function () {
    const batchName = `mapFile.import ${mapFile._id}`;
    CalendarService.instance = { now: "2020-01-01T00:05:00Z" };
    let batchJob = await batchJobService.upsertByName(batchName, "team-id");
    await batchJobService.schedule(batchName);

    await mapFileImportStream(
      db,
      { metadata: { mime: "text/csv" } },
      readStream,
      mapFile,
      "team-id",
      resolvers
    );

    batchJob = await batchJobService.getByName(batchName);
    batchJob.runHistory.length.should.equal(1);
  });

  it("does not start an import if a batch job is running", async function () {
    const batchName = `mapFile.import ${mapFile._id}`;
    CalendarService.instance = { now: "2020-01-01T00:05:00Z" };
    let batchJob = await batchJobService.upsertByName(batchName, "team-id");
    await batchJobService.start(batchName);

    await mapFileImportStream(
      db,
      { metadata: { mime: "text/csv" } },
      readStream,
      mapFile,
      "team-id",
      resolvers
    );

    batchJob = await batchJobService.getByName(batchName);
    batchJob.runHistory.length.should.equal(1);
  });

  it("does starts an import if a batch job is running for more than 5 mins", async function () {
    const batchName = `mapFile.import ${mapFile._id}`;

    let batchJob = await batchJobService.upsertByName(batchName, "team-id");

    let currentRun = await batchJobService.start(batchName);
    currentRun.ping = "2020-01-01T00:00:00Z";
    await batchJobService.updateRun(batchName, currentRun);

    CalendarService.instance = { now: "2020-01-01T00:05:00Z" };

    await mapFileImportStream(
      db,
      { metadata: { mime: "text/csv" } },
      readStream,
      mapFile,
      "team-id",
      resolvers
    );

    batchJob = await batchJobService.getByName(batchName);
    batchJob.runHistory.length.should.equal(2);

    batchJob.runHistory[0].status.should.equal("timeout");
    batchJob.runHistory[1].status.should.equal("success");
  });
});
