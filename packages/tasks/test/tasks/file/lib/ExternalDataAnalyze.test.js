/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { normalize } from "../../../../source/tasks/files/lib/base.js";
import { should } from "chai";
import { ExternalDataAnalyze } from "../../../../source/tasks/files/lib/ExternalDataAnalyze.js";
should();

describe("ExternalDataAnalyze", function () {
  let service;

  beforeEach(function () {
    service = new ExternalDataAnalyze();
  });

  it("adds an id field to the list", async function () {
    service.addData({
      _id: "5ca8b5c4ef1f7e0100081b4a",
    });

    service.fields.should.deep.equal([
      {
        key: "_id",
        destination: "_id",
        preview: ["5ca8b5c4ef1f7e0100081b4a"],
      },
    ]);
  });

  it("adds an id field once and new values to the preview", async function () {
    service.addData({ _id: "1" });
    service.addData({ _id: "2" });

    service.fields.should.deep.equal([
      {
        key: "_id",
        destination: "_id",
        preview: ["1", "2"],
      },
    ]);
  });

  it("adds an id field once and new values to the preview", async function () {
    service.addErrors([
      {
        type: "Quotes",
        code: "MissingQuotes",
        message: "",
        row: 1,
      },
    ]);

    service.fields.should.deep.equal([]);
    service.errors.should.deep.equal([
      {
        code: "Quotes.MissingQuotes",
        line: 1,
        col: -1,
      },
    ]);
  });

  it("maps feature field names", async function () {
    ExternalDataAnalyze.guessTransformedKey("_id").should.equal("_id");
    ExternalDataAnalyze.guessTransformedKey("maps").should.equal("maps");
    ExternalDataAnalyze.guessTransformedKey("description").should.equal(
      "description"
    );
    ExternalDataAnalyze.guessTransformedKey("position").should.equal(
      "position"
    );
    ExternalDataAnalyze.guessTransformedKey("pictures").should.equal(
      "pictures"
    );
    ExternalDataAnalyze.guessTransformedKey("sounds").should.equal("sounds");
    ExternalDataAnalyze.guessTransformedKey("icons").should.equal("icons");
    ExternalDataAnalyze.guessTransformedKey("visibility").should.equal(
      "visibility"
    );
    ExternalDataAnalyze.guessTransformedKey("contributors").should.equal(
      "contributors"
    );
  });

  it("maps the info fields", async function () {
    ExternalDataAnalyze.guessTransformedKey("info.createdOn").should.equal(
      "info.createdOn"
    );
    ExternalDataAnalyze.guessTransformedKey("info.lastChangeOn").should.equal(
      "info.lastChangeOn"
    );
    ExternalDataAnalyze.guessTransformedKey("info.author").should.equal(
      "info.author"
    );
    ExternalDataAnalyze.guessTransformedKey("info.originalAuthor").should.equal(
      "info.originalAuthor"
    );
    ExternalDataAnalyze.guessTransformedKey("author").should.equal(
      "info.originalAuthor"
    );
  });

  it("maps the sounds fields", async function () {
    ExternalDataAnalyze.guessTransformedKey("sounds").should.equal("sounds");
    ExternalDataAnalyze.guessTransformedKey("audio").should.equal("sounds");
  });

  it("maps generic field names", async function () {
    ExternalDataAnalyze.guessTransformedKey("id").should.equal("_id");
    ExternalDataAnalyze.guessTransformedKey("ID").should.equal("_id");
    ExternalDataAnalyze.guessTransformedKey("maps.name").should.equal("maps");
    ExternalDataAnalyze.guessTransformedKey("lon").should.equal("position.lon");
    ExternalDataAnalyze.guessTransformedKey("lat").should.equal("position.lat");
    ExternalDataAnalyze.guessTransformedKey("position.lon").should.equal(
      "position.lon"
    );
    ExternalDataAnalyze.guessTransformedKey("position.lat").should.equal(
      "position.lat"
    );
    ExternalDataAnalyze.guessTransformedKey("position.wkt").should.equal(
      "position"
    );
    ExternalDataAnalyze.guessTransformedKey("position.geoJson").should.equal(
      "position"
    );
    ExternalDataAnalyze.guessTransformedKey("pictures.name").should.equal(
      "pictures"
    );
    ExternalDataAnalyze.guessTransformedKey("icons.name").should.equal("icons");
    ExternalDataAnalyze.guessTransformedKey("visibility").should.equal(
      "visibility"
    );
    ExternalDataAnalyze.guessTransformedKey(
      "attributes.Wetlands.attr1"
    ).should.equal("attributes.Wetlands.attr1");
    ExternalDataAnalyze.guessTransformedKey("attributes.attr1").should.equal(
      "attributes.other.attr1"
    );
    ExternalDataAnalyze.guessTransformedKey("attr1").should.equal(
      "attributes.other.attr1"
    );
    ExternalDataAnalyze.guessTransformedKey("a.b.c").should.equal(
      "attributes.a.b_c"
    );
  });

  it("adds the objects inside an array with an array key", function () {
    service.addData({
      a: [
        {
          b: "c",
        },
      ],
    });

    service.fields.should.deep.equal([
      {
        key: "a[].b",
        destination: "attributes.a[].b",
        preview: ["c"],
      },
    ]);
  });

  it("adds an object with 2 levels as a single column", function () {
    service.addData({
      a: {
        b: "c",
      },
    });

    service.fields.should.deep.equal([
      {
        key: "a.b",
        destination: "attributes.a.b",
        preview: ["c"],
      },
    ]);
  });

  it("adds an object with 3 levels as a single column with an object value", function () {
    service.addData({
      a: {
        b: {
          c: "d",
        },
      },
    });

    service.fields.should.deep.equal([
      {
        key: "a.b",
        destination: "attributes.a.b",
        preview: [`{"c":"d"}`],
      },
    ]);
  });

  it("adds a geo json feature", async function () {
    service.addData(
      normalize({
        type: "Feature",
        properties: {
          Id: 0,
          Cod_LMI_04: "PH-II-m-A-16490.02",
        },
        geometry: {
          type: "MultiPolygon",
          coordinates: [],
        },
      })
    );

    service.fields.should.deep.equal([
      {
        key: "Id",
        destination: "_id",
        preview: ["0"],
      },
      {
        key: "Cod_LMI_04",
        destination: "attributes.other.Cod_LMI_04",
        preview: ["PH-II-m-A-16490.02"],
      },
      {
        key: "geometry",
        destination: "position",
        preview: ['{"type":"MultiPolygon","coordinates":[]}'],
      },
    ]);
  });
});
