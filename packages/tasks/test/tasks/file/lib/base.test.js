/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";

import { should } from "chai";
should();

import { registerNamedProjections } from "../../../../source/lib/crs.js";
registerNamedProjections();

import CalendarService from "../../../../source/lib/CalendarService.js";
import {
  normalize,
  simplifyMultiGeometry,
  transformRecord,
} from "../../../../source/tasks/files/lib/base.js";

describe("base", function () {
  const resolvers = {
    featureById(id) {
      return { _id: id };
    },

    featureByName(name) {
      return { name };
    },

    featureByRemoteId(remoteId) {
      return { source: { remoteId } };
    },

    queryMaps(terms) {
      return [`queried: ${terms}`];
    },

    queryIcons(terms) {
      return [`icons queried: ${terms}`];
    },

    queryUsers(terms) {
      return [`users queried: ${terms}`, "another user"];
    },

    handlePictures(terms) {
      return [`pictures preview queried: ${terms}`];
    },

    handleSounds(terms) {
      return [`sounds preview queried: ${terms}`];
    },

    queryMapIconSets() {
      return [];
    },

    getComputedVisibility() {
      return {};
    },
  };

  before(function () {
    CalendarService.instance = {
      now: "2022-02-03T09:22:23Z",
    };
  });

  describe("simplifyMultiGeometry", function () {
    it("ignores null", function () {
      const result = simplifyMultiGeometry(null);
      expect(result).to.be.null;
    });

    it("simplifies a multi polygon with one component", function () {
      const result = simplifyMultiGeometry({
        type: "MultiPolygon",
        coordinates: [[[[1, 2]]]],
      });

      expect(result).to.deep.equal({
        type: "Polygon",
        coordinates: [[[1, 2]]],
      });
    });

    it("simplifies a multi line with one component", function () {
      const result = simplifyMultiGeometry({
        type: "MultiLineString",
        coordinates: [[[1, 2]]],
      });

      expect(result).to.deep.equal({
        type: "LineString",
        coordinates: [[1, 2]],
      });
    });

    it("simplifies a multi point with one component", function () {
      const result = simplifyMultiGeometry({
        type: "MultiPoint",
        coordinates: [[1, 2]],
      });

      expect(result).to.deep.equal({
        type: "Point",
        coordinates: [1, 2],
      });
    });
  });

  describe("normalize", function () {
    it("groups the properties of a geojson feature with the geometry", function () {
      const result = normalize({
        type: "Feature",
        properties: {
          Id: 0,
          a: "b",
        },
        geometry: {
          type: "Point",
          coordinates: [1, 2],
        },
      });

      result.should.deep.equal({
        Id: 0,
        a: "b",
        geometry: {
          type: "Point",
          coordinates: [1, 2],
        },
      });
    });
  });

  describe("transformRecord", function () {
    it("queries the resolver feature byId when updateBy is _id", async function () {
      const result = await transformRecord(
        { id: "id" },
        { updateBy: "_id", fields: [{ key: "id", destination: "_id" }] },
        resolvers
      );

      result.should.deep.equal({
        errors: [],
        validationError: {
          code: "missing-fields",
          message:
            "The feature must have all the required fields: maps, name, description, position.",
          vars: "maps, name, description, position",
        },
        result: {
          _id: "id",
          visibility: 1,
          computedVisibility: {},
          info: {
            changeIndex: 0,
            createdOn: new Date("2022-02-03T09:22:23Z"),
            lastChangeOn: new Date("2022-02-03T09:22:23Z"),
          },
        },
      });
    });

    it("queries the resolver feature byName when updateBy is name", async function () {
      const result = await transformRecord(
        { n: "name" },
        {
          updateBy: "name",
          fields: [
            { key: "id", destination: "_id" },
            { key: "n", destination: "name" },
          ],
        },
        resolvers
      );

      result.should.deep.equal({
        errors: [],
        validationError: {
          code: "missing-fields",
          message:
            "The feature must have all the required fields: maps, description, position.",
          vars: "maps, description, position",
        },
        result: {
          name: "name",
          visibility: 1,
          computedVisibility: {},
          info: {
            changeIndex: 0,
            createdOn: new Date("2022-02-03T09:22:23Z"),
            lastChangeOn: new Date("2022-02-03T09:22:23Z"),
          },
        },
      });
    });

    it("queries the resolver feature byRemoteId when updateBy is remoteId", async function () {
      const result = await transformRecord(
        { n: "name", remoteId: "1" },
        {
          updateBy: "remoteId",
          fields: [
            { key: "id", destination: "_id" },
            { key: "n", destination: "name" },
          ],
        },
        resolvers
      );

      result.should.deep.equal({
        errors: [],
        validationError: {
          code: "missing-fields",
          message:
            "The feature must have all the required fields: maps, description, position.",
          vars: "maps, description, position",
        },
        result: {
          source: { remoteId: "1" },
          info: {
            createdOn: new Date("2022-02-03T09:22:23Z"),
            lastChangeOn: new Date("2022-02-03T09:22:23Z"),
            changeIndex: 0,
          },
          computedVisibility: {},
          visibility: 1,
          name: "name",
        },
      });
    });

    it("converts the name, description and lon/lat position to a feature", async function () {
      const result = await transformRecord(
        {
          n: "name",
          d: "description",
          lon: 1,
          lat: 2,
        },
        {
          fields: [
            { key: "n", destination: "name" },
            { key: "d", destination: "description" },
            { key: "lon", destination: "position.lon" },
            { key: "lat", destination: "position.lat" },
          ],
        }
      );

      result.should.deep.equal({
        errors: [],
        validationError: {
          code: "empty-maps",
          message: "The feature must have at least one map.",
        },
        result: {
          maps: [],
          icons: [],
          name: "name",
          description: "description",
          visibility: 1,
          position: {
            type: "Point",
            coordinates: [1, 2],
          },
          info: {
            changeIndex: 1,
            createdOn: new Date("2022-02-03T09:22:23Z"),
            lastChangeOn: new Date("2022-02-03T09:22:23Z"),
          },
        },
      });
    });

    it("throws when the lat is not a number", async function () {
      const result = await transformRecord(
        {
          n: "name",
          d: "description",
          lon: "",
          lat: "",
        },
        {
          fields: [
            { key: "n", destination: "name" },
            { key: "d", destination: "description" },
            { key: "lon", destination: "position.lon" },
            { key: "lat", destination: "position.lat" },
          ],
        }
      );

      result.should.deep.equal({
        errors: [
          {
            destination: "position.lon",
            error: "Error: The `lon` value is not a number.",
            key: "lon",
          },
          {
            destination: "position.lat",
            error: "Error: The `lat` value is not a number.",
            key: "lat",
          },
        ],
        validationError: {
          code: "empty-maps",
          message: "The feature must have at least one map.",
        },
        result: {
          maps: [],
          icons: [],
          name: "name",
          visibility: 1,
          description: "description",
          position: {
            type: "Point",
            coordinates: [0, 0],
          },
          info: {
            changeIndex: 1,
            createdOn: new Date("2022-02-03T09:22:23Z"),
            lastChangeOn: new Date("2022-02-03T09:22:23Z"),
          },
        },
      });
    });

    it("converts the name, description from a nested source to a feature", async function () {
      const result = await transformRecord(
        {
          n: { n: "name" },
          "d.d": "description",
        },
        {
          fields: [
            { key: "n.n", destination: "name" },
            { key: "d.d", destination: "description" },
          ],
        }
      );

      result.should.deep.equal({
        errors: [],
        validationError: {
          code: "empty-maps",
          message: "The feature must have at least one map.",
        },
        result: {
          maps: [],
          icons: [],
          name: "name",
          description: "description",
          visibility: 1,
          position: {
            type: "Point",
            coordinates: [0, 0],
          },
          info: {
            changeIndex: 1,
            createdOn: new Date("2022-02-03T09:22:23Z"),
            lastChangeOn: new Date("2022-02-03T09:22:23Z"),
          },
        },
      });
    });

    it("does not set the visibility to 1 when it is set by the integration", async function () {
      const result = await transformRecord(
        {
          n: { n: "name" },
          "d.d": "description",
          v: "0",
        },
        {
          fields: [
            { key: "n.n", destination: "name" },
            { key: "d.d", destination: "description" },
            { key: "v", destination: "visibility" },
          ],
        }
      );

      result.result.visibility.should.equal(0);
    });

    it("ignores an invalid feature key", async function () {
      const result = await transformRecord(
        {
          n: "name",
        },
        { fields: [{ key: "n", destination: "invalid" }] }
      );

      result.should.deep.equal({
        errors: [
          {
            error: `invalid must begin with "attributes."`,
            key: "n",
            destination: "invalid",
          },
        ],
        validationError: {
          code: "empty-name",
          message: "The feature must have a name.",
        },
        result: {
          maps: [],
          icons: [],
          name: "",
          visibility: 1,
          description: "",
          position: {
            type: "Point",
            coordinates: [0, 0],
          },
          info: {
            changeIndex: 1,
            createdOn: new Date("2022-02-03T09:22:23Z"),
            lastChangeOn: new Date("2022-02-03T09:22:23Z"),
          },
        },
      });
    });

    describe("the source attribute", function () {
      it("adds the source section when the record has a remoteId", async function () {
        const result = await transformRecord(
          {
            remoteId: "source-id",
          },
          {
            fields: [],
            source: {
              type: "DataBinding",
              modelId: "databinding-id",
            },
          }
        );

        result.result.should.deep.equal({
          maps: [],
          icons: [],
          name: "",
          description: "",
          visibility: 1,
          position: {
            type: "Point",
            coordinates: [0, 0],
          },
          source: {
            type: "DataBinding",
            modelId: "databinding-id",
            remoteId: "source-id",
            syncAt: new Date("2022-02-03T09:22:23Z"),
          },
          info: {
            changeIndex: 1,
            createdOn: new Date("2022-02-03T09:22:23Z"),
            lastChangeOn: new Date("2022-02-03T09:22:23Z"),
          },
        });
      });

      it("adds the source section when the record has no remoteId but the options have one", async function () {
        const result = await transformRecord(
          {},
          {
            fields: [],
            source: {
              type: "DataBinding",
              modelId: "databinding-id",
            },
          }
        );

        result.result.should.deep.equal({
          maps: [],
          icons: [],
          name: "",
          visibility: 1,
          description: "",
          position: {
            type: "Point",
            coordinates: [0, 0],
          },
          source: {
            type: "DataBinding",
            modelId: "databinding-id",
            syncAt: new Date("2022-02-03T09:22:23Z"),
          },
          info: {
            changeIndex: 1,
            createdOn: new Date("2022-02-03T09:22:23Z"),
            lastChangeOn: new Date("2022-02-03T09:22:23Z"),
          },
        });
      });

      it("does not add the source when the options.source are missing", async function () {
        const result = await transformRecord(
          {
            remoteId: "source-id",
          },
          {
            fields: [],
          }
        );

        result.result.should.deep.equal({
          maps: [],
          icons: [],
          name: "",
          visibility: 1,
          description: "",
          position: {
            type: "Point",
            coordinates: [0, 0],
          },
          info: {
            changeIndex: 1,
            createdOn: new Date("2022-02-03T09:22:23Z"),
            lastChangeOn: new Date("2022-02-03T09:22:23Z"),
          },
        });
      });
    });

    describe("parsing the attributes", function () {
      it("ignores an invalid attribute key", async function () {
        const result = await transformRecord(
          {
            a: "name1",
            b: "name2",
            c: "name3",
          },
          {
            fields: [
              { key: "a", destination: "attributes.invalid" },
              { key: "b", destination: "attributes.invalid.b.c" },
              { key: "c", destination: "a.b.c" },
            ],
          }
        );

        result.should.deep.equal({
          errors: [
            {
              error: `attributes.invalid must have only 3 "."`,
              key: "a",
              destination: "attributes.invalid",
            },
            {
              error: `attributes.invalid.b.c must have only 3 "."`,
              key: "b",
              destination: "attributes.invalid.b.c",
            },
            {
              error: `a.b.c must begin with "attributes."`,
              key: "c",
              destination: "a.b.c",
            },
          ],
          validationError: {
            code: "empty-name",
            message: "The feature must have a name.",
          },
          result: {
            maps: [],
            icons: [],
            name: "",
            visibility: 1,
            description: "",
            position: {
              type: "Point",
              coordinates: [0, 0],
            },
            info: {
              changeIndex: 1,
              createdOn: new Date("2022-02-03T09:22:23Z"),
              lastChangeOn: new Date("2022-02-03T09:22:23Z"),
            },
          },
        });
      });

      it("sets an attribute value", async function () {
        const result = await transformRecord(
          {
            n: "name",
          },
          { fields: [{ key: "n", destination: "attributes.some group.value" }] }
        );

        result.should.deep.equal({
          errors: [],
          validationError: {
            code: "empty-name",
            message: "The feature must have a name.",
          },
          result: {
            maps: [],
            icons: [],
            name: "",
            visibility: 1,
            description: "",
            position: {
              type: "Point",
              coordinates: [0, 0],
            },
            attributes: {
              "some group": {
                value: "name",
              },
            },
            info: {
              changeIndex: 1,
              createdOn: new Date("2022-02-03T09:22:23Z"),
              lastChangeOn: new Date("2022-02-03T09:22:23Z"),
            },
          },
        });
      });

      it("sets an attribute object", async function () {
        const result = await transformRecord(
          {
            n: { key: "value" },
          },
          { fields: [{ key: "n", destination: "attributes.some group" }] }
        );

        result.should.deep.equal({
          errors: [],
          validationError: {
            code: "empty-name",
            message: "The feature must have a name.",
          },
          result: {
            maps: [],
            icons: [],
            name: "",
            visibility: 1,
            description: "",
            position: {
              type: "Point",
              coordinates: [0, 0],
            },
            attributes: {
              "some group": { key: "value" },
            },
            info: {
              changeIndex: 1,
              createdOn: new Date("2022-02-03T09:22:23Z"),
              lastChangeOn: new Date("2022-02-03T09:22:23Z"),
            },
          },
        });
      });

      it("sets an attribute list of objects", async function () {
        const result = await transformRecord(
          {
            n: [{ key: "value1" }, { key: "value2" }],
          },
          { fields: [{ key: "n", destination: "attributes.some group" }] }
        );

        result.should.deep.equal({
          errors: [],
          validationError: {
            code: "empty-name",
            message: "The feature must have a name.",
          },
          result: {
            maps: [],
            icons: [],
            name: "",
            visibility: 1,
            description: "",
            position: {
              type: "Point",
              coordinates: [0, 0],
            },
            attributes: {
              "some group": [{ key: "value1" }, { key: "value2" }],
            },
            info: {
              changeIndex: 1,
              createdOn: new Date("2022-02-03T09:22:23Z"),
              lastChangeOn: new Date("2022-02-03T09:22:23Z"),
            },
          },
        });
      });

      it("ignores a list of non object values", async function () {
        const result = await transformRecord(
          {
            a: [1, "3"],
            b: [[], []],
          },
          {
            fields: [
              { key: "a", destination: "attributes.some group" },
              { key: "b", destination: "attributes.some group" },
            ],
          }
        );

        result.should.deep.equal({
          errors: [
            {
              error: "attributes.some group must contain only objects",
              key: "a",
              destination: "attributes.some group",
            },
            {
              error: "attributes.some group must contain only objects",
              key: "b",
              destination: "attributes.some group",
            },
          ],
          validationError: {
            code: "empty-name",
            message: "The feature must have a name.",
          },
          result: {
            maps: [],
            icons: [],
            name: "",
            visibility: 1,
            description: "",
            position: {
              type: "Point",
              coordinates: [0, 0],
            },
            info: {
              changeIndex: 1,
              createdOn: new Date("2022-02-03T09:22:23Z"),
              lastChangeOn: new Date("2022-02-03T09:22:23Z"),
            },
          },
        });
      });

      it("sets an attribute list of objects", async function () {
        const result = await transformRecord(
          {
            n: [{ key: "value1" }, {}, { key: "value2" }],
          },
          {
            fields: [
              { key: "n[].key", destination: "attributes.some group.other" },
            ],
          }
        );

        result.should.deep.equal({
          errors: [],
          validationError: {
            code: "empty-name",
            message: "The feature must have a name.",
          },
          result: {
            maps: [],
            icons: [],
            name: "",
            visibility: 1,
            description: "",
            position: {
              type: "Point",
              coordinates: [0, 0],
            },
            attributes: {
              "some group": [{ other: "value1" }, { other: "value2" }],
            },
            info: {
              changeIndex: 1,
              createdOn: new Date("2022-02-03T09:22:23Z"),
              lastChangeOn: new Date("2022-02-03T09:22:23Z"),
            },
          },
        });
      });
    });

    describe("parsing the position field", function () {
      it("converts the wkt position to a feature", async function () {
        const result = await transformRecord(
          {
            pos: "POINT(1 2)",
          },
          { fields: [{ key: "pos", destination: "position" }] }
        );

        result.should.deep.equal({
          errors: [],
          validationError: {
            code: "empty-name",
            message: "The feature must have a name.",
          },
          result: {
            maps: [],
            icons: [],
            name: "",
            visibility: 1,
            description: "",
            position: {
              type: "Point",
              coordinates: [1, 2],
            },
            info: {
              changeIndex: 1,
              createdOn: new Date("2022-02-03T09:22:23Z"),
              lastChangeOn: new Date("2022-02-03T09:22:23Z"),
            },
          },
        });
      });

      it("converts to another crs", async function () {
        const result = await transformRecord(
          {
            pos: {
              type: "Point",
              coordinates: [562451.522585529834032, 392321.304685156792402],
            },
          },
          {
            crs: "urn:ogc:def:crs:EPSG::31700",
            fields: [{ key: "pos", destination: "position" }],
          }
        );

        result.should.deep.equal({
          errors: [],
          validationError: {
            code: "empty-name",
            message: "The feature must have a name.",
          },
          result: {
            maps: [],
            icons: [],
            name: "",
            visibility: 1,
            description: "",
            position: {
              type: "Point",
              coordinates: [25.791047601388794, 45.02789292569293],
            },
            info: {
              changeIndex: 1,
              createdOn: new Date("2022-02-03T09:22:23Z"),
              lastChangeOn: new Date("2022-02-03T09:22:23Z"),
            },
          },
        });
      });

      it("converts the geojson string position to a feature", async function () {
        const result = await transformRecord(
          {
            pos: `{ "type": "Point", "coordinates": [3, 4] }`,
          },
          { fields: [{ key: "pos", destination: "position" }] }
        );

        result.should.deep.equal({
          errors: [],
          validationError: {
            code: "empty-name",
            message: "The feature must have a name.",
          },
          result: {
            maps: [],
            icons: [],
            name: "",
            visibility: 1,
            description: "",
            position: {
              type: "Point",
              coordinates: [3, 4],
            },
            info: {
              changeIndex: 1,
              createdOn: new Date("2022-02-03T09:22:23Z"),
              lastChangeOn: new Date("2022-02-03T09:22:23Z"),
            },
          },
        });
      });

      it("converts the geojson object position to a feature", async function () {
        const result = await transformRecord(
          {
            pos: { type: "Point", coordinates: [3, 4] },
          },
          { fields: [{ key: "pos", destination: "position" }] }
        );

        result.should.deep.equal({
          errors: [],
          validationError: {
            code: "empty-name",
            message: "The feature must have a name.",
          },
          result: {
            maps: [],
            icons: [],
            name: "",
            visibility: 1,
            description: "",
            position: {
              type: "Point",
              coordinates: [3, 4],
            },
            info: {
              changeIndex: 1,
              createdOn: new Date("2022-02-03T09:22:23Z"),
              lastChangeOn: new Date("2022-02-03T09:22:23Z"),
            },
          },
        });
      });

      it("ignores an invalid position string", async function () {
        const result = await transformRecord(
          {
            pos: `invalid`,
          },
          { fields: [{ key: "pos", destination: "position" }] }
        );

        result.should.deep.equal({
          errors: [
            {
              error: "Error: Expected geometry type",
              key: "pos",
              destination: "position",
            },
          ],
          validationError: {
            code: "empty-name",
            message: "The feature must have a name.",
          },
          result: {
            maps: [],
            icons: [],
            name: "",
            visibility: 1,
            description: "",
            position: {
              type: "Point",
              coordinates: [0, 0],
            },
            info: {
              changeIndex: 1,
              createdOn: new Date("2022-02-03T09:22:23Z"),
              lastChangeOn: new Date("2022-02-03T09:22:23Z"),
            },
          },
        });
      });
    });

    describe("parsing the info fields", function () {
      it("processes the info.author field", async function () {
        const result = await transformRecord(
          {
            a: "user-id",
          },
          { fields: [{ key: "a", destination: "info.author" }] },
          resolvers
        );

        result.should.deep.equal({
          errors: [],
          validationError: {
            code: "empty-name",
            message: "The feature must have a name.",
          },
          result: {
            maps: [],
            icons: [],
            name: "",
            visibility: 1,
            computedVisibility: {},
            description: "",
            info: {
              author: "users queried: user-id",
              changeIndex: 1,
              createdOn: new Date("2022-02-03T09:22:23Z"),
              lastChangeOn: new Date("2022-02-03T09:22:23Z"),
            },
            position: {
              type: "Point",
              coordinates: [0, 0],
            },
          },
        });
      });

      it("processes the info.originalAuthor field", async function () {
        const result = await transformRecord(
          {
            a: "user-id",
          },
          { fields: [{ key: "a", destination: "info.originalAuthor" }] },
          resolvers
        );

        result.should.deep.equal({
          errors: [],
          validationError: {
            code: "empty-name",
            message: "The feature must have a name.",
          },
          result: {
            maps: [],
            icons: [],
            visibility: 1,
            computedVisibility: {},
            name: "",
            description: "",
            info: {
              changeIndex: 1,
              createdOn: new Date("2022-02-03T09:22:23Z"),
              lastChangeOn: new Date("2022-02-03T09:22:23Z"),
              originalAuthor: "users queried: user-id",
            },
            position: {
              type: "Point",
              coordinates: [0, 0],
            },
          },
        });
      });

      it("processes a valid info.createdOn field", async function () {
        const result = await transformRecord(
          {
            a: "2018-01-01T14:00:00.000Z",
          },
          { fields: [{ key: "a", destination: "info.createdOn" }] },
          resolvers
        );

        result.should.deep.equal({
          errors: [],
          validationError: {
            code: "empty-name",
            message: "The feature must have a name.",
          },
          result: {
            maps: [],
            icons: [],
            name: "",
            description: "",
            visibility: 1,
            computedVisibility: {},
            info: {
              changeIndex: 1,
              lastChangeOn: new Date("2022-02-03T09:22:23Z"),
              createdOn: "2018-01-01T14:00:00.000Z",
            },
            position: {
              type: "Point",
              coordinates: [0, 0],
            },
          },
        });
      });

      it("processes a valid info.createdOn field", async function () {
        const result = await transformRecord(
          {
            a: "2019-01-01T14:00:00.000Z",
          },
          { fields: [{ key: "a", destination: "info.lastChangeOn" }] },
          resolvers
        );

        result.should.deep.equal({
          errors: [],
          validationError: {
            code: "empty-name",
            message: "The feature must have a name.",
          },
          result: {
            maps: [],
            icons: [],
            name: "",
            visibility: 1,
            computedVisibility: {},
            description: "",
            info: {
              changeIndex: 1,
              createdOn: new Date("2022-02-03T09:22:23Z"),
              lastChangeOn: "2019-01-01T14:00:00.000Z",
            },
            position: {
              type: "Point",
              coordinates: [0, 0],
            },
          },
        });
      });

      it("ignores an invalid date fields", async function () {
        const result = await transformRecord(
          {
            a: "2invalid",
          },
          {
            fields: [
              { key: "a", destination: "info.createdOn" },
              { key: "b", destination: "info.lastChangeOn" },
            ],
          },
          resolvers
        );

        result.should.deep.equal({
          errors: [
            {
              error: "Invalid date time format",
              key: "a",
              destination: "info.createdOn",
            },
          ],
          validationError: {
            code: "empty-name",
            message: "The feature must have a name.",
          },
          result: {
            maps: [],
            icons: [],
            name: "",
            computedVisibility: {},
            visibility: 1,
            description: "",
            position: {
              type: "Point",
              coordinates: [0, 0],
            },
            info: {
              changeIndex: 1,
              createdOn: new Date("2022-02-03T09:22:23Z"),
              lastChangeOn: new Date("2022-02-03T09:22:23Z"),
            },
          },
        });
      });
    });

    describe("parsing the visibility field", function () {
      it("sets 1 as visibility", async function () {
        const result = await transformRecord(
          {
            vis: 1,
          },
          { fields: [{ key: "vis", destination: "visibility" }] }
        );

        result.should.deep.equal({
          errors: [],
          validationError: {
            code: "empty-name",
            message: "The feature must have a name.",
          },
          result: {
            maps: [],
            icons: [],
            name: "",
            description: "",
            visibility: 1,
            position: {
              type: "Point",
              coordinates: [0, 0],
            },
            info: {
              changeIndex: 1,
              createdOn: new Date("2022-02-03T09:22:23Z"),
              lastChangeOn: new Date("2022-02-03T09:22:23Z"),
            },
          },
        });
      });

      it("sets Approved as visibility public", async function () {
        const transformed = await transformRecord(
          {
            vis: "Approved",
          },
          { fields: [{ key: "vis", destination: "visibility" }] }
        );

        transformed.result.visibility.should.equal(1);
      });

      it("sets Rejected as visibility private", async function () {
        const transformed = await transformRecord(
          {
            vis: "Rejected",
          },
          { fields: [{ key: "vis", destination: "visibility" }] }
        );

        transformed.result.visibility.should.equal(0);
      });

      it("sets Pending as visibility pending", async function () {
        const transformed = await transformRecord(
          {
            vis: "Pending",
          },
          { fields: [{ key: "vis", destination: "visibility" }] }
        );

        transformed.result.visibility.should.equal(-1);
      });

      it("when visibility is 2 it is set as 0, because is invalid", async function () {
        const result = await transformRecord(
          {
            vis: 2,
          },
          { fields: [{ key: "vis", destination: "visibility" }] }
        );

        result.should.deep.equal({
          errors: [],
          validationError: {
            code: "empty-name",
            message: "The feature must have a name.",
          },
          result: {
            maps: [],
            icons: [],
            name: "",
            description: "",
            visibility: 0,
            position: {
              type: "Point",
              coordinates: [0, 0],
            },
            info: {
              changeIndex: 1,
              createdOn: new Date("2022-02-03T09:22:23Z"),
              lastChangeOn: new Date("2022-02-03T09:22:23Z"),
            },
          },
        });
      });
    });

    describe("parsing the maps field", function () {
      it("queries the map and it ads it to the new feature", async function () {
        const result = await transformRecord(
          {
            map: "some;map;name",
          },
          { fields: [{ key: "map", destination: "maps" }] },
          resolvers
        );

        result.should.deep.equal({
          errors: [],
          validationError: {
            code: "empty-name",
            message: "The feature must have a name.",
          },
          result: {
            maps: ["queried: some,map,name"],
            icons: [],
            name: "",
            visibility: 1,
            computedVisibility: {},
            description: "",
            position: {
              type: "Point",
              coordinates: [0, 0],
            },
            info: {
              changeIndex: 1,
              createdOn: new Date("2022-02-03T09:22:23Z"),
              lastChangeOn: new Date("2022-02-03T09:22:23Z"),
            },
          },
        });
      });

      it("queries the maps when they are set as an array", async function () {
        const result = await transformRecord(
          {
            map: ["1", 2],
          },
          { fields: [{ key: "map", destination: "maps" }] },
          resolvers
        );

        result.should.deep.equal({
          errors: [],
          validationError: {
            code: "empty-name",
            message: "The feature must have a name.",
          },
          result: {
            maps: ["queried: 1,2"],
            icons: [],
            name: "",
            description: "",
            visibility: 1,
            computedVisibility: {},
            position: {
              type: "Point",
              coordinates: [0, 0],
            },
            info: {
              changeIndex: 1,
              createdOn: new Date("2022-02-03T09:22:23Z"),
              lastChangeOn: new Date("2022-02-03T09:22:23Z"),
            },
          },
        });
      });

      it("sets the map as an empty array when is an object", async function () {
        const result = await transformRecord(
          { map: {} },
          { fields: [{ key: "map", destination: "maps" }] },
          resolvers
        );

        result.should.deep.equal({
          errors: [],
          validationError: {
            code: "empty-name",
            message: "The feature must have a name.",
          },
          result: {
            maps: [],
            icons: [],
            name: "",
            visibility: 1,
            computedVisibility: {},
            description: "",
            position: {
              type: "Point",
              coordinates: [0, 0],
            },
            info: {
              changeIndex: 1,
              createdOn: new Date("2022-02-03T09:22:23Z"),
              lastChangeOn: new Date("2022-02-03T09:22:23Z"),
            },
          },
        });
      });

      it("sets the destination map option when is set", async function () {
        const result = await transformRecord(
          { map: "some;map;name" },
          {
            destinationMap: "map-id",
            fields: [{ key: "map", destination: "maps" }],
          },
          resolvers
        );

        result.should.deep.equal({
          errors: [],
          validationError: {
            code: "empty-name",
            message: "The feature must have a name.",
          },
          result: {
            maps: ["queried: map-id"],
            visibility: 1,
            computedVisibility: {},
            icons: [],
            name: "",
            description: "",
            position: {
              type: "Point",
              coordinates: [0, 0],
            },
            info: {
              changeIndex: 1,
              createdOn: new Date("2022-02-03T09:22:23Z"),
              lastChangeOn: new Date("2022-02-03T09:22:23Z"),
            },
          },
        });
      });
    });

    describe("parsing the icons field", function () {
      it("queries the icons and it ads it to the new feature", async function () {
        const result = await transformRecord(
          {
            icons: "some;icon;name",
          },
          { fields: [{ key: "icons", destination: "icons" }] },
          resolvers
        );

        result.should.deep.equal({
          errors: [],
          validationError: {
            code: "empty-name",
            message: "The feature must have a name.",
          },
          result: {
            icons: ["icons queried: some,icon,name"],
            maps: [],
            name: "",
            visibility: 1,
            computedVisibility: {},
            description: "",
            position: {
              type: "Point",
              coordinates: [0, 0],
            },
            info: {
              changeIndex: 1,
              createdOn: new Date("2022-02-03T09:22:23Z"),
              lastChangeOn: new Date("2022-02-03T09:22:23Z"),
            },
          },
        });
      });

      it("queries the icons when they are set as an array", async function () {
        const result = await transformRecord(
          { icons: ["1", "2"] },
          { fields: [{ key: "icons", destination: "icons" }] },
          resolvers
        );

        result.should.deep.equal({
          errors: [],
          validationError: {
            code: "empty-name",
            message: "The feature must have a name.",
          },
          result: {
            maps: [],
            icons: ["icons queried: 1,2"],
            name: "",
            description: "",
            visibility: 1,
            computedVisibility: {},
            position: {
              type: "Point",
              coordinates: [0, 0],
            },
            info: {
              changeIndex: 1,
              createdOn: new Date("2022-02-03T09:22:23Z"),
              lastChangeOn: new Date("2022-02-03T09:22:23Z"),
            },
          },
        });
      });

      it("appends the icons when there are 2 icons fields", async function () {
        const result = await transformRecord(
          { icons1: ["1", "2"], icons2: ["3", "4"] },
          {
            fields: [
              { key: "icons1", destination: "icons" },
              { key: "icons2", destination: "icons" },
            ],
          },
          resolvers
        );

        result.result.icons.should.deep.equal([
          "icons queried: 1,2",
          "icons queried: 3,4",
        ]);
      });

      it("sets the icons as an empty array when is an object", async function () {
        const result = await transformRecord(
          { icons: {} },
          { fields: [{ key: "icons", destination: "icons" }] },
          resolvers
        );

        result.should.deep.equal({
          errors: [],
          validationError: {
            code: "empty-name",
            message: "The feature must have a name.",
          },
          result: {
            maps: [],
            icons: [],
            name: "",
            visibility: 1,
            computedVisibility: {},
            description: "",
            position: {
              type: "Point",
              coordinates: [0, 0],
            },
            info: {
              changeIndex: 1,
              createdOn: new Date("2022-02-03T09:22:23Z"),
              lastChangeOn: new Date("2022-02-03T09:22:23Z"),
            },
          },
        });
      });
    });

    describe("parsing the primary icon field", function () {
      it("queries the icons and it adds it to the new feature", async function () {
        const result = await transformRecord(
          {
            icons: "some;icon;name",
          },
          { fields: [{ key: "icons", destination: "primary icon" }] },
          resolvers
        );

        result.should.deep.equal({
          errors: [],
          validationError: {
            code: "empty-name",
            message: "The feature must have a name.",
          },
          result: {
            icons: ["icons queried: some"],
            maps: [],
            name: "",
            description: "",
            visibility: 1,
            computedVisibility: {},
            position: {
              type: "Point",
              coordinates: [0, 0],
            },
            info: {
              changeIndex: 1,
              createdOn: new Date("2022-02-03T09:22:23Z"),
              lastChangeOn: new Date("2022-02-03T09:22:23Z"),
            },
          },
        });
      });

      it("queries the the first array icon when it's a list", async function () {
        const result = await transformRecord(
          { icons: ["1", "2"] },
          { fields: [{ key: "icons", destination: "primary icon" }] },
          resolvers
        );

        result.should.deep.equal({
          errors: [],
          validationError: {
            code: "empty-name",
            message: "The feature must have a name.",
          },
          result: {
            maps: [],
            icons: ["icons queried: 1"],
            name: "",
            visibility: 1,
            computedVisibility: {},
            description: "",
            position: {
              type: "Point",
              coordinates: [0, 0],
            },
            info: {
              changeIndex: 1,
              createdOn: new Date("2022-02-03T09:22:23Z"),
              lastChangeOn: new Date("2022-02-03T09:22:23Z"),
            },
          },
        });
      });

      it("sets the icons as an empty array when it is an object", async function () {
        const result = await transformRecord(
          { icons: {} },
          { fields: [{ key: "icons", destination: "primary icon" }] },
          resolvers
        );

        result.should.deep.equal({
          errors: [],
          validationError: {
            code: "empty-name",
            message: "The feature must have a name.",
          },
          result: {
            maps: [],
            icons: [],
            name: "",
            description: "",
            visibility: 1,
            computedVisibility: {},
            position: {
              type: "Point",
              coordinates: [0, 0],
            },
            info: {
              changeIndex: 1,
              createdOn: new Date("2022-02-03T09:22:23Z"),
              lastChangeOn: new Date("2022-02-03T09:22:23Z"),
            },
          },
        });
      });

      it("adds the primary icon before the existing ones", async function () {
        const result = await transformRecord(
          { icons: ["1", "2"], primaryIcon: ["3"] },
          {
            fields: [
              { key: "icons", destination: "icons" },
              { key: "primaryIcon", destination: "primary icon" },
            ],
          },
          resolvers
        );

        result.result.icons.should.deep.equal([
          "icons queried: 3",
          "icons queried: 1,2",
        ]);
      });

      it("removes duplicated icons", async function () {
        const result = await transformRecord(
          { icons: ["3"], primaryIcon: ["3"] },
          {
            fields: [
              { key: "icons", destination: "icons" },
              { key: "primaryIcon", destination: "primary icon" },
            ],
          },
          resolvers
        );

        result.result.icons.should.deep.equal(["icons queried: 3"]);
      });
    });

    describe("parsing the pictures field", function () {
      it("queries the pictures and it ads it to the new feature", async function () {
        const result = await transformRecord(
          {
            pictures: "some;pictures;name",
          },
          { fields: [{ key: "pictures", destination: "pictures" }] },
          resolvers
        );

        result.should.deep.equal({
          errors: [],
          validationError: {
            code: "empty-name",
            message: "The feature must have a name.",
          },
          result: {
            pictures: ["pictures preview queried: some,pictures,name"],
            maps: [],
            icons: [],
            name: "",
            description: "",
            visibility: 1,
            computedVisibility: {},
            position: {
              type: "Point",
              coordinates: [0, 0],
            },
            info: {
              changeIndex: 1,
              createdOn: new Date("2022-02-03T09:22:23Z"),
              lastChangeOn: new Date("2022-02-03T09:22:23Z"),
            },
          },
        });
      });

      it("queries the pictures when they are set as an array", async function () {
        const result = await transformRecord(
          { pictures: ["1", "2"] },
          { fields: [{ key: "pictures", destination: "pictures" }] },
          resolvers
        );

        result.should.deep.equal({
          errors: [],
          validationError: {
            code: "empty-name",
            message: "The feature must have a name.",
          },
          result: {
            maps: [],
            icons: [],
            pictures: ["pictures preview queried: 1,2"],
            name: "",
            description: "",
            visibility: 1,
            computedVisibility: {},
            position: {
              type: "Point",
              coordinates: [0, 0],
            },
            info: {
              changeIndex: 1,
              createdOn: new Date("2022-02-03T09:22:23Z"),
              lastChangeOn: new Date("2022-02-03T09:22:23Z"),
            },
          },
        });
      });

      it("sets the pictures as an empty array when is an object", async function () {
        const result = await transformRecord(
          { pictures: {} },
          { fields: [{ key: "pictures", destination: "pictures" }] },
          resolvers
        );

        result.should.deep.equal({
          errors: [],
          validationError: {
            code: "empty-name",
            message: "The feature must have a name.",
          },
          result: {
            maps: [],
            icons: [],
            name: "",
            visibility: 1,
            computedVisibility: {},
            description: "",
            position: {
              type: "Point",
              coordinates: [0, 0],
            },
            info: {
              changeIndex: 1,
              createdOn: new Date("2022-02-03T09:22:23Z"),
              lastChangeOn: new Date("2022-02-03T09:22:23Z"),
            },
          },
        });
      });
    });

    describe("parsing the sounds field", function () {
      it("queries the sounds and it ads it to the new feature", async function () {
        const result = await transformRecord(
          {
            sounds: "some;sounds;name",
          },
          { fields: [{ key: "sounds", destination: "sounds" }] },
          resolvers
        );

        result.should.deep.equal({
          errors: [],
          validationError: {
            code: "empty-name",
            message: "The feature must have a name.",
          },
          result: {
            sounds: ["sounds preview queried: some,sounds,name"],
            maps: [],
            icons: [],
            name: "",
            visibility: 1,
            computedVisibility: {},
            description: "",
            position: {
              type: "Point",
              coordinates: [0, 0],
            },
            info: {
              changeIndex: 1,
              createdOn: new Date("2022-02-03T09:22:23Z"),
              lastChangeOn: new Date("2022-02-03T09:22:23Z"),
            },
          },
        });
      });

      it("queries the sounds when they are set as an array", async function () {
        const result = await transformRecord(
          { sounds: ["1", "2"] },
          { fields: [{ key: "sounds", destination: "sounds" }] },
          resolvers
        );

        result.should.deep.equal({
          errors: [],
          validationError: {
            code: "empty-name",
            message: "The feature must have a name.",
          },
          result: {
            maps: [],
            icons: [],
            sounds: ["sounds preview queried: 1,2"],
            name: "",
            description: "",
            visibility: 1,
            computedVisibility: {},
            position: {
              type: "Point",
              coordinates: [0, 0],
            },
            info: {
              changeIndex: 1,
              createdOn: new Date("2022-02-03T09:22:23Z"),
              lastChangeOn: new Date("2022-02-03T09:22:23Z"),
            },
          },
        });
      });
    });

    describe("parsing the contributors field", function () {
      it("queries the contributors and it ads it to the new feature", async function () {
        const result = await transformRecord(
          {
            contributors: "some;contributors;name",
          },
          { fields: [{ key: "contributors", destination: "contributors" }] },
          resolvers
        );

        result.should.deep.equal({
          errors: [],
          validationError: {
            code: "empty-name",
            message: "The feature must have a name.",
          },
          result: {
            contributors: [
              "users queried: some,contributors,name",
              "another user",
            ],
            maps: [],
            icons: [],
            name: "",
            visibility: 1,
            computedVisibility: {},
            description: "",
            visibility: 1,
            position: {
              type: "Point",
              coordinates: [0, 0],
            },
            info: {
              changeIndex: 1,
              createdOn: new Date("2022-02-03T09:22:23Z"),
              lastChangeOn: new Date("2022-02-03T09:22:23Z"),
            },
          },
        });
      });

      it("queries the contributors when they are set as an array", async function () {
        const result = await transformRecord(
          { contributors: ["1", "2"] },
          { fields: [{ key: "contributors", destination: "contributors" }] },
          resolvers
        );

        result.should.deep.equal({
          errors: [],
          validationError: {
            code: "empty-name",
            message: "The feature must have a name.",
          },
          result: {
            maps: [],
            icons: [],
            contributors: ["users queried: 1,2", "another user"],
            name: "",
            description: "",
            visibility: 1,
            computedVisibility: {},
            position: {
              type: "Point",
              coordinates: [0, 0],
            },
            info: {
              changeIndex: 1,
              createdOn: new Date("2022-02-03T09:22:23Z"),
              lastChangeOn: new Date("2022-02-03T09:22:23Z"),
            },
          },
        });
      });
    });

    describe("parsing the author field", function () {
      it("queries the author and it ads it to the new feature", async function () {
        const result = await transformRecord(
          {
            author: "some author",
          },
          { fields: [{ key: "author", destination: "author" }] },
          resolvers
        );

        result.should.deep.equal({
          errors: [],
          validationError: {
            code: "empty-name",
            message: "The feature must have a name.",
          },
          result: {
            info: {
              originalAuthor: "users queried: some author",
              changeIndex: 1,
              createdOn: new Date("2022-02-03T09:22:23Z"),
              lastChangeOn: new Date("2022-02-03T09:22:23Z"),
            },
            maps: [],
            icons: [],
            name: "",
            visibility: 1,
            computedVisibility: {},
            description: "",
            position: {
              type: "Point",
              coordinates: [0, 0],
            },
          },
        });
      });

      it("adds the userId as original author for new records when no binding is set", async function () {
        const result = await transformRecord(
          {
            author: "some author",
          },
          { fields: [], userId: "user-id" },
          resolvers
        );

        result.result.should.deep.equal({
          info: {
            originalAuthor: "user-id",
            author: "user-id",
            changeIndex: 1,
            createdOn: new Date("2022-02-03T09:22:23Z"),
            lastChangeOn: new Date("2022-02-03T09:22:23Z"),
          },
          maps: [],
          icons: [],
          name: "",
          description: "",
          computedVisibility: {},
          visibility: 1,
          position: {
            type: "Point",
            coordinates: [0, 0],
          },
        });
      });
    });

    describe("the descriptionTemplate option", function () {
      it("does not add a description when the descriptionTemplate is an empty string", async function () {
        const result = await transformRecord(
          {},
          { fields: [], descriptionTemplate: "\t" },
          resolvers
        );

        result.should.deep.equal({
          errors: [],
          validationError: {
            code: "empty-name",
            message: "The feature must have a name.",
          },
          result: {
            info: {
              changeIndex: 1,
              createdOn: new Date("2022-02-03T09:22:23Z"),
              lastChangeOn: new Date("2022-02-03T09:22:23Z"),
            },
            maps: [],
            icons: [],
            name: "",
            visibility: 1,
            computedVisibility: {},
            description: "",
            position: {
              type: "Point",
              coordinates: [0, 0],
            },
          },
        });
      });

      it("adds a description when the descriptionTemplate is an empty string", async function () {
        const result = await transformRecord(
          {},
          { fields: [], descriptionTemplate: "test description" },
          resolvers
        );

        result.should.deep.equal({
          errors: [],
          validationError: {
            code: "empty-name",
            message: "The feature must have a name.",
          },
          result: {
            info: {
              changeIndex: 1,
              createdOn: new Date("2022-02-03T09:22:23Z"),
              lastChangeOn: new Date("2022-02-03T09:22:23Z"),
            },
            maps: [],
            icons: [],
            name: "",
            visibility: 1,
            computedVisibility: {},
            description: "test description",
            position: {
              type: "Point",
              coordinates: [0, 0],
            },
          },
        });
      });

      it("adds a description when the descriptionTemplate is an empty string", async function () {
        const result = await transformRecord(
          { name: "some name" },
          { fields: [], descriptionTemplate: "the name is: `{{name}}`" },
          resolvers
        );

        result.should.deep.equal({
          errors: [],
          validationError: {
            code: "empty-name",
            message: "The feature must have a name.",
          },
          result: {
            info: {
              changeIndex: 1,
              createdOn: new Date("2022-02-03T09:22:23Z"),
              lastChangeOn: new Date("2022-02-03T09:22:23Z"),
            },
            maps: [],
            icons: [],
            name: "",
            visibility: 1,
            computedVisibility: {},
            description: "the name is: `some name`",
            position: {
              type: "Point",
              coordinates: [0, 0],
            },
          },
        });
      });

      it("uses the description field mapping when the descriptionTemplate and description field are set", async function () {
        const result = await transformRecord(
          { name: "some name", description: "description" },
          {
            fields: [{ key: "description", destination: "description" }],
            descriptionTemplate: "the name is: `{{name}}`",
          },
          resolvers
        );

        result.should.deep.equal({
          errors: [],
          validationError: {
            code: "empty-name",
            message: "The feature must have a name.",
          },
          result: {
            info: {
              changeIndex: 1,
              createdOn: new Date("2022-02-03T09:22:23Z"),
              lastChangeOn: new Date("2022-02-03T09:22:23Z"),
            },
            maps: [],
            icons: [],
            name: "",
            visibility: 1,
            computedVisibility: {},
            description: "description",
            position: {
              type: "Point",
              coordinates: [0, 0],
            },
          },
        });
      });
    });
  });
});
