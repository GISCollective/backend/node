/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import {
  newsletterHtmlTemplate,
  articleToHtml,
  blockToHtml,
  articleToText,
  blockToText,
} from "../../../source/tasks/newsletter/email.js";
import Db from "models/source/db.js";
import { MongoMemoryServer } from "mongodb-memory-server";
import { expect } from "chai";
import { should } from "chai";
should();

describe("The Newsletter.Email task", function () {
  let mongoServer;
  let db;
  let newsletterId;

  beforeEach(async function () {
    this.timeout(60000);

    mongoServer = await MongoMemoryServer.create();
    await mongoServer.getUri();

    db = new Db({
      url: `mongodb://${mongoServer._instanceInfo.ip}:${mongoServer._instanceInfo.port}/tests`,
      configuration: { collection: "test" },
    });

    await db.connectWithPromise();
  });

  afterEach(async function () {
    await db?.close?.();
    await mongoServer?.stop?.();
  });

  beforeEach(async function () {
    await db.addItem("space", {
      domain: "giscollective.com",
      logo: "624c05b4bb96c0010091351d",
      name: "GISCollective",
      visibility: {
        isDefault: true,
      },
    });

    const team = await db.addItem("teams", {
      isPublic: false,
      logo: "000000000000000000000001",
      name: "West Team",
      guests: [],
      isPublisher: false,
      isDefault: false,
      pictures: [],
      canEdit: true,
    });

    const newsletter = await db.addItem("newsletter", {
      name: "test",
      description: "some description",
      visibility: { isDefault: false, isPublic: true, team: team.insertedId },
      info: {
        originalAuthor: "",
        author: "",
        createdOn: "2015-01-01T00:00:00Z",
        lastChangeOn: "2015-01-01T00:00:00Z",
        changeIndex: 0,
      },
    });

    newsletterId = newsletter.insertedId.toString();
  });

  describe("newsletterHtmlTemplate", function () {
    it("fills in the placeholders", async function () {
      const result = await newsletterHtmlTemplate(db, newsletterId);

      expect(result).to.contain(
        `<img src="https://giscollective.com/api-v1/pictures/000000000000000000000001/picture/png-512" height="90" />`
      );
      expect(result).to.contain(
        `<a href="https://giscollective.com/manage/newsletter/${newsletterId}/unsubscribe/{$id}"`
      );
      expect(result).to.contain(`<b>GISCollective</b> platform.`);
      expect(result).to.contain(
        `This email was sent to you by the <b>West Team</b> team`
      );
      expect(result).to.contain(`<b>test</b> newsletter`);
    });
  });

  describe("articleToHtml", function () {
    it("returns an empty string for undefined", function () {
      const result = articleToHtml();

      expect(result).to.equal("");
    });

    it("renders a paragraph", function () {
      const result = articleToHtml([
        { type: "paragraph", data: { text: "Lorem ipsum." } },
      ]);

      expect(result).to.equal(`<p>Lorem ipsum.</p>`);
    });

    it("renders 2 paragraphs", function () {
      const result = articleToHtml([
        { type: "paragraph", data: { text: "Lorem ipsum." } },
        { type: "paragraph", data: { text: "Second text." } },
      ]);

      expect(result).to.equal(`<p>Lorem ipsum.</p>\n<p>Second text.</p>`);
    });
  });

  describe("blockToHtml", function () {
    it("converts a paragraph", function () {
      const result = blockToHtml({
        type: "paragraph",
        data: { text: "Lorem ipsum." },
      });

      expect(result).to.equal(`<p>Lorem ipsum.</p>`);
    });

    it("converts a header level 1", function () {
      const result = blockToHtml({
        type: "header",
        data: {
          level: 1,
          text: "Head1",
        },
      });

      expect(result).to.equal(`<h1>Head1</h1>`);
    });

    it("converts a header level 2", function () {
      const result = blockToHtml({
        type: "header",
        data: {
          level: 2,
          text: "Head2",
        },
      });

      expect(result).to.equal(`<h2>Head2</h2>`);
    });

    it("converts a quote", function () {
      const result = blockToHtml({
        type: "quote",
        data: {
          text: "Hero can be anyone.",
          alignment: "left",
          caption: "Batman",
        },
      });

      expect(result).to.equal(
        `<div class="quote"><p>Hero can be anyone.</p><p class="author">- Batman</p></div>`
      );
    });

    it("converts a delimiter", function () {
      const result = blockToHtml({
        type: "delimiter",
        data: {},
      });

      expect(result).to.equal(`<div class="delimiter">***</div>`);
    });

    it("converts an ordered list", function () {
      const result = blockToHtml({
        type: "list",
        data: {
          style: "ordered",
          items: ["step1", "step2", "step3"],
        },
      });

      expect(result).to.equal(
        `<ol>\n<li>step1</li>\n<li>step2</li>\n<li>step3</li>\n</ol>`
      );
    });

    it("converts an unordered list", function () {
      const result = blockToHtml({
        type: "list",
        data: {
          style: "unordered",
          items: ["step1", "step2", "step3"],
        },
      });

      expect(result).to.equal(
        `<ul>\n<li>step1</li>\n<li>step2</li>\n<li>step3</li>\n</ul>`
      );
    });

    it("converts an unordered list using the new format", function () {
      const result = blockToHtml({
        type: 'list',
        data: {
          items: [
            {
              content: "level 1",
              items: [{
                content: "level 2",
                items: [{
                  content: "level 3",
                  items: [],
                  meta: {}
                }],
                meta: {}
              }],
              meta: {}
            }
          ]
        },
      });

      expect(result).to.equal(
        `<ul>\n` +
        `<li>level 1\n` +
        `  <ul>\n` +
        `  <li>level 2\n` +
        `    <ul>\n` +
        `    <li>level 3    </li>\n` +
        `</ul>\n` +
        `  </li>\n` +
        `</ul>\n` +
        `</li>\n` +
        `</ul>`
      );
    });

    it("converts a picture with caption", function () {
      const result = blockToHtml({
        type: "image",
        data: {
          withBorder: false,
          file: {
            url: "http://localhost:9091/pictures/63c5d9b0eb6372fc410abce7/picture/lg",
          },
          stretched: false,
          withBackground: true,
          caption: "hello!",
        },
      });

      expect(result).to.equal(
        `<div><img src="http://localhost:9091/pictures/63c5d9b0eb6372fc410abce7/picture/lg" width="100%" alt="hello!"><div class="image-caption">hello!</div></div>`
      );
    });

    it("converts a picture without caption", function () {
      const result = blockToHtml({
        type: "image",
        data: {
          withBorder: false,
          file: {
            url: "http://localhost:9091/pictures/63c5d9b0eb6372fc410abce7/picture/lg",
          },
          stretched: false,
          withBackground: true,
          caption: "",
        },
      });

      expect(result).to.equal(
        `<img src="http://localhost:9091/pictures/63c5d9b0eb6372fc410abce7/picture/lg" width="100%">`
      );
    });
  });

  describe("articleToText", function () {
    it("returns an empty string for undefined", function () {
      const result = articleToText();

      expect(result).to.equal("");
    });

    it("renders a paragraph", function () {
      const result = articleToText([
        { type: "paragraph", data: { text: "Lorem ipsum." } },
      ]);

      expect(result).to.equal(`Lorem ipsum.`);
    });

    it("renders 2 paragraphs", function () {
      const result = articleToText([
        { type: "paragraph", data: { text: "Lorem ipsum." } },
        { type: "paragraph", data: { text: "Second text." } },
      ]);

      expect(result).to.equal(`Lorem ipsum.\n\nSecond text.`);
    });
  });

  describe("blockToText", function () {
    it("converts a paragraph", function () {
      const result = blockToText({
        type: "paragraph",
        data: { text: "Lorem ipsum." },
      });

      expect(result).to.equal(`Lorem ipsum.`);
    });

    it("removes html tags from paragraphs", function () {
      const result = blockToText({
        type: "paragraph",
        data: { text: "<b>Lorem</b> <i>ipsum</i>." },
      });

      expect(result).to.equal(`Lorem ipsum.`);
    });

    it("converts a header level 1", function () {
      const result = blockToText({
        type: "header",
        data: {
          level: 1,
          text: "Head1",
        },
      });

      expect(result).to.equal(`Head1\n---`);
    });

    it("converts a header level 2", function () {
      const result = blockToText({
        type: "header",
        data: {
          level: 2,
          text: "Head2",
        },
      });

      expect(result).to.equal(`Head2\n===`);
    });

    it("converts a header level 3", function () {
      const result = blockToText({
        type: "header",
        data: {
          level: 3,
          text: "Head3",
        },
      });

      expect(result).to.equal(`Head3\n###`);
    });

    it("converts a header level 4", function () {
      const result = blockToText({
        type: "header",
        data: {
          level: 4,
          text: "Head4",
        },
      });

      expect(result).to.equal(`Head4\n+++`);
    });

    it("converts a header level 5", function () {
      const result = blockToText({
        type: "header",
        data: {
          level: 5,
          text: "Head5",
        },
      });

      expect(result).to.equal(`Head5\n^^^`);
    });

    it("converts a header level 6", function () {
      const result = blockToText({
        type: "header",
        data: {
          level: 6,
          text: "Head6",
        },
      });

      expect(result).to.equal(`Head6\n***`);
    });

    it("converts a quote", function () {
      const result = blockToText({
        type: "quote",
        data: {
          text: "Hero can be anyone.",
          alignment: "left",
          caption: "Batman",
        },
      });

      expect(result).to.equal(`"Hero can be anyone."\n - Batman`);
    });

    it("converts a delimiter", function () {
      const result = blockToText({
        type: "delimiter",
        data: {},
      });

      expect(result).to.equal(`* * *`);
    });

    it("converts an ordered list", function () {
      const result = blockToText({
        type: "list",
        data: {
          style: "ordered",
          items: ["step1", "step2", "step3"],
        },
      });

      expect(result).to.equal(`1. step1\n2. step2\n3. step3`);
    });

    it("converts an unordered list", function () {
      const result = blockToText({
        type: "list",
        data: {
          style: "unordered",
          items: ["step1", "step2", "step3"],
        },
      });

      expect(result).to.equal(`* step1\n* step2\n* step3`);
    });

    it("converts an unordered list using the new format", function () {
      const result = blockToText({
        type: 'list',
        data: {
          items: [
            {
              content: "level 1",
              items: [{
                content: "level 2",
                items: [{
                  content: "level 3",
                  items: [],
                  meta: {}
                }],
                meta: {}
              }],
              meta: {}
            }
          ]
        },
      });

      expect(result).to.equal(
        `* level 1\n  * level 2\n    * level 3`
      );
    });

    it("ignores a picture with caption", function () {
      const result = blockToText({
        type: "image",
        data: {
          withBorder: false,
          file: {
            url: "http://localhost:9091/pictures/63c5d9b0eb6372fc410abce7/picture/lg",
          },
          stretched: false,
          withBackground: true,
          caption: "hello!",
        },
      });

      expect(result).to.equal(``);
    });

    it("ignores a picture without caption", function () {
      const result = blockToText({
        type: "image",
        data: {
          withBorder: false,
          file: {
            url: "http://localhost:9091/pictures/63c5d9b0eb6372fc410abce7/picture/lg",
          },
          stretched: false,
          withBackground: true,
          caption: "",
        },
      });

      expect(result).to.equal(``);
    });
  });
});
