/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { featureLarge } from "../../../source/tasks/feature/large.js";
import Db from "models/source/db.js";
import { ObjectId } from "mongodb";
import { MongoMemoryServer } from "mongodb-memory-server";
import { Readable } from "stream";
import CalendarService from "../../../source/lib/CalendarService.js";
import BatchJobService from "../../../source/lib/BatchJobService.js";
import ModelChangeNotificationService from "../../../source/lib/ModelChangeNotificationService.js";
import sinon from "sinon";

import { should, expect } from "chai";

should();

describe("FeatureLarge", function () {
  let job;
  let db;
  let mongoServer;

  beforeEach(async function () {
    this.timeout(60000);
    mongoServer = await MongoMemoryServer.create();
    await mongoServer.getUri();

    db = new Db({
      url: `mongodb://${mongoServer._instanceInfo.ip}:${mongoServer._instanceInfo.port}/tests`,
      configuration: { collection: "test" },
    });
    await db.connectWithPromise();
  });

  afterEach(async function () {
    await db.close();
    await mongoServer.stop();
  });

  it("sets the isLarge flag to false for a small MultiPolygon", async function () {
    const result = await db.addItem("sites", {
      maps: [],
      info: {},
      position: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [-73.979966, 40.768902],
              [-73.97984, 40.76865000000001],
              [-73.97975, 40.76853],
              [-73.97974000000001, 40.76956000000001],
              [-73.97978999999999, 40.76949999999999],
              [-73.97996999999999, 40.76914999999997],
              [-73.97996000000001, 40.76893999999999],
            ],
          ],
        ],
      },
    });
    const id = result.insertedId;

    await featureLarge(db, { id });

    const record = await db.getItem("sites", id);

    expect(record.isLarge).to.equal(false);
  });

  it("sets the isLarge flag to true for a large MultiPolygon", async function () {
    let points = [];
    for (let i = 0; i < 1001; i++) {
      points.push([0, 0]);
    }

    const result = await db.addItem("sites", {
      maps: [],
      info: {},
      position: {
        type: "MultiPolygon",
        coordinates: [[points]],
      },
    });
    const id = result.insertedId;

    await featureLarge(db, { id });

    const record = await db.getItem("sites", id);

    expect(record.isLarge).to.equal(true);
  });

  it("sets the isLarge flag to true for a large Polygon", async function () {
    let points = [];
    for (let i = 0; i < 1001; i++) {
      points.push([0, 0]);
    }

    const result = await db.addItem("sites", {
      maps: [],
      info: {},
      position: {
        type: "Polygon",
        coordinates: [points],
      },
    });
    const id = result.insertedId;

    await featureLarge(db, { id });

    const record = await db.getItem("sites", id);

    expect(record.isLarge).to.equal(true);
  });

  it("sets the isLarge flag to true for a large MultiLineString", async function () {
    let points = [];
    for (let i = 0; i < 1001; i++) {
      points.push([0, 0]);
    }

    const result = await db.addItem("sites", {
      maps: [],
      info: {},
      position: {
        type: "MultiLineString",
        coordinates: [points],
      },
    });
    const id = result.insertedId;

    await featureLarge(db, { id });

    const record = await db.getItem("sites", id);

    expect(record.isLarge).to.equal(true);
  });

  it("sets the isLarge flag to true for a large LineString", async function () {
    let points = [];
    for (let i = 0; i < 1001; i++) {
      points.push([0, 0]);
    }

    const result = await db.addItem("sites", {
      maps: [],
      info: {},
      position: {
        type: "LineString",
        coordinates: points,
      },
    });
    const id = result.insertedId;

    await featureLarge(db, { id });

    const record = await db.getItem("sites", id);

    expect(record.isLarge).to.equal(true);
  });

  it("sets the isLarge flag to false for a point", async function () {
    const result = await db.addItem("sites", {
      maps: [],
      info: {},
      position: {
        type: "Point",
        coordinates: [0, 0],
      },
    });
    const id = result.insertedId;

    await featureLarge(db, { id });

    const record = await db.getItem("sites", id);

    expect(record.isLarge).to.equal(false);
  });
});
