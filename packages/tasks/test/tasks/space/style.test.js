/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import {
  generateColorVariables,
  copyVariables,
  generateStyle,
  generateCustomColorsStyles,
  generateFontStyles,
} from "../../../source/tasks/space/style.js";
import { should } from "chai";

should();

describe("space.style", function () {
  describe("generateColorVariables", function () {
    it("generates the variables for the blue color", function () {
      const variables = generateColorVariables([], "blue", "#1a5fb4");

      variables.length.should.equal(26);
    });

    it("returns an empty list when the value is not a string", function () {
      const variables = generateColorVariables([], "blue");

      variables.should.deep.equal([]);
    });
  });

  describe("copyVariables", function () {
    it("copies the colors from the map when they exist", function () {
      const variables = copyVariables(
        [
          { name: "value1", value: "#1A5FB4" },
          { name: "value2", value: "26, 95, 180" },
        ],
        { "value1-copy": "value1", "value2-copy": "value2" }
      );

      variables.should.deep.equal([
        { name: "value1", value: "#1A5FB4" },
        { name: "value2", value: "26, 95, 180" },
        { name: "value1-copy", value: "#1A5FB4" },
        { name: "value2-copy", value: "26, 95, 180" },
      ]);
    });

    it("does nothing when the list is empty", function () {
      const variables = copyVariables([], {
        value1: "value1-copy",
        value2: "value2-copy",
      });

      variables.should.deep.equal([]);
    });
  });

  describe("generateFontStyles", function () {
    it("returns an empty string when there is a null", function () {
      generateFontStyles(null).should.equal("");
    });

    it("returns an empty string when there is an empty object", function () {
      generateFontStyles({}).should.equal("");
    });

    it("returns a h1 style when set", function () {
      generateFontStyles({
        h1: ["ff-anton", "fw-light", "text-size-30"],
      }).should.equal(
        "h1 { font-family: var(--ff-anton); font-weight: lighter; font-size: 4.4rem }"
      );
    });

    it("returns styles for each prefix", function () {
      generateFontStyles(
        {
          h1: ["ff-anton", "fw-light", "text-size-30"],
        },
        ["prefix1 ", "prefix2 "]
      ).should.equal(
        "prefix1 h1 { font-family: var(--ff-anton); font-weight: lighter; font-size: 4.4rem }\n" +
        "prefix2 h1 { font-family: var(--ff-anton); font-weight: lighter; font-size: 4.4rem }"
      );
    });

    it("returns a h2 and h3 styles when set", function () {
      generateFontStyles({
        h2: ["ff-anton", "fw-light", "text-size-30"],
        h3: ["ff-anton", "fw-light", "text-size-30"],
      }).should.equal(
        "h2 { font-family: var(--ff-anton); font-weight: lighter; font-size: 4.4rem }\n" +
        "h3 { font-family: var(--ff-anton); font-weight: lighter; font-size: 4.4rem }"
      );
    });

    it("returns a p style when set", function () {
      generateFontStyles({
        paragraph: ["ff-anton", "fw-bold", "text-size-12"],
      }).should.equal(
        "p, div, ul, ol { font-family: var(--ff-anton); font-weight: bold; font-size: 1.2rem }"
      );
    });

    it("replaces non alphanumeric chars with dash", function () {
      generateFontStyles({ h1: ["ff-1;a,3"] }).should.equal(
        "h1 { font-family: var(--ff-1-a-3) }"
      );
    });

    it("ignores an invalid class", function () {
      generateFontStyles({ h1: ["invalid-class"] }).should.equal("h1 {  }");
    });
  });


  describe("generateCustomColorsStyles", function() {
    it("returns an empty string when there is a null", function () {
      generateCustomColorsStyles(null).should.equal("");
    });

    it("returns the clases for one color", function () {
      generateCustomColorsStyles([{ name: "Color 1" }]).should.equal(` .bg-color-1 { background-color: rgba(var(--bs-color-1-rgb), 1) !important; }
 .border-color-1, .border-color-color-1 { border-color: rgba(var(--bs-color-1-rgb), 1) !important; }
 .btn-color-1 { --bs-btn-color: var(--bs-color-1-text);
  --bs-btn-bg: var(--bs-color-1);
  --bs-btn-border-color: var(--bs-color-1);
  --bs-btn-hover-color: var(--bs-color-1-text);
  --bs-btn-hover-bg: var(--bs-color-1-var1);
  --bs-btn-hover-border-color: var(--bs-color-1-var1);
  --bs-btn-focus-shadow-rgb: 49, 132, 253;
  --bs-btn-active-color: var(--bs-color-1-text);
  --bs-btn-active-bg: var(--bs-color-1-var2);
  --bs-btn-active-border-color: var(--bs-color-1-var2);
  --bs-btn-active-shadow: inset 0 3px 5px rgba(0, 0, 0, 0.125);
  --bs-btn-disabled-color: var(--bs-color-1-text);
  --bs-btn-disabled-bg: rgba(var(--bs-color-1-rgb), 0.8);
  --bs-btn-disabled-border-color: rgba(var(--bs-color-1-rgb), 0.8);
}
 .text-color-1 { color: rgba(var(--bs-color-1-rgb), 1) !important; }`);
    });
  });

  describe("generateStyle", function () {
    let variables;

    it("can generate the style for the red and blue colors", function () {
      variables = generateStyle({ blue: "#1a5fb4", red: "#e50620" });
      variables.should.equal(`--bs-blue: #1A5FB4;
--bs-blue-rgb: 26, 95, 180;
--bs-blue-100: #CADFF7;
--bs-blue-100-rgb: 202, 223, 247;
--bs-blue-200: #95BEF0;
--bs-blue-200-rgb: 149, 190, 240;
--bs-blue-300: #609DE8;
--bs-blue-300-rgb: 96, 157, 232;
--bs-blue-400: #2B7CE0;
--bs-blue-400-rgb: 43, 124, 224;
--bs-blue-500: #1A5FB4;
--bs-blue-500-rgb: 26, 95, 180;
--bs-blue-600: #16519A;
--bs-blue-600-rgb: 22, 81, 154;
--bs-blue-700: #12437F;
--bs-blue-700-rgb: 18, 67, 127;
--bs-blue-800: #0F3565;
--bs-blue-800-rgb: 15, 53, 101;
--bs-blue-900: #0B284B;
--bs-blue-900-rgb: 11, 40, 75;
--bs-blue-shade-100: #0E3564;
--bs-blue-shade-100-rgb: 14, 53, 100;
--bs-blue-shade-200: #10396C;
--bs-blue-shade-200-rgb: 16, 57, 108;
--bs-blue-shade-300: #113D73;
--bs-blue-shade-300-rgb: 17, 61, 115;
--bs-red: #E50620;
--bs-red-rgb: 229, 6, 32;
--bs-red-100: #FECBD1;
--bs-red-100-rgb: 254, 203, 209;
--bs-red-200: #FC95A1;
--bs-red-200-rgb: 252, 149, 161;
--bs-red-300: #FB5F71;
--bs-red-300-rgb: 251, 95, 113;
--bs-red-400: #F92941;
--bs-red-400-rgb: 249, 41, 65;
--bs-red-500: #E50620;
--bs-red-500-rgb: 229, 6, 32;
--bs-red-600: #C3051B;
--bs-red-600-rgb: 195, 5, 27;
--bs-red-700: #A10417;
--bs-red-700-rgb: 161, 4, 23;
--bs-red-800: #7F0312;
--bs-red-800-rgb: 127, 3, 18;
--bs-red-900: #5D020D;
--bs-red-900-rgb: 93, 2, 13;
--bs-red-shade-100: #7F0312;
--bs-red-shade-100-rgb: 127, 3, 18;
--bs-red-shade-200: #890413;
--bs-red-shade-200-rgb: 137, 4, 19;
--bs-red-shade-300: #920414;
--bs-red-shade-300-rgb: 146, 4, 20;
--bs-primary: #1A5FB4;
--bs-primary-rgb: 26, 95, 180;
--bs-primary-bg-subtle: #CADFF7;
--bs-primary-border-subtle: #95BEF0;
--bs-primary-text: #16519A;
--bs-primary-shade-100: #0E3564;
--bs-primary-shade-200: #10396C;
--bs-primary-shade-300: #113D73;
--bs-danger: #E50620;
--bs-danger-rgb: 229, 6, 32;
--bs-danger-bg-subtle: #FECBD1;
--bs-danger-border-subtle: #FC95A1;
--bs-danger-text: #C3051B;
--bs-danger-shade-100: #7F0312;
--bs-danger-shade-200: #890413;
--bs-danger-shade-300: #920414;`);
    });

    it("can generate the style for custom colors", function () {
      variables = generateStyle({
        customColors: [{
          name: "Color 1",
          lightValue: "#E8DEC4",
          darkValue: "#000002",
        }, {
          name: "Color & 2",
          lightValue: "#000003",
          darkValue: "#000004",
        }]
      });

      variables.should.equal(`--bs-color-1: #E8DEC4;
--bs-color-1-rgb: 232, 222, 196;
--bs-color-1-text: #3E3318;
--bs-color-1-text-rgb: 62, 51, 24;
--bs-color-1-var1: #D0BC86;
--bs-color-1-var1-rgb: 208, 188, 134;
--bs-color-1-var2: #B89949;
--bs-color-1-var2-rgb: 184, 153, 73;
--bs-color-2: #000003;
--bs-color-2-rgb: 0, 0, 3;
--bs-color-2-text: #000005;
--bs-color-2-text-rgb: 0, 0, 5;
--bs-color-2-var1: #000004;
--bs-color-2-var1-rgb: 0, 0, 4;
--bs-color-2-var2: #000004;
--bs-color-2-var2-rgb: 0, 0, 4;`);
    });
  });
});
