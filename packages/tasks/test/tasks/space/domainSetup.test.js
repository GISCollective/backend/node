/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect, should } from "chai";
import { domainSetup } from "../../../source/tasks/space/domainSetup.js";
import Db from "models/source/db.js";
import { MongoMemoryServer } from "mongodb-memory-server";

should();

describe("space.domainSetup", function () {
  let mongoServer;
  let db;
  let broadcast;
  let kc;
  let spaceId;
  let lastChannel;
  let lastMessage;
  let lastNs;
  let lastYaml;

  beforeEach(async function () {
    this.timeout(60000);

    mongoServer = await MongoMemoryServer.create();
    const url = mongoServer.getUri();

    db = new Db(
      {
        url,
        configuration: { collection: "test" },
      },
      false
    );

    await db.connectWithPromise();

    const result = await db.addItem("space", {
      domain: "test.com",
      visibility: {
        team: "some-team",
      },
    });

    spaceId = result.insertedId.toString();

    lastNs = null;
    lastYaml = null;

    kc = {
      makeApiClient: () => ({
        createNamespacedIngress: (ns, yaml) => {
          lastNs = ns;
          lastYaml = yaml;
        },
        listNamespacedIngress: () => ({ body: { items: [] } }),
      }),
    };

    lastChannel = null;
    lastMessage = null;
    broadcast = {
      push: (channel, message) => {
        lastChannel = channel;
        lastMessage = message;
      },
    };
  });

  afterEach(async function () {
    await db.close();
    await mongoServer.stop();
  });

  describe("domainSetup", function () {
    it("notifies the team that the domain is ready", async function () {
      let message = {
        id: spaceId,
      };

      await domainSetup(db, message, broadcast, kc);

      expect(lastChannel).to.equal("notifications.message");

      const storedMessage = await db.getItem("message", lastMessage.id);
      storedMessage._id = storedMessage._id.toString();

      expect(storedMessage.actions).to.deep.equal({
        "View website": "https://test.com",
      });

      expect(storedMessage.to).to.deep.equal({
        type: "team",
        value: "some-team",
      });

      expect(storedMessage.subject).to.equal(
        "Your domain test.com is reserved"
      );
      expect(storedMessage.text).to.equal(
        "Great news! Your domain test.com is officially reserved, and your space will be up and running there soon."
      );
      expect(storedMessage.type).to.equal("message");
    });

    it("updates the space domain status on success", async function () {
      let message = {
        id: spaceId,
      };

      await domainSetup(db, message, broadcast, kc);

      const storedSpace = await db.getItem("space", spaceId);

      expect(storedSpace.domainStatus).to.equal("ready");
    });

    it("updates the space domain status when the domain name is too short", async function () {
      let message = {
        id: spaceId,
      };

      const space = await db.getItem("space", spaceId);
      space.domain = "a.c";
      await db.setItem("space", spaceId, space);

      await domainSetup(db, message, broadcast, kc);

      const storedSpace = await db.getItem("space", spaceId);

      expect(storedSpace.domainStatus).to.equal("invalid-domain");
    });

    it("creates an ingress with the new domain when the space exists", async function () {
      let message = {
        id: spaceId,
      };

      await domainSetup(db, message, broadcast, kc);

      expect(lastNs).to.equal("ogm");
      expect(lastYaml.metadata).to.deep.equal({
        annotations: {
          "cert-manager.io/cluster-issuer": "letsencrypt-prod",
          "kubernetes.io/ingress.class": "nginx",
          "nginx.ingress.kubernetes.io/proxy-body-size": "20m",
          "nginx.ingress.kubernetes.io/rewrite-target": "/$1",
          "nginx.ingress.kubernetes.io/x-forwarded-prefix": "true",
          "giscollective/space-id": spaceId,
        },
        name: "test.com",
        labels: { createdBy: "node-client" },
      });
      expect(lastYaml.spec.rules).to.deep.equal([
        {
          host: "test.com",
          http: {
            paths: [
              {
                path: "/(sitemap.txt|sitemap.xml)",
                pathType: "ImplementationSpecific",
                backend: {
                  service: {
                    name: "ogm-api-service",
                    port: {
                      number: 80,
                    },
                  },
                },
              },
              {
                backend: {
                  service: { name: "ogm-fonts-service", port: { number: 80 } },
                },
                path: "/api-v1/fonts/(.*)",
                pathType: "ImplementationSpecific",
              },
              {
                backend: {
                  service: { name: "ogm-fonts-service", port: { number: 80 } },
                },
                path: "/api-v1/fonts.json",
                pathType: "ImplementationSpecific",
              },
              {
                backend: {
                  service: { name: "ogm-api-service", port: { number: 80 } },
                },
                path: "/api-v1/(.*)",
                pathType: "ImplementationSpecific",
              },
              {
                backend: {
                  service: {
                    name: "ogm-frontend-service",
                    port: { number: 80 },
                  },
                },
                path: "/(.*)",
                pathType: "ImplementationSpecific",
              },
            ],
          },
        },
      ]);
      expect(lastYaml.spec.tls).to.deep.equal([
        { hosts: ["test.com"], secretName: `space-${spaceId}` },
      ]);
    });

    it("notifies the admins when a domain setup failes", async function () {
      kc = {
        makeApiClient: () => ({
          createNamespacedIngress: () => {
            throw {
              body: {
                message: "some message",
              },
            };
          },
          listNamespacedIngress: () => ({ body: { items: [] } }),
        }),
      };

      let message = {
        id: spaceId,
      };

      await domainSetup(db, message, broadcast, kc);

      expect(lastChannel).to.equal("notifications.message");

      const storedMessage = await db.getItem("message", lastMessage.id);
      storedMessage._id = storedMessage._id.toString();

      expect(storedMessage.actions).to.deep.equal({});

      expect(storedMessage.to).to.deep.equal({
        type: "",
        value: "",
      });

      expect(storedMessage.subject).to.equal(
        "kubernetes error on domain setup for test.com"
      );
      expect(storedMessage.text).to.equal("some message");
      expect(storedMessage.type).to.equal("admin");
    });

    it("does not apply the ingress when the domain is already set up", async function () {
      const deleted = [];
      kc = {
        makeApiClient: () => ({
          deleteNamespacedIngress: (name, ns) => {
            deleted.push({ name, ns });
          },
          createNamespacedIngress: () => {
            throw {
              body: {
                message: "some message",
              },
            };
          },
          listNamespacedIngress: () => ({
            body: {
              items: [
                {
                  metadata: {
                    annotations: {
                      "giscollective/space-id": spaceId,
                    },
                    name: "test",
                  },
                  spec: {
                    rules: [
                      {
                        host: "test.com",
                      },
                    ],
                  },
                },
              ],
            },
          }),
        }),
      };

      let message = {
        id: spaceId,
      };

      await domainSetup(db, message, broadcast, kc);

      expect(deleted).to.have.length(0);
      expect(lastChannel).not.to.exist;
    });

    it("deletes an ingress when the existing has a different domain", async function () {
      const deleted = [];
      kc = {
        makeApiClient: () => ({
          deleteNamespacedIngress: (name, ns) => {
            deleted.push({ name, ns });
          },
          createNamespacedIngress: () => {
            throw {
              body: {
                message: "some message",
              },
            };
          },
          listNamespacedIngress: () => ({
            body: {
              items: [
                {
                  metadata: {
                    annotations: {
                      "giscollective/space-id": spaceId,
                    },
                    name: "test",
                  },
                  spec: {
                    rules: [
                      {
                        host: "other.com",
                      },
                    ],
                  },
                },
              ],
            },
          }),
        }),
      };

      let message = {
        id: spaceId,
      };

      await domainSetup(db, message, broadcast, kc);

      expect(deleted).to.deep.equal([{ name: "test", ns: "ogm" }]);
      expect(lastChannel).to.equal("notifications.message");
    });
  });
});
