/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Db from "models/source/db.js";
import { MongoMemoryServer } from "mongodb-memory-server";
import { expect, should } from "chai";
import {
  notificationsMessage,
  addTemplate,
} from "../../../source/tasks/notifications/message.js";
import sinon from "sinon";
import CalendarService from "../../../source/lib/CalendarService.js";
import nock from "nock";

should();

describe("The notifications.message task", function () {
  let mongoServer;
  let db;
  let userId;
  let sendEmail;
  let now;
  let preference;

  before(function () {
    sendEmail = sinon.stub();
    now = "2020-01-01T00:05:00Z";
    CalendarService.instance = { now };
  });

  beforeEach(async function () {
    this.timeout(60000);

    mongoServer = await MongoMemoryServer.create();
    await mongoServer.getUri();

    db = new Db({
      url: `mongodb://${mongoServer._instanceInfo.ip}:${mongoServer._instanceInfo.port}/tests`,
      configuration: { collection: "test" },
    });

    await db.connectWithPromise();

    const result = await db.addItem("users", {
      email: "user@me.com",
      firstName: "John",
      lastName: "Doe",
      username: "test",
    });

    userId = result.insertedId.toString();

    preference = await db.addItem("preference", {
      name: "integrations.slack.webhook",
      value: "",
    });

    await db.addItem("space", {
      domain: "giscollective.com",
      logo: "624c05b4bb96c0010091351d",
      name: "GISCollective",
      visibility: {
        isDefault: true,
      },
    });
  });

  afterEach(async function () {
    sendEmail.reset();

    await db?.close?.();
    await mongoServer?.stop?.();
  });

  describe("handling regular messages to an email address", function () {
    let messageId;

    beforeEach(async function () {
      const result = await db.addItem("message", {
        to: {
          type: "email",
          value: "email@me.com",
        },
        subject: "subject",
        text: "text",
        html: "html",
      });

      messageId = result.insertedId;
    });

    it("sends an email to an user when the recipient.type is email", async function () {
      await notificationsMessage(db, { id: messageId }, { sendEmail });

      sinon.assert.calledOnce(sendEmail);
      sinon.assert.calledWith(sendEmail, db, {
        to: ["email@me.com"],
        subject: "subject",
        text: "text",
        html: "html",
      });
    });
  });

  describe("handling regular messages to a team", function () {
    let messageId;

    beforeEach(async function () {
      const users = [];

      for (const i of [0, 1, 2, 3, 4, 5, 6]) {
        const userResult = await db.addItem("users", {
          email: `${i}@me.com`,
          firstName: "John",
          lastName: "Doe",
          username: "test",
        });

        users.push(userResult.insertedId);
      }

      const teamResult = await db.addItem("teams", {
        isPublic: true,
        members: [users[0], users[4], users[5], users[6]],
        owners: [users[1]],
        leaders: [users[2]],
        name: "Open Green Map",
        guests: [users[3]],
      });

      const result = await db.addItem("message", {
        to: {
          type: "team",
          value: teamResult.insertedId,
        },
        subject: "subject",
        text: "text",
        html: "html",
      });

      messageId = result.insertedId;
    });

    it("sends an email to all team leaders and owners", async function () {
      await notificationsMessage(db, { id: messageId }, { sendEmail });

      sinon.assert.calledOnce(sendEmail);
      sinon.assert.calledWith(sendEmail, db, {
        to: ["1@me.com", "2@me.com"],
        subject: "subject",
        text: "text",
        html: "html",
      });
    });
  });

  describe("handling regular messages to a user", function () {
    let messageId;

    beforeEach(async function () {
      const result = await db.addItem("message", {
        to: {
          type: "user",
          value: userId,
        },
        subject: "subject",
        text: "text",
        html: "html",
      });

      messageId = result.insertedId;
    });

    it("sends an email to an user when the recipient.type is user", async function () {
      await notificationsMessage(db, { id: messageId }, { sendEmail });

      sinon.assert.calledOnce(sendEmail);
      sinon.assert.calledWith(sendEmail, db, {
        to: ["user@me.com"],
        subject: "subject",
        text: "text",
        html: "html",
      });
    });

    it("does not send a sent message", async function () {
      const result = await db.addItem("message", {
        to: {
          type: "user",
          value: userId,
        },
        subject: "subject",
        text: "text",
        html: "html",
        isSent: true,
      });

      messageId = result.insertedId;

      await notificationsMessage(db, { id: messageId }, { sendEmail });

      sinon.assert.notCalled(sendEmail);
    });

    it("does not send a message that failed 10 times", async function () {
      const result = await db.addItem("message", {
        to: {
          type: "user",
          value: userId,
        },
        subject: "subject",
        text: "text",
        html: "html",
        failCount: 10,
      });

      messageId = result.insertedId;

      await notificationsMessage(db, { id: messageId }, { sendEmail });

      sinon.assert.notCalled(sendEmail);
    });

    it("sets the message as sent on success", async function () {
      await notificationsMessage(db, { id: messageId }, { sendEmail });

      const message = await db.getItem("message", messageId);

      expect(message.isSent).to.equal(true);
      expect(message.failCount).to.equal(0);
      expect(message.sentOn).to.equal(now);
    });

    it("does not set the message as sent on fail", async function () {
      sendEmail.throws(new Error("error"));
      await notificationsMessage(db, { id: messageId }, { sendEmail });

      const message = await db.getItem("message", messageId);

      expect(message.isSent).to.equal(false);
      expect(message.failCount).to.equal(1);
      expect(message.sentOn).to.equal(now);
    });

    it("uses the message template when useGenericTemplate=true", async function () {
      const result = await db.addItem("message", {
        to: {
          type: "user",
          value: userId,
        },
        subject: "subject",
        text: "text",
        html: "html",
        useGenericTemplate: true,
      });

      messageId = result.insertedId;

      await notificationsMessage(db, { id: messageId }, { sendEmail });

      const html = await addTemplate(db, "notification.html", "html");

      sinon.assert.calledOnce(sendEmail);
      sinon.assert.calledWith(sendEmail, db, {
        to: ["user@me.com"],
        subject: "subject",
        text: "text\n\n--------------------------\n\nThis is a notification sent by the GISCollective platform.\nhttps://giscollective.com\n",
        html,
      });
    });

    it.skip("adds the action links when set", async function () {
      const result = await db.addItem("message", {
        to: {
          type: "user",
          value: userId,
        },
        subject: "subject",
        text: "text",
        html: "html",
        actions: {
          a: "b",
          c: "d",
        },
      });

      messageId = result.insertedId;

      await notificationsMessage(db, { id: messageId }, { sendEmail });

      sinon.assert.calledOnce(sendEmail);
      sinon.assert.calledWith(sendEmail, db, {
        subject: "subject",
        to: ["user@me.com"],
        text: "text\n\na: b\nc: d",
        html: 'html\n\n<a href="b">a</a>  <a href="d">c</a>',
      });
    });
  });

  describe("handling admin messages without webhooks", function () {
    let messageId;

    beforeEach(async function () {
      await db.addItem("users", {
        email: `admin1@me.com`,
        scopes: ["admin"],
      });

      await db.addItem("users", {
        email: `admin2@me.com`,
        scopes: ["admin"],
      });

      const result = await db.addItem("message", {
        to: {},
        type: "admin",
        subject: "subject",
        text: "text",
        html: "html",
      });

      messageId = result.insertedId;
    });

    it("sends an email to all admins", async function () {
      await notificationsMessage(db, { id: messageId }, { sendEmail });

      sinon.assert.calledOnce(sendEmail);
      sinon.assert.calledWith(sendEmail, db, {
        to: [`admin1@me.com`, `admin2@me.com`],
        subject: "subject",
        text: "text",
        html: "html",
      });
    });

    it("uses the webhook when it is defined", async function () {
      await db.deleteWithQuery("preference", {
        name: "integrations.slack.webhook",
      });

      await db.addItem("preference", {
        name: "integrations.slack.webhook",
        value: "http://slack.com/hook",
      });

      let body;

      nock("http://slack.com")
        .matchHeader("Content-type", "application/json")
        .post("/hook", function (b) {
          body = b;
          return b;
        })
        .reply(200, {});

      await notificationsMessage(db, { id: messageId }, { sendEmail });

      sinon.assert.notCalled(sendEmail);
      expect(body).to.deep.equal({
        blocks: [
          {
            text: {
              text: "*subject*",
              type: "mrkdwn",
            },
            type: "section",
          },
          {
            text: {
              text: "text",
              type: "mrkdwn",
            },
            type: "section",
          },
        ],
      });
    });
  });
});
