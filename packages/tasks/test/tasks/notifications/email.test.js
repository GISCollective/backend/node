/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { updateEmailList } from "../../../source/tasks/notifications/email.js";
import Db from "models/source/db.js";
import { MongoMemoryServer } from "mongodb-memory-server";
import { should } from "chai";
should();

describe("The Notifications.Email task", function () {
  let mongoServer;
  let db;

  beforeEach(async function () {
    this.timeout(60000);

    mongoServer = await MongoMemoryServer.create();
    await mongoServer.getUri();

    db = new Db({
      url: `mongodb://${mongoServer._instanceInfo.ip}:${mongoServer._instanceInfo.port}/tests`,
      configuration: { collection: "test" },
    });

    await db.connectWithPromise();
  });

  afterEach(async function () {
    await db?.close?.();
    await mongoServer?.stop?.();
  });

  describe("updateEmailList", function () {
    it("returns an empty list when there is no user defined", async function () {
      const resolved = await updateEmailList(db, ["a@a.s"]);

      resolved.should.have.length(0);
    });

    describe("when there is a user in the db", function () {
      let userId;

      beforeEach(async function () {
        const result = await db.addItem("users", {
          email: "user@me.com",
          firstName: "John",
          lastName: "Doe",
          username: "test",
        });

        userId = result.insertedId.toString();
      });

      it("returns the user when it's email is found", async function () {
        const resolved = await updateEmailList(db, ["user@me.com"]);
        resolved.should.deep.equal(["user@me.com"]);
      });

      it("replaces ids with emails", async function () {
        const resolved = await updateEmailList(db, [userId]);
        resolved.should.deep.equal(["user@me.com"]);
      });

      it("removes the emails that are not in the db", async function () {
        const resolved = await updateEmailList(db, [
          "user@me.com",
          "other@me.com",
        ]);
        resolved.should.deep.equal(["user@me.com"]);
      });
    });
  });
});
