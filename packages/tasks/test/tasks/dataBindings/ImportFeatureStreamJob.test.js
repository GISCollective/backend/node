/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { ImportFeatureStreamJob } from "../../../source/tasks/dataBindings/import.js";
import Db from "models/source/db.js";
import { ObjectId } from "mongodb";
import { MongoMemoryServer } from "mongodb-memory-server";
import { Readable } from "stream";
import CalendarService from "../../../source/lib/CalendarService.js";
import BatchJobService from "../../../source/lib/BatchJobService.js";
import ModelChangeNotificationService from "../../../source/lib/ModelChangeNotificationService.js";
import sinon from "sinon";

import { should, expect } from "chai";

should();

const airtableDataBinding = {
  _id: new ObjectId("00008c97ecd8490100caba00"),
  name: "",
  destination: {
    type: "Map",
    modelId: "000000000000000000000002",
    deleteNonSyncedRecords: true,
  },
  extraIcons: ["1", "2"],
  updateBy: "name",
  crs: "some crs",
  fields: [],
  descriptionTemplate: "some template",
  connection: {
    type: "airtable",
    config: {
      _apiKey: "api key",
      baseId: "base id",
      tableName: "table name",
      viewName: "view name",
    },
  },
  visibility: {
    team: new ObjectId("000000000000000000000001"),
    isDefault: false,
    isPrivate: false,
  },
  info: {},
};

describe("ImportFeatureStreamJob", function () {
  describe("prepareFromDataBinding with airtable type", function () {
    let job;
    let db;
    let mongoServer;

    beforeEach(async function () {
      this.timeout(60000);
      mongoServer = await MongoMemoryServer.create();
      await mongoServer.getUri();

      db = new Db({
        url: `mongodb://${mongoServer._instanceInfo.ip}:${mongoServer._instanceInfo.port}/tests`,
        configuration: { collection: "test" },
      });
      await db.connectWithPromise();

      job = new ImportFeatureStreamJob(db, "user-id");

      BatchJobService.instance = new BatchJobService(db);
      ModelChangeNotificationService.instance =
        new ModelChangeNotificationService(db, { push: () => {} });
    });

    afterEach(async function () {
      await db.close();
      await mongoServer.stop();
    });

    beforeEach(async function () {
      CalendarService.instance = {
        now: "2022-02-02T12:23:22Z",
      };

      await job.prepareDataBinding(airtableDataBinding);
    });

    it("sets the name to include the data binding id", function () {
      job.name.should.equal("airtable import for 00008c97ecd8490100caba00");
    });

    it("sets the mime to json", function () {
      job.mime.should.equal("text/ndjson");
    });

    it("sets the team id", function () {
      job.teamId.toString().should.equal("000000000000000000000001");
    });

    it("sets the map id", function () {
      job.mapId.toString().should.equal("000000000000000000000002");
    });

    it("prepares the stream", function () {
      expect(job.stream).to.exist;
      expect(job.stream.apiKey).to.equal("api key");
      expect(job.stream.baseId).to.equal("base id");
      expect(job.stream.tableName).to.equal("table name");
      expect(job.stream.viewName).to.equal("view name");
    });

    it("sets the resolvers", function () {
      expect(job.resolvers).to.exist;
    });

    it("sets the transformOptions", function () {
      expect(job.transformOptions).to.deep.equal({
        crs: "some crs",
        destinationMap: "000000000000000000000002",
        extraIcons: ["1", "2"],
        fields: [],
        updateBy: "name",
        descriptionTemplate: "some template",
        userId: "user-id",
        source: {
          type: "DataBinding",
          modelId: "00008c97ecd8490100caba00",
        },
      });
    });

    it("sets the deleteNonSyncedRecords", function () {
      expect(job.deleteNonSyncedRecords).to.equal(true);
    });

    it("sets the current time in the startAt property", function () {
      expect(job.startAt).to.deep.equal(new Date("2022-02-02T12:23:22Z"));
    });
  });

  describe("drainQueue", function () {
    let queue;
    let stream;

    beforeEach(function () {
      queue = {
        push: sinon.stub(),
        drain: sinon.stub(),
      };

      stream = new Readable({
        read() {},
      });
    });

    it("does nothing when there is an empty stream", async function () {
      stream.push("");
      stream.push(null);

      const job = new ImportFeatureStreamJob();
      job.stream = stream;
      job.queue = queue;
      job.mime = "text/ndjson";

      await job.drainQueue();

      queue.push.calledOnce.should.equal(false);
      queue.drain.calledOnce.should.equal(true);
    });

    it("pushes an item when the stream has an object", async function () {
      stream.push(`{}\n`);
      stream.push(null);

      const job = new ImportFeatureStreamJob();
      job.stream = stream;
      job.queue = queue;
      job.mime = "text/ndjson";

      await job.drainQueue();

      queue.push.calledOnce.should.equal(true);
      queue.drain.calledOnce.should.equal(true);
    });
  });

  describe("saveRecord", function () {
    let job;
    let db;
    let mongoServer;

    beforeEach(async function () {
      this.timeout(60000);
      mongoServer = await MongoMemoryServer.create();
      await mongoServer.getUri();

      db = new Db({
        url: `mongodb://${mongoServer._instanceInfo.ip}:${mongoServer._instanceInfo.port}/tests`,
        configuration: { collection: "test" },
      });
      await db.connectWithPromise();

      job = new ImportFeatureStreamJob(db);

      BatchJobService.instance = {
        added: sinon.stub(),
        updated: sinon.stub(),
      };
    });

    afterEach(async function () {
      await db.close();
      await mongoServer.stop();
    });

    it("adds a new feature when it does not have an _id", async function () {
      const feature = {
        name: "feature 1",
      };

      await job.saveRecord(feature);

      const storedFeature = await db.getItemWithQuery("sites", {
        name: "feature 1",
      });

      expect(storedFeature).to.exist;
    });

    // it("triggers an add notification on new features", async function () {
    //   const feature = {
    //     name: "feature 2",
    //   };

    //   await job.saveRecord(feature);

    //   const storedFeature = await db.getItemWithQuery("sites", {
    //     name: "feature 2",
    //   });
    //   BatchJobService.instance.updated.calledOnce.should.equal(false);
    //   assert.calledWith(
    //     BatchJobService.instance.added,
    //     "Feature",
    //     storedFeature
    //   );
    // });

    it("updates a feature when it has an _id field", async function () {
      const feature = {
        name: "feature 3",
      };

      const result = await db.addItem("sites", feature);

      await job.saveRecord({
        _id: result.insertedId,
        name: "new feature 3",
      });

      const insertedFeature = await db.getItemWithQuery("sites", {
        name: "feature 3",
      });
      const storedFeature = await db.getItemWithQuery("sites", {
        name: "new feature 3",
      });

      expect(insertedFeature).not.to.exist;
      expect(storedFeature).to.exist;
    });

    // it("triggers an update notification on new features", async function () {
    //   const feature = {
    //     name: "feature 4",
    //   };

    //   const result = await db.addItem("sites", feature);

    //   await job.saveRecord({
    //     _id: result.insertedId,
    //     name: "new feature 4",
    //   });

    //   const storedFeature = await db.getItemWithQuery("sites", {
    //     name: "new feature 4",
    //   });

    //   BatchJobService.instance.added.calledOnce.should.equal(false);
    //   assert.calledWith(
    //     BatchJobService.instance.updated,
    //     "Feature",
    //     {
    //       _id: storedFeature._id,
    //       name: "feature 4",
    //     },
    //     storedFeature
    //   );
    // });
  });

  describe("deleteOldRecords", function () {
    let job;
    let db;
    let mongoServer;

    beforeEach(async function () {
      this.timeout(60000);
      mongoServer = await MongoMemoryServer.create();
      await mongoServer.getUri();

      db = new Db({
        url: `mongodb://${mongoServer._instanceInfo.ip}:${mongoServer._instanceInfo.port}/tests`,
        configuration: { collection: "test" },
      });
      await db.connectWithPromise();

      job = new ImportFeatureStreamJob(db);
      job.transformOptions = {
        source: {
          type: "DataBinding",
          modelId: "1",
        },
      };
    });

    afterEach(async function () {
      await db.close();
      await mongoServer.stop();
    });

    it("removes records older than startAt when deleteNonSyncedRecords is true", async function () {
      const feature = {
        name: "feature 3",
        source: {
          type: "DataBinding",
          modelId: "1",
          syncAt: new Date("2020-02-02T12:23:22Z"),
        },
      };

      await db.addItem("sites", feature);
      job.deleteNonSyncedRecords = true;
      job.startAt = new Date("2022-02-02T12:23:22Z");

      await job.deleteOldRecords();

      const allFeatures = await db.query("sites", {});

      expect(allFeatures).to.have.length(0);
    });

    it("does not remove records newer than startAt when deleteNonSyncedRecords is true", async function () {
      const feature = {
        name: "feature 3",
        source: {
          type: "DataBinding",
          modelId: "1",
          syncAt: new Date("2023-02-02T12:23:22Z"),
        },
      };

      await db.addItem("sites", feature);
      job.deleteNonSyncedRecords = true;
      job.startAt = new Date("2022-02-02T12:23:22Z");

      await job.deleteOldRecords();

      const allFeatures = await db.query("sites", {});

      expect(allFeatures).to.have.length(1);
    });

    it("does not remove records older than startAt when deleteNonSyncedRecords is false", async function () {
      const feature = {
        name: "feature 3",
        source: {
          type: "DataBinding",
          modelId: "1",
          syncAt: new Date("2020-02-02T12:23:22Z"),
        },
      };

      await db.addItem("sites", feature);
      job.deleteNonSyncedRecords = false;
      job.startAt = new Date("2022-02-02T12:23:22Z");

      await job.deleteOldRecords();

      const allFeatures = await db.query("sites", {});
      expect(allFeatures).to.have.length(1);
    });

    it("does not remove records older than startAt when it has a different source model id", async function () {
      const feature = {
        name: "feature 3",
        source: {
          type: "DataBinding",
          modelId: "2",
          syncAt: new Date("2020-02-02T12:23:22Z"),
        },
      };

      await db.addItem("sites", feature);
      job.deleteNonSyncedRecords = true;
      job.startAt = new Date("2022-02-02T12:23:22Z");

      await job.deleteOldRecords();

      const allFeatures = await db.query("sites", {});
      expect(allFeatures).to.have.length(1);
    });

    it("does not remove records older than startAt when it has a different source type id", async function () {
      const feature = {
        name: "feature 3",
        source: {
          type: "File",
          modelId: "1",
          syncAt: new Date("2020-02-02T12:23:22Z"),
        },
      };

      await db.addItem("sites", feature);
      job.deleteNonSyncedRecords = true;
      job.startAt = new Date("2022-02-02T12:23:22Z");

      await job.deleteOldRecords();

      const allFeatures = await db.query("sites", {});
      expect(allFeatures).to.have.length(1);
    });
  });
});
