/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { AnalyzeFeatureStreamJob } from "../../../source/tasks/dataBindings/analyze.js";
import Db from "models/source/db.js";
import { ObjectId } from "mongodb";
import { MongoMemoryServer } from "mongodb-memory-server";
import { expect, should } from "chai";
import { stub } from "sinon";
import { Readable } from "stream";

should();

const airtableDataBinding = {
  _id: new ObjectId("00008c97ecd8490100caba00"),
  name: "",
  destinationMap: "000000000000000000000002",
  extraIcons: [],
  fields: [
    {
      key: "name",
      destination: "attributes.some group.value",
      preview: ["a", "b"],
    },
  ],
  connection: {
    type: "airtable",
    config: {
      _apiKey: "api key",
      baseId: "base id",
      tableName: "table name",
      viewName: "view name",
    },
  },
  visibility: {
    team: new ObjectId("000000000000000000000001"),
    isDefault: false,
    isPrivate: false,
  },
  info: {},
};

describe("AnalyzeFeatureStreamJob", function () {
  describe("prepareFromDataBinding with airtable type", function () {
    let job;
    let db;
    let mongoServer;

    beforeEach(async function () {
      this.timeout(60000);
      mongoServer = await MongoMemoryServer.create();
      await mongoServer.getUri();

      db = new Db({
        url: `mongodb://${mongoServer._instanceInfo.ip}:${mongoServer._instanceInfo.port}/tests`,
        configuration: { collection: "test" },
      });
      await db.connectWithPromise();

      job = new AnalyzeFeatureStreamJob(db);
      await job.prepareDataBinding(airtableDataBinding);
    });

    afterEach(async function () {
      await db.close();
      await mongoServer.stop();
    });

    it("sets the name to include the data binding id", function () {
      job.name.should.equal("airtable import for 00008c97ecd8490100caba00");
    });

    it("sets the mime to json", function () {
      job.mime.should.equal("text/ndjson");
    });

    it("sets the team id", function () {
      job.teamId.toString().should.equal("000000000000000000000001");
    });

    it("sets the map id", function () {
      job.mapId.toString().should.equal("000000000000000000000002");
    });

    it("prepares the stream", function () {
      expect(job.stream).to.exist;
      expect(job.stream.apiKey).to.equal("api key");
      expect(job.stream.baseId).to.equal("base id");
      expect(job.stream.tableName).to.equal("table name");
      expect(job.stream.viewName).to.equal("view name");
    });

    it("sets the resolvers", function () {
      expect(job.resolvers).to.exist;
    });

    it("sets the stored fields without previews", function () {
      expect(job.fields).to.deep.equal({
        name: {
          key: "name",
          destination: "attributes.some group.value",
          preview: [],
        },
      });
    });
  });

  describe("drainQueue", function () {
    let queue;
    let stream;

    beforeEach(function () {
      queue = {
        push: stub(),
        drain: stub(),
      };

      stream = new Readable({
        read() {},
      });
    });

    it("does nothing when there is an empty stream", async function () {
      stream.push("");
      stream.push(null);

      const job = new AnalyzeFeatureStreamJob();
      job.updateDataBinding = stub();
      job.stream = stream;
      job.queue = queue;
      job.mime = "text/ndjson";

      await job.drainQueue();

      queue.push.calledOnce.should.equal(false);
      queue.drain.calledOnce.should.equal(true);
    });

    it("pushes an item when the stream has an object", async function () {
      stream.push(`{}\n`);
      stream.push(null);

      const job = new AnalyzeFeatureStreamJob();
      job.updateDataBinding = stub();
      job.stream = stream;
      job.queue = queue;
      job.mime = "text/ndjson";

      await job.drainQueue();

      queue.push.calledOnce.should.equal(true);
      queue.drain.calledOnce.should.equal(true);
    });
  });

  describe("handleRecord", function () {
    let job;
    let db;
    let mongoServer;

    beforeEach(async function () {
      this.timeout(60000);
      mongoServer = await MongoMemoryServer.create();
      await mongoServer.getUri();

      db = new Db({
        url: `mongodb://${mongoServer._instanceInfo.ip}:${mongoServer._instanceInfo.port}/tests`,
        configuration: { collection: "test" },
      });
      await db.connectWithPromise();

      job = new AnalyzeFeatureStreamJob(db);
      await job.prepareDataBinding(airtableDataBinding);

      job.currentRun = {};
    });

    afterEach(async function () {
      await db.close();
      await mongoServer.stop();
    });

    it("creates a new field for a record with a field", async function () {
      await job.handleRecord({
        key: "value",
      });

      expect(job.fields).to.deep.equal({
        key: {
          destination: "",
          key: "key",
          preview: ["value"],
        },
        name: {
          destination: "attributes.some group.value",
          key: "name",
          preview: [],
        },
      });
    });

    it("adds only one preview value when a record is added twice", async function () {
      await job.handleRecord({
        key: "value",
      });

      await job.handleRecord({
        key: "value",
      });

      expect(job.fields).to.deep.equal({
        key: {
          destination: "",
          key: "key",
          preview: ["value"],
        },
        name: {
          destination: "attributes.some group.value",
          key: "name",
          preview: [],
        },
      });
    });
  });

  describe("updateDataBinding", function () {
    let job;
    let db;
    let mongoServer;

    beforeEach(async function () {
      this.timeout(60000);
      mongoServer = await MongoMemoryServer.create();
      await mongoServer.getUri();

      db = new Db({
        url: `mongodb://${mongoServer._instanceInfo.ip}:${mongoServer._instanceInfo.port}/tests`,
        configuration: { collection: "test" },
      });
      await db.connectWithPromise();

      job = new AnalyzeFeatureStreamJob(db);
      await job.prepareDataBinding(airtableDataBinding);

      job.currentRun = {};

      await db.addItem("dataBinding", airtableDataBinding);
    });

    afterEach(async function () {
      await db.close();
      await mongoServer.stop();
    });

    it("ignores fields with no preview values", async function () {
      await job.updateDataBinding();

      const dataBinding = await db.getItem(
        "dataBinding",
        airtableDataBinding._id
      );

      expect(dataBinding.fields).to.deep.equal([]);
    });

    it("adds the fields when they have preview values", async function () {
      job.fields = {
        key: {
          key: "key",
          destination: "destination",
          preview: [1, 2],
        },
      };

      await job.updateDataBinding();

      const dataBinding = await db.getItem(
        "dataBinding",
        airtableDataBinding._id
      );

      expect(dataBinding.fields).to.deep.equal([
        {
          key: "key",
          destination: "destination",
          preview: [1, 2],
        },
      ]);
    });
  });
});
