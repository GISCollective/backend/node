/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import {
  MapFileResolvers,
  termsToQuery,
} from "../../../../source/tasks/dataBindings/lib/MapFileResolvers.js";
import { MongoMemoryServer } from "mongodb-memory-server";
import Db from "models/source/db.js";
import { ObjectId } from "mongodb";
import { should, expect as _expect } from "chai";
should();
const expect = _expect;

describe("MapFileResolvers", function () {
  let service;
  let db;
  let mongoServer;

  beforeEach(async function () {
    this.timeout(60000);

    mongoServer = await MongoMemoryServer.create();
    const url = await mongoServer.getUri();

    db = new Db({
      url,
      configuration: { collection: "test" },
    });
    await db.connectWithPromise();
  });

  afterEach(async function () {
    await db.close();
    await mongoServer.stop();
  });

  describe("termsToQuery", function () {
    it("returns an empty query when there are no terms", function () {
      const result = termsToQuery([]);

      expect(result).to.deep.equal([]);
    });

    it("returns an id query when there is one id", function () {
      const result = termsToQuery(["60bd37e9cf0b02de7b998a36"]);

      expect(result).to.deep.equal([
        { _id: new ObjectId("60bd37e9cf0b02de7b998a36") },
      ]);
    });

    it("returns two ids query when there are two ids", function () {
      const result = termsToQuery([
        "60bd37e9cf0b02de7b998a36",
        "60bd37e9cf0b02de7b998a37",
      ]);

      expect(result).to.deep.equal([
        {
          _id: {
            $in: [
              new ObjectId("60bd37e9cf0b02de7b998a36"),
              new ObjectId("60bd37e9cf0b02de7b998a37"),
            ],
          },
        },
      ]);
    });

    it("returns a name query when there is one name", function () {
      const result = termsToQuery(["some name"]);

      expect(result).to.deep.equal([
        { name: "some name" },
        { otherNames: "some name" },
      ]);
    });

    it("returns a name query when there are two names", function () {
      const result = termsToQuery(["name1", "name2"]);

      expect(result).to.deep.equal([
        { name: { $in: ["name1", "name2"] } },
        { otherNames: { $all: ["name1", "name2"] } },
      ]);
    });

    it("adds all names when there a list of names", function () {
      const result = termsToQuery([
        "Awareness Raising",
        "Conservation & Biodiversity",
        "Activism & Lobbying",
        "Sharing Economy",
        "Transport",
        "Health & Wellbeing",
        "Greenspaces",
        "Waste",
        "Adaptation & Resilience",
        "Art & Culture",
        "Community Support",
        "Energy",
        "Education",
        "Food Growing",
      ]);

      expect(result).to.deep.equal([
        {
          name: {
            $in: [
              "Awareness Raising",
              "Conservation & Biodiversity",
              "Activism & Lobbying",
              "Sharing Economy",
              "Transport",
              "Health & Wellbeing",
              "Greenspaces",
              "Waste",
              "Adaptation & Resilience",
              "Art & Culture",
              "Community Support",
              "Energy",
              "Education",
              "Food Growing",
            ],
          },
        },
        {
          otherNames: {
            $all: [
              "Awareness Raising",
              "Conservation & Biodiversity",
              "Activism & Lobbying",
              "Sharing Economy",
              "Transport",
              "Health & Wellbeing",
              "Greenspaces",
              "Waste",
              "Adaptation & Resilience",
              "Art & Culture",
              "Community Support",
              "Energy",
              "Education",
              "Food Growing",
            ],
          },
        },
      ]);
    });
  });

  describe("when a map and a feature are defined", function () {
    let featureId;
    let team;
    let map;
    let otherMap;
    let otherTeam;

    beforeEach(async function () {
      otherTeam = new ObjectId("00008c97ecd8490100caba00");
      otherMap = new ObjectId("00008c97ecd8490100caba00");
      team = new ObjectId("5ca88c97ecd8490100caba38");

      const mapResult = await db.addItem("maps", {
        visibility: {
          team,
          isPublic: false,
          isDefault: false,
        },
      });

      map = mapResult.insertedId;

      const result = await db.addItem("sites", {
        name: "test feature",
        maps: [map],
        source: {
          remoteId: "test",
        },
      });

      await db.addItem("sites", {
        name: "other feature",
        maps: [map],
      });

      featureId = result.insertedId;
    });

    describe("featureByName with a selected map", function () {
      it("returns a feature when it matches by name and selectedMap 1", async function () {
        service = new MapFileResolvers(db, team, map);
        const result = await service.featureByName(`test`);

        result._id.toString().should.equal(`${featureId}`);
      });

      it("returns undefined when another map is selected", async function () {
        service = new MapFileResolvers(db, team, otherMap);
        const result = await service.featureByName(`test`);

        expect(result).to.be.undefined;
      });

      it("returns undefined when the map is not accessible by the team", async function () {
        service = new MapFileResolvers(db, otherTeam, map);
        const result = await service.featureByName(`test`);

        expect(result).to.be.undefined;
      });
    });

    describe("featureByName with no selected map", function () {
      it("returns a feature when it matches by name", async function () {
        service = new MapFileResolvers(db, team);
        const result = await service.featureByName(`test`);

        result._id.toString().should.equal(`${featureId}`);
      });

      it("returns undefined when the map is not accessible by the team", async function () {
        service = new MapFileResolvers(db, otherTeam);
        const result = await service.featureByName(`test`);

        expect(result).to.be.undefined;
      });
    });

    describe("featureById", function () {
      it("returns a feature when it matches", async function () {
        service = new MapFileResolvers(db, team);
        const result = await service.featureById(`${featureId}`);

        result._id.toString().should.equal(`${featureId}`);
      });

      it("returns undefined when the map is not accessible by the team", async function () {
        service = new MapFileResolvers(db, otherTeam);
        const result = await service.featureById(`${featureId}`);

        expect(result).to.be.undefined;
      });
    });

    describe("featureByRemoteId with a selected map", function () {
      it("returns a feature when it matches by name and selectedMap", async function () {
        service = new MapFileResolvers(db, team, map);
        const result = await service.featureByRemoteId(`test`);

        result._id.toString().should.equal(`${featureId}`);
      });

      it("returns undefined when another map is selected", async function () {
        service = new MapFileResolvers(db, team, otherMap);
        const result = await service.featureByRemoteId(`test`);

        expect(result).to.be.undefined;
      });

      it("returns undefined when the map is not accessible by the team", async function () {
        service = new MapFileResolvers(db, otherTeam, map);
        const result = await service.featureByRemoteId(`test`);

        expect(result).to.be.undefined;
      });
    });

    describe("featureByRemoteId with no selected map", function () {
      it("returns a feature when it matches by name", async function () {
        service = new MapFileResolvers(db, team);
        const result = await service.featureByRemoteId(`test`);

        result._id.toString().should.equal(`${featureId}`);
      });

      it("returns undefined when the map is not accessible by the team", async function () {
        service = new MapFileResolvers(db, otherTeam);
        const result = await service.featureByRemoteId(`test`);

        expect(result).to.be.undefined;
      });
    });
  });

  describe("when there is a public icon set with an icon", function () {
    let iconId;
    let team;
    let iconSet;
    let otherTeam;

    beforeEach(async function () {
      otherTeam = new ObjectId("00008c97ecd8490100caba00");
      team = new ObjectId("5ca88c97ecd8490100caba38");

      const iconSetResult = await db.addItem("iconsets", {
        visibility: {
          team,
          isPublic: true,
          isDefault: false,
        },
      });

      iconSet = iconSetResult.insertedId;

      const result = await db.addItem("icons", {
        name: "test icon",
        iconSet,
      });

      await db.addItem("icons", {
        name: "other icon",
        iconSet,
      });

      iconId = result.insertedId;
    });

    it("matches the icon by id from the team", async function () {
      service = new MapFileResolvers(db, team);
      const result = await service.queryIcons([`${iconId}`]);

      expect(result).to.have.length(1);
      expect(result[0]?.toString()).to.equal(`${iconId}`);
    });

    it("matches the icon by id from other team", async function () {
      service = new MapFileResolvers(db, otherTeam);
      const result = await service.queryIcons([`${iconId}`]);

      expect(result).to.have.length(1);
      expect(result[0]?.toString()).to.equal(`${iconId}`);
    });

    it("matches the icon by name from the team", async function () {
      service = new MapFileResolvers(db, team);
      const result = await service.queryIcons([`test icon`]);

      expect(result).to.have.length(1);
      expect(result[0]?.toString()).to.equal(`${iconId}`);
    });

    it("matches the icon by name from other team", async function () {
      service = new MapFileResolvers(db, team);
      const result = await service.queryIcons([`test icon`]);

      expect(result).to.have.length(1);
      expect(result[0]?.toString()).to.equal(`${iconId}`);
    });
  });

  describe("when there is a private icon set with an icon", function () {
    let iconId;
    let team;
    let iconSet;
    let otherTeam;

    beforeEach(async function () {
      team = new ObjectId("5ca88c97ecd8490100caba38");
      otherTeam = new ObjectId("00008c97ecd8490100caba00");

      const iconSetResult = await db.addItem("iconsets", {
        visibility: {
          team,
          isPublic: false,
          isDefault: false,
        },
      });

      iconSet = iconSetResult.insertedId;

      const result = await db.addItem("icons", {
        name: "test icon",
        iconSet,
      });

      await db.addItem("icons", {
        name: "other icon",
        iconSet,
      });

      iconId = result.insertedId;
    });

    it("matches the icon by id from the team", async function () {
      service = new MapFileResolvers(db, team);
      const result = await service.queryIcons([`${iconId}`]);

      expect(result).to.have.length(1);
      expect(result[0]?.toString()).to.equal(`${iconId}`);
    });

    it("does not match the icon by id from other team", async function () {
      service = new MapFileResolvers(db, otherTeam);
      const result = await service.queryIcons([`${iconId}`]);

      expect(result).to.have.length(0);
    });

    it("matches the icon by name from the team", async function () {
      service = new MapFileResolvers(db, team);
      const result = await service.queryIcons([`test icon`]);

      expect(result).to.have.length(1);
      expect(result[0]?.toString()).to.equal(`${iconId}`);
    });

    it("matches the icon by name from other team", async function () {
      service = new MapFileResolvers(db, otherTeam);
      const result = await service.queryIcons([`test icon`]);

      expect(result).to.have.length(0);
    });
  });
});
