/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { pictureAnalize } from "../../../source/tasks/pictures/analyze.js";
import Db from "models/source/db.js";
import { MongoMemoryServer } from "mongodb-memory-server";
import { should } from "chai";
import { uploadFile } from "models/source/files.js";
should();

describe("The Pictures.Analyze task", function () {
  let mongoServer;
  let db;
  let webpFile;
  let heicFile;
  let xlxsFile;

  beforeEach(async function () {
    this.timeout(60000);

    mongoServer = await MongoMemoryServer.create();
    const url = mongoServer.getUri();

    db = new Db(
      {
        url,
        configuration: { collection: "test" },
      },
      false
    );

    await db.connectWithPromise();

    const storage = db.getFileStorage("pictures");

    webpFile = await uploadFile(storage, "test/_files/test.webp", {
      mime: "image/webp",
    });

    heicFile = await uploadFile(storage, "test/_files/test.heic", {
      mime: "image/heic",
    });

    xlxsFile = await uploadFile(storage, "test/_files/test.xlsx", {
      mime: "application/xlsx",
    });
  });

  afterEach(async function () {
    await db.close();
    await mongoServer.stop();
  });

  it("updates the picture size when there is a webp image with no width", async function () {
    this.timeout(10000);

    const insertResult = await db.addItem("pictures", {
      picture: webpFile._id,
      meta: { height: 200 },
    });

    await pictureAnalize(db, { id: insertResult.insertedId.toString() });

    const picture = await db.getItem("pictures", insertResult.insertedId);

    picture.meta.format.should.deep.equal("WEBP");
    picture.meta.height.should.deep.equal(3024);
    picture.meta.mime.should.deep.equal("image/webp");
    picture.meta.width.should.deep.equal(4032);
  });

  it("ignores the picture size when there is a xlsx image with no size", async function () {
    this.timeout(10000);

    const insertResult = await db.addItem("pictures", {
      picture: xlxsFile._id,
      meta: {},
    });

    await pictureAnalize(db, { id: insertResult.insertedId.toString() });

    const picture = await db.getItem("pictures", insertResult.insertedId);

    picture.meta.should.deep.equal({});
  });
});
