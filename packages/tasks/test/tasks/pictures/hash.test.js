/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { pictureHash } from "../../../source/tasks/pictures/hash.js";
import { createStubInstance, spy } from "sinon";
import Db from "models/source/db.js";
import { ImageHash } from "../../../source/tasks/pictures/lib/hash.js";
import { ImageResize } from "../../../source/tasks/pictures/lib/resize.js";
import { createReadStream } from "fs";
import { resolve } from "path";
import { should } from "chai";
should();

import path from "path";
import { fileURLToPath } from "url";
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

describe("The Pictures.Hash task", function () {
  let readStream;

  beforeEach(function () {
    readStream = createReadStream(
      resolve(__dirname, "..", "..", "_files", "logo.png")
    );
  });

  it("can successfully convert an image to hash", async function () {
    const db = createStubInstance(Db, {
      getItem: {
        picture: "5d7691374100fa0100497efc",
      },
      getFile: readStream,
      setItem: spy(),
    });

    ImageResize.instance = new ImageResize();
    ImageHash.instance = new ImageHash();

    await pictureHash(db, { id: "some-id" });

    db.setItem.lastCall.firstArg.should.equal("pictures");
    db.setItem.lastCall.lastArg.hash.should.equal(
      "UT6.aBiTQ8l0uikRV{eaU[gZVXaKbdaOVEb:"
    );
  });
});
