/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import { createStubInstance, stub, spy } from "sinon";
import Db from "models/source/db.js";
import BatchJobService from "../../source/lib/BatchJobService.js";
import CalendarService from "../../source/lib/CalendarService.js";

describe("BatchJobService", function () {
  let service;
  let db;

  beforeEach(function () {
    db = createStubInstance(Db, {
      getItem: stub(),
      getItemWithQuery: stub(),
      setItem: spy(),
      addItem: spy(),
    });

    service = new BatchJobService(db);
    CalendarService.instance = {
      get now() {
        return "now";
      },
    };
  });

  it("creates a batch job when the name does not exist", async function () {
    db.getItemWithQuery
      .onCall(0)
      .returns(undefined)
      .onCall(1)
      .returns({ _id: "queried-item" });

    const result = await service.upsertByName("new job", "team-id");

    db.addItem.lastCall.args.should.deep.equal([
      "batchjobs",
      {
        info: {
          author: "@batch",
          changeIndex: 0,
          createdOn: "now",
          lastChangeOn: "now",
        },
        visibility: {
          isPublic: false,
          team: "team-id",
        },
        name: "new job",
        runHistory: [],
      },
    ]);

    expect(result).to.deep.equal({ _id: "queried-item" });
  });

  it("returns the current batch job when the name exists", async function () {
    db.getItemWithQuery.onCall(0).returns({ _id: "new-item" });

    const result = await service.upsertByName("new job", "team-id");

    db.addItem.calledOnce.should.equal(false);
    expect(result).to.deep.equal({ _id: "new-item" });
  });
});
