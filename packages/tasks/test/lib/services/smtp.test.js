/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { getSmtpConfig, sendEmail } from "../../../source/lib/services/smtp.js";
import { updateEmailList } from "../../../source/tasks/notifications/email.js";
import Db from "models/source/db.js";
import { MongoMemoryServer } from "mongodb-memory-server";
import { mock, createTransport } from "nodemailer-mock";
import { expect, should } from "chai";

should();

describe("The Notifications.Email task", function () {
  let mongoServer;
  let db;

  beforeEach(async function () {
    this.timeout(60000);

    mongoServer = await MongoMemoryServer.create();
    await mongoServer.getUri();

    db = new Db({
      url: `mongodb://${mongoServer._instanceInfo.ip}:${mongoServer._instanceInfo.port}/tests`,
      configuration: { collection: "test" },
    });

    await db.connectWithPromise();
  });

  afterEach(async function () {
    await db.close();
    await mongoServer.stop();
  });

  describe("getting the smtp configuration", function () {
    beforeEach(async function () {
      await db.addItem("preference", {
        name: "secret.smtp.authType",
        value: "auth type",
      });
      await db.addItem("preference", {
        name: "secret.smtp.connectionType",
        value: "connection type",
      });
      await db.addItem("preference", {
        name: "secret.smtp.tlsValidationMode",
        value: "validation mode",
      });
      await db.addItem("preference", {
        name: "secret.smtp.tlsVersion",
        value: "version",
      });
      await db.addItem("preference", {
        name: "secret.smtp.host",
        value: "smtp.example.com",
      });
      await db.addItem("preference", {
        name: "secret.smtp.port",
        value: "587",
      });
      await db.addItem("preference", {
        name: "secret.smtp.localname",
        value: "local name",
      });
      await db.addItem("preference", {
        name: "secret.smtp.password",
        value: "pass",
      });
      await db.addItem("preference", {
        name: "secret.smtp.username",
        value: "user",
      });
      await db.addItem("preference", {
        name: "secret.smtp.from",
        value: "from",
      });
    });

    it("queries the preferences from db and creates the preferences object", async function () {
      const result = await getSmtpConfig(db);

      result.should.deep.equal({
        host: "smtp.example.com",
        port: 587,
        secure: false,
        tls: {
          rejectUnauthorized: false,
        },
        auth: {
          user: "user",
          pass: "pass",
        },
      });
    });
  });

  describe("sending messages", function () {
    this.afterEach(function () {
      mock.reset();
    });

    it("sends a text message to an email", async function () {
      const transport = createTransport({});

      await sendEmail(transport, {
        from: "me@a.b",
        to: ["other@a.b"],
        subject: "subject",
        text: "some text message",
        html: undefined,
      });

      const sentMail = mock.getSentMail();

      sentMail.should.deep.equal([
        {
          headers: {},
          from: "me@a.b",
          subject: "subject",
          text: "some text message",
          html: undefined,
          to: "other@a.b",
        },
      ]);
    });

    it("sends a text message to two emails", async function () {
      const transport = createTransport({});

      await sendEmail(transport, {
        from: "me@a.b",
        to: ["1@a.b", "2@a.b"],
        subject: "subject",
        text: "some text message",
        html: undefined,
      });

      const sentMail = mock.getSentMail();

      sentMail.should.deep.equal([
        {
          headers: {},
          from: "me@a.b",
          subject: "subject",
          text: "some text message",
          html: undefined,
          to: "1@a.b",
        },
        {
          headers: {},
          from: "me@a.b",
          subject: "subject",
          text: "some text message",
          html: undefined,
          to: "2@a.b",
        },
      ]);
    });

    it("triggers the onSendEvent for each email", async function () {
      const transport = createTransport({});
      const emails = [];

      await sendEmail(
        transport,
        {
          from: "me@a.b",
          to: ["1@a.b", "2@a.b"],
          subject: "subject",
          text: "some text message",
          html: undefined,
        },
        {},
        (email) => {
          emails.push(email);
        }
      );

      expect(emails).to.deep.equal(["1@a.b", "2@a.b"]);
    });

    it("triggers the onFailEvent for each failed email", async function () {
      const transport = createTransport({});
      mock.setShouldFailOnce();

      const emails = [];

      await sendEmail(
        transport,
        {
          from: "me@a.b",
          to: ["1@a.b", "2@a.b"],
          subject: "subject",
          text: "some text message",
          html: undefined,
        },
        {},
        () => {},
        (email) => {
          emails.push(email);
        }
      );

      expect(emails).to.deep.equal(["1@a.b"]);
    });

    it("replaces the personalization variables", async function () {
      const message = {
        from: "me@a.b",
        to: ["1@a.b", "2@a.b"],
        subject: "subject",
        text: "id={$id}",
        html: "id={$id}",
      };

      const transport = createTransport({});

      await sendEmail(transport, message, [
        {
          email: "1@a.b",
          substitutions: [
            {
              var: "id",
              value: "one",
            },
          ],
        },
      ]);

      const sentMail = mock.getSentMail();

      sentMail.should.deep.equal([
        {
          headers: {},
          from: "me@a.b",
          subject: "subject",
          to: "1@a.b",
          text: "id=one",
          html: "id=one",
        },
        {
          headers: {},
          from: "me@a.b",
          subject: "subject",
          to: "2@a.b",
          text: "id={$id}",
          html: "id={$id}",
        },
      ]);
    });
  });

  describe("updateEmailList", function () {
    it("returns an empty list when there is no user devined", async function () {
      const resolved = await updateEmailList(db, ["a@a.s"]);

      resolved.should.have.length(0);
    });

    describe("when there is a user in the db", function () {
      let userId;

      beforeEach(async function () {
        const result = await db.addItem("users", {
          email: "user@me.com",
          firstName: "John",
          lastName: "Doe",
          username: "test",
        });

        userId = result.insertedId.toString();
      });

      it("returns the user when it's email is found", async function () {
        const resolved = await updateEmailList(db, ["user@me.com"]);
        resolved.should.deep.equal(["user@me.com"]);
      });

      it("replaces ids with emails", async function () {
        const resolved = await updateEmailList(db, [userId]);
        resolved.should.deep.equal(["user@me.com"]);
      });

      it("removes the emails that are not in the db", async function () {
        const resolved = await updateEmailList(db, [
          "user@me.com",
          "other@me.com",
        ]);
        resolved.should.deep.equal(["user@me.com"]);
      });
    });
  });
});
