/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import {
  getMailerSendConfig,
  sendBulkEmail,
} from "../../../source/lib/services/mailersend.js";
import Db from "models/source/db.js";
import { MongoMemoryServer } from "mongodb-memory-server";
import { should } from "chai";

should();

describe("The mailer send lib", function () {
  let mongoServer;
  let db;

  beforeEach(async function () {
    this.timeout(60000);

    mongoServer = await MongoMemoryServer.create();
    await mongoServer.getUri();

    db = new Db({
      url: `mongodb://${mongoServer._instanceInfo.ip}:${mongoServer._instanceInfo.port}/tests`,
      configuration: { collection: "test" },
    });

    await db.connectWithPromise();

    await db.addItem("preference", {
      name: "appearance.name",
      value: "appearance.name",
    });
  });

  afterEach(async function () {
    await db?.close?.();
    await mongoServer?.stop?.();
  });

  describe("getting the mailer send configuration", function () {
    beforeEach(async function () {
      await db.addItem("preference", {
        name: "secret.mailersend.key",
        value: "key",
      });
      await db.addItem("preference", {
        name: "secret.mailersend.from",
        value: "from",
      });
    });

    it("queries the preferences from db and creates the preferences object", async function () {
      const result = await getMailerSendConfig(db);

      result.should.deep.equal({
        key: "key",
        from: "from",
      });
    });
  });

  describe("sendBulkEmail", function () {
    let mailersend;
    let emailArgs;
    let message;

    beforeEach(async function () {
      emailArgs = {};
      mailersend = {
        send(args) {
          emailArgs = JSON.parse(JSON.stringify(args));
        },
      };

      message = {
        to: [],
        subject: "test email",
        html: "html",
        text: "text",
      };

      await db.addItem("preference", {
        name: "secret.mailersend.key",
        value: "key",
      });
      await db.addItem("preference", {
        name: "secret.mailersend.from",
        value: "from",
      });
    });

    it("sends a text message to an email", async function () {
      message.to = ["me@a.b"];

      await sendBulkEmail(db, mailersend, message);

      emailArgs.should.deep.equal({
        from: {
          email: "from",
          name: "appearance.name",
        },
        html: "html",
        personalization: [],
        to: [
          {
            email: "me@a.b",
          },
        ],
        subject: "test email",
        text: "text",
      });
    });

    it("sends a text message to 2 emails", async function () {
      message.to = ["1@a.b", "2@a.b"];

      await sendBulkEmail(db, mailersend, message);

      emailArgs.should.deep.equal({
        from: {
          email: "from",
          name: "appearance.name",
        },
        html: "html",
        personalization: [],
        to: [
          {
            email: "1@a.b",
          },
          {
            email: "2@a.b",
          },
        ],
        subject: "test email",
        text: "text",
      });
    });

    it("triggers the onSendEvent for each email", async function () {
      let emails = [];
      message.to = ["1@a.b", "2@a.b"];

      await sendBulkEmail(db, mailersend, message, {}, (email) => {
        emails.push(email);
      });

      emails.should.to.deep.equal(["1@a.b", "2@a.b"]);
    });

    it("triggers the onFailEvent for each failed email", async function () {
      let emails = [];
      message.to = ["1@a.b", "2@a.b"];

      mailersend.send = () => {
        throw new Error();
      };

      await sendBulkEmail(
        db,
        mailersend,
        message,
        {},
        () => {},
        (email) => {
          emails.push(email);
        }
      );

      emails.should.to.deep.equal(["1@a.b", "2@a.b"]);
    });

    it("passes the personalization object", async function () {
      message.to = ["1@a.b", "2@a.b"];

      await sendBulkEmail(db, mailersend, message, [
        {
          email: "1@a.b",
          substitutions: [
            {
              var: "id",
              value: "value",
            },
          ],
        },
      ]);

      emailArgs.should.deep.equal({
        from: {
          email: "from",
          name: "appearance.name",
        },
        html: "html",
        personalization: [
          {
            email: "1@a.b",
            substitutions: [
              {
                var: "id",
                value: "value",
              },
            ],
          },
        ],
        to: [{ email: "1@a.b" }, { email: "2@a.b" }],
        subject: "test email",
        text: "text",
      });
    });
  });
});
