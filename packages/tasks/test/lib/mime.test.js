/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import { mimeFromFileObject } from "../../source/lib/mime.js";

describe("mimeFromFileObject", function () {
  it("returns application binary for a file with no data", async function () {
    const result = mimeFromFileObject({});
    expect(result).to.equal("application/binary");
  });

  it("returns the mime meta if it is set", async function () {
    const result = mimeFromFileObject({
      metadata: {
        mime: "application/some",
      },
    });

    expect(result).to.equal("application/some");
  });

  it("returns a mime based on the file extension when the meta is not set", async function () {
    const result = mimeFromFileObject({
      filename: "test.geoJson",
    });

    expect(result).to.equal("application/geo+json");
  });
});
