/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/

let db;
let mongoServer;

async function setupDb() {
  mongoServer = new MongoMemoryServer();
  await mongoServer.getUri();

  db = new Db({
    url: `mongodb://${mongoServer._instanceInfo.ip}:${mongoServer._instanceInfo.port}/tests`,
    configuration: { collection: "test" },
  });

  await db.connectWithPromise();

  return db;
}

async function stopDb() {
  await db.close();
  await mongoServer.stop();
}
