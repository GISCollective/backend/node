/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { sourceAttributesClean } from "../../source/tasks/sourceAttributesClean.js";
import Db from "models/source/db.js";
import { MongoMemoryServer } from "mongodb-memory-server";
import { expect } from "chai";
import { ObjectId } from "mongodb";

import { should } from "chai";
should();

describe("sourceAttributesClean", function () {
  let broadcast;
  let db;
  let mongoServer;

  beforeEach(async function () {
    this.timeout(60000);

    mongoServer = await MongoMemoryServer.create();
    await mongoServer.getUri();

    db = new Db({
      url: `mongodb://${mongoServer._instanceInfo.ip}:${mongoServer._instanceInfo.port}/tests`,
      configuration: { collection: "test" },
    });

    await db.connectWithPromise();
  });

  afterEach(async function () {
    await db.close();
    await mongoServer.stop();
  });

  beforeEach(function () {
    broadcast = {
      messages: [],
      push(channel, message) {
        this.messages.push({ channel, message });
      },
    };
  });

  describe("when there is an feature without source", function () {
    it("broadcasts an icon deletion", async function () {
      await sourceAttributesClean({}, db, broadcast);
      broadcast.messages.length.should.deep.equal(0);
    });
  });

  describe("when there is a feature with a source attribute", function () {
    let feature;

    beforeEach(async function () {
      feature = {
        attributes: {
          source: {
            type: "campaign",
            answer: "616bcd1048f38e01006a9d01",
            campaign: "60342b3c33a972010038a3c4",
          },
        },
      };
    });

    it("updates the features by moving the attribute to the model fields", async function () {
      const result = await sourceAttributesClean(feature, db, broadcast);

      result.should.deep.equal("sourceAttributesClean");

      expect(feature).to.deep.equal({
        source: {
          type: "Campaign",
          modelId: "60342b3c33a972010038a3c4",
          remoteId: "616bcd1048f38e01006a9d01",
        },
        attributes: {},
      });
    });
  });

  describe("when there is a feature with the source field and source attribute", function () {
    let feature;

    beforeEach(async function () {
      feature = {
        source: {},
        attributes: {
          source: {
            type: "campaign",
            answer: "616bcd1048f38e01006a9d01",
            campaign: "60342b3c33a972010038a3c4",
          },
        },
      };
    });

    it("does not update the featureswhen the source is set", async function () {
      const result = await sourceAttributesClean(feature, db);

      result.should.deep.equal(false);
      expect(feature).to.deep.equal({
        source: {},
        attributes: {
          source: {
            type: "campaign",
            answer: "616bcd1048f38e01006a9d01",
            campaign: "60342b3c33a972010038a3c4",
          },
        },
      });
    });
  });
});
