/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import { visibilityCheck } from "../../source/tasks/visibilityCheck.js";
import Db from "models/source/db.js";
import { MongoMemoryServer } from "mongodb-memory-server";
import CalendarService from "tasks/source/lib/CalendarService.js";

import { should } from "chai";
should();

describe("visibilityCheck", function () {
  let privateMapId;
  let now;
  let mongoServer;
  let broadcast;
  let db;

  beforeEach(async function () {
    this.timeout(60000);

    mongoServer = await MongoMemoryServer.create();
    const url = mongoServer.getUri();

    db = new Db({
      url,
      configuration: { collection: "test" },
    });

    await db.connectWithPromise();

    now = CalendarService.instance.nowDate;
    let result = await db.addItem("maps", {
      visibility: {
        isDefault: false,
        isPublic: false,
        team: "000000000000000000000002",
      },
      info: {
        lastChangeOn: now,
      },
    });

    privateMapId = result.insertedId;
  });

  afterEach(async function () {
    await db.close();
    await mongoServer.stop();
  });

  it("updates the feature with the computed visibility when they are not the same", async function () {
    const feature = {
      _id: "",
      maps: [privateMapId],
      visibility: 1,
      info: {
        lastChangeOn: now,
      },
    };

    const results = await visibilityCheck(feature, db, broadcast);

    expect(results).to.deep.equal("visibilityCheck");
    expect(feature.computedVisibility).to.deep.equal({
      isDefault: false,
      isPublic: false,
      team: "000000000000000000000002",
    });
  });

  it("updates the feature with the computed visibility when the team is null", async function () {
    const feature = {
      maps: [privateMapId],
      visibility: 1,
      info: {
        lastChangeOn: now.setYear(1999),
      },
      computedVisibility: {
        isDefault: false,
        isPublic: false,
        team: null,
      },
    };

    const results = await visibilityCheck(feature, db, broadcast);

    expect(results).to.deep.equal("visibilityCheck");
    expect(feature.computedVisibility).to.deep.equal({
      isDefault: false,
      isPublic: false,
      team: "000000000000000000000002",
    });
  });

  it("updates the feature with the computed visibility when the team is undefined", async function () {
    const feature = {
      maps: [privateMapId],
      visibility: 1,
      info: {
        lastChangeOn: now.setYear(1999),
      },
      computedVisibility: {
        isDefault: false,
        isPublic: false,
        team: undefined,
      },
    };

    const result = await visibilityCheck(feature, db, broadcast);

    expect(result).to.deep.equal("visibilityCheck");
    expect(feature.computedVisibility).to.deep.equal({
      isDefault: false,
      isPublic: false,
      team: "000000000000000000000002",
    });
  });

  it("does not update a feature when it has the same visibility as the set", async function () {
    const feature = {
      maps: [privateMapId],
      visibility: 1,
      computedVisibility: {
        isDefault: false,
        isPublic: false,
        team: "000000000000000000000002",
      },
      info: {
        lastChangeOn: now,
      },
    };

    const result = await visibilityCheck(feature, db, broadcast);

    expect(result).to.deep.equal(false);

    expect(feature.computedVisibility).to.deep.equal({
      isDefault: false,
      isPublic: false,
      team: "000000000000000000000002",
    });
  });
});
