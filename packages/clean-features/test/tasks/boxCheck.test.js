/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import { boxCheck } from "../../source/tasks/boxCheck.js";
import Db from "models/source/db.js";
import { MongoMemoryServer } from "mongodb-memory-server";

import { should } from "chai";
should();

describe("boxCheck", function () {
  let featuresBoxCheck;
  let privateMapId;
  let now;
  let mongoServer;
  let db;
  let broadcast;
  let channel;
  let message;

  beforeEach(async function () {
    this.timeout(60000);

    mongoServer = await MongoMemoryServer.create();
    const url = mongoServer.getUri();

    db = new Db({
      url,
      configuration: { collection: "test" },
    });

    await db.connectWithPromise();

    channel = null;
    message = null;

    broadcast = {
      push(c, m) {
        channel = c;
        message = m;
      },
    };

    let result = await db.addItem("maps", {
      info: {
        lastChangeOn: now,
      },
    });

    privateMapId = result.insertedId;
  });

  afterEach(async function () {
    await db.close();
    await mongoServer.stop();
  });

  it("triggers a job when there is no box for a polygon", async function () {
    const feature = {
      _id: 2,
      maps: [privateMapId],
      info: {
        lastChangeOn: now,
      },
      position: {
        type: "Polygon",
        coordinates: [
          [
            [-73.979966, 40.768902],
            [-73.97984, 40.76865000000001],
            [-73.97975, 40.76853],
            [-73.97974000000001, 40.76956000000001],
            [-73.97978999999999, 40.76949999999999],
            [-73.97996999999999, 40.76914999999997],
            [-73.97996000000001, 40.76893999999999],
          ],
        ],
      },
    };

    const result = await boxCheck(feature, db, broadcast);

    expect(channel).to.equal("Feature.Box");
    expect(message).to.deep.equal({ id: 2, uid: 2 });

    expect(result).to.deep.equal(true);
  });

  it("triggers a job when there is a point", async function () {
    const feature ={
      _id: 1,
      maps: [privateMapId],
      position: {
        type: "Point",
        coordinates: [-73.979966, 40.768902],
      },
    }

    const result = await boxCheck(feature, db, broadcast);

    expect(channel).to.equal("Feature.Box");
    expect(message).to.deep.equal({ id: 1, uid: 1 });

    expect(result).to.deep.equal(true);
  });

  it("does not trigger a job when there is a polygon with a box", async function () {
    const feature = {
      maps: [privateMapId],
      position: {
        type: "Polygon",
        coordinates: [
          [
            [-73.979966, 40.768902],
            [-73.97984, 40.76865000000001],
            [-73.97975, 40.76853],
            [-73.97974000000001, 40.76956000000001],
            [-73.97978999999999, 40.76949999999999],
            [-73.97996999999999, 40.76914999999997],
            [-73.97996000000001, 40.76893999999999],
          ],
        ],
      },
      positionBox: {
        type: "Polygon",
        coordinates: [
          [
            [-73.979966, 40.768902],
            [-73.97984, 40.76865000000001],
            [-73.97975, 40.76853],
            [-73.97974000000001, 40.76956000000001],
            [-73.97978999999999, 40.76949999999999],
            [-73.97996999999999, 40.76914999999997],
            [-73.97996000000001, 40.76893999999999],
          ],
        ],
      },
    };

    const result = await boxCheck(feature, db, broadcast);

    expect(channel).not.to.exist;
    expect(message).not.to.exist;

    expect(result).to.deep.equal(false);
  });
});
