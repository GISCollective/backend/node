/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { decorationsCheck } from "../../source/tasks/decorationsCheck.js";
import Db from "models/source/db.js";
import { MongoMemoryServer } from "mongodb-memory-server";
import { expect } from "chai";

import { should } from "chai";
should();

describe("decorationsCheck", function () {
  let mongoServer;
  let db;
  let broadcast;
  let iconId;

  beforeEach(async function () {
    broadcast = {
      messages: [],
      push(channel, message) {
        this.messages.push({ channel, message });
      },
    };

    this.timeout(60000);

    mongoServer = await MongoMemoryServer.create();
    await mongoServer.getUri();

    db = new Db({
      url: `mongodb://${mongoServer._instanceInfo.ip}:${mongoServer._instanceInfo.port}/tests`,
      configuration: { collection: "test" },
    });

    await db.connectWithPromise();

    const result = await db.addItem("icons", {});

    iconId = `${result.insertedId}`;
  });

  afterEach(async function () {
    await db.close();
    await mongoServer.stop();
  });

  it("does not trigger a `Feature.decorators` task when there is no feature", async function () {
    await decorationsCheck({}, db, broadcast);

    expect(broadcast.messages).deep.equal([]);
  });

  describe("when there is a feature without decorators", function () {
    let feature;

    beforeEach(async function () {
      feature = {
        _id: 1,
        info: { changeIndex: 1 },
        icons: [iconId],
      };
    });

    it("triggers a `Feature.decorators` task", async function () {
      await decorationsCheck(feature, db, broadcast);

      expect(broadcast.messages).deep.equal([
        {
          channel: "Feature.decorators",
          message: {
            id: "1",
            uid: "1",
          },
        },
      ]);
    });
  });

  describe("when there is a feature with custom decorators", function () {
    let feature;

    beforeEach(async function () {
      feature = {
        _id: 2,
        decorators: {
          useDefault: false,
        },
      }
    });

    it("does not trigger a `Feature.decorators` task", async function () {
      await decorationsCheck(feature, db, broadcast);

      expect(broadcast.messages).deep.equal([]);
    });
  });

  describe("when there is a feature with the default decorators with matching indexes", function () {
    let feature;

    beforeEach(async function () {
      feature = {
        _id: 3,
        info: {
          changeIndex: 99,
        },
        decorators: {
          useDefault: true,
          changeIndex: 99,
        },
      };
    });

    it("does not trigger a `Feature.decorators` task", async function () {
      await decorationsCheck(feature, db, broadcast);

      expect(broadcast.messages).deep.equal([]);
    });
  });

  describe("when there is a feature with the default decorators with unmatching indexes", function () {
    let feature;

    beforeEach(async function () {
      feature = {
        _id: 4,
        info: {
          changeIndex: 9,
        },
        icons: [iconId],
        decorators: {
          useDefault: true,
          changeIndex: 1,
        },
      };
    });

    it("does not trigger a `Feature.decorators` task", async function () {
      await decorationsCheck(feature, db, broadcast);

      expect(broadcast.messages).deep.equal([]);
    });
  });

  describe("when there is a feature without icons", function () {
    let feature;

    beforeEach(async function () {
      feature = {
        _id: 5,
        info: { changeIndex: 1 },
        icons: [],
      };
    });

    it("triggers a `Feature.decorators` task", async function () {
      await decorationsCheck(feature, db, broadcast);

      expect(broadcast.messages).deep.equal([]);
    });
  });
});
