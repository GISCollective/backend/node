/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { featureNeedsMaskUpdate } from "../../source/tasks/maskCheck.js";

import { should } from "chai";
should();

describe("maskCheck", function () {
  describe("featureNeedsMaskUpdate", function () {
    it("should return true when the feature does not have the unmasked field", function () {
      featureNeedsMaskUpdate({}, 0).should.equal(true);
    });

    describe("when the feature has the unmasked field", function () {
      it("should return false if the mask type is index map", function () {
        featureNeedsMaskUpdate(
          {
            unmasked: { type: "Point", coordinates: [10.44, 20.55] },
            attributes: { mask: { type: "index map" } },
          },
          0
        ).should.equal(false);
      });

      it("should return true if there is no mask type set", function () {
        featureNeedsMaskUpdate(
          {
            unmasked: { type: "Point", coordinates: [10.44, 20.55] },
          },
          0
        ).should.equal(true);
      });

      it("should return false if the number of decimals for masking has not changed", function () {
        featureNeedsMaskUpdate(
          {
            unmasked: { type: "Point", coordinates: [10.44, 20.55] },
            position: { type: "Point", coordinates: [10, 20] },
            attributes: { mask: { type: "0" } },
          },
          0
        ).should.equal(false);
      });

      it("should return true if the number of decimals for masking has changed", function () {
        featureNeedsMaskUpdate(
          {
            unmasked: { type: "Point", coordinates: [10.44, 20.55] },
            position: { type: "Point", coordinates: [10, 20] },
            attributes: { mask: { type: "0" } },
          },
          1
        ).should.equal(true);
      });
    });
  });
});
