/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Db from "models/source/db.js";
import Broadcast from "hmq-js-client/source/broadcast.js";
import Config from "hmq-js-client/source/config.js"
import { decorationsCheck } from "./tasks/decorationsCheck.js";
import { largeGeometryCheck } from "./tasks/largeGeometryCheck.js"
import { boxCheck } from "./tasks/boxCheck.js"
import { maskCheck } from "./tasks/maskCheck.js";
import { visibilityCheck } from "./tasks/visibilityCheck.js";
import { sourceAttributesClean } from "./tasks/sourceAttributesClean.js";
import { simpleFeatureCheck } from "./tasks/simpleFeatureCheck.js";

import path from "path";
import yargs from "yargs";

import { fileURLToPath } from "url";

function resolve(name) {
  return process.env[name];
}

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const argv = yargs(process.argv.slice(2)).default(
  "configuration",
  function randomValue() {
    return path.join(__dirname, `../config/configuration.json`);
  }
).argv;

const appConfig = new Config(argv["configuration"], resolve).toJson();
const dbConfig = new Config(
  appConfig["general"]["db"] + "/mongo.json",
  resolve
).toJson();

const db = new Db(dbConfig, true);
const broadcast = new Broadcast(appConfig["http"], appConfig["httpMq"]);

const jobs = [
  decorationsCheck,
  largeGeometryCheck,
  boxCheck,
  maskCheck,
  visibilityCheck,
  sourceAttributesClean,
  // simpleFeatureCheck
];

const stats = {
  total: 0,
  updated: 0,
  triggered: 0
};

db.connect(async function (err) {
  if (err) {
    return console.error(err);
  }

  console.log("CONNECTED");
  const query = {};

  await db.query("sites", query, async (feature) => {
    let shouldSave = false;

    stats.total++;

    for(const job of jobs) {
      const result = await job(feature, db, broadcast);

      if(result === true) {
        stats.triggered++;
      }

      if(typeof result === "string") {
        if(!stats[result]) {
          stats[result] = 0;
        }

        stats[result]++;
        shouldSave = true;
      }

      if(shouldSave) {
        await db.setItem("sites", feature._id, feature);
      }
    }
  });

  await db.close();

  console.log(stats);
});
