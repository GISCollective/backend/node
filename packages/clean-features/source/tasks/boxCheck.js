/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/

export async function boxCheck(feature, db, broadcast) {
  if (typeof feature.positionBox == "object" && feature.positionBox) {
    return false;
  }

  await broadcast.push("Feature.Box", { id: feature._id, uid: feature._id });
  return true;
}
