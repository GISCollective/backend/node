/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { ObjectId } from "mongodb";

const deepCount = (arr = []) => {
  return arr.reduce((acc, val) => {
    return acc + (Array.isArray(val) ? deepCount(val) : 0);
  }, arr.length);
};

const validTypes = ["Polygon", "MultiPolygon"];

export async function simpleFeatureCheck(feature, db, broadcast) {
  if (!Array.isArray(feature.position?.coordinates)) {
    return false;
  }

  if(!validTypes.includes(feature.position?.type)) {
    return false;
  }

  const points = deepCount(feature.position.coordinates);

  console.log("points", points);

  if (points > 2000) {
    console.log(
      `Skipping feature "${feature._id}" because it has too many points: ${points}`,
    );
    return false;
  }

  const indexedMaps = await db.count("maps", {
    isIndex: true,
    _id: { $in: feature.maps?.map((a) => new ObjectId(a)) },
  });

  if (indexedMaps > 0) {
    console.log(
      `Skipping feature "${feature._id}" because it is on an indexed map.`,
    );
    return false;
  }

  const exists = await db.getItemWithQuery("simpleFeature", {
    featureId: new ObjectId(feature._id),
    _ver: { $ne: 1 },
  });

  if (exists) {
    return false;
  }

  await broadcast.push("Feature.Simplify", {
    id: feature._id,
    uid: feature._id,
  });

  return true;
}
