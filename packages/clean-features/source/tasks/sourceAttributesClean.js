/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/

export async function sourceAttributesClean(feature, db, broadcast) {
  if (feature.source) {
    return false;
  }

  if (feature.attributes?.source?.type != "campaign") {
    return false;
  }

  feature.source = {
    type: "Campaign",
    modelId: feature.attributes.source.campaign,
    remoteId: feature.attributes.source.answer,
  };

  delete feature.attributes.source;

  return "sourceAttributesClean";
}
