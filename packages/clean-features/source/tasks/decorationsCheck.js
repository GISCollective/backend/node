/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { ObjectId } from "mongodb";

export async function decorationsCheck(feature, db, broadcast) {
  if (feature.decorators?.useDefault) {
    return false;
  }

  if (!feature.icons?.length) {
    return false;
  }

  let hasFirstIcon;

  try {
    hasFirstIcon = await db.exists("icons", {
      _id: new ObjectId(feature.icons?.[0]),
    });
  } catch (err) {
    console.log("Can't check if the first icon exists");
  }

  if (!hasFirstIcon) {
    return false;
  }

  if (
    feature.info?.changeIndex != feature.decorators?.changeIndex &&
    feature.icons?.length > 0
  ) {
    await broadcast.push("Feature.decorators", {
      id: `${feature._id}`,
      uid: `${feature._id}`,
    });
  }

  return true;
}
