/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
let maskedMaps = [];
let precision;

export async function maskCheck(feature, db, broadcast) {
  if (maskedMaps.length == 0) {
    await db.query(
      "maps",
      {
        "mask.isEnabled": true,
      },
      async (map) => {
        maskedMaps.push(map._id.toString());
      },
    );
  }

  let shouldBeMasked = false;

  const maps = feature.maps?.map?.(a => a.toString());

  for(const map of maps) {
    if(maskedMaps.includes(map)) {
      shouldBeMasked = true;
      break;
    }
  }

  if (!precision) {
    precision = await db.getItemWithQuery("preference", {
      name: "locationServices.maskingPrecision",
    });
  }

  // need to update the mask?
  if(shouldBeMasked && !featureNeedsMaskUpdate(feature, precision)) {
    return false;
  }

  // need to remove mask?
  if(!shouldBeMasked && !feature.unmasked) {
    return false
  }

  await broadcast.push("Feature.Mask", {
    id: feature._id,
    uid: feature._id,
  });

  return true;
}

export function featureNeedsMaskUpdate(feature, precisionValue) {
  if (!feature.hasOwnProperty("unmasked")) {
    return true;
  }

  if (feature.attributes?.mask?.type == "index map") {
    return false;
  }

  if (feature.attributes?.mask?.type) {
    return feature.attributes.mask.type != precisionValue;
  }

  return true;
}
