/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/

export async function largeGeometryCheck(feature, db, broadcast) {
  const isLarge = feature.isLarge ?? false;
  let pointCount = 0;
  const type = feature?.position?.type;

  if (type == "MultiPolygon") {
    for (const polygon of feature.position.coordinates) {
      for (const ring of polygon) {
        pointCount += ring.length;
      }
    }
  }

  if (type == "Polygon" || type == "MultiLineString") {
    for (const ring of feature.position.coordinates) {
      pointCount += ring.length;
    }
  }

  if (type == "LineString") {
    pointCount += feature.position.coordinates.length;
  }

  const shouldBeLarge = pointCount > 1000;

  if (shouldBeLarge == isLarge) {
    return false;
  }

  await broadcast.push("feature.large", { id: feature._id, uid: feature._id });

  return true;
}
