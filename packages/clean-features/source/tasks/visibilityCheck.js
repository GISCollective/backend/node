/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/

import { getComputedVisibility } from "models/source/validators/feature.js";

export async function visibilityCheck(feature, db, broadcast) {
  const computedVisibility = await getComputedVisibility(db, feature);

  if (
    JSON.stringify(feature?.computedVisibility) ==
    JSON.stringify(computedVisibility)
  ) {
    return false;
  }

  feature.computedVisibility = computedVisibility;

  return "visibilityCheck";
}
