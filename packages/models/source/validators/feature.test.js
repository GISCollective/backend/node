/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { validateFeature, getComputedVisibility } from "./feature.js";
import { should } from "chai";

should();

import Db from "models/source/db.js";
import { MongoMemoryServer } from "mongodb-memory-server";

describe("validateFeature", function () {
  it("returns a message when the feature is not an object", function () {
    const message = validateFeature([]);

    message.should.deep.equal({
      code: "invalid-type",
      message: "The feature must be an object.",
    });
  });

  it("returns a message when all required fields are missing", function () {
    const message = validateFeature({});

    message.should.deep.equal({
      code: "missing-fields",
      vars: "maps, name, description, position",
      message:
        "The feature must have all the required fields: maps, name, description, position.",
    });
  });

  it("returns a message when the name is an empty string", function () {
    const message = validateFeature({
      name: " ",
      description: "",
      position: {},
      maps: [],
    });

    message.should.deep.equal({
      code: "empty-name",
      message: "The feature must have a name.",
    });
  });

  it("returns a message when the map list is empty", function () {
    const message = validateFeature({
      name: "some name",
      description: "",
      position: {},
      maps: [],
    });

    message.should.deep.equal({
      code: "empty-maps",
      message: "The feature must have at least one map.",
    });
  });

  it("returns a message when the position is invalid", function () {
    const message = validateFeature({
      name: "some name",
      description: "",
      position: {},
      maps: ["map-id"],
    });

    message.should.deep.equal({
      code: "invalid-position",
      message: "The feature must be a valid geojson.",
    });
  });

  it("returns a message when the position is 0, 0", function () {
    const message = validateFeature({
      name: "some name",
      description: "",
      position: {
        type: "Point",
        coordinates: [0, 0],
      },
      maps: ["map-id"],
    });

    message.should.deep.equal({
      code: "invalid-position",
      message: "The feature must be a valid geojson.",
    });
  });
});

describe("getComputedVisibility", function () {
  let mongoServer;
  let db;
  let privateMapId;
  let publicMapId;

  beforeEach(async function () {
    this.timeout(60000);

    mongoServer = await MongoMemoryServer.create();
    await mongoServer.getUri();

    db = new Db({
      url: `mongodb://${mongoServer._instanceInfo.ip}:${mongoServer._instanceInfo.port}/tests`,
      configuration: { collection: "test" },
    });

    await db.connectWithPromise();

    let result = await db.addItem("maps", {
      visibility: {
        isDefault: false,
        isPublic: false,
        team: "000000000000000000000002",
      },
    });

    privateMapId = result.insertedId;

    result = await db.addItem("maps", {
      visibility: {
        isDefault: false,
        isPublic: true,
        team: "000000000000000000000001",
      },
    });

    publicMapId = result.insertedId;
  });

  afterEach(async function () {
    await db?.close?.();
    await mongoServer?.stop?.();
  });

  it("returns a private visibility when the feature and map are private", async function () {
    const feature = { visibility: 0, maps: [privateMapId] };

    const result = await getComputedVisibility(db, feature);

    result.should.deep.equal({
      isDefault: false,
      isPublic: false,
      team: "000000000000000000000002",
    });
  });

  it("returns a public visibility when the feature and map are public", async function () {
    const result = await getComputedVisibility(db, {
      maps: [publicMapId],
      visibility: 1,
    });

    result.should.deep.equal({
      isDefault: false,
      isPublic: true,
      team: "000000000000000000000001",
    });
  });

  it("returns a public visibility when the feature is pending and the map public", async function () {
    const result = await getComputedVisibility(db, {
      maps: [publicMapId],
      visibility: -1,
    });

    result.should.deep.equal({
      isDefault: false,
      isPublic: true,
      team: "000000000000000000000001",
    });
  });

  it("returns a public visibility when the feature is public and one map is public and one is private", async function () {
    const result = await getComputedVisibility(db, {
      maps: [publicMapId, privateMapId],
      visibility: -1,
    });

    result.should.deep.equal({
      isDefault: false,
      isPublic: true,
      team: "000000000000000000000002",
    });
  });

  it("returns a private visibility when the feature is private and the map public", async function () {
    const result = await getComputedVisibility(db, {
      maps: [publicMapId],
      visibility: 0,
    });

    result.should.deep.equal({
      isDefault: false,
      isPublic: false,
      team: "000000000000000000000001",
    });
  });

  it("returns a private visibility when the feature is public and the map private", async function () {
    const result = await getComputedVisibility(db, {
      maps: [privateMapId],
      visibility: 1,
    });

    result.should.deep.equal({
      isDefault: false,
      isPublic: false,
      team: "000000000000000000000002",
    });
  });

  it("generates a visibility when it is not set and it has no maps", async function () {
    const result = await getComputedVisibility(db, {
      maps: [],
      visibility: 1,
    });

    result.should.deep.equal({
      isDefault: false,
      isPublic: false,
      team: undefined,
    });
  });
});
