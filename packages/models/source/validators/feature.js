/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { valid } from "geojson-validation";
import { ObjectId } from "mongodb";

export function validateFeature(feature) {
  if (typeof feature != "object" || Array.isArray(feature)) {
    return {
      code: "invalid-type",
      message: "The feature must be an object.",
    };
  }

  const missingFields = [];

  if (!Array.isArray(feature.maps)) {
    missingFields.push("maps");
  }

  if (typeof feature.name != "string") {
    missingFields.push("name");
  }

  if (
    typeof feature.description != "string" &&
    typeof feature.description != "object"
  ) {
    missingFields.push("description");
  }

  if (typeof feature.position != "object") {
    missingFields.push("position");
  }

  if (missingFields.length) {
    return {
      code: "missing-fields",
      vars: missingFields.join(", "),
      message: `The feature must have all the required fields: ${missingFields.join(
        ", "
      )}.`,
    };
  }

  if (feature.name.trim() == "") {
    return {
      code: "empty-name",
      message: `The feature must have a name.`,
    };
  }

  if (feature.maps.length == 0) {
    return {
      code: "empty-maps",
      message: `The feature must have at least one map.`,
    };
  }

  if (!valid(feature.position)) {
    return {
      code: "invalid-position",
      message: `The feature must be a valid geojson.`,
    };
  }

  if (
    feature.position.type == "Point" &&
    feature.position.coordinates[0] == 0 &&
    feature.position.coordinates[1] == 0
  ) {
    return {
      code: "invalid-position",
      message: `The feature must be a valid geojson.`,
    };
  }
}

export async function getComputedVisibility(db, feature) {
  const mapIds = feature["maps"].map((a) => new ObjectId(a));

  const maps = await db.query("maps", { _id: { $in: mapIds } });

  if (maps.length == 0 && feature["computedVisibility"]) {
    feature["computedVisibility"]["isPublic"] = false;
    return feature["computedVisibility"];
  }

  const teams = maps.map((a) => a["visibility"]["team"]);
  const isPublicMap = maps.map((a) => !!a["visibility"]["isPublic"]);

  let isPublic = feature["visibility"] != 0;

  if (isPublic && !isPublicMap.filter((a) => a).length) {
    isPublic = false;
  }

  const computedVisibility = {};
  computedVisibility["isDefault"] = false;
  computedVisibility["isPublic"] = isPublic;

  computedVisibility["team"] = teams?.[0];

  return computedVisibility;
}
