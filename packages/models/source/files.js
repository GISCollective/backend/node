import path from "path";
import fs from "fs";
import im from "imagemagick";

const ext = {
  "application/octet-stream": "unknown",
  "application/pdf": "pdf",
  "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet": "xlsx",
  "application/vnd.openxmlformats-officedocument.wordprocessingml.document":
    "docx",
  "image/bmp": "bmp",
  "image/gif": "gif",
  "image/heic": "heic",
  "image/jpeg": "jpg",
  "image/jpg": "jpg",
  "image/png": "png",
  "image/tiff": "tiff",
  "image/avif": "avif",
  "image/svg+xml": "svg",
  "image/webp": "webp",
  "multipart/related": "unknown",
  "text/html": "html",
  "video/mp4": "mp4",
  "video/quicktime": "mov",
  "video/avif": "avi",
};

export async function downloadFile(source, file) {
  if (!fs.existsSync("pictures")) {
    fs.mkdirSync("pictures");
  }

  const sourceStream = source.openDownloadStream(file._id);

  if (!ext[file.metadata.mime]) {
    console.log("file.metadata.mime", file.metadata.mime);
  }
  const filename = `${file.filename.substring(0, 20)}.${
    ext[file.metadata.mime]
  }`;
  const localPath = path.join(process.cwd(), "pictures", filename);

  let writeStream = fs.createWriteStream(localPath);
  sourceStream.pipe(writeStream);

  await new Promise((resolve, reject) => {
    writeStream.on("finish", resolve);
    writeStream.on("error", reject);
    sourceStream.on("error", reject);
  });

  return localPath;
}

export async function uploadFile(storage, localPath, metadata) {
  const filename = path.basename(localPath);

  const destinationStream = storage.openUploadStream(filename, {
    metadata,
    contentType: metadata.mime,
  });

  const stream = fs.createReadStream(localPath);

  return await new Promise((resolve, reject) => {
    stream.on("error", reject);

    destinationStream.on("error", reject);
    destinationStream.on("finish", () => {
      resolve(destinationStream.gridFSFile);
    });

    stream.on("open", function () {
      stream.pipe(destinationStream);
    });
  });
}

export async function getPictureInfo(storage, file, stats) {
  let path = typeof file == "string" ? file : null;
  let data = {};

  try {
    if(!path) {
      path = await downloadFile(storage, file);
    }

    const stringOutput = await new Promise((res, rej) => {
      im.identify(['-verbose', path], function (err, features) {
        if (err) return rej(err);
        res(features);
      });
    });

    data = parseIdentify(stringOutput);
    const geometry = data['geometry'].split(/x/);
    data.width = parseInt(geometry[0]);
    data.height = parseInt(geometry[1]);
    data.format = data.format.match(/\S*/)[0]
  } catch (err) {
    stats.errors++;
    console.error("Can't get size for file: ", JSON.stringify(file));
    console.error(err);
  }

  data.path = path;
  return data;
}

function parseIdentify(input) {
  var lines = input.split("\n"),
      prop = {},
      props = [prop],
      prevIndent = 0,
      indents = [indent],
      currentLine, comps, indent, i;

  lines.shift(); //drop first line (Image: name.jpg)

  for (i in lines) {
    currentLine = lines[i];
    indent = currentLine.search(/\S/);
    if (indent >= 0) {
      comps = currentLine.split(': ');
      if (indent > prevIndent) indents.push(indent);
      while (indent < prevIndent && props.length) {
        indents.pop();
        prop = props.pop();
        prevIndent = indents[indents.length - 1];
      }
      if (comps.length < 2) {
        props.push(prop);
        prop = prop[currentLine.split(':')[0].trim().toLowerCase()] = {};
      } else {
        prop[comps[0].trim().toLowerCase()] = comps[1].trim()
      }
      prevIndent = indent;
    }
  }
  return prop;
};
