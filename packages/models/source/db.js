/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { MongoClient } from "mongodb";
import { GridFSBucket } from "mongodb";
import { ObjectId } from "mongodb";
import { enforce } from "./exception.js";

const tableName = {
  Feature: "sites",
  Map: "maps",
  Team: "teams",
  CampaignAnswer: "campaignAnswers",
  Article: "articles",
  BaseMap: "basemaps",
  Campaign: "campaigns",
  IconSet: "iconsets",
  Page: "page",
  Presentation: "presentation",
  Space: "space",
  UserProfile: "userProfile",
};

export default class Db {
  constructor(mongoConfig, withLog) {
    let url;

    if (mongoConfig["url"]) {
      url = mongoConfig["url"];
    } else {
      const hostList = mongoConfig["configuration"]?.hosts
        ?.map((a) => `${a["name"]}:${a["port"]}`)
        .join(",");
      url = `mongodb://${hostList}`;
    }

    this.config = mongoConfig["configuration"];

    if (withLog) {
      this.withLog = true;
      console.log("Creating client for ", url);
    }

    this.client = new MongoClient(url, {});
  }

  async connect(callback) {
    if (this.withLog) {
      console.log("Connecting to MongoDb...");
    }

    try {
      await this.client.connect();
    } catch (err) {
      console.error(err);

      return callback(...arguments);
    }

    if (this.withLog) {
      console.log("Connected to MongoDb...");
    }

    if (this.withLog) {
      console.log(`Using "${this.config["database"]}" db.`);
    }

    this.db = this.client.db(this.config["database"]);

    if (this.withLog) {
      console.log("Connected to MongoDb.");
    }

    callback();
  }

  connectWithPromise() {
    return new Promise((resolve, reject) => {
      this.connect((err) => {
        if (err) {
          return reject(err);
        }

        resolve();
      });
    });
  }

  close() {
    this.client.close();
  }

  getMeta(type, itemId) {
    return this.getItemWithQuery("meta", { type, itemId: `${itemId}` });
  }

  getItem(model, id) {
    return this.getItemWithQuery(model, { _id: new ObjectId(id) });
  }

  setItem(model, _id, item) {
    return this.db
      .collection(tableName[model] || model)
      .updateOne({ _id: new ObjectId(_id) }, { $set: item });
  }

  addItem(model, item) {
    return this.db.collection(model).insertOne(item);
  }

  getItemWithQuery(model, query) {
    return this.db.collection(tableName[model] || model).findOne(query);
  }

  async deleteWithQuery(model, query) {
    if (this.withLog) {
      console.log(`deleting from "${model}" with:`, query);
    }

    return this.db.collection(model).deleteMany(query);
  }

  getFileStorage(bucketName, writeConcern) {
    return new GridFSBucket(this.db, {
      bucketName,
      writeConcern,
    });
  }

  getFile(bucketName, _id) {
    return this.getFileStorage(bucketName).openDownloadStream(
      new ObjectId(_id)
    );
  }

  async deleteFile(bucketName, _id) {
    if (this.withLog) {
      console.log(`deleting "${bucketName}" file with id:`, _id);
    }

    await this.deleteWithQuery(`${bucketName}.chunks`, {
      $or: [{ files_id: _id }, { files_id: _id.toString() }],
    });
    await this.deleteWithQuery(`${bucketName}.files`, {
      $or: [{ _id: _id }, { _id: _id.toString() }],
    });
  }

  getFileObject(bucketName, id) {
    return this.getItem(`${bucketName}.files`, id);
  }

  async copyFile(bucketName, file, destinationBucketName) {
    const source = this.getFileStorage(bucketName);
    const dest = this.getFileStorage(destinationBucketName, { w: 1 });

    const exists = await dest
      .find({ filename: file.filename })
      .countDocuments({ limit: 1 });
    enforce(
      exists === 0,
      `The file "${file.filename}" already exists in "${destinationBucketName}"`
    );

    const sourceStream = source.openDownloadStream(file._id);
    const destinationStream = dest.openUploadStream(file.filename, {
      metadata: source.metadata,
      contentType: source.contentType,
    });

    sourceStream.pipe(destinationStream);

    await new Promise(function (resolve, reject) {
      destinationStream.on("finish", resolve);
      sourceStream.on("error", reject);
      destinationStream.on("error", reject);
    });

    let cursor = dest.find({ filename: file.filename });
    let newFile;
    for await (const file of cursor) {
      newFile = file;
    }

    enforce(
      newFile.length == file.length,
      `The file "${file.filename}" was not copied correctly! The size differs.`
    );
    enforce(newFile, `The file "${file.filename}" was not copied!`);

    await this.setItem(`${destinationBucketName}.files`, newFile._id, {
      ...newFile,
      metadata: file.metadata,
    });

    cursor = dest.find({ filename: file.filename });
    for await (const file of cursor) {
      newFile = file;
    }

    return newFile;
  }

  async storeFileStream(
    sourceStream,
    filename,
    contentType,
    destinationBucketName
  ) {
    const dest = this.getFileStorage(destinationBucketName, { w: 1 });

    const destinationStream = dest.openUploadStream(filename, {
      metadata: {
        mime: contentType ?? "application/binary",
      },
      contentType: contentType ?? "application/binary",
    });

    sourceStream.pipe(destinationStream);

    await new Promise(function (resolve, reject) {
      destinationStream.on("finish", resolve);
      sourceStream.on("error", reject);
      destinationStream.on("error", reject);
    });

    const cursor = dest.find({ filename });
    let newFile;
    for await (const file of cursor) {
      newFile = file;
    }

    enforce(newFile, `The file "${filename}" was not copied!`);
    return newFile;
  }

  async getAllMeta(type, iterator) {
    const cursor = this.db.collection("meta").find({ type });

    for await (const item of cursor) {
      await iterator(item);
    }
  }

  async get(model, iterator) {
    const cursor = this.db.collection(model).find();
    for await (const item of cursor) {
      await iterator(item);
    }
  }

  async query(model, query, iterator) {
    const result = [];
    const cursor = this.db.collection(model).find(query);

    for await (const item of cursor) {
      if (iterator) {
        await iterator(item);
      } else {
        result.push(item);
      }
    }

    return result;
  }

  async count(model, query) {
    return await this.db.collection(model).countDocuments(query);
  }

  async exists(model, query) {
    try {
      return (
        (await this.db
          .collection(tableName[model] || model)
          .countDocuments(query, { limit: 1 })) > 0
      );
    } catch (err) {
      return false;
    }
  }
}

Db.tableName = tableName;
