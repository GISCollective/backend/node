/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/

import { readFileSync } from "fs";

class Config {
  constructor(path, resolver) {
    this.data = JSON.parse(readFileSync(path, { encoding: "utf8", flag: "r" }));
    this.resolver = resolver;
  }

  resolve(value) {
    const reg = new RegExp("((\\$|\\#)[a-z]+)", "gi");

    let group = reg.exec(value);
    let newValue = value;

    while (group) {
      const k = group[0];
      const v = this.resolver(k.slice(1));
      newValue = newValue.replace(k, v);

      group = reg.exec(value);
    }

    return newValue;
  }

  toJson() {
    const resolve = (a) => this.resolve(a);

    function walk(value) {
      if (Array.isArray(value)) {
        value.forEach((item, index) => {
          value[index] = walk(item);
        });

        return value;
      }

      if (typeof value == "object") {
        Object.keys(value).map((key) => {
          value[key] = walk(value[key]);
        });

        return value;
      }

      if (typeof value == "string") {
        return resolve(value);
      }

      return value;
    }

    this.data = walk(this.data);

    return this.data;
  }
}

export default Config;
