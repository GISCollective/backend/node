/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import buildUrl from "build-url";
import { server } from "@hapi/hapi";
import fetch from "node-fetch";

class Broadcast {
  constructor(httpConfig, hmqConfig) {
    this.httpConfig = httpConfig;
    this.hmqConfig = hmqConfig;

    this.handlers = {};

    this.server = server({
      port: this.httpConfig?.port ?? 80,
      host: this.httpConfig?.hostName ?? "localhost",
      debug: {
        request: "*",
        log: "*",
      },
    });
  }

  async start() {
    this.server.ext("onRequest", function (request, h) {
      console.log(`[${request.method}] ${request.url.href}`);
      return h.continue;
    });

    const t = this;
    this.server.route({
      method: "POST",
      path: "/{channel}",
      handler: function (request, h) {
        console.log(`Got message on channel "${request.params.channel}"`);
        const channel = request.params.channel;

        if (channel && typeof t.handlers[channel] === "function") {
          const response = async () => {
            try {
              console.log(`Handling message on "${channel}"`);
              return await t.handlers[channel](request.payload);
            } catch (err) {
              console.error(err);
              return "";
            }
          };

          return response();
        }

        return "";
      },
    });

    this.server.route({
      method: "GET",
      path: "/{channel}",
      handler: function (request, h) {
        if (request.params.channel == "stats") {
          return;
        }

        console.log(`Got ping for channel "${request.params.channel}"`);
        return new Date().toISOString();
      },
    });

    await this.server.start();
    console.log("Server running on %s", this.server.info.uri);
  }

  push(channel, message) {
    const url = buildUrl(this.hmqConfig.url, {
      path: channel,
    });

    let strJson = JSON.stringify(message);
    if(strJson.length > 50) {
      strJson = `${strJson.substring(0, 50)}...`
    }

    console.log(`Pushing "${strJson}" to "${url}"`);

    return fetch(url, {
      method: "post",
      body: JSON.stringify(message),
      headers: { "Content-Type": "application/json" },
    }).catch((err) => {
      console.error("Failed to push message.");
    });
  }

  get subscribeUrl() {
    return buildUrl(this.hmqConfig.url, {
      path: "subscribe",
    });
  }

  get url() {
    const hostPort =
      this.httpConfig.port == 80 ? "" : `:${this.httpConfig.port}`;
    return `http://${this.httpConfig.hostName}${hostPort}/`;
  }

  resubscribe() {
    const all = Object.keys(this.handlers).map((a) => this.subscribeRequest(a));
    return Promise.all(all);
  }

  subscribe(channel, messageHandler) {
    this.handlers[channel] = messageHandler;

    return this.subscribeRequest(channel);
  }

  subscribeRequest(channel) {
    console.log(
      `Subscribing to "${channel}" at "${this.subscribeUrl}" as "${this.url}"`
    );

    const message = {
      channel,
      url: this.url,
      max: 10,
    };

    return fetch(this.subscribeUrl, {
      method: "PUT",
      body: JSON.stringify(message),
      headers: { "Content-Type": "application/json" },
    }).catch((err) => {
      console.error("Failed to push message.");
    });
  }
}

export default Broadcast;
