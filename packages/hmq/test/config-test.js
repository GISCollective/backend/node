/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Config from "../source/config.js";
import chai from "chai";
chai.should();

describe("Config class", function () {
  it("should replace the variables in the model file", function () {
    const config = new Config("./config/configuration.docker.json", function (
      name
    ) {
      return "resolved:" + name;
    });

    config.toJson().should.deep.equal({
      general: {
        serviceName: "resolved:serviceName",
        serviceUrl: "resolved:serviceUrl",
        apiUrl: "resolved:apiUrl",

        db: "/home/node/app/config/db",
        pidFolder: "./",
        logLevel: "resolved:logLevel",
        logFolder: "/home/node/app/logs",

        stats: {
          interval: 15,
          pushGatewayUrl: "resolved:pushGatewayUrl",
        },
      },

      hmq: {
        url: "resolved:httpMqUrl",
      },

      http: {
        hostName: "resolved:HOSTNAMEresolved:DOMAIN",
        port: 80,
        keepAliveTimeout: 0,
        maxRequestHeaderSize: 8192,
        maxRequestSize: 2097152,
        serverString: "tasks-js",
        webSocketPingInterval: 0,
      },
    });
  });
});
