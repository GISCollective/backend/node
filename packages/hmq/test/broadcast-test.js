/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Broadcast from "../source/broadcast.js";
import { expect, should } from "chai";

should();

describe("Broadcast class", function () {
  describe("when no settings are provided", function () {
    let broadcast;

    beforeEach(function () {
      broadcast = new Broadcast();
    });

    it("throws an error on start", async function () {
      await expect(broadcast.start).to.be.rejected();
    });
  });
});
