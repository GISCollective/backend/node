/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
const { Db } = require("models");
const { Broadcast, Config } = require("hmq-js-client");

const PicturesExtract = require("./files/PicturesExtract");
const path = require("path");
const yargs = require("yargs");

function resolve(name) {
  return process.env[name];
}

import path from "path";
import { fileURLToPath } from "url";
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const argv = yargs(process.argv.slice(2)).default(
  "configuration",
  function randomValue() {
    return path.join(__dirname, `../config/configuration.json`);
  }
).argv;

const appConfig = new Config(argv["configuration"], resolve).toJson();
const dbConfig = new Config(
  appConfig["general"]["db"] + "/mongo.json",
  resolve
).toJson();

const db = new Db(dbConfig);
const broadcast = new Broadcast(appConfig["http"], appConfig["httpMq"]);

const jobs = [PicturesExtract];

db.connect(function (err) {
  if (err) {
    return console.error(err);
  }

  const promises = jobs
    .map((job) => new job())
    .map((job) => {
      console.log(`Starting "${job.name}"...`);
      return job.start(db, broadcast);
    });

  Promise.all(promises)
    .then(() => {
      db.close();
      console.log("Done.");
    })
    .catch((err) => {
      db.close();
      console.log("Done with errors. ", err);
    });
});
