/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
const fs = require("fs");
const path = require("path");

class PicturesExtract {
  constructor() {
    this.name = "PicturesExtract";
  }

  async start(db, broadcast) {
    const storage = db.getFileStorage("pictures");

    return db.get("pictures.files", async (file) => {
      const files = storage.find({ filename: file.filename });
      const source = db.getFileStorage("pictures");

      for await (const file of files) {
        const sourceStream = source.openDownloadStream(file._id);

        let writeStream = fs.createWriteStream(
          path.join("pictures", file.filename)
        );

        // write some data with a base64 encoding
        sourceStream.pipe(writeStream);

        await new Promise((resolve, reject) => {
          writeStream.on("finish", resolve);
          writeStream.on("error", reject);
          sourceStream.on("error", reject);
        });
      }
    });
  }
}

module.exports = PicturesExtract;
