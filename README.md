# GISCollective Node.js apps

This repository is a monorepo using [yarn workspaces](https://yarnpkg.com/features/workspaces). This monorepo contains:

- `batch` is a batch process that checks the db consistency and cleans unused records.
- `triggers` is a batch process that checks the db for records that need to be trigger to run some actions.
- `fonts` is web server that serves fonts requested by the vector tiles styles.
- `tasks` is a tasks server similar to https://gitlab.com/GISCollective/backend/tasks which implements tasks that take advantage of some available npm packages.
- `hmq` is a client library for the [HMQ](https://gitlab.com/GISCollective/backend/hmq) server.
- `models` is a library used to connect to the mongodb database.
- `scripts` is a collection of scripts used by our servers.
- `tools` is a collection of tools used to interact with GISCollective.

You can read more about the architecture at [https://guide.giscollective.com/en/develop/architecture/](https://guide.giscollective.com/en/develop/architecture/) or find more about our platform at
[https://giscollective.com](https://giscollective.com/).

## Prerequisites

You will need the following things properly installed on your computer.

- [Git](https://git-scm.com/)
- [Node.js](https://nodejs.org/) (with yarn)
- [Mongo](https://www.mongodb.com)

## Installation

- `git clone <repository-url>` this repository
- `cd node`
- `yarn`

## Running / Development

- You need to switch your current directory to the project that you want to start `cd packages/<name>` and run `yarn start`.

### Writing Tests

We use [mocha](https://mochajs.org/) and [chai](https://www.chaijs.com/) for writing tests.

### Running Tests

- `cd packages/<package>`
- `yarn test`

### Deploying

The apps from this repository are deployed using the docker containers, build by our CI. They can be found in this registry: https://gitlab.com/GISCollective/backend/node/container_registry

If you want to build your own containers you can run the following commands

- cp packages/tasks/Dockerfile .
- docker build --build-arg APP_NAME=`<app name>` --no-cache=true -t $IMAGE_TAG .
- docker push $IMAGE_TAG

## Contributing

You can find our contribution guide at [https://guide.giscollective.com/en/develop/CONTRIBUTING/](https://guide.giscollective.com/en/develop/CONTRIBUTING/)

## Code of conduct

You can find our code of conduct at [https://guide.giscollective.com/en/develop/CODE_OF_CONDUCT/](https://guide.giscollective.com/en/develop/CODE_OF_CONDUCT/)
