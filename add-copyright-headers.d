#!/usr/bin/rdmd --shebang -version=test -O

module add_copyright_headers;

import std.stdio: writeln;
import std.file;
import std.algorithm;
import std.string;

void main() {
  auto jsFiles = dirEntries("","*.{js}",SpanMode.depth);

  auto header = readText("source-header.txt");
  string[] folders = ["packages"];

  foreach (d; jsFiles) {
    bool isValidPrefix;

    foreach (string prefix; folders) {
      isValidPrefix = isValidPrefix || d.name.indexOf(prefix) == 0;
    }

    if(!isValidPrefix || d.name.canFind("node_modules")) {
      writeln("SKIP: ", d.name);
      continue;
    }

    writeln("update: ", d.name);

    auto content = readText(d.name);
    if(content.indexOf("/*") == 0) {
      auto last = content.indexOf("*/");

      if(last == -1) {
        continue;
      }

      content = content[last+3 .. $];
    }


    auto newContent = header ~ "\n" ~ content;

    std.file.write(d.name, newContent);
  }
}